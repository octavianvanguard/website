# OV website

# Introduction
This readme file describes the OV web-site and provides a basic guide to 
maintenance.

The site itself is a series of mark-down format files which are parsed by the 
static site generator 'go hugo'.

# Pre-requisites

## Software requirements

* Installed copy of go hugo https://gohugo.io/
  The last version this site is known to work with is v0.36.0
* Installed copy of git (Source control management) http://git-scm.com/
* Web browser of your choice

## Required knowledge

* How web sites work
* Basic git usage
* Basic mark-down usage
* Some background reading about gohugo

# Quick notes on gohugo

Gohugo is a static web-site generator.
It takes a series of mark-down formatted pages representing the site (articles),
along with a 'theme' which controls how articles are displayed.
When executed, it generates HTML files representing your web-site.
Images and style sheets can also be provided.

The generated HTML is output to the public/ folder. This folder is the only 
thing which is published on the web-site

# Source controlled files

The contents of the source repository contain the information used by hugo to
generate the site. The actual generated content (html) is not stored.

# Coding Standard

New articles should obey the following coding standard rules

- Limit line lengths to 80 characters
- Use 4 spaces rather than tab characters
- Prioritise clarify of reading for file layouts. Code is read far more than
  it is written

# Hints & Notes

## Adding articles

Generally this involves:

* Retreive the site git repository from bitbucket
* Adding a new page using mark-down format in a sub-directory of content
    * For news posts content/post
      Prefix file name with the date of the post and name file after article
      title.
    * For general sections about specific games, a directory under games/.
      an _index.md must be created to define the content
      e.g. content/<mygame>/_index.md
* Adding images for the article in static/img/
* Always set the "banner" attribute for the page (use the same image as every 
  other page if you are not sure what image to use)
* Use the hugo server command to preview the new page/article
* Use git to commit the changes locally
* Push to the remote repository on bitbucket
* Pester Ima to update the site

See the hugo documentation for how to create new pages.

## Menus

config.toml can be used to edit basic site information including the menu items.
The readme file for the theme itself (themes/hugo-icarus-ov/) contains some info
about this as well.

## Theme

The site uses a modified version of the free 'hugo icarus' theme (MIT licence). 
The original theme was copied from
https://github.com/digitalcraftsman/hugo-icarus-theme.git
and is included in themes/hugo-icarus.

Generally the OV theme (themes/hugo-icarus-ov) provides a darker display - 
black background with orange links - like the OV word-press site that came 
before it. This is basically a customisation of the .css file.

## Article banner images
The banner for each article must be explictly set to the typical image we use.
The theme uses the same image for both the logo display next to the "Recents" 
display on the right and also as the banner image when displaying pages 
matching a selected category. 

Unfortunately, for the latter, the theme letter-boxes the image so you get a
partial logo. The image we're using looks good in both cases, but if you don't
set a banner for the article then it will end up using an OV logo without the 
bullets which doesn't look particularly great.

## Deployment

The site is configured to allow deployment on amazon s3. Unfortunately
amazon won't redirect from site.com/directory to site.com/directory/index.html
automatically.

The work-around for this is to enable uglyurls. This makes pages link to
index.html directly, but unfortunately this also flattens the generated
document structure in places from the source structure.

## Original OV site

hugo-export.zip contains the articles and data downloaded from the original
OV wordpress site and converted to hugo-format. These are not directly used in the updated site. Each article was ported over to the revised site and many of the images were duplicated.

This file is provided for reference and is not part of the deployment.
