---
banner: "css/images/banner.png"
title: Forum
author: Imaboyo
layout: page
date: 2018-05-12
menu: "main"

---

After many years the members of OV have moved on with their lives. The forums have now been
closed down as we predominantly communicate via discord.

If you wish to get in contact with members of OV, then please join us in our discord channel.
https://discord.gg/fYQ5Cf2
