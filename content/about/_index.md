---
banner: "css/images/banner.png"
title: About The Octavian Vanguard
author: Lupus
layout: page
date: 2018-04-07
menu: "main"
disable_comments: true
---
The Octavian Vanguard traces its beginnings to the very start of [Jumpgate: The Reconstruction Initiative][1], in 2001. As stalwart defenders of the Octavian
Empire, we struck fear into our enemies, and inspired admiration in our allies.
Those who struggled in vain against the obvious superiority of the red empire
did not always like us, but they respected us. For years we have fought with
honour, bravery, discipline, and skill; above all though we enjoyed the fight.
Most of our founding members remain with us to this day, and we continue to
uphold the ideals of the Empire.

Since then, many of our pilots retired from active flight duties, but continued
on into other games; and over the years we have met and welcomed many new
members who share our outlook on all the worlds we visit. We still hold to our
core principles: having fun, fighting hard, and being great at what we do. We
work as a team, and because of that, we achieve impressive results, regardless
of the game. In Planetside, we excelled as a small, tightly disciplined unit,
punching well above our weight, just like our corporation in EVE. As costumed
heroes (and villains), tank commanders, zombie survivors, or fighting for the
RED and BLU companies, in all we have conquered and celebrated.

Our members are a varied lot; men and women of all ages and from all walks of
life. What we share is an appreciation for both fun and achievement. We work to
be a good team, with discipline where it is useful, and fun wherever we find it.
We don't cheat. We don't grief. We don't talk smack to others. We play, hard and
well, and celebrate our wins. If you meet us on the field of battle, prepare for
a tough fight. And if you meet us in the bar afterwards, then you'd better buy
us a drink, or we'll <del>steal</del>liberate yours.

[1]: http://en.wikipedia.org/wiki/Jumpgate:_The_Reconstruction_Initiative

