---
banner: "css/images/banner.png"
title: World of Tanks
author: Zeshin
layout: page
date: 2018-04-07

---
<img class="alignnone" title="WoT Banner" src="https://worldoftanks.com/dcont/fb/imagesforarticles/wotbigbar03.jpg" alt=""/>

**World of Tanks** is a team-based massively multiplayer online action game dedicated to armored warfare. Throw yourself into the epic tank battles of World War II with other steel cowboys all over the world. Your arsenal includes more than 150 armored vehicles from America, Germany, and the Soviet Union, carefully detailed with historical accuracy.

[Octavian Vanguard Company][1]

&nbsp;

 [1]: http://worldoftanks.eu/community/clans/500000405-OV/
