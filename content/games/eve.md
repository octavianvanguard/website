---
banner: "css/images/banner.png"
title: EVE
author: Lupus
layout: page
date: 2018-04-07

---

OV has a corporation within EVE.

## About

[Corp Info](https://evewho.com/corp/Octavian+Vanguard)

The Octavian Vanguard is a community of mature gamers which has been established
since 2003 across a variety of different games.

The Corporation was established in EVE shortly after release and has tried
pretty much everything in the years since. We�re now kicking back and relaxing
in low sec for fun and profit, with a mix of mission running, exploration, WH
operations, some PvE and some PvP, and a little bit of production and trading.
We have players with a wide range of SP and experience, and currently have no
minimum SP requirement.

## Split from Razor Alliance
Initially part of the Razor Alliance, [OV EVE changed direction][1] after
approximately 5 years.

> The demands of 0.0 life, and the lack of succession planning had caused
> burnout at the top levels of the corp and it was decided that the only way to
> continue in 0.0 was to move into the RG corporation. Nearly all pilots have
> joined the RG corp within razor which will allow them to continue to fight the
> good fights under the RZR banner and the directors to go back to enjoying
> themselves instead of fuelling POS for the rest of their mortal lives. The OV
> banner has been returned to the control of its founder Tar-om who has taken it
> back to empire for a life of retirement. OV in EVE will now become a rest home
> for aged EVE players who want to chill out and chat, and perhaps raid the odd
> wormhole for fun and profit. If you want to join (or re-join) us then just
> sling in an app. 

 [1]: http://www.octavianvanguard.net/forum/viewtopic.php?t=15231
