---
banner: "css/images/banner.png"
title: Planetside
author: Lupus
layout: page
date: 2018-04-07

---
OV had a presence within both Planetside and was well known for turning the
tide of war inthe TR's favour.

Later, OV took to Planetside 2 making a signiciant different for the TR -
however never became as large a presence as in Planetside 1.

## OV in Planetside 1

![Mantra](/planetside/psbanner.gif)

## "Ghostbusters" movie
OV's Divvur produced a Planetside movie named "Ghostbusters".

- [Youtube](https://www.youtube.com/watch?v=aDcsR5j_eZs)
- [AVI](http://www.octavianvanguard.net.s3-website-eu-west-1.amazonaws.com/files/whoyougonnacall.avi)
- [WMV](http://www.octavianvanguard.net.s3-website-eu-west-1.amazonaws.com/files/whoyougonnacall.wmv)

### The Octavian Vanguard

The Octavian Vanguard is an online games clan. We formed in a MMORPG called
Jumpgate as a factionalist PvP based squad. We passed our 5th anniversary fairly
recently although some of us have been gaming together for much longer than
that, and we now play numerous games from most genres. We have large active
player base mostly in Europe, English is our common language.

The term "community" gets overused on the internet, but in our case it is
appropriate. We meet up offline as well as online, we chat to each other every
day, we help each other out in real life as well as in games.

### OV in Planetside

The Octavian Vanguard is an elite rapid response outfit. We have a strong
emphasis on teamwork, intelligent and tactical play, and the ability to react
quickly to a changing global situation. We like to shoot stuff, we like to do it
well, and we have fun shooting stuff together. It really is that simple.

We are currently recruiting players of all battle ranks, although we will limit
ourselves to just a couple of completely new players at any one time in order to
be able to give the help needed to people learning the game from scratch. We are
looking for people who will play well as part of a squad, think about the game,
and contribute to the outfit to the best of their abilities.

We run a teamspeak 2 server and expect everyone to use it.

We have outfit squads running every evening, and events once or twice a week.
Our command staff put a massive amount of effort not only in to making OV
successful, but also to making the game we play fun and entertaining for our
members. We have players of a wide variety of ingame experience, and the more
experienced members of the outfit will offer a huge amount of help to newer
players learning the game.

### What we expect from our members

OV as a community expect a certain standard of behaviour from our players. We
have a player base spanning a lot of games and behaviour of members in one game
reflects on those in all our games. In general OV has been respected by the
majority of people we've fought against in most of the games we've played, this
is something that is important to us. We will not tolerate our members abusing
other players, particularly on the grounds of any out of game prejudices.

We have a complete zero tolerance policy on the use of any third party "hacks",
and our members are expected to refrain from any behaviour which abuses the
game's mechanics to their advantage or the detriment of others.

### OV Background in Planetside
The Octavian Legion is a penal battalion led by General Octavius. Each member
is a criminal in some regard as judged by the Terran Empire. There are three
parts to the legion: The rear echelon (REMFs) where the General and his
trustees watch the battles. The main body which is called on to exploit the
gaps these are the average prisoner who lacks the urge to die. The Vanguard are those people who have broken the law or angered the general in some way and
have been sentenced to die. The Vanguard lead from the front for death is
certain. When death is certain, all that remains is to die with honour.

![Planetside](/planetside/planetside.jpg)

