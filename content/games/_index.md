---
banner: "css/images/banner.png"
title: Games
author: Lupus
layout: page
date: 2018-04-07
menu: "main"

---
While OV started out in Jumpgate, we play a wide variety of multi-player games, both massive and regular sized. Here are some of our favourites:

  * MMOs 
      * [Jumpgate]({{< relref "jumpgate.md" >}})
      * [EVE]({{< relref "eve.md" >}})
      * [City of Heroes / Villains]({{< relref "coh.md" >}})
      * [The Old Republic]({{< relref "oldrepublic.md" >}})
      * [Planetside]({{< relref "planetside.md" >}})
  * Multi-player games 
      * Team Fortress 2
      * [World of Tanks]({{< relref "wot.md" >}})
      * [Battlefield 3]({{< relref "battlefield3.md" >}})
      * Dawn of War
