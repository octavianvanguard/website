---
banner: "css/images/banner.png"
title: City of Heroes
author: Lupus
date: 2018-04-07

---
OV in CoH/CoV originated on the US servers before CoH was available in the EU, then transferred over to the EU servers when the option became available.

With the US/EU server merge in May 2011 followed by Issue 21:Freedom in September, OV continue to play predominately on the Union, Defiant and Exalted(VIP) servers.

- [Striga Hess TF Comic]({{< relref "striga.md" >}})

