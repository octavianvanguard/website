---
banner: "css/images/banner.png"
title: Striga Hess TF Comic
author: Terrahawk
date: 2018-04-07

---
![Striga Comic Prelude Pt1](/striga/striga-tf.png)

![Striga Comic Prelude Pt2](/striga/victory-server-up.png)


![Striga Comic Prelude Pt3](/striga/striga-tf-prelude.png)


![Striga Comic Pt1](/striga/001-striga-tf.jpg)

  
![Striga Comic Pt2](/striga/002-striga-tf.jpg)

  
![Striga Comic Pt3](/striga/003-striga-tf.jpg)

  
![Striga Comic Pt4](/striga/004-striga-tf.jpg)

  
![Striga Comic Pt5](/striga/005-striga-tf.jpg)

  
![Striga Comic Pt6](/striga/006-striga-tf.jpg)


![Striga Comic Pt7](/striga/007-striga-tf.jpg)


![Striga Comic Pt8](/striga/008-striga-tf.jpg)

 
