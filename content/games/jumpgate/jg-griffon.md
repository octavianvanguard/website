---
banner: "css/images/banner.png"
title: Griffon
author: Lupus
layout: page
date: 2018-04-07

---
![Griffon](http://www.octavianvanguard.net/ov/img/jossh/griffon.jpg)

Initially customised from the standard Condor tow, this production version of a &#8220;BattleTow&#8221; is unable to carry the cargo pod of the original but gains a smaller more combat effective profile. A prototype is reported to be currently undergoing trials at Ares Prime.

<table width="500" border="0" cellspacing="1" cellpadding="5" align="CENTER" bgcolor="#222222">
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">Production Center(s):</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">TBA</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">CodeName:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">Griffon</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">Classification:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">Battle Tow</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">TechLevel:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">30</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">Size(ucs):</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">56.4</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">Mass(kg):</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">56,000.0</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">Engines:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">2</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">MAX Engine Size:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">5</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">Gun Hardpoints:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">2</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">Missile Hardpoints:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">4</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">MODx Hardpoints:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">7</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">Drag Factor:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">36.0</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">MAX Pitch:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">39.0</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">MAX Roll:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">28</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">MAX Yaw:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">28</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">MAX Cargo:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">5</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">MAX PP Size:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">5</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">MAX Radar Size:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">4</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">MAX ECM Size:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">3</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">MAX Shield Size:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">4</span>
    </td>
  </tr>
  
  <tr align="RIGHT" valign="TOP" bgcolor="#000000">
    <td width="300">
      <span style="color: #ffffff;">MAX Capacitor Size:</span>
    </td>
    
    <td>
      <span style="color: #ffffff;">4</span>
    </td>
  </tr>
</table>
