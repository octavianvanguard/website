---
banner: "css/images/banner.png"
title: Parts I-X
author: fellblade
layout: page
date: 2018-04-07

---
<h2>
  Part I
</h2>

<p>
  The sleek flying wing of the Raven light fighter cut through space, green drive flares blazing out into space and leaving a trail of ionised gasses in it´s wake. The pilot inside checked his radar for what seemed like the fiftieth time and, having assured himself that there was still no-one in range, he turned back and carried on with his previous task&#8230;
</p>

<p>
  The shield generator was pretty much mangled.. it had been cheap, true, and it had seen him through a fair number of skirmishes, but it was now pretty much dead. He was going to have to have Words with the dealer he had bought it off at Evening´s End, it was meant to have anti-backblast protection&#8230; evidently it didn´t. Once a life-saver, now a slightly charred and smoking lump with a small melted hole where the power consumption readout was meant to be and a faint smell of burning cheese from an old sandwich that he had left inside it the last time he had opened it up.
</p>

<p>
  Sighing, he picked up a spanner, and started removing the bolts from the outer casing. He had managed to take almost all of them off when he noticed the small puddle of liquid glass that had seeped out from a hole near the bottom of the unit. Oh well, he thought, that´s shot then, need another fibre optic bundle for the main power router. He looked again at the bits of plastic dotted around the cockpit: the remains of the insulating on the cabling leading to the generator, he looked at the still-red blob of molten iron stuck to the back of his pilot´s seat, and he looked at the crater where a high-velocity lump of metal had smashed into the inner hull. No, he decided, it was going to take more than a new fibre optic bundle.
</p>

<p>
  A beep from the nav computer pulled him out of his contemplation of his expensive and now useless ex-shield generator and back into reality. The ship was nearing a ´roid field. Hauling himself back into the cockpit as the asteroid field neared, he grabbed the joystick and started to carefully curve the ship around the lethal lumps of rock. At this velocity an impact on any of them would mean that he would be breathing vacuum very quickly. And he had no particular need of that&#8230; not today&#8230; or any other day for that matter.
</p>

<p>
  As the Raven banked and turned around the grey-brown asteroids, the pilot´s eyes swept over them, watching for any tell-tale light, sign of movement. Anything that might be someone else, looking for him, waiting for him. Checking the radar again he still saw nothing. Not that that means anything, he pondered, they might be using ECM and his radar was pretty short range anyway; he had had to grab whatever was on the market and pretty quickly when he had last launched. How long ago was that? he thought, glancing at the on-board clock. Fifteen hours&#8230; oh well.. another few and he would be there&#8230;
</p>

<p>
  As the ship left the asteroid field, Arouin Simell sat back in his pilot´s seat and set the clock to wake him in 2 hours.. 2 hours should be enough sleep&#8230; hopefully&#8230;
</p>

<p>
  After a couple of hours of fitful sleep, Arouin awoke, fumbled for the alarm shut-off button on the clock, and then settled with hitting it with his beer tankard until it stopped. The lite-glo panel which provided the clock display fell off, he cursed, tried to get up to look for it, then broke it in two when he swung his legs off of the control board. Things were not going well.
</p>

<p>
  As he neared his destination he got more nervous, starting to fidget occasionally with his goatee beard and idly tapping his fingers on the controls. He stopped with the idle tapping when he accidentally hit the emergancy fuel jettison button and almost lost what remained of his afterburner fuel. He resorted to futilely trying to clean his radar display instead, and only seemed to succeed in moving around the grease over it´s pitted and scratched plexiglas surface. After a few minutes of this Arouin noticed an asteroid appear on his radar. The asteroid. He swung his ship around to head more directly towards it, and stared expectantly at the radar display. As he neared the asteroid, more appeared on his radar, and as he came even closer he began to pick them out of the star-studded blackness of space. As he moved between them, ship twisting this way and that to avoid a fatal collision, he noticed the object that his searching eyes had been looking for. An old piece of a pre-collapse space station, scarred, twisted beyond all recognition, bearing the flaking remains of an unfamiliar emblem painted upon one of its less damaged faces. He brought his Raven around to face it, and then fired his retros to bring his ship to a complete stop. After a few moments, several nearby asteroids opened hatches from which sprung leathal looking turreted missile launchers, from which launched leathal looking missiles.
</p>

<p>
  The radar went yellow, maybe thirty missiles inbound, quite a variety, a mix of Purgatorys and Morningstars in case he tried to dodge, with Katakas in case he ran and Lances in case he was stupid enough to keep still. As the missile warning alarm went off, yellow light plaintively strobing in a dark recess of the control panel, speaker blaring out its harsh warning, Arouin settled back into his seat and sat there, waiting for the inevitable, smiling slightly.
</p>

<h1>
</h1>

<h2>
  Part II
</h2>

<p>
  &#8230;.and after a few more moments Arouin didn´t get the inevitable; he got what he expected.
</p>

<p>
  The missiles slammed into his ship unarmed, the fried shield generator letting them smash into the delicate paintwork on the outside of his Raven. He winced slightly as one of the Lance´s slammed home with a mettallic crash, leaving a ringing in his ears as the ship reverberated. His tankard fell to the floor with a clatter, and as he leant over the edge of his seat to retrieve it from where it had fallen, the overhead compartment snapped open as a Kataka smashed into the side of the ship. Fending off falling boxes of rations he groped for the comms key, grasped it, then hammered at it yelling into his helmet pickup.
</p>

<p>
  &#8220;Cut out the god damn missiles Voris it´s me you bloody idiot! My shield gen´s toasty and you´ve just demolished a rather nice paint job on my bird!&#8221;
</p>

<p>
  There was a crackle from the comm unit and then a rasping cough.
</p>

<p>
  &#8220;Heh, sorry old friend, you can´t be too sure these days, can you?&#8221;
</p>

<p>
  Arouin sighed and slumped back into the chair, wiping off the remains of a packet of spicy sauce that had exploded over his tunic.
</p>

<p>
  &#8220;No, I guess you can´t&#8230; well, I´m waiting, gimme some rings&#8221;
</p>

<p>
  &#8220;Okay&#8230; (Jurhern, turn on the rings, tightband to our guy in the Raven&#8230; oh you bloody idiot.. give that here&#8230; there&#8230;) Okay, done, see ya in a few minutes buddy&#8221;
</p>

<p>
  The navi-puter recieved the docking ring holographic information from the unseen speaker, and then a large asteroid a few kilometers had a series of docking rings flicker into view leading into a crater on it´s massive pockmarked surface. Arouin grabbed the joystick and pulled the fighter around, flicking the throttle up, then span it around as the little ship entered the docking rings, fired the retros, then slid sliently towards the crater on the surface.
</p>

<p>
  If, of course, he had tried to get away from the missiles, the only ´logical´ thing to do in the situation, they would have armed themselves and he would have been left floating as pieces of a lifeless corpse in a fused, twisted piece of metal. These safeguards were needed; the TRI was getting more thourough with it´s investigations into the activities of Arouin and other´s of his ilk. When his ship was a scant hundred meters from the crater´s floor, the sides snapped apart revealing a more-or-less standard docking tube, then snapped close again behind him, wreathing the ship in a grimy blackness.
</p>

<p>
  As the bay lift brought the Raven Light Fighter into the hangar bay, the fluro-strip lights flickered on uncertainly, strobing momentarily before stabilising at a slightly-too-harsh brightness. Arouin hit the cockpit release button, then stood up, pushing the canopy open. He had discarded the automatic opening and closing of the canopy since he had see his old, and now dead, friend Granneth Haars dock at a TRI station, only to have some lousy technician monitoring the docking tubes notice his ship registration and open the canopy remotely while the old guy´s Typhoon was still in hard vacuum. Some lessons came hard, and you either learn from them, or something terminal happened to you. As he got his first breath of the station´s air he noticed how stuffy it had been in the cockpit and decided to get the oxygen reclaimers looked at. By someone reliable.
</p>

<p>
  Arouin clambered out, shook his legs to try and work the stiffness out of them, then looked up towards the far wall of the hangar bay as a door opened and Voris Kolther walked out. A little too fat, thought Arouin, he hasn´t been exercising for a while. Getting rusty&#8230; not very good for a pirate to get rusty.
</p>

<p>
  Voris held his arms apart expressively as he walked towards the newly landed pilot. A tatty green cloak trailed behind him, flapping as he moved, covered with a few decade´s worth of grime and dust. Not as grimy as his face, however. Looked like he had been skipping on sleep, no matter how well fed he was. Arouin embraced him as the other man clapped arms around his shoulders,
</p>

<p>
  &#8220;Looks like you made it in one piece!&#8221;
</p>

<p>
  &#8220;Yeah, no thanks to you, you bastard&#8230; have you got any idea how much it´s gonna cost to get the whole ship resprayed?&#8221;
</p>

<p>
  The two men turned and walked away from the Raven towards the door chatting about how much Voris was going to pay for the respray.. they were his missiles, after all&#8230;
</p>

<h1>
</h1>

<h2>
  Part III
</h2>

<p>
  The pair walked through a series of dank corridors, the occasional bit of moss growing in the corners, grease and grime smeared on the walls. Metal shavings crunched noisily under Arouins booted feet, while Vorbis made unpleasant screeching sounds as his softer soled shoes got bits stuck in them, leaving bright lines and the occasional spark on the metal floor. Vorbis was slightly the taller of the two men, bulkier as well, and with distinctly less hair. Or, as one of Arouin´s friends put it, &#8220;A big fat bald bastard&#8221;. As they walked past a doorway, chatting idly, Arouin glanced into a dark room and noticed the movement of someone ducking back out of sight. Vorbis carried on with what was rapidly growing into a monologue, with Arouin inserting the occasional ´yes´ ´hmm..´ ´ah&#8230;´ and generally floating through the conversation on autopilot, while looking around, taking everything in. Then Arouin remembered.
</p>

<p>
  As they turned a corner, he stuck one hand into his inner jacket pocket, and pulled out a large calibre handgun. Vorbis looked slightly alarmed at first, then relaxed slightly as it was proffered to him handle first instead of barrel.
</p>

<p>
  &#8220;What do you think?&#8221;
</p>

<p>
  &#8220;Good weight, like the grip, very comfy&#8221;
</p>

<p>
  Vorbis stepped carefully over a hole in the floor leading to the deck below
</p>

<p>
  &#8220;Got it from a guy flogging ´borrowed´ Octavian new-tech weaponry over at GBS&#8230; bit pricey but I don´t mind; you need the protection in this day and age&#8221;
</p>

<p>
  &#8220;You do at that.&#8221;, Vorbis agreed, nodding.
</p>

<p>
  At last they came to a metal doorway set at the end of a corridor. There was an armed man outside. Arouin didn´t think he could be a guard, because guards usually looked like they were guarding something. This one was sitting on the floor browsing through the stations holo-vid guide. Vorbis slapped his hand onto a panel next to the door, which opened with a swishing sound, sliding into the wall. Unfortunately it ricocheted back again and stopped half closed. Vorbis looked back at his friend with a sigh
</p>

<p>
  &#8220;I carry on meaning to get it fixed&#8230; but I don´t get many decent station maintenance techs around here, as you might have guessed&#8221;
</p>

<p>
  Vorbis pushed the door back. It didn´t move. Then he kicked it.
</p>

<p>
  &#8220;Get outta it you godforsak&#8230; ah&#8230; come in&#8221;
</p>

<p>
  They stepped through into a fairly bland room, a couple of circular tables with chairs that went out of fashion about fifty years ago in the middle of it. Someone a few years ago must have made a futile attempt to brighten it up with a lick of paint, most of which was in little fragments at the bottom of the walls, and rust was beginning to show at the top of the metal sheets. Someone had put a couple of bowls of something on the table, and they were steaming. Arouin didn´t know what was in them, but he hoped it was edible because he was going to eat it. Ten or fifteen men sat around the outside of the room in the shadows left by non-functional fluro-strips, playing dice, drinking, eating, or cleaning weaponry or tools. Vorbis walked over to the far side of the table, fidgeting with Arouin´s new hand artillery, and sat down, motioning for Arouin to seat himself. He sat down on the chair, which creaked a little even under his fairly light weight, looked at the unidentified stew before him, and picked up the spoon that was lying next to the bowl. He ignored the pirate´s custom of swapping meals before eating (supposedly borne out of mutual mistrust centuries ago), and tucked in. The mush was beige&#8230; soya proteins and water with flavouring in&#8230; roast beef flavour&#8230; well.. that´s what the packet said, he had never actually had roast beef before in his life and doubted he ever would. You occasionally heard of some Real Meat being shipped up from planetside&#8230; each steak going for about seven million credits&#8230; Vorbis picked up his spoon, and pushed the mess in the bowl around a bit before looking up at Arouin until the other man noticed him, then hurriedly wiping a bit of dribbly food from his chin. Vorbis grinned.
</p>

<p>
  &#8220;So, tell me my friend, tell me of why you are here, and why in such a hurry&#8230;&#8221;
</p>

<p>
  Arouin scooped another spoonful of stuff from his bowl and into his mouth, swallowed, and then tapped the spoon on the side of the bowl a few times, composing his thoughts.
</p>

<p>
  &#8220;Well, it all started about 2 days ago. I had been flying around in the neutral sectors after picking up my new piece,&#8221; Arouin gestured towards the gun that was lying beside Vorbis´ bowl, then continued, &#8220;looking for a nice target or three. Found a couple of low ranking rookies, let ´em slide, they weren´t worth the effort, ya´ know? Itty bitty little ships, a right bitch to hit em, and if they do decide to pay up they haven´t exactly got millions to give you anyway.&#8221;, Vorbis nodded his approval as Arouin spoke.
</p>

<p>
  &#8220;So I thought I would hit the central sectors, looking for some juicy tows trying to avoid the latest wave of fighting between the Octavians and those Solrain ´Economic Terrorist´ guys. Round about Four Fingers I came across a rather interesting little&#8230; development.&#8221; Arouin paused, swallowing another spoonful. Vorbis idly stirred his meal around a little more.
</p>

<p>
  &#8220;As I jumped in I headed for Inner Storm, and I was half way across the sector when I noticed that the power lead to my radar had fallen out again, so I kicked it a couple of times until it made a connection, and my display sprung up. There on the radar was a dogfight. Looked like 6 Solrain Intensities using lasers were fighting a force of about 15 Octavian Phoenixes, and weren´t having too much success. I thought it wasn´t a good idea to interrupt them, since they were so busy and all, so I headed merrily on my way, giving them a wide berth. When I got to within about 10 klicks of the gate I got a radar image for a second, but whatever it was jumped out before I could target it. I jumped through, and carried on.. then noticed the signiature again, so I throttled up and went to take a look. It was a Solrain Light Transport using one hell of a good ECM system, and it was moving slowly so it had a small visual and motion tracker signiature. Whatever it was doing, it was being sneaky about it. If it was being sneaky it had something that it wanted to keep&#8230; and anything that people want to keep..&#8221; Arouin grinned, &#8220;..I want to take away from them and sell. Preferably for lots of money.&#8221;
</p>

<p>
  &#8220;So I pulled up next to this guy, gave him a good look at the underside of my Raven with it´s 2 Lances, and a pair of morningstars to boot, then opened up a closed comm channel to him and told him to heave to or I would blow him into next Thursday. He offered me 20 million to leave him alone. Twenty million bloody credits. That´s Real Money my friend, Real Money. But I was adamant.. I wanted his cargo.. and while he had been sitting still I had been scanning it. There was about forty tons of grain onboard, a couple of iron, and one small package. Now, grain ain´t worth 20 million credits. And I haven´t noticed a steep increase in the price of iron around here either, so I told him to jetisson the package. After quite a bit of the ´What package?´ bullshit he caved in, and jettisoned it. I scooped it up, bid him farewell, and then pumped a Lance into him at point blank and started racking off the rounds on the Barraks.&#8221;
</p>

<p>
  Vorbis motioned Arouin to stop, then looked at him quizzically, &#8220;Barraks? On a Raven?&#8221;
</p>

<p>
  Arouin laughed, &#8220;Yeah&#8230; you know that techie over at Tripoint, the madman who is always on about how he can squeeze and extra 15m/s out of an unmodified gust by tweaking the power regulator to the engine?&#8221;
</p>

<p>
  Vorbis nodded, &#8220;I know the one, Rayknay isn´t it?&#8221;
</p>

<p>
  &#8220;Yeah, that´s the one&#8230; well, he reckoned that if you fiddled around with the weapon power feeds and ammo containers you could smash through the partition between the two size one bays on each wing and cram in a Barrak&#8230; turned out he was right. Unfortunately the manufacturers haven´t designed them to go in such a compact area, and we had to cut holes in the wings and weld on steel blisters to accommodate the shell power loaders. Anyway, where was I&#8230; oh yeah, I was blowing up that guy,&#8221; Arouin grinned, &#8220;well, I could risk him letting the guys fighting the octs know that I was there and what I was doing, could I? So I decided to head for Klatches and try and find out what the hell it was that I had just picked up&#8230;&#8221;
</p>

<p>
  &#8220;Why didn´t you go to Evening´s End? Much closer surely? Or Lothar´s?&#8221;
</p>

<p>
  &#8220;I´d heard that Evening´s had just been raided by TRI cops.. didn´t you hear?&#8221;
</p>

<p>
  Vorbis shook his head
</p>

<p>
  &#8220;Well, McReily´s been locked up for about fifty years..&#8221;
</p>

<p>
  &#8220;What for?&#8221;
</p>

<p>
  &#8220;Illicit gambling, weapons smuggling, contraband smuggling, running an unlicensed bar, dealing in pornography, running an unlicensed distillery, shooting down TRI civilian ships with undue provocation, shooting down TRI police ships, resisting arrest (four counts), grievous bodily harm (two counts), grievous bodily harm against a TRI official (seventeen counts), inciting factionalist riots, printing anti-TRI propaganda, holding drugs with intent to consume, holding drugs with intent to supply, flying a ship while under the influence, flying an unregistered ship, damaging TRI property, and causing a breach of the peace (fifty-seven counts)&#8221;
</p>

<p>
  Vorbis raised an eyebrow, fidgeting with the gun &#8220;That´s quite a lot, how did they get the evidence?&#8221;
</p>

<p>
  &#8220;well&#8230; you know those security cameras McReily had covering his shop and bar in case someone tried to nick something&#8230;&#8221;
</p>

<p>
  &#8220;Ah.&#8221;
</p>

<p>
  Arouin noticed that Vorbis hadn´t touched his soya&#8230;
</p>

<p>
  &#8220;Yeah. And as for Lothar´s&#8230; a cargo tow ran into the docking bay doing 300m/s a few days beforehand. I think they are still trying to clean up&#8230; it was carrying soya, water and plutonium, so they have a pile of radioactive mush clogging up the docking systems there.. but that it should be cleared up by mid next week. So, you see I couldn´t go elsewhere except for Klatches and GBS, and since I had just left GBS after browsing the market looking for anyone needing&#8230; services.. I figured that there wouldn´t be an business there that I hadn´t already picked up on. Well, I went through to Quantar Gate, trying to take a round-a-bout route and what did I find there but another two Phoenixes, both with no registration. One hailed me, and asked if I had seen a Solrain Transport around, of course I wasn´t gonna say ´yes and I killed it´, so I told em no, and then went on my way. Of course while they were sitting there I managed to get a reading on their emissions and tuned into their squad radio frequency. Not much happening.. just seemed they were looking for the Transport. Then, about ten, fifteen minutes later I heard one of them saying that he had found a debris field, but that ´the package´ wasn´t there. Naturally, I had a severe case of will to live and high tailed it towards Klatches pretty damn fast. Not long after that a Phoenix ended up in the same sector as me&#8230; I jammed his transmission and then had to engage him&#8230; either that or let him get away and tell his buddies. Wasn´t nice fighting a Phoenix in a Raven, but he was pretty spooked by me using Barraks. After smoking him, and getting my shield generator fried in the firefight, I heard that they were setting up ambushes at all stations to try and catch me. What could I do? No-where to run&#8230; I had to come here&#8230; and then you, ya bastard, went and scratched my paint job to boot.&#8221; Arouin finished
</p>

<p>
  Vorbis nodded, and stood up &#8220;Well, my friend, I am afraid I need that package&#8221;
</p>

<p>
  Arouin put his head on one side, a puzzled expression on his face, &#8220;Hey, wait just a moment mate, that is worth 20 million credits at least, so I want a decent price, call it&#8230; 40 mil?&#8221;
</p>

<p>
  Vorbis brought up the gun in his left hand and aimed it at Arouin
</p>

<p>
  &#8220;I don´t intend to pay&#8221;
</p>

<p>
  Arouin laughed softly, grinned, and stood up
</p>

<p>
  Vorbis pulled the trigger
</p>

<p>
  Everything slowed down
</p>

```text

   [Firing Sequence Initiated]
   [Reading User Fingerprint]
   [User Fingerprint Incorrect]
   [Reading User DNA]
   [User DNA Incorrect, 23% match]
   [If weapon mode = safe then: null, else: feedback]
   [null: Exit program]
   [feedback: Power Generator Self Destruct]
   [end]
```

<p>
  The gun became a blue-white flare in Vorbis´ hand, then detonated in an explosion that left Arouins´ ears ringing, and Vorbis lying on the floor a few meters away, smoking slightly, screaming, and clutching at the cauterised stump of his left hand. The men around the room moved as if in treacle, jumping to their feet, diving away from the blast, rolling to clutch at guns, or shielding their eyes or ears from the flash and detonation. Arouin was blown backwards, but he was expecting it, and was already pulling out twin nano-flechette pistols from his holsters underneath his pseudo-leather jacket as he fell towards the floor. His right hand started pulling again and again at the trigger, flechettes flying from his pistol as he hit the metal grating on the deck, knocking the wind out of him. One of the men took a flechette in the shoulder, the thin metal dart flying straight through after expanding on impact, leaving a two centimetre wide hole through skin, muscle, sinew and bone. Another two on his right took flechettes to the arm and hip respectively, the impacts spinning them round in slow motion. He moved his right hand, firing continually on semi-auto, wildly looked left as he thumbed both guns to full-auto mode and started spraying the men who were getting to their feet as he lay on his back on the floor. One on his left managed to duck the flechettes and pull out an assault rifle, a big, bulky, reliable gun which fired big, bulky, reliable shells. The assault rifle started firing, cartridge cases springing into the air with puffs of smoke, Arouin rolled, sparks flying around him as slugs hit the metal floor. He scrabbled to his feet, then kicked one of the tables over, springing behind it as he sprayed one side of the room with flechettes from both pistols. The remaining men on the right side fell under a hail of expanding flechettes, the hail that missed pockmarking the metal wall, chipping paint and sending sparks showering onto the floor. Shots from the assault rifle hammered into the tabletop sheltering Arouin, denting and twisting the metal. As the assault rifle clicked and ran out of ammunition, Arouin sprang to his feet and span firing, reflecting that the day had become even more intresting&#8230;.
</p>

<h1>
</h1>

<h2>
  Part IV
</h2>

<p>
  The first man with an assault rifle took a pair of flechettes off-centre in the chest, spinning him back into the wall behind him, before dropping to the floor with a sickening thud. Two left&#8230; Arouin dived forward under a hail of caseless ammunition from another´s machine pistol, the tearing noise of the bullets whipping past him making him wince involuntarily as he sprayed flechettes over him. Hitting the deck face forward, knocking the wind out of himself, he swang around to look back over his shoulder as he clambered to his feet at the remaining foe bringing up another assault rifle to bear on him. The rifle flared, the report deafening in the close quarters of the room as Arouin took a round through the arm, spraying a little blood out and spinning him back to the ground, the flechette pistol in his left hand falling to the ground with a clatter. Grimacing in pain with bared teeth, he brought his other flechette pistol to bear and squeezed the trigger, spraying it across the guy who was still firing, trying to adjust his aim back down at his newly prone target and fighting the recoil of the big rifle. As the rifle made an ominous ´click´ing noise, the flechettes stopped spitting out of the gun as the line of them was about to tear him in two, and the gun quietly said in feminine tones,
</p>

<p>
  &#8220;Ammunition Depleted&#8221;
</p>

<p>
  The guy with the assault rifle went for his spare magazine, but Arouin just whipped out a large bore, no-nonsense pistol from the back of his belt, and fired with a deafening roar, splitting apart the other man´s head like a water melon and sending him flying backwards across the room.
</p>

<p>
  The complete firefight had lasted a little over ten seconds. And Vorbis was lying on the floor in a corner, still screaming, after having rolled over there in the commotion. Vorbis stopped screaming and looked up in fear as the footsteps approached him across the room.
</p>

<p>
  &#8220;I-I-I-I didn´t&#8230;&#8221;
</p>

<p>
  &#8220;You´re dead you traitorous bastard.&#8221;
</p>

<p>
  &#8220;I-I-I..&#8221;
</p>

<p>
  The pistol flared again and again, the glow reflecting the hate and anger filling Arouin´s eyes. The noise and light stopped, there was a tinkling sound as the last cartridge case hit the floor, and a slow, gentle exhalation of breath from the expired corpse of Vorbis. As the blood seeped from the body´s multiple gunshot wounds, scorched from the close range of the muzzle flame, the anger in Arouin´s eyes faded. Time to try and stay alive. He drew another clip for the pistol from his pocket and slammed it home, catching one finger with the edge of it. Arouin winced, then noticed the pain in his left arm as the red mist of adrenaline faded. Keeping a close watch on the door, with the pistol beside him on the floor, he carefully took off his jacket and looked at the wound. He had been lucky, it had ripped through some muscle on his left bicep, and it hurt like hell, but there wasn´t much he could do about it. He slipped the jacket back on, took another look at the door, then stood up and walked over to it. No lock, handle, just that damn palm scanner. He slapped his hand onto it and was replied by another generic feminine voice saying,
</p>

<p>
  &#8220;Access not authorised&#8221;
</p>

<p>
  He sighed, then, reminded by the voice, walked over to where his nano-flechette pistols lay on the ground. One was completely depleted, the other had about fifteen rounds left in it. The clips held about three hundred rounds each, the guns were experimental, highly illegal, and not strictly his. Oh well, better ditch them. The user-reliant gun had been pretty expensive; he had had to bribe three different TRI officials to get his hands on one which had cost quite a bit. Of course it <em>was</em> worth it to still be alive&#8230; Much better to have a gun you knew the capabilities of in someone else´s hands rather than one of their own&#8230; especially if it was booby trapped. Might be an idea just to sod it and set up a lump of plastic explosive in a gun casing and rig up the trigger to detonate it. He looked back at the door. He looked at the cooling corpse of Vorbis. That really isn´t a very pleasant thing to do, he thought, ah, what the hell. He walked over to Vorbis´ body, took it by the remaining hand then aimed his pistol, shielded his eyes, and pumped the trigger twice. A couple of meaty thuds, some tugging, and an unpleasant fleshy tearing sound later, he put down Vorbis´ palm on the scanner, then tucked it and the rest of the dead man´s forearm into one of his outer jacket pockets. Hey, he thought, might need it later. The door slid back, Arouin stuck his foot in the way to stop it springing back, then spung around the corner and shot the guard in the face as he looked up.
</p>

<p>
  Time to get to the hangar, he wanted to get back to his Raven before something Bad happened.
</p>

<p>
  Running through the corridors retracing his earlier steps, and wincing occasionally as his injured arm brushed something, he neared the hangar, and heard the gunfire. Not <em>again</em>.
</p>

<p>
  Arouin cautiously stuck his head around the corner of the doorway into the hangar bay, and looked at the scene in front of him. There was a splintered wooden pallet lying on the hangar bay deck in front of his Raven, and the remains of the outer covering of The Package that he had stolen from the Solrain Transport. Lying beside the pallet were the bodies of three technicians, two with pistols by them and cartridge cases on the floor. There were another three ships in the hangar which hadn´t been there before. One was a Solrain transport, painted black, and with heavier armour than was the norm. Another was a Quantar cargo tow with a large ramp leading from the cargo container. The last was an official TRI ship, looked like a personnel transporter. Scattered all over the hangar bay were bodies in Solrain, Quantar, and Octavian battle armour. He couldn´t hear any more firing, so he took a few steps back from the corner, and considered his options. Well&#8230; there weren´t very many were there? Just run to his ship, he supposed, then get out of here as quickly as possible and fly away&#8230; sod the package&#8230;
</p>

<p>
  Arouin stood, leaning on the grimy corridor wall looking dazed and confused for a few moments before making up his mind. Time to go. He pulled out his pistol, leapt around the corner, then buckled down and made a mad dash across the hangar bay for his Raven. He stopped about half way, unwilling, but too confused to run any further. He had just seen one of the bodies in Octavian combat armour with it´s faceplate up. The person in side was a Quantar. He looked around wildly, and figures stepped out from the shadows lining the outside of the cargo bay.
</p>

<p>
  One stepped forward into a patch of light from a slightly swinging fluro-bulb, a late-middle aged Solrain man with thinning grey hair, wearing a black jump-suit and wearing the badge of a TRI official.
</p>

<p>
  &#8220;Good day, Mr Arouin, I think we need a word&#8221;
</p>

<p>
  Arouin looked left.
</p>

<p>
  Figures advancing.
</p>

<p>
  Arouin looked right.
</p>

<p>
  The clomp of heavy, booted feet on metal gratings.
</p>

<p>
  &#8220;Oh, <em>screw you asshole</em>&#8221; he snarled, and raised the pistol in his good hand&#8230;.
</p>

<h1>
</h1>

<h2>
  Part V
</h2>

<p>
  Blackness. Humming. Breathing. Yep, thought Arouin, he was alive, not entirely sure where he was, or how he got there, but alive, and that would do him very nicely at the moment. He lay where he was without moving, finding out as much as he could from sound alone about his situation. He could hear other people breathing in the room&#8230; the occasional set of footsteps&#8230; and the whirring of the machines. He was clothed, he could feel that, and they weren´t his own, he was sure of that; there was no knife handle against the small of his back, and the felt looser than his own. He could feel metal with the tips of both his little fingers; a bed with rails to stop him falling off while unconscious.. he couldn´t feel any straps. Then, a female voice;
</p>

<p>
  &#8220;Dr Erischon, our patient´s brain activity has just increased fourfold.. looks like he is about to wake up.&#8221;
</p>

<p>
  <em>Wrong, lady, I´m already quite awake thank you</em>
</p>

<p>
  &#8220;Okay, go and get The Director, he will want to know immediately.&#8221;
</p>

<p>
  More footsteps. Arouin mulled over the new information in his mind. That was Director with a very definite capital ´D´. Probably not good news. Time to go. Where wasn´t the question right now, or the point; the destination didn´t matter; <em>away</em> was important.
</p>

<p>
  Taking a sudden breath, Arouin snapped his eyes open, and hurled himself over the rail of the surgical bed he was lying on, landing both feet on the floor in a slight crouch, swiftly taking in the contents of the room. Not good. In addition to two doctors and a male nurse, there were three security guards with helmets on their heads, TRI logos emblazoned on their breasts, stun rifles in their hands, and pistols by their hips. A fraction of a second after landing, Arouin pulled himself back up to his full height, and thoughtfully put his hands behind his head, sighing to himself.
</p>

<p>
  There were some situations that you just couldn´t get out of. Thankfully, this wasn´t one of them, he thought to himself happily.
</p>

<p>
  The nearest guard moved towards him, pointing the barrel of the stun gun at him, and motioned him back towards the bed. He moved back at a speed carefully calculated to be a little too slow&#8230; The guard took another step forward and made to prod Arouin with the barrel of the rifle. Oops. As the barrel moved forward, Arouin turned so that it glanced off of the arm it made contact with, and the guard stumbled forward, thrown off balance by the lack of anything to push. Arouin grabbed the stun rifle as the guard fell past, smacked the butt of the gun into the back of the guard´s head, propelling him into the floor, and span at the guard next nearest to him, bringing the stun rifle to bear. A flash of blue-white energy&#8230; darkness&#8230; falling&#8230;
</p>

<p>
  Blackness. Humming. Breathing. Again. Oh well, that didn´t go so well, did it? Arouin opened his eyes this time, and looked down, as far as he could. Hmm. Well, less comfortable now as well; grey restraining straps held him to the bed, his arms to his sides, and his fingers taped together as well. What attention to detail, he thought bitterly, how nice. Interrupting his thoughts, someone´s head hove into view, with a slightly amused expression on it´s face, glasses, and no hair.
</p>

<p>
  &#8220;Looks like he´s awake sir&#8230; what do you want us to do now?&#8221; The apparition said, in a slightly nasal whine
</p>

<p>
  &#8220;Leave the room please, you nine as well, he´s secure now I think you´ll find.&#8221;
</p>

<p>
  A veritable troop of footsteps leaving the room, and a swish-thunk of an electric door closing behind them. Then, a scraping noise as a chair was pulled across the floor. The strap across his forehead holding his head down was removed, and he turned to look at the same face he had seen in his ´friends´ secret space station.
</p>

<p>
  Whispy hair combed across the top of his head, a fairly jovial expression, little fat, and grey-blue eyes that stared out at him.
</p>

<p>
  &#8220;Hello again Mr Simmel. I regret we had to stun you&#8230; twice&#8230; but we did expect that somewhat. As you may have guessed I am a TRI official. I am The Director of a branch of TRI, and this facility you are in goes with the job, you might say.&#8221;
</p>

<p>
  &#8220;Now, you have been very careful covering your tracks Mr Simmel. No-one witnesses any crimes you might, theoretically, commit, although a lot of bodies of people who might have witnessed you have been found. Not that there were any&#8230; living witnesses to how <em>they</em> died either&#8230; You seem to have been exceptionally careful. For your last change of ship registration, the person who carried it out you for you seems to have had a nasty accident during an EVA three hours later. There was purportedly a nasty fight you were involved in at Evenings End a few weeks back&#8230; although no firm witnesses as it seemed the entire bar population died because of a tragic fire that occurred.&#8221;, the Director looked down at some notes, and flipped a page, &#8220;Yes, there seems to have been a fault with the door locking system, and then the heating system overloaded. Tragic. And, most recently, the highly secretive piece of repaired pre-collapse positron feed coupling that you stole from a Solrain transport craft&#8230; which unfortunately seems to have blown up, for reasons unknown.&#8221;
</p>

<p>
  That got Arouin´s attention all right; &#8220;Pre-collapse?&#8221;
</p>

<p>
  &#8220;Yes, Mr. Simmel&#8230; Pre-collapse. Let me fill you in on a few details. Four weeks ago a piece of a large pre-collapse space station was found; we believe it was part of a hangar bay. In it was the gouged out remains of a pre-collapse spacecraft. Unfortunately, most of the ship was fused into a solid mass of metal, but one area at the rear of the ship seemed fairly untouched. A crew cut into it, and retrieved a number of pieces of pre-collapse technology, some fairly minor, one segment not. The piece of interest was what seemed like a power generator, with an advanced power feed system. The research team at the site was a joint TRI team, including the Octavian scientist Kless Turivich. Kless realised that the output of this power plant was too much for our ship´s systems to handle, and he set about building a power regulator for the system. He was the only man capable of envisioning such a complex system as was realised was necessary to use it&#8230;&#8221;
</p>

<p>
  &#8220;Past tense?&#8221;, interrupted Arouin
</p>

<p>
  &#8220;I will get on to that&#8230; In order that a hard line factionalist group didn´t seize all the pieces, and the in-design power regulator, they were distributed amongst the factions. The Solrain took back the power generator itself, the Quantar took back the positron coupling, and the Octavians took back the only man who could make the system useable&#8230;&#8221;
</p>

<h1>
</h1>

<h2>
  Part VI
</h2>

<p>
  Arouin lay, listening to the older man telling him the tale of how things had come to pass, and how he had been caught up in the events, paying attention lest he should miss some vital point.
</p>

<p>
  &#8220;However,&#8221; the Director explained, &#8220;we had not banked on the resourcefulness of the factionalists. 14 Hours after Kless Turivich finished his prototype for a power regulator the system, his body was found in his lab, with,&#8221; The Director consulted his notes, &#8220;fifteen shots from various weapons in him. An automatic cleaner had been set to clear the floor of any boot marks his killers might have left, and had cleared up and incinerated any cartridge cases left there. The complex´s security cameras were down for a routine overhaul at this time, and four guards were found dead inside the lab; they were posted outside, it appears their bodies had been dragged in. Ten minutes before the attack a Solrain transport had docked at the Science Factory outside Great Pillars Station, where the lab was located, and witnesses report a group of four people getting out dressed in Octavian battle gear. No-one was sighted leaving the facility, the prototype had been removed along with the deceased scientist´s notes. I have a network of spies inside the factionalist groups in the various factions; Thorest Vippen, my agent in the hard-line Quantar factionalist force &#8220;The Green Chaplains&#8221; reports that they now have posession of it, and, moreover, it was condoned and supplied by the Quantar Church&#8230;&#8221; Arouin interrupted,
</p>

<p>
  &#8220;Faction leaders working against TRI?&#8221;
</p>

<p>
  &#8220;It wouldn´t be the first time,&#8221; admitted the Director, somewhat sadly, &#8220;although we need to work together to sort out this bloody mess the Collapse left us in, everyone tries to vie for power, to exert their will over others. However, back to my story. So, the Quantar now had the plans for the power regulator and the only prototype, and the positron coupling that they originally salvaged from the wreck. Unfortunately for them, it seems that they don´t <em>actually</em> have it &#8211; they have a replica &#8211; which explains why some of their leading scientists are having a hard time getting it working. The Quantar Heavy Transport Vessel &#8220;Methodical&#8221; which brought the coupling back to Quantar Core station, seems to have had a traitor on board. The traitor in question disguised the power coupling as&#8230;&#8221; The Director raised one eyebrow, &#8220;&#8230;a coffee machine, and sent it off via normal recorded delivery to his Solrain masters. The Directors Board of Solaria also approved this action by the Solrain factionalist group &#8220;Monopoly&#8221;, Irin Vapout tells me. This is the ship you stole the cargo from and then shot down, hence everyone chasing you.&#8221; The Director flipped a page of his notes over, &#8220;And finally, to complete the set, as it were, there is the power generator itself, previously in the hands of the Solrain, currently in the hands of the Octavian Government. Apparently they weren´t even subtle about it, although I am not too surprised. A squadron of Octavian Phoenixes jumped into Cornea Station space, blew up everything in sight, leaving only 2 witnesses, both mining at the time. They then started blow bits of the station up until someone asked what they wanted to stop, then carried on blowing bits of the station up until someone thoughtfully shot the power generator out of a launching tube. Once they had the generator, they inspected it, found it was a fake, landed, and then,&#8221; The Director frowned, &#8220;They shot everyone they saw, stole the generator, and went off.&#8221; He paused. &#8220;After blowing up some more of the station, apparently. The two witnesses both reported that although the electronic registrations of the ships in question were missing, the numbers were still painted on in big white type on the side of each ship. They were IDed as official ships from the Octavian Navy, who maintain that they were stolen, then put back again, refueled, rearmed and repaired without them knowing.&#8221;
</p>

<p>
  Arouin was laughing quietly to himself
</p>

<p>
  &#8220;Yes, it would be rather amusing if people weren´t being killed over it. This is why you are here. TRI wishes to get these pieces, get some decent scientists on the job, and get this damn thing working. To illustrate to you how important this is, the powerplant is approximately forty times more powerful than the largest powerplant we currently have available, and it it one tenth of the size. This means that TRI can fit more powerful sensors and scanning devices on all ships with which to find more remains of pre-collapse civilization.&#8221; The Director smiled, warmly.
</p>

<p>
  <em>Or, instead of sensors</em> thought Arouin, <em>you put a huge shield on a Phoenix, and even more guns, and then rob people.</em>
</p>

<p>
  Arouin smiled back.
</p>

<p>
  &#8220;So,&#8221; said The Director, &#8220;we have need of your particular&#8230; talents. We need you to retrieve, for us, the three pieces of this system. We don´t have anyone with as many underground connections or the legitimacy in the pirate community that you have. Think of the benefits for all of the five factions! We will give you and equip for you a Phoenix, we will remove it´s electronic registration signiature for you, and we will equip it for you, and then, you must go and get these pieces back for us&#8230;&#8221;
</p>

<p>
  &#8220;And what do I get out of this?&#8221;
</p>

<p>
  &#8220;Well,&#8221; The Director began to count out on his fingers, looking down at Arouin, who was still recumbant on the bed, &#8220;one, you get to keep the Phoenix we give you, two, we will wipe your surveillance record clean, three, we won´t execute you.&#8221;
</p>

<p>
  &#8220;I like number three, and number one isn´t too bad either&#8221;
</p>

<p>
  &#8220;So we have a deal?&#8221;
</p>

<p>
  Arouin considered his options, strapped down in a bed in the middle of a TRI facility with a possible execution order on his head.
</p>

<p>
  &#8220;You´re on&#8230; when do I start?&#8221;
</p>

<p>
  &#8220;Immediately, no time like the present&#8221;
</p>

<p>
  The Director walked over to one door and pressed the intercom button,
</p>

<p>
  &#8220;It´s okay doctor, I´m finised with him now. You can give him back his clothes and let the Lieutenant deal with him after that.&#8221;
</p>

<p>
  The thin man turned back to Arouin
</p>

<p>
  &#8220;I hope we haven´t inconvienienced you too much, I will no doubt be seeing you at a later date.&#8221;
</p>

<p>
  He smiled as he turned, hit the open control for the door, and walked you, sidling past a pair of doctors who were trying to get into the room.
</p>

<p>
  <em>Twat</em>, thought Arouin.
</p>

<p>
  One of the two doctors walked over to a cupboard on the wall, and the second over to the bed where he lay, and started undoing the straps. He rolled out of the bed, stretched his arms, and then gratefully took his clothes that were proffered by the first doctor. He changed behind a screen they thoughtfully erected, and then walked out into the corridor where he met ´The Lieutenant´, a fairly handsome clean shaven young man who looked about twenty six, Arouin thought, and was impeccably turned out. He was a good five inches taller than Arouin, who walked up to him and stood in front of him looking up into the other man´s face.
</p>

<p>
  &#8220;At that height I bet you don´t do too much flying.&#8221;
</p>

<p>
  The Lieutenant looked down,
</p>

<p>
  &#8220;No, I don´t&#8221;
</p>

<p>
  &#8220;Didn´t think so, where are you meant to be taking me?&#8221;
</p>

<p>
  &#8220;To the hangar, follow me.&#8221;
</p>

<p>
  The young man snapped around and walked smartly off, Arouin trailing behind in a decidedly more relaxed fashion. They walked through some beige-walled well-lit corridors, past doors with intresting signs such as &#8220;Dr. Rev. Turrin, MCC, MSFT, KLI, JIT, JHUIT w/ Honors&#8221;, and past a number of men destined to spend most of their life replacing the signs with ever-increasingly long ones as scientists and doctors attained higher and newer academic achievements.
</p>

<p>
  Then, abruptly, the Lieutenant´s clicking footsteps led them into a new area with grey walling, with the occasional greasy smudge of a dirty handprint on them. One or two panels were off the wall, one with a couple of technicians crouched beside it, poking at wiring out of view and having an animated discussion over the intricacies of re-cabling the section. A few moments later Arouin caught a wiff of afterburner fuel, and seconds afterward they were entering the hangar bay. The bays were all alike, throughout the known Universe. If, tomorrow, a new life form was discovered, say a silicoid species from some distant planet orbiting a distant star. They might eat rocks, make love using radio waves, enjoy staring at gravel, entertain themselves by walking around very slowly, but, if they had space travel, then their hangar bays would <em>still look like this</em>. Grease, blackened walls from the occasional test-fire or miss-fire of engines, the smell of fuel from ill-fitting seals around fuel canisters, the occasional drop of blood on the floor where someone had been cut by a sharp new fitting. All familiar sights to Arouin, and ones he felt instantly at home with. What wasn´t familiar were the ships in this hangar bay. Two odd, organic-looking, black craft which seemed perfectly matte. A cruiser of some type Arouin had never seen before bristling with spiney protrusions. A fighter which was actually too small for a person to actually fit into. A triple-engined Phoenix. Arouin hurried forward, and grasped the shoulder of the man ahead of him, halting him and spinning him around,
</p>

<p>
  &#8220;Is that one mine?&#8221;
</p>

<p>
  The taller man looked witheringly at him
</p>

<p>
  &#8220;No.&#8221;
</p>

<p>
  &#8220;Oh.&#8221; Arouin felt crestfallen, like a little boy again who had just been told that he isn´t going to get what he wants for his birthday, no matter how nicely he asks. The Lieutenant pointed over to the back of the bay,
</p>

<p>
  &#8220;That´s your ship.&#8221;
</p>

<p>
  Arouin walked over to it, and whistled. A brand new Phoenix, no paint chips or burn marks or anything. A technician was busy filling her tanks up with afterburner fuel. Arouin walked over to her,
</p>

<p>
  &#8220;Hello there miss, what´s this baby got on her?&#8221;
</p>

<p>
  The woman looked up into his slightly smiling face,
</p>

<p>
  &#8220;I don´t know, I´m ´fraid, I´ll go ´n get Othel, he´s the one who outfitted her.&#8221;
</p>

<p>
  &#8220;Thanks&#8221; The technician got to her feet and walked off towards one of the sides of the hangar bay. Arouin looked back over his shoulder at the Lieutenant who was looking uncomforatble around all this high-tech machinery. <em>Definately a station-sitter</em>, thought Arouin, <em>he can´t stand the technology. Probably an honest-to-goodness technophobe. Probably was too stupid to understand that it was the technology he feared that let him breathe the air he breathed and stay on the deck he stood on.</em>
</p>

<p>
  Arouin turned back as another technician approached, this one slightly less visually pleasing than the last; and definately not female. There were two types of technician you got around space stations, reflected Arouin, there were the wirey ones, who looked like they survived on about 20 calories a day, darkened by dry grime, they fidgeted constantly, hated people touching anything they were working on, and made modifications left right and centre to their given task. Then there was the other type, fat, constantly eating, sweaty, greasy with machine oil, talked loud and made lurid jokes, and generally were heavily overweight lazy bastards. This one was <em>definately</em> of type two. He walked with a pronounced waddle, the front of his boiler suit was streched, seemingly almost to breaking point, and he held a doughnut in one hand. In the other he held a flask of coffee.
</p>

<p>
  &#8220;I hear ya´ wanna nawh whatf fshe´sf got inner?&#8221; he said loudly to Arouin, who got an un-needed look at half-eaten doughnut in the mouth of the speaker and a healthy portion of what was in the other man´s mouth sprayed over his shoes and the floor around them. Arouin looked back up from his shoes at the other man´s face.
</p>

<p>
  &#8220;I take it you´re Othel?&#8221;
</p>

<p>
  &#8220;Yeffire, Blaarg Othelf fthe name.&#8221; he sprayed. Arouin took a step back. Blaarg seemed either not to notice or not to care.
</p>

<p>
  &#8220;Okay&#8230; what´s this bird equipped with?&#8221; Arouin asked. Blaarg, thankfully, swallowed before replying,
</p>

<p>
  &#8220;Well mister, she´s godda Pint cap, cauze we ain´t got anneh Deepols arroun´, an´ git a dream in theyre,´n all the usual stuff, n´&#8230;&#8221;
</p>

<p>
  Arouin interrupted him. He took another bite of the doughnut.
</p>

<p>
  &#8220;I´ll&#8230; find out myself I think.&#8221;
</p>

<p>
  &#8220;You fsure?&#8221;. A brief shower of icing and dough.
</p>

<p>
  &#8220;Positive&#8221; said Arouin, talking towards his new ship&#8230;.
</p>

<h1>
</h1>

<h2>
  Part VII
</h2>

<p>
  The ship loomed before him as he strode purposefully toward it, dull brown with the Octavian Phoenix insignia visible, and no serial number printed on the side. As he climbed up the slightly oily rungs leading to the cockpit, he dwelt on what that meant. There was no registration number on the side of the ship, and the electronic registration was supposedly purged as well. Well, that meant he had to get a holo-shift plate so he could switch ´painted´ serial numbers, and a serial number generator for the electronic side of things. Then he just had to find the tracker that they put on the ship; he wasn´t naive enough to think that there wasn´t one. He hit the cockpit release pad, noting that he would have to get it replaced with a palm scanner when he could, and climbed in. There was a great deal more space than he was used to in his Raven, and even a hatch down into the (admittedly small) cargo bay was there. The shield generator and ECm systems and diagnostics panels were easily accessible, and there was an access hatch in the cargo bay up to the underside of the engine which meant that, if necessary, the engine could be lowered into the cargo bay for emergany maintenance.
</p>

<p>
  <em>Very nice,</em> thought Arouin, <em>I won´t be parting with this in a hurry.</em>
</p>

<p>
  He turned around to face to the front again, noting the cluster of three people still watching him, and sat down heavily in the synth-leather upholstered pilot´s seat, making it rock back and forth slightly. He swung the control board around, and the joystick control with it; he hated the flight yokes that they used on some of the larger ships. Then, Arouin initialised the main generator, shut off the radar, shield and engine power, and disconnected the capacitor and ECM, all with a deft few strokes of his left hand. And then, finally, he brought up the power usage screen. With all systems off he should be able to see where the power drainage was a few fractions of a watt too large; where there was that extra tiny bit of power drain that might show a bug connected to his power system. But nothing showed. That either meant it had it´s own power source; unlikely to last for very long, or&#8230;.
</p>

<p>
  Arouin grinned, stood up, walked around to the back of the pilot´s seat, and opened the hatch to the cargo hold. Standing at the bottom of the warning-striped ladder, he looked behind it and swung it out the way to reveal the panel hiding the power generator. A few movements with a screwdriver had the metal panel clattering to the floor, and he moved forward to get a closer look at the generator. It was a Sport Plus, a mother of a power generator; the same one they used to power the massive cargo tows, and it would take up fully half of the cargo bay if he moved it out.
</p>

<p>
  <em>No,</em> he thought, <em>technicians were lazy, and they wouldn´t think that he would look for a bug, would they?</em>
</p>

<p>
  Looking down, Arouin saw a hatch, screwed down, with a ´warranty void if broken´ seal on it. The seal was broken. Another few twists of the screwdriver, and another, quieter clatter had Arouin looking with satisfaction at a small, black sphere nestled within a circuit board. A firm tug and it was removed, and then placed in his pocket for later disposal. Happy with his work, Arouin replaced both panels, brought down the ladder, climbed up, closed and latched the hatch, then spoke into the comms mike while sitting down in his seat,
</p>

<p>
  &#8220;Ok, this is Mr Simmel, I´m ready to rock ladies and gentlemen.&#8221;
</p>

<p>
  There was a crackle and a muffled thud as a technician, obviously not at the control console they should have been at, sat down quickly
</p>

<p>
  &#8220;This is Tower; we will move you into the airlock now.&#8221;
</p>

<p>
  Arouin flicked a switch changing his comms to external audio mode.
</p>

<p>
  &#8220;Ok folks,&#8221; his voice boomed out over the hangar from the speakers on the Phoenix, &#8220;time for me to leave, and I don´t think you three want to be breathing vacuum, so if you would kindly step aside from the lift&#8230;&#8221;
</p>

<p>
  The little group turned and walked away, one leaving a small train of crumbs.
</p>

<p>
  Another flick of the switch
</p>

<p>
  &#8220;Ok, this is Simmel, Tower, everyone is clear, repeat, I am clear to go&#8221;
</p>

<p>
  &#8220;Roger that&#8221;
</p>

<p>
  There was a moment of silence, the thud of a lock disengaging, then a lurch from the sudden downward motion of the lift that the Phoenix was on. The landing bay floor rose up to meet him, then engulfed the cockpit in darkness, the only light streaming in from above with dust motes gleaming in it like far-off stars. The drone of the lift continued, while a whirr started as the door above slid across, cutting off the light from above like a moving blade of darkness. For another few seconds there was almost complete blackness, with only the faint illumination from the cockpit controls shedding any light, before there was another lurch as the ship´s descent was halted abruptly.
</p>

<p>
  &#8220;This is Tower, about to open airlock door; hope you have everything buckled down&#8221;
</p>

<p>
  &#8220;Simmel; gotcha, ready to go&#8221;
</p>

<p>
  There was a sudden pumping rush as motors sucked the precious air back into the station, then the outer airlock doors opened and the Phoenix was moved forward into the lauching tube.
</p>

<p>
  &#8220;Good bye Mr Simmel&#8221;
</p>

<p>
  &#8220;Cya ´round, Tower&#8221;
</p>

<p>
  A sudden sensation of increased gravity, hammering him back into the padding of his rather nice new seat, and he was away. Stars shone in the distance, a faint turquoise nebula glowed in the distance to his left, a dust storm making a dirty grey-brown smudge against its beauty. How I love space, thought Arouin.
</p>

<p>
  Abruptly a voice broke him out of his quiet contemplation of the void,
</p>

<p>
  &#8220;Simmel, this is The Director,&#8221; the old man´s crisp voice informed him, &#8220;take the only jumpgate out of here; it goes to The Stith. On the far side you will be in another jumpgate; it does not transmit its position like others in space; you need to know where it is to find it. When you need it, we will contact you; there is no rotacol on the ship we have provided. We are watching you, Mr Simmel, be sure of that&#8221;
</p>

<p>
  <em>Oh, I bet you are,</em> he thought, <em>I bet you are&#8230;</em>
</p>

<p>
  &#8220;Roger that big D. See you around.&#8221;
</p>

<p>
  And with that, he turned his ship swiftly, and headed towards the trace on his radar.
</p>

<p>
  Two minutes later, after the swirling blue vortex of energy had engulfed him and thrown him across the universe, he stood up, dumped the tracker in the waste disposal chute, ejected it, and then happily headed for The Gurge. Arouin smiled, knowing that he had been a little more experienced than they had accounted for.
</p>

<p>
  <em>Time to call on some old favours I´m owed,</em> he thought, cheerful enough that he was alive, and with A Plan.
</p>

<p>
  A few hundred parsecs away, The Director looked hawkishly at the tracker terminal. The trace on it had not moved for fifteen minutes; possible, but unlikely.
</p>

<p>
  <em>Most probable that the pirate has found the tracker,</em> he thought, <em>oh well.</em> A man in uniform hurried through the door, forgetting to knock. The Director looked around at him, one eyebrow raised,
</p>

<p>
  &#8220;Excuse me sir, but the stealth scout we have in Ring View reports that the target has moved off.&#8221;
</p>

<p>
  The Director looked back at the stationary trace on the tracker monitor, then back over his shoulder at the red faced officer.
</p>

<p>
  &#8220;Very well, activate the secondary and tertiary trackers.&#8221;
</p>

<p>
  &#8220;Yes sir.&#8221;
</p>

<p>
  The man hurried you again.
</p>

<p>
  The Director permitted himself a thin smile as, after a minute or so, two overlapping blips showed up on the tracker screen.
</p>

<h1>
</h1>

<h2>
  Part VIII
</h2>

<p>
  The arrow shape of the Phoenix fighter cut through the void, a faint red glow reflecting off of one side of it as it passed by a tuned TRI Beacon. Inside, Arouin smiled quietly to himself as he sipped at some coffee, which was a little too hot for his liking, and thought of what he should do first.
</p>

<p>
  Rjin Fiddick will sort me out with the registration number changing equipment&#8230; just need someone to tweak the weapons systems and check this baby out. Who do I know in Outpost who would do the job? Hmmm&#8230;.
</p>

<p>
  Arouin took a rather too long gulp of coffee and was rewarded with scalding the back of his throat and burning his tongue. He cursed, and put the coffee to one side to let it cool while he ran through his inventory and ship systems for the umpteenth time. Then, for the first time, he noticed his ´available funds´ count, and cursed again. He never usually bothered with money; he had enough that he didn´t have to keep track of it all the time, but now it looked like he was going to have to, at least for a bit. The pale green display readout showed a big fat zero.
</p>

<p>
  Well, he thought, I´m going to need a few creds to pay for the work I need done&#8230; so time for some light piracy
</p>

<p>
  He smiled to himself; who said you couldn´t mix business with pleasure?
</p>

<p>
  He spent the next half an hour sweeping the sector for potential targets; there were a few cargo tows around, but none seemed loaded enough to make it worthwhile risking attacking them; one or two had escort as well. He was damned if he was going to go after a fighter in this thing, untried and untested, as pretty and deadly as it may look, he couldn´t catch the new advanced scouts which rolled off of the production lines six months past, and scouts and light fighters just weren´t worth enough to threaten. Which meant he needed a light transport to mug.
</p>

<p>
  At the end of that half hour, he approached a medium-sized asteroid field, and began paying slightly more attention to his radar, as well as scanning nearby rocks visually. After another few minutes he caught the tell-take flicker of a mining laser in his peripheral vision. Arouin slowed his ship slightly, then looked hard at the radar. Yes&#8230; there it was; if you looked closely enough you could just about see two seperate traces on the radar, almost merged into one with proximity; no wonder the targeting computer couldn´t pick it up. He started spiralling in towards the ship, not making a direct approach which might alert the pilot of the mining ship too quickly. Suddenly, eight kilometers out, he swung around, and hammered the afterburner toggle, the suddenly increased thrust picking him up bodily and driving him into the back of the seat. Seven kilometers, six kilometers, five kilometers&#8230;.
</p>

<p>
  Arouin thumbed the comms channel switch to closed-beam, and aimed it at the mining ship, and spoke into it, loudly, firmly, and calmly;
</p>

<p>
  &#8220;This is the Pirate Vessel Lithe Shadow; pay immediate fine of two hundred and fifty thousand credits or be destroyed.&#8221;
</p>

<p>
  The reply was not exactly what he expected,
</p>

<p>
  &#8220;Bunnis, Wilks, we got a pirate here, look sharp and waste this sonnofabitch!&#8221;
</p>

<p>
  <em>Oh dear.</em>
</p>

<p>
  Two more traces appeared on radar, and the mining ship disengaged it´s lasers and turned to face him, moving out of the radar shadow of the asteroid. Arouin flipped through the three targets rapidly.
</p>

<p>
  <em>Shit.</em>
</p>

<p>
  The transport was a modified ´Aggressor´ variant of the standard Quantar light transport &#8211; the Hurricane. He had seen the Aggressor at a show at Quantar Core when he had been there a few months back; the religious freaks showing off some of their newest and fanciest technology. That thing had an extended cargo bay&#8230; with four hitmen in it, and the rest of the expanded bay&#8230; was ammo. Plus the standard missile loadout of at least six purgatorys, equals trouble. He could outrun the purgs, but he didn´t fancy his chances of being able to outrun them while weaving and dodging asteroids.
</p>

<p>
  The other two ships were both light fighters, one Solrain Interceptor, one Quantar Cyclone, and they had appeared at his seven and four o´clock.
</p>

<p>
  <em>Time to rock and roll,</em> Arouin thought.
</p>

<p>
  He hung on the stick, slamming his Phoenix around as the missile warning indicator light blinked on, blazing a cheerful yellow light all over the cockpit. His radar showed three inbound tracks, all from the transport
</p>

<p>
  &#8220;Three missiles inbound, two Purgatory class, one Calypso class&#8221;, a pleasant melodic voice infomed him, startling him somewhat,
</p>

<p>
  &#8220;What the&#8230;?&#8221;
</p>

<p>
  <em>NO TIME!</em>
</p>

<p>
  Glacing at the VDU showing him the rear view from just in front of the engine nozzle, he saw three bright streaks arcing towards him.
</p>

<p>
  <em>Time to move</em>
</p>

<p>
  Glacing around quickly for an asteroid, he saw a cluster of small rocks a mere click away, and broke his turn off into a long arc bringing him behind the roid´s just as the missiles were about to hit, slamming two into the balls of rock. One of the purgatorys managed to make it through, and he span 180° and hammered on the afterburner again, the missile whipping past him as it overshot, it´s guidance systems unable to keep track of the sudden change he had made in his speed. The two light fighters closed, the transport holding ground, waiting for another chance to get off some missiles. The Cyclone was slightly ahead of the Interceptor, and he swung around making a beeline for it through the middle of another asteroid cluster, having to roll his ship to avoid chopping off a wingtip on one of the ´roids. Closing to 3000 meters he opened fire with the quad Barraks that the Phoenix was currently equipped with, dragging the four lines of death into the path of the closing Cyclone with a practiced eye. After seven years of combat he didn´t need targetting computers any more. Blue Barrak shells smashed into the Quantar ship´s shields, giving a pleasing green rippling glow effect. It rolled, trying to get hit by as few of the bolts as it could, then started firing back with a Hammer and a pair of Strakers. Blue and gold glowing shells whizzed past the cockpit, Arouin locked his eyes firmly on the ship ahead of him, adjusting his aim constantly, holding true as shells started to plough into his shields. At one kilometer range he ceased fire, put a little skew velocity on his ship, rolled, and began to thrust again bringing himself around in an arc past a large asteroid. The Cyclone shot past, and circled around the other side of the asteroid, the Interceptor closing now to one and a half klicks and opening fire with a pair of lasers, the pair of darting blue beams vainly reaching out for a touch of his shields. As he circled the asteroid the Interceptor followed him around, the occasional blast of the paired lasers taking out small chunks of rock from the asteroid and releasing puffs of dust and glowing molten metal. The Cyclong pilot hadn´t slowed as much as Arouin´s turn had allowed him to, and he appeared in Arouin´s sights, turning across the nose of his ship to expose himself to the shortest amount of fire possible. Another roll, and another haul on the stick changed that, Arouin arcing in behind the Quantar pilot, and opening fire again. As he closed, firing, the lightning-blue bolts ripped through the other pilot´s shield and began hammering on the armour of his ship. The Quantar jinked around, avoiding a good portion of Arouin´s shots, while his buddy in the Interceptor formed up behind Arouin, then hit the afterburner to close the gap. The Phoenix slid around like a dog being dragged behind the Cyclone at the end of a leash, the pair dodging in and around asteroids as the Interceptor steadily closed the gap. Laser fire started searing past, smashing off lumps of rock from the asteroids that Arouin was weaving around; he needed to do something, fast. He aimed at an asteroid ahead of him, and let fly with a stiletto missile, then engaged the afterburner so that it slowly crawled away from him on a slightly different heading. The missile slammed into the asteroid, splitting off a good dozen medium-size lumps of metal and rock, while span wildly around, moving away from the parent ´roid as Arouin tore past, the rocks a grey-brown blur of motion. The Interceptor pilot broke off suddenly to avoid the spreading pile of rocks from the explosion, rolled his ship to avoid one, then caught a glancing blow from another which lit up his shields and drew a collection of sparks as it managed to penetrate and bounce off his armour. The Cyclone ahead, taking dangerous amounts of armour damage, curved again towards the transport, throwing in the occasional jink, but basicly running hell-for-leather for safety. Behind, the Interceptor began to line up for another run, in front the Quantar was slowly loosing ground. Too slowly. Arouin reached across the control panel and tapped the flashfire ignition control. A shockwave ran through the ship and the air was forced from his lungs as massive acceleration from the fuel additive brought the engine thrust up to twice its rated limit. The distance to target reading began to blur on the first and second digits. The ammo counter began to blur as Arouin jammed his finger on the trigger. The seeking, probing shells found a chink in the armour of the Cyclone, tearing a sheet of it spinning off into space. Shells smashed into delicate internal systems, reducing them to worthless slag, chopping conduits, tearing seals and rupturing tanks. Then the ammo store went off, scattering bits of the ship outward in a firey explosion that started some of the nearby asteroids moving. Small chunks of metal impacted on Arouin´s shield as he smiled grimly in satisfaction at the rip, and as other bits of metal ricocheted off of nearby ´roids, a small, grey escape pod whizzed away at high speed, leaving a small green trail behind it. Targetting the Interceptor, he found it was already running&#8230; but not in the direction of the transport. The transport itself had turned, and was accelerating away&#8230; slowly.
</p>

<p>
  <em>He´s loaded,</em> thought Arouin, <em>no wonder he didn´t engage, he´s probably got a good forty or fifty tons of ore in there.</em>
</p>

<p>
  Arouind smiled to himself as he brought the ship around and headed for the transport.
</p>

<p>
  <em>And The winner takes all&#8230;</em> he thought to himself.
</p>

<h1>
</h1>

<h2>
  Part IX
</h2>

<p>
  Arouin sat back and grinned as three hundred thousand credits were electronically transferred to him across the void from the pilot of the Quantar transport. A closer scan of the Hurricane had revealed that the hitmen in the weapon extension bay were actually fakes, and the ammo storage was being used to hold minerals that the ship was mining. The pilot of the other vessel had become suddenly compliant after his two escorts were blown away and fled respectively.
</p>

<p>
  And now the Hurricane sat in space before him as he prepared to move out of the asteroid field. He reached out for the throttle with one hand, while fingers on his other hovered over buttons and triggers that could send a lethal rain of munitions hurtling across the space toward the miner´s ship. He relaxed, pulled back on the stick to point his Phoenix towards a comparitively empty region of space, and jammed the throttle forwards.
</p>

<p>
  <em>Time to go&#8230;</em>
</p>

<p>
  A few jumps later he was outside the Outpost Station &#8211; the Octavian station closest to those of the other factions, where the rule of the Octavian Empire was least strong. It was a comparitively old station; paintwork chipped in most places, scorch marks from pilots franticly trying to avoid sudden high-velocity meetings with the metal walls. Lights blinked on and off sparodically, marking the edges and corners of the station in case of severe conditions limiting visibility. One bunch of lights was non-functional, it looked like a power generator had gone down or a section of cabling damaged somewhere, and one of the launching tubes had the remains of a Solrain cargo tow jammed in it, metal twisted and ruptured from multiple weapons impacts. Here, raids were organised and raids were targetted, on the outskirts of what passed for the law in Octavian space, you could lay your hands on almost anything, get anyone for any need you might have, and, in Arouin´s case, get attacked by the station´s defence droids. His non-registered ship bought him some problems when the droids lauched &#8211; the station controllers assuming that he was actually part of a raiding party &#8211; and he had a brief, tiwsting dogfight with a couple before he destroyed them both. Although good at linear calculation of where the target should be, the droids were pretty dumb &#8211; any half skilled pilot could take them out.
</p>

<p>
  Traffic moved back and forth, Arouin´s eyes scanning around, looking for any potential aggressors. A couple of Solrain cargo tows left the docking tubes, followed by a pair of Intensities. A few light transports were moving towards and away from the station, another couple mining from rocks a few tens of kilometers away. He headed towards the angular, brooding hulk of the station, his docking ring holos activating at 6 klicks range. A trainer ship whizzed past him easily doing 250m/s and cratered on edge of the docking tube, a flurry of molten metal sent flying from the broiling, firey explosion. The droplets cooled rapidly from white hot to orange then soft red, then, finally back to a dull silver-grey. A few rattled against the outer hull of his ship as he involuntarily winced at the explosion, then laughed quietly to himself as the escape pod emerged, scorched but unharmed, from the blast, and then docked at the station.
</p>

<p>
  <em>Well,</em> he thought, <em>you have to learn don´t you&#8230; there´s always something new to learn. In this case, docking</em>
</p>

<p>
  He grinned again, and moved the ship towards the shadowy, gaping maw of the docking tube.
</p>

<p>
  Darkness enclosed the ship again, the whirring and clanking and screeching of metal of a not-particularly well looked after lift systems grinding on Arouin´s ears. Gradually the lift doors into the hangar above creaked open, sticking occasionally, and light flooded down again. Flickery light from old fluro-tubing, but light nonetheless, and Arouin welcomed it; it meant that his ship had not been clamped in the docking bay, trapped by officials anxious to find out exactly why he didn´t have a registration number on his ship&#8230; A few seconds later the lift shakily stopped and locked into position. Arouin stood up, stretched his legs, and hit the cockpit release switch as he looked out over the hangar. Pilots, technicians, salesmen and generic hangers-on cluttered the hangar; there were a good thirty ships docked, with room for another seventy if necessary. He scrambled down the side of the ship slapping the cockpit lock panel as he went down, and started off towards one corner of the hangar. Stepping over cables and piping lying on the floor, ducking under the wings of the occasional ship he passed under, he looked around as he walked, making sure that nothing too unexpected was around. He reached the hangar wall after brushing off a couple of errant grubby cleaners offering to wash his ship and ducked into one of the many corridors leading away from the hangar. Ignoring the signs on the walls, helpfully showing him to a hundred different places he didn´t want or need to be, he made his way to a service shop on the second storey balcony of one of the main shopping areas. It was marginally cleaner than the filth of the hangar, very marginally, but noticeably so. Someone had made an effort to polish the hand railings, he could see that, but the swirled, mottled film of grease showed that they had been more sucessful in spreading it around than getting rid of it. The security grille was down over the shop window, and the door was locked. Closer inspection showed that there was a barely readable notice embossed on the door informing any trespasser that there were a range of lethal booby traps behind the door, and it really would be best for their personal safety if they didn´t try and get in. Arouin paused for a moment, then knocked hesitantly at the door. No answer. He looked down through the meshed metal flooring beneath his feet at the people swirling between and around each other, all in a hurry to get somewhere. A couple leant against each other by the side of a shop doorway, kissing. Another group of people laughed as they swigged some Octavia Light, stumbling around a corner out of view, a man in a dark brown jacket and blue shirt accessing a public information terminal.
</p>

<p>
  <em>I can´t stand around here all day,</em> Arouin thought.
</p>

<p>
  He turned back at the door and knocked at it again, slightly more forcefully than before, the door shaking on it´s hinges slightly. Arouin pressed his ear to the plasteel door, listening for anything behind it. Nothing. He took a step backwards and started kicking at the door, again and again. The racket he was causing attracted some attention from those above and below him, but he ignored them and carried on. After a few more seconds there was a sound of running feet from inside,
</p>

<p>
  &#8220;What the hell do you want? And stop trying to demolish my bloody door!&#8221; said a muffled female voice from inside, as a small hatch in the doorway opened, revealing a pair of green eyes and a whisp of brown hair.
</p>

<p>
  Arouin smiled.
</p>

<p>
  &#8220;Oh it´s you. Okay, come in.&#8221;
</p>

<p>
  There was a series of clicks as latches and bolts were unlocked, then the door opened and the diminutive woman inside took a step forward, ushering Arouin in as she gazed suspiciously out at the people looking at him.
</p>

<p>
  Once he was through the doorway she slammed it shut, closed a series of latches, then swung a second, heavily reinforced steel door across behind it. Then she set up a small plastic explosive charge rigged to a laser tripwire. Arouin watched in fascination as she completed her task, then turned to face him.
</p>

<p>
  &#8220;Long time no see, my pirate friend&#8221; she said, smiling, as she pushed past him and through a doorway off to one side of the corridor they were in. Grey-green paint flaked off the walls, and red-brown rust stains from the floor above showed at the top of them. Arouin followed her into the room. It was cluttered with miscillaneous electronic components, papers, and three different computer terminals all at one desk. A table by one wall had three chairs by it, and it was here she sat down, motioning Arouin to take the chair opposite. He sat, and she dialled up a couple of coffees from the drinks machine set into the wall, stirring Arouin´s before passing it to him. She sipped at the steaming brown liquid and then looked up at him,
</p>

<p>
  &#8220;So, what do you need?&#8221;
</p>

<p>
  &#8220;Quite a few things, Raquel. Firstly, I need a complete computer systems check on my new ship; I have reasons to think it might have a few things wrong or&#8230; different about it&#8230;&#8221;, here she raised an eyebrow, but let him continue, &#8220;Secondly I need you to perform the usual override-proofing on the cockpit release control, and wire up the user-recognition on the main flight controls. I think that´s it&#8230; how much do you want?&#8221;
</p>

<p>
  &#8220;Well, lets see&#8230; I´ll do the systems check for twenty, the cockpit release for another ten, then thirty five for the installation of the user-recognition system; it will cost you another fifteen for the unit itself.&#8221; Arouin gulped down a mouthful of the coffee, it was quite passable, then looked at her seriously.
</p>

<p>
  &#8220;Ten for the cockpit release? Come on Raquel; I´m a good customer&#8230; aren´t you doing a loyalty card scheme or anything yet?&#8221;
</p>

<p>
  She grinned at him thoughtfully, sipped her coffee,
</p>

<p>
  &#8220;Ok, I´ll do the cockpit release on the house, since you´re hiring me to do the rest.&#8221;
</p>

<p>
  &#8220;Deal&#8221;
</p>

<p>
  &#8220;Where is your new baby parked, and what ship name?&#8221;
</p>

<p>
  &#8220;Bay&#8230; uhm.. 47, and she´s the ´Lithe Shadow´&#8221;
</p>

<p>
  &#8220;Ok, if you let me shut up shop I´ll head over there now, I am assuming you want it done ASAP&#8230;&#8221;
</p>

<p>
  &#8220;Of course&#8230;&#8221;
</p>

<p>
  &#8220;Right, well, it should be done in about three hours, give or take. Come to bay 47 then and we´ll sort payment&#8221;
</p>

<p>
  &#8220;Sounds good; I have things I have to do here&#8230; sort the registration for one.&#8221;
</p>

<p>
  Raquel looked up hopefully as they stood up and Arouin pushed his chair aside.
</p>

<p>
  &#8220;I sort registrations now as well you know&#8221;
</p>

<p>
  Arouin turned, putting the remains of his coffee on the tabletop.
</p>

<p>
  &#8220;I know you do my dear, but how big is your database of registrations to assume? You know Fiddick has the biggest and the best. He has at least twenty of us using his database, and he has ninety complete ship registrations.&#8221;
</p>

<p>
  &#8220;Yeah, but I bribed a tech down in Core to let me have access to his terminal for ten minutes &#8211; I had fifty complete registration profiles all ready. I uploaded them, and now they are mine to use&#8230; and I haven´t got anyone using them yet.&#8221;
</p>

<p>
  Arouin raised an eyebrow,
</p>

<p>
  &#8220;Fifty to one? Your on, dependant on cost, of course.&#8221;
</p>

<p>
  &#8220;I´ll sort the complete reg system for 200. Monthly fee of 30, two months included in initial purchase.&#8221;
</p>

<p>
  &#8220;Uhmm&#8230; okay, since it´s a one to fifty ratio I guess you can command a bit of a premium&#8221;
</p>

<p>
  She grinned
</p>

<p>
  &#8220;You won´t regret it. It´ll take me another 2 hours on top of the rest.&#8221;
</p>

<p>
  &#8220;Well, I´ll be coming along; now I haven´t got anything else I need to do&#8221;
</p>

<p>
  &#8220;Let´s go.&#8221;
</p>

<p>
  She stood, and walked through the doorway back into the corridor; Arouin, remembering the security system, followed slightly more cautiously&#8230;
</p>

<h1>
</h1>

<h2>
  Part X
</h2>

<p>
  As Raquel left the run-down shop, closing the outer door and locking the final tumbler-lock behind them, Arouin looked down. The couple had moved off&#8230; a group of children ran through the mall below, stealing the occasional sweet or magazine from the shops they moved past. Arouin caught a wiff of something possibly edible being cooked below.
</p>

<p>
  &#8220;Raquel, I´ll catch up with you in a few minutes honey, you head off to the ship, I´m going to go and get myself a meal.&#8221;
</p>

<p>
  He grinned at her and patted her on the shoulder. She smiled back, and moved off down the walkway. Arouin leant over the edge of the walkway, looking below for the source of the enticing smell, and after a few moments he spotted it, a small Che-tak servery below and to his left. He swung his legs over the rail, standing on the outside, lowered himself on both arms, then waited for a convienient gap in the crowd before letting go. He landed on both feet, almost being knocked off balance by a group of people surging past him, then balanced himself, and headed towards the wafting smell. Pushing through the crowd he came to the black-fronted servery, and picked his way carefully through most of the customers sitting and eating their Che-tak on the floor to get to the counter.
</p>

<p>
  Peering over the plexi-glass fronting a small, dark skinned woman surveyed him critically.
</p>

<p>
  &#8220;What flavour then?&#8221;
</p>

<p>
  Arouin looked over the options laid out in steaming bowls the other side of the plexi; they were all the same, just with different synthetic flavourings.
</p>

<p>
  &#8220;I´ll got for the spicy&#8221;
</p>

<p>
  &#8220;Not the extra spicy?&#8221;
</p>

<p>
  &#8220;No,&#8221; said Arouin, remembering a time at Rakki´s Che-tak restaurant a few months back, &#8220;not the extra spicy&#8221;
</p>

<p>
  The small woman looked somewhat mollified. Most places serving Che-Tak vied with one another to produce the most powerful mix of synthetic flavourings, and by the way Arouin´s eyes were watering slightly, he felt that this was probably considered qutie a good one.
</p>

<p>
  Nevertheless, she handed over a polystyrene bowl of the steaming mess, which he happily accepted, and gave her fifty credits, five over what she was charging, feeling slightly guilty for his lack of enthusiasm over the extra spicy flavour. Che-tak was expensive; it should be. It consisted of a mix of whatever ´real´ food the cook had managed to beg, barter or steal for over the past few weeks, all mixed in together and boiled up. The more expensive the Che-tak, the less grain it had in it; the more likely it was to have some small piece of real meat, usually gristle, or maybe a mass-farmed prawn or two.
</p>

<p>
  Arouin wandered away from the eatery, walking down the mall, looking at the shop fronts. Clothes shops, mostly with second hand legal or stolen products on offer, news stands, one posh restaurant &#8211; you could tell &#8211; it had six armed guards outside, some part´s shops, the occasional flea-pit tri-vid cinema. As he finished his Che-tak and dumped the bowl by the wall, he noticed a group of young men looking around them shiftily as they walked into an allyway. A few seconds later there was a shout, and the sound of running feet. Arouin, intrigued, drew his large-calibre pistol and moved swiftly to the alley´s entrance. Inside, five youths surrounded a well-dressed trader. Quietly, pressing himself towards the wall, he moved closer, putting his feet down in time with the constant drip of water from a damaged on-high cooling system. After a few seconds, he was close enough to make out more clearly what they were saying&#8230;
</p>

<p>
  &#8220;Well, what do we have here,&#8221; one, taller than the rest and blad, was saying, pacing around, &#8220;a fat, rich trader who doesn´t give a shit about us poor folks who have to make the stuff he so cheerfully buys and sells to pad his own pocket&#8221;
</p>

<p>
  &#8220;I.. I..&#8221;
</p>

<p>
  &#8220;Yeah,&#8221; chimed in another, shorter one wearing a bandana, &#8220;I think that he should distribute the wealth a little&#8221;
</p>

<p>
  &#8220;Yeah&#8221;, nodded the third.
</p>

<p>
  &#8220;I.. I won´t give you any money&#8230; I work hard for this..&#8221;
</p>

<p>
  &#8220;Yeah yeah, fat man, spare me the tears, I ain´t beggin´, I´m tellin´ you what you´re gonna do.&#8221; The taller one punctuated his sentance with a swift blow to the trader´s stomach. He doubled up with a grunt, and his breathing became heavier.
</p>

<p>
  As he fell to the floor the others in the group started kicking him around.
</p>

<p>
  <em>Time to intervene,</em> Arouin thought.
</p>

<p>
  Stepping into the light and aiming his pistol at the largest, evidently the leader, he cleared his throat, slightly louder than was strictly necessary.
</p>

<p>
  &#8220;So boys and girls, what are we doing here?&#8221;
</p>

<p>
  &#8220;What the fuck does it have to do with you?&#8221;
</p>

<p>
  &#8220;Naughty, naughty, don´t swear.&#8221;
</p>

<p>
  Arouin brought the gun up level with the youth´s head and pulled the trigger. The blast in the narrow alleyway was deafening, and blood showered down on the man on the floor. The half-headed corpse fell over backwards slowly, making a meaty thud as it hit the floor.
</p>

<p>
  &#8220;I suggest you leave, kiddies, the party is most definately over&#8221;
</p>

<p>
  There was a scramble as the remaining four fought to get away from the over-armed figure, one of them retching as he did so. Arouin leant down and grabbed the trader by the hand, pulling him upright.
</p>

<p>
  &#8220;You okay?&#8221;
</p>

<p>
  The larger man was still breathing heavily, and was obviously still in pain.
</p>

<p>
  &#8220;I´ll&#8230; be okay&#8230; thank you, thank you very much&#8230;&#8221;
</p>

<p>
  &#8220;No problem. Always glad to help.&#8221;
</p>

<p>
  Arouin was looking impatient.
</p>

<p>
  &#8220;I&#8230; had better be on my way&#8230; I was&#8230; on my way to&#8230; an important conference&#8230;&#8221;
</p>

<p>
  Arouin was looking more impatient.
</p>

<p>
  &#8220;Excuse me&#8230; no reward?&#8221;
</p>

<p>
  &#8220;No&#8230; you did a good deed that should be enough in itself, surely?&#8221;
</p>

<p>
  <em>How naive</em>
</p>

<p>
  A split second later the over-sized firearm´s barrel was stuck in the trader´s mouth.
</p>

<p>
  &#8220;I disagree, a reward is compulsory.&#8221;
</p>

<p>
  Holding the gun in the other man´s mouth with one hand, Arouin went through his pockets with another. The trader was sweating profusely, red in the face and very wide eyed.
</p>

<p>
  &#8220;Oooo, look,&#8221; Arouin held up a wadge of cash in front of the man, &#8220;that´s about twenty thousand credits&#8230; that´s very, very kind of you.&#8221;
</p>

<p>
  He grinned, then hit the trader on the side of the head with the butt of his gun. Stuffing the money into his inside jacket pocket, he moved back into the main asile, holstered his pistol, straightened his clothes, and headed for the hangar&#8230;
</p>

