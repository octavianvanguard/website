---
banner: "css/images/banner.png"
title: Betrayal
author: fellblade
layout: page
date: 2018-04-07

---
> The sleek flying wing of the Raven light fighter cut through space, green drive flares blazing out into space and leaving a trail of ionised gasses in it�s wake. The pilot inside checked his radar for what seemed like the fiftieth time and, having assured himself that there was still no-one in range, he turned back and carried on with his previous task&#8230;

 * [Betrayal - Parts I-X]({{< relref "betrayal-1.md" >}})
 * [Betrayal - Parts XI-XX]({{< relref "betrayal-2.md" >}})
 * [Betrayal - Parts XXI-XXVIII]({{< relref "betrayal-3.md" >}})
 * [Betrayal - Parts XXIX-END]({{< relref "betrayal-4.md" >}})

Copyright [Fellblade](http://www.fellblade.net/). First published on Planet Jumpgate and republished here with permission from the author. 
A newer edition of &#8220;Betrayal&#8221; is available from [Fellblade.Net](http://www.fellblade.net/)</a>.
