---
banner: "css/images/banner.png"
title: Parts XXIX – END
author: fellblade
layout: page
date: 2018-04-07

---
## Part XXIX

*The journey to The Stith where the stealth-scout picked them up and led them to the hidden jumpgate was fairly uneventful, although long. Most pilots were either staying docked to avoid the cross-fire or were in the fighting themselves, so there was little traffic to notice them and their hurried journey into unregulated space. There had been little conversation along the way, Arouin and Yyanis both lost in thought, Arouin maintaining a steady gaze out of the cockpit and into the depths of space beyond.*

*They jumped through into the hidden sector with it´s secret TRI base, the glow from the jump fading as Arouin smoothly accelerated the Phoenix out of the gate, then swung it around to head for the station. His eyes flickered away from the HUD, looking out across the void, and he lost his breath.*

&#8220;What the fuck are those?&#8221; he said, eyes wide. Yyanis had been lying down on the floor, trying to get some rest. She pulled herself to her feet, rubbed her eyes, then walked to the front of the cockpit, yawning with her hand to her mouth as she looked out. She froze, mid-yawn.

&#8220;I don´t have a clue&#8230; I´ve never seen ships like them before.&#8221; she said, hand slowly falling back to her side. One of the ships sped over to them, trailing three drive trails, and blazed past them with a flash of darkness. As they flew slowly towards the station, it doubled-back and flew up alongside them, holding formation, and they got a good look at it. Matte-black painted, all angles, no curves, a faint shimmer where there might have been plexi-glass for a cockpit canopy. It was a double-pronged sliver of darkness, meanacing, shaped like a delta-wing, with two leading points, vicious-looking.

&#8220;I want one.&#8221; murmured Arouin, staring sideways out of the cockpit at the object of his desire.

&#8220;Arouin?&#8221; said Yyanis, tapping him on the shoulder.

&#8220;What?&#8221; he asked, somewhat dreamily.

&#8220;We´re going to crash into the station unless you do something about it.&#8221; she pointed out. Arouin snapped out of it, turned, and sat back down at the pilot´s chair. The dark spacecraft peeled off, three drive trails flaring brightly as it headed away from them.

&#8220;This is the _Lithe Shadow_ to docking control, requesting clearance to land.&#8221;

&#8220;This is docking control, you´re cleared. Welcome back, _Lithe Shadow_&#8221;

The docking rings sprang into life as they coasted towards the station. Arouin bursted the thrust a few times to steady them, then pushed the throttle gently forwards, easing them into the darkness.

The Director was waiting for them as the lift brought them up into the hangar bay, standing perfectly still amongst a group of heavily-armed security guards who looked around the area with unease. Arouin hit the canopy release before the lift had stopped moving, then climbed down the ladder quickly, giving Yyanis a hand as she took the last step down to the floor, bundle under one arm.

&#8220;You have it?&#8221; said the Director, smiling.

&#8220;We have it&#8221; said Arouin, nodding. Yyanis walked forward and passed the bundle to the Director, who took off the jacket which it was wrapped in and dropped it distastefully on the floor before gazing, eyes seemingly alight, at his new prize. Yyanis took a step forward and picked her jacket off of the floor, annoyed.

&#8220;I am indebted to you. We will organise your respective payment methods over the next few days, during which you are invited to stay onboard the station as our guests. We will, of course, also need to debrief you, lay out what you cannot say about your time doing work for&#8230; TRI.&#8221; Yyanis nodded, Arouin remained silent for a moment before speaking up,

&#8220;What are those ships out there?&#8221; The Director looked uncomfortable for a moment before replying,

&#8220;I´m sorry that´s classified information. Need-to-know basis. And you don´t need to know.&#8221;

Arouin looked pained.

&#8220;Can I buy one?&#8221; The Director laughed,

&#8220;I´m afraid not, Mr Simmel, no matter how much you wish for it to happen it´s not going to, sorry to shatter any illusions you may have been harbouring about receiving one of those spacecraft in payment.&#8221; The Director turned and walked away, followed by his security contingent. After a few moments, one of the men who had stayed behind walked up to them and nodded to each in turn.

&#8220;If you will follow me, I´ll show you to your quarters.&#8221;

&#8220;Yes, of course&#8221; said Yyanis, smiling. Arouin grunted his approval then followed the man as he walked away. Yyanis fell into step beside him.

&#8220;You don´t seem too happy&#8221; she remarked.

&#8220;Why should I?&#8221; asked Arouin, glumly, &#8220;Back to being hunted after a few days of operations, no doubt. There´s a ship out there which can beat mine into a bloody pulp by the looks of it, at least. It´s been exciting, made a change from my normal way of life, doing something for good instead of just myself. Meeting new people.&#8221; He smiled at Yyanis, then looked away slowly.

&#8220;You don´t have to stop ´meeting new people´, you know&#8221; she said as they walked through the corridors, a smile on her lips.

&#8220;Sooner or later I´m going to get killed. By what or who I don´t know, but sooner or later I will. I´ve gone too far along this road now, I can´t just turn back. Even if I stop my various illegal activities, there are some people who won´t care that I no longer have a criminal record. They want me dead, it just so happened that before the law was on their side. Now it´s against them, but that won´t stop them, I´ve made too many enemies.&#8221; Their guide stopped in front of a pair of doors.

&#8220;Here are your rooms.&#8221; he said in a neutral tone of voice.

&#8220;Thanks&#8221; said Arouin, nodding to the man and opening the door in front of him. &#8220;I need sleep. Lots of sleep. I´ll&#8230; see you later.&#8221; He walked inside, sat down on the bed, and took off his boots, setting them on the floor, then swung his legs up onto the matress. Arouin closed his eyes, and let himself drift into unconcious bliss.

Arouin awoke and lay there in comforting darkness for a few moments, eyes shut against the world. An electronic chime from the door sounded, faint and insignificant in his still-fading dreamscape of dark nothingness. It sounded again, and Arouin opened his eyes slowly, seeing the ceiling, ghostly white, barely iluminated by the dimmed lighting in the room. He slowly rolled himself out of the bed and onto his feet, then padded softly over to the door where he rubbed his eyes with one hand before opening it. Yyanis was stood there.

&#8220;They thought it was about time to wake you up, they were going to send someone else, but I figured I should do it.&#8221; she said, by way of explanation. Arouin ran a hand through his hair then rubbed his face again.

&#8220;A good choice. I feel lousy. Anyone else and I might have bitten their head off, verbally or otherwise.&#8221; he said, chuckling quietly to himself, &#8220;How long have I been out for?&#8221;

&#8220;Thirty nine hours&#8221; said Yyanis, with a wry smile. Arouin yawned, putting the back of his hand to his mouth.

&#8220;Thirty nine hours? And they didn´t drug me or anything?&#8221;

&#8220;Apparently not&#8221;

&#8220;Wow.&#8221; he said, looking impressed, &#8220;That´s pretty damned good for me.&#8221;

&#8220;Making up for lost time I think.&#8221;

&#8220;I guess so&#8221; he said, nodding, &#8220;what have we got in store for us today? Whenever today might be, that is.&#8221;

&#8220;The Director wants to debrief us, let us know when we´re getting our goodies.&#8221;

&#8220;Oh joy. Sounds like such fun. Well, I guess we´d better move.&#8221;

Arouin stepped outside the door, brushing past Yyanis, and closed it behind him before stopping and turning around again. He opened the door, stepped inside, then put on his boots.

&#8220;That´s better&#8221; he said to Yyanis, who took his arm and started off down the corridor. A couple of security guards appeared behind them and kept pace.

&#8220;What´s with the shadow?&#8221; he whispered in her ear.

&#8220;I guess there are some things they don´t want us seeing&#8221; she suggested as they rounded the last corner and stopped in front of the door into The Director´s office. Arouin knocked smartly on the door, then took a step back until the reply came from within,

&#8220;Enter.&#8221; The Director looked up from an electronic clipboard, &#8220;Ah, Mr Simmel and the delightful Ms Tenan, I trust you are both well?&#8221; The grey-haired man gestured to the two chairs on the side of the desk nearest to them, &#8220;Please, be seated. Now, you have obtained for me the three items of past and present technology that I have requested. You have had a few ups and downs, but nothing that hasn´t stopped you completing your mission, obviously. And now to payment. Ms Tenan, we agreed that we would remove the explosive charge that was implanted inside your skull by your previous employers. The operation will take place in twelve hours time, and should last about three hours. After the operation you will be confined to your bed for a period of twenty-four hours. I hope this satisfies you.&#8221; Yyanis nodded, smiling, the Director continued, &#8220;Mr Simmel, we agreed to wipe the TRI database of your criminal record which does, of course, include the many infractions of common TRI law that you made during these covert missions. In addition you will take the _Lithe Shadow_ Phoenix-class Octavian Fighter with you.&#8221; Arouin nodded as well, abeit with slightly less enthusiasm than Yyanis had. &#8220;Finally, you must be aware that these missions were covert. TRI must not be connected with them, you must not speak of this station, or the sector that it resides in. Anything you did, or saw happen during your stay here or during your mission at the other stations is highly-classified and must not be mentioned to anyone except for myself. Do not try to contact me after we break contact, if I need contact with you I will do so through my own channels. Thank you for your assistance, the research is already under way, we already have the prototype system up and running.&#8221; he finished with a smile.. &#8220;Now, if you don´t mind, I have work to do.&#8221;

Arouin and Yyanis stood and walked out of the office, Yyanis closing the door behind them.

&#8220;Heavy stuff&#8230;&#8221; muttered Arouin as they walked away.

&#8220;Yes&#8230; but I get my operation in twelve hours&#8221; she said, smiling. Arouin looked back at her, grinning,

&#8220;Free at last, hey?&#8221;

&#8220;Yeah, I suppose so.&#8221;

&#8220;The Director´s quite a guy, you know&#8230; the only time I´ve actually seen him happy is when he got his hands on that positron coupling. I wish I knew more about him&#8230;&#8221; said Arouin, musing quietly. Yyanis looked at him, surprised,

&#8220;You didn´t check out any information on him before you took the job?&#8221;

&#8220;I was strapped to a bed surrounded by armed guards and if I _didn´t_ take the job I was going to be shot.&#8221;

&#8220;Oh,&#8221; said Yyanis, looking slightly put out, &#8220;okay. Well, I had the leisure of a slightly more relaxed recruitment, and I did quite a bit of research into him.&#8221;

&#8220;And?&#8221; asked Arouin as they walked into a cafeteria.

&#8220;It was hard to find much on him, mainly bits from old news transmissions, databases and the like. Probably the most interesting thing about him is that he´s from Hyperial.&#8221; At this Arouin turned around to stare at her, eyes wide.

&#8220;Hyperial?!&#8221; he said in surprise, pausing half-way through sitting down at a bench.

&#8220;So it seems. Since the war between Quantar and Hyperial, the Hyperions have had little or no spacefaring activity, while remaining fairly advanced. The war´s over now, of course, and they needed someone to head up TRI. It was thought that since a Hyperion would have no factionalist tendancies, since there weren´t any of his faction´s ships in space, so they would be fair to all the other factions. Obviously they might be a little covertly hostile to the Quantar, but nothing that couldn´t be spotted and easily patched over.&#8221; Arouin rested his elbows on the table and put one hand to each temple, slumping forwards slightly, lost in thought.

&#8220;Yyanis&#8230; could you do something for me?&#8221; Yyanis looked at him dubiously.

&#8220;I guess so,&#8221; she said cautiously, smiling slightly, &#8220;what?&#8221;

&#8220;I need you to try and get access to the station´s files and find out what those ships are.&#8221;

&#8220;Oh.&#8221; she said, deflating slightly, &#8220;Okay, I guess so.&#8221;

&#8220;Want something to eat?&#8221; asked Arouin.

&#8220;Sure, just get me whatever you´re having.

&#8220;Soy-beef soup sound okay to you?&#8221;

&#8220;Yeah I guess so.&#8221;

Arouin walked over to the vending machine, then returned a few moments later with their meals, which they sat and ate together in silence.

A couple of hours later, they were sat in Yyanis´ quarters, looking at her terminal screen as her hands flew over the keyboard.

&#8220;Have you got any idea what we´re looking for?&#8221; she asked.

&#8220;Not really&#8230; check anything you can relating to hangar bay operations, refueling, I don´t know&#8230; something that would store ID codes or ship specifications. I want to find out what that ship can do&#8230; I might have to fight one someday.&#8221; Lists of files and directories scrolled past the screen, illuminating Yyanis´ face in the dimmed lighting of the room.

&#8220;Hmm&#8230; they´ve got an automated factory set up here.&#8221;

&#8220;What´s it making?&#8221;

&#8220;What´s that got to do with the ships?&#8221;

&#8220;Call it natural inquisitiveness&#8221; said Arouin, grinning. Yyanis´ hands tapped out a sequence of keys, and a batch production listing came up on the screen.

&#8220;´X-Type Power System´, got five hundred counts. Nothing else.&#8221; she shrugged.

&#8220;The new system&#8230; the one we retrieved the technology for?&#8221; mused Arouin.

&#8220;Probably right&#8230; they must have worked like demons. The batch started a day ago&#8230; so yeah, it could be the new system.&#8221;

&#8220;Is it linked with any others files?&#8221;

&#8220;Let´s see&#8230; there´s a file for the X-Type specs, I´ll pull that up.&#8221; There as a pause as the data loaded to the terminal, then Yyanis nodded, &#8220;Very good guess, dead on. You can see from the blueprints, there are the three pieces of tech that we picked up.&#8221; She pointed to each in turn on the screen..

&#8220;Why only five hundred?&#8221; asked Arouin, to no-one in particular. Yyanis shrugged,

&#8220;I don´t know, but you wanted to find out about those ships. I´ll take a look in the hangar bay log files. Hangar bay one or two?&#8221; Arouin looked up in alarm,

&#8220;There´s a hangar bay _two_?&#8221;

&#8220;I tell no lies, look for yourself.&#8221;

&#8220;Hangar bay two then, I guess.&#8221; As Yyanis´ hands flickered over the keyboard, there was a knock at the door.

&#8220;Go away.&#8221; said Arouin loudly, still staring at the screen.

&#8220;There´s four hundred and eighty-three ships docked there!&#8221; she said in amazement, &#8220;It must be huge!&#8221;

The knock at the door came again, considerably louder.

&#8220;**Go. Away.**&#8221; said Arouin, louder.

&#8220;Open the door now, damnit.&#8221; came a muffled reply. Yyanis was staring at the screen, opening another file.

&#8220;They´re&#8230; they´re&#8230;&#8221;

&#8220;What, damnit?&#8221; asked Arouin, still looking at the door.

&#8220;Hyperial, Xavier-class Heavy Interceptors. Five hundred of them are currently stationed here.&#8221; she lapsed into silence. The knocking at the door stopped, then came a thump on it as someone hit it hard. And again.

&#8220;Five hundred&#8230;&#8221; breathed Arouin, looking down at Yyanis, &#8220;&#8230;Hyperial Ships&#8230;&#8221;

&#8220;&#8230;with a Hyperion as director of TRI&#8230;&#8221; she gulped, &#8220;&#8230;a fleet that no-one knows about&#8230;&#8221;

The hammering at the door intensified, the metal buckled slightly.

&#8220;&#8230;equipped with brand new power plants that are god-knows how many times more powerful than anything before, and smaller to boot&#8230; &#8230;more shields&#8230;. &#8230;more guns&#8230; &#8230;more engine power&#8230;&#8221; said Arouin, mouth agape.

The door buckled further.

&#8220;&#8230;and after two hundred years of playing second fiddle&#8230; &#8230;they might just feel it´s time for them to take the limelight.&#8221;

*&#8220;<strong>Ladies and Gentlemen!</strong>&#8221; cried Arouin theatrically, springing to his feet and pulling out a pair of <em>Bullroarers</em> from his trenchcoat as a pair of shots were fired through the door and it was kicked in, &#8220;<strong>We are officially in shit!</strong>&#8220;*

## Part XXX

Arouin didn´t hesitate for a moment longer, the gun blazed in his hand and punched gaping holes in the metal and plastic, light from the corridor outside streaming them into the darkened room within. There was a burst of automatic fire from outside and a line of bullet-holes stitched themselves along the wall, Arouin dropping to one knee as the blind firing from outside went high, sending bullets screaming through the air right above his head.

&#8220;Get out of here!&#8221; he shouted at Yyanis, face a grimace as he carried on pounding at the trigger, the muzzle flashes lighting up his features with a firey yellow glow. Yyanis looked around desperately for a moment searching for anything that could help them, before her eyes travelled upwards and came to rest on one of the ceiling panels. Arouin fired another two rounds into the door then scrambled on his hands and knees over the the desk where Yyanis was sheltering, dropping a clip out of his pistol and quickly replacing it as he did so.

&#8220;We can´t stay in here,&#8221; he said, the click of the new clip slamming home punctuating the sentance, &#8220;they can just throw a grneade in or something and we´re dead and,&#8221; Arouin popped up and took a quick look over the desk, &#8220;I can´t get us out of here with brute force.&#8221;

&#8220;The ceiling, look&#8221;, said Yyanis, pointing up at the roof. Arouin looked up at the panelling above, then back down at Yyanis.

&#8220;I´ll keep them busy, you get up there and get ready to help me up.&#8221; he said, looking unsettled. Yyanis nodded quickly. &#8220;Three, two, one&#8230;&#8221;

Arouin lept to his feet, his other hand drawing out another pistol, then moved across the width of the room, guns alternately belching flame and shaking from the recoil, gunshots ringing out and mingling with the scream of someone outside the room. As he ran across the room a pair of assault rifles opened up outside and the lines of bullet-holes rushed across the wall as he moved, trying to catch up with his fleeting form. Yyanis jumped up onto the desk and struck out at the ceiling panel above her as Arouin slammed boldily into the far wall, fired another couple of shots from his pistols as the lines of bullet-holes closed the distance to him, then flung himself into a roll under the twinned streams of death that flew over his head. Yyanis hoisted herself up into the pitch black space above the ceiling as Arouin crawled the remaining few paces to the desk, the roar from outside not lessening as the pair of assault rifles carried on with a steady barrage of ammunition at the point which Arouin had last shot from. Larger bits of the wall were being flung into the room as the bullets tore into it, then the pieces were hit again as they fell sending them jinking around as sucessive bullets smashed into them, light streaming through the increasing number of holes that they left. Arouin lept onto the desk, then jumped up and caught hold of the edge of the hole before hauling himself up into the space above with Yyanis´ help. It was hot and dark, the ceiling panels visible only as glowing outlines where light was let through the thinner edges of the panels and imperfect joins. The corridors were clearly visible from above, the tops of them poking up through the ceiling to make miniature walls about half a meter high; the result was a low-height carbon copy of the floor below. Yyanis was standing balanced on a pair of struts which kept the ceiling panels up, holding onto a metal pipe that ran overhead.

&#8220;Are these safe to walk on?&#8221; asked Arouin, quietly, pointing at the ceiling tiles.

&#8220;I don´t actually know&#8221; said Yyanis, looking dubiously at them, &#8220;I haven´t tried yet.&#8221; There was a shout from below and a crash as the door was kicked off of it´s hinges.

&#8220;Now seems like a good time to find out&#8221; said Arouin, grabbing Yyanis firmly by the arm and starting to run across the ceiling tiles, which buckled as their feet hit them, but held. They ran through the hot darkness, making low, fast jumps over the tops of the walls, ducking under the occasional pipe and girder, until Arouin stopped suddenly. Yyanis almost fell over, but regained her footing, and stood there breathing heavily, exhasted. She brushed some sweat-matted hair out of her eyes before looking at Arouin.

&#8220;What now?&#8221;

&#8220;Oh nothing much,&#8221; said Arouin, checking the clips in each of the pistols he was holding, &#8220;we´re just going to pay someone a visit.&#8221;

He pointed one pistol at the ceiling panel he was standing on, and pulled the trigger twice. The panel disintegrated and Arouin dropped through the gap, landing heavily on the floor in the room below, bits of panelling cascading down around him. He sprang up from the crouch he had dropped into, and span, a gun in each hand, to face the grey-haired man behind the desk. The Director sat there, alone in his office, looking defenceless, yet unafraid. He was smiling slightly, in a distant way, and held Arouin´s gaze a moment before saying anything.

&#8220;I don´t know what you hope to achieve by coming here.&#8221; said a quiet, tired voice, &#8220;it´s not as if you can make any difference any more.&#8221;

Arouin remained impassive, staring him down, taking in the view on a pair of raised terminal screens, security forces running through the corridors that they had run over the top of, guns still trained on the older man

&#8220;You´ve played me for a fool&#8230;&#8221; said Arouin, shaking his head slowly, pistols raised, eyebrows knotted.

&#8220;It was necessary,&#8221; said the Director, in a not unkind tone, &#8220;I didn´t enjoy it, but you were the only man for the job. I needed you, and I don´t think anything could have convinced you to help me if I hadn´t tricked you.&#8221;

&#8220;And what about everyone else?&#8221; said Arouin, gesturing around with a pistol, &#8220;what about everyone else on this damn station?&#8221;

&#8220;Quite a few of them are in on the plot,&#8221; said the Director in a neutral tone, smiling slightly, &#8220;the rest, well, they will have to co-operate.&#8221;

Arouin shook his head, and slowly raised his right hand, knuckles white around the cold metal of the pistols.

&#8220;It won´t help.&#8221; said the Director, patiently, &#8220;You might as well accept what´s going to happen, it´s not as if you can stop it. There are five hundred ships with the new power system docked in the hangars, nothing can stand up against that, not even you&#8230;&#8221; the Director smiled thinly at this, then continued, &#8220;Aren´t you interested in the larger picture, don´t you want to know what´s going to happen next? We´ve got untold power, Hyperial can unite TRI like nothing else has managed to&#8230; You must admit that the whole plan is far in excess of anything that´s ever been attempted before, you must, at the very least, want to know the details of the scheme.&#8221;

&#8220;Not really, I think I know enough.&#8221;

The blast from the pistol sent the Director and the chair he was sitting on careening back into the wall behind him, where it bounced off and toppled over onto one side. The Director looked down in surprise at the red stain spreading over the front of his shirt, touched it with his finger, then drew it away quickly, as though the pain from the wound was nothing, yet the sensation of the blood on his fingers was a burning agony. A pair of booted feet slowly made their way across this horizontal vision of the floor, and he looked up along them, up to the face of an angered man.

&#8220;You´re not supposed&#8230; to do that&#8230;&#8221; he managed, an incredulous look on his face as he tried to push himself up off the debris-strewn carpet.

&#8220;If you think I´m going to sit around all day why you cackle madly over the fine details of your plot for galaxy-wide domination, you´re sorely mistaken.&#8221; Arouin said grimly, bringing the gun to bear on the Director´s forehead, &#8220;I´ve got work to do.&#8221;

The crack of the gunshot rang out, echoed quietly, there was a barely-audible thump as the body hit the floor for one last time, then silence flooded back into the room.

*Arouin turned and looked up into the space above the ceiling at Yyanis´ white face.*

&#8220;Let´s go&#8221;

Yyanis nodded slowly, then lowered herself down from the ceiling above.

&#8220;Where first?&#8221; she asked

There was a knock at the door, and a moment´s pause, then Arouin´s hand whipped back up and put three rounds through the metal and plastic panelling. There was a split-second of silence, then the tinkle of the cartridge cases hitting the floor and the thud of a body hitting the ground outside the door. Arouin walked over to it, and kicked the door open, a sickening fleshy noise coming from the body of the security guard who had been outside the door as it was crushed against the wall. Arouin strode out of the door without giving it a second glance, but Yyanis gave the cooling body a wide berth as she walked by.

*&#8220;We have a few of our own objectives now,&#8221; explained Arouin, striding forcefully down the corridor, one pistol held out in front of him as he walked, &#8220;we need to take out 500 fighters of a completely unknown type and power, then get the hell out of here, preferably alive.&#8221; They went around a corner in the corridor and came face to face with a pair of running security guards, Arouin barely blinking as he pumped at the trigger, sending the two men sprawling backwards to the floor screaming, then walked past them without breaking his stride.*

&#8220;Any suggestions?&#8221; he asked as they approached a T-junction and the sound of running feet approached.

&#8220;Uh, none right now&#8221; said Yyanis, drawing a pistol from her pocket. Arouin dropped to one knee as the first security guard sprinted around the corner, his eyes widening when he saw them, then he tried to slow down and bring his weapon to bear, but too late. Arouin´s pistols barked, flame gouting from their barrels, and the guard jerked spasmodically as the bullets tore through him before succumbing to gravity and falling to the floor. Yyanis heard something behind them and span suddenly, just in time to see two guards coming around the corner of the corridor.

&#8220;Behind us!&#8221; she shouted, bringing the pistol to bear and opening fire. One of the guards dropped to the floor just in time to avoid the first shot, but his colleague behind him took the bullet in the face, sending him sprawling into the wall behind him. Yyanis shifted her aim downwards and dropped into a crouch as a burst of automatic rifle fire zipped over her shoulder, squeezing off three quick shots which tore into the torso of the man, spinning him like a top before he hit the floor. Arouin was still firing as more guards came around the corner, firing their weapons, the heat and smoke in the corridor building up rapidly and the clamour of battle ringing down the hallway. Through the haze Arouin saw a rifle swing around the corner, the owner out of sight. He reached back to Yyanis who was franticly reloading, and shouted a warning,

&#8220;**Get down!**&#8220;, he screamed, pushing her out the way before diving forward as the rifle opened up. Bullets rained down the corrdior, hitting the metal walls and sending sparks flying off them which arced through the air and rained down on Yyanis´ prone form. Arouin hit the ground hard and slid along the corridor floor, slick with blood, into the corridor at the end of the T-junction. The guard blind-firing the assault rifle looked down with surprise as Arouin slid into view, then was blown up into the air by a flurry of rounds from the blazing akimbo pistols. Another guard appeared through the haze from the other side, looked down to see Arouin lying on the floor, mid-reload, and brought his rifle up, only for Yyanis´ blazing pistol to send bullets smashing through him, shaking his body and spraying blood on the wall.

&#8220;Nice one,&#8221; said Arouin, rolling back onto his feet and wiping a fleck of someone else´s blood from his brow, then looking around warily. He finished reloading, then prodded one of the nearby bodies with one foot. It rolled over, revealing the glassy stare of a body with no soul left in it, bereft of life, then rolled back when Arouin took his foot away. Yyanis was flat against one side of the corridor, back against the wall, looking down the length of the hallway, pistol held tightly in one hand and breathing heavily.

&#8220;I don´t think I can take much more of this&#8221; she said, glancing up and down the hallway again. Arouin, sweat dripping from his brow, looked at her through the haze,

&#8220;It´s not a question of what you can and cannot take, it´s a simple question of whether we´re going to survive this or not.&#8221;

&#8220;I just want to get out of here&#8221; she said, shaking her head slowly.

&#8220;Until those ships are destroyed,&#8221; said Arouin, starting to walk down the corridor again, &#8220;we aren´t going anywhere.&#8221;

&#8220;How _are_ you going to destroy five hundred ships?&#8221; said Yyanis, still feeling jumpy, glancing nervously from side to side.

## 

&#8220;I´m not to sure on that point myself,&#8221; he admitted, &#8220;I´m hoping something will present itself.&#8221;

&#8220;So&#8230; you´re _relying_ on good luck?&#8221; she said incredulously as they took a turn into the corridor which led to the second hangar.

&#8220;Hasn´t let me down so far, has it?&#8221; he replied, closing the distance to the door into the hangar with purposful strides.

&#8220;Luck runs out.&#8221;

&#8220;Sometimes you have to make your own luck.&#8221;

&#8220;Doesn´t sound like an exact science to me, not something I´d like to stake my life on.&#8221;

Arouin stopped just before the door, and turned to face Yyanis.

&#8220;It´s not like we have a choice&#8230; how do you fancy your chances trying to outrun five hundred ships?&#8221;

&#8220;I suppose so, it´s just that sometimes, just occasionally, it´s nice to maintain the illusion that we´re working to some kind of plan here.&#8221;

&#8220;Never mind eh?&#8221; he said, turning back to the door, pushing it open, and walking through into the hangar. Arouin stood still and gazed around the cavernous space, ceiling veiled in darkness, the floor a stark grey, and the ships, stretching off into the distance, in neat, eye-straining rows. Black, venomous, deadly, they looked although they were the essence of speed and destruction condensed into material form. Yyanis walked up behind Arouin and put one hand on his shoulder, the other still holding her pistol.

&#8220;You´re busy wondering if they´ve got a self-destruct mechanism?&#8221; she said, airily.

&#8220;Uh&#8230; something like that.&#8221;

&#8220;I think not.&#8221;

&#8220;Neither do I,&#8221; he admitted, &#8220;but I do have an idea.&#8221;

&#8220;Is it a good idea?&#8221;

&#8220;No, it´s a stupid idea.&#8221; he said, gazing out over the rows of gleaming black fighters.

&#8220;But it might just work?&#8221;

&#8220;Yeah&#8230;&#8221; he said, &#8220;it´d better&#8230;&#8221;

## Part XXXI

Arouin ran. The avenue of glistening black fighters stretched away ahead of him, gleaming where the widely spaced overhead lights illuminated them, reflected light contrasting sharply with the darkness around. Lights flashed past above him as he ran, Yyanis following, feet hammering on the hangar floor.

&#8220;Any hints on what this idea is?&#8221; she managed between laboured breaths.

&#8220;We´re headed&#8230; for Docking&#8230; Control&#8230; for&#8230; this&#8230; hangar bay.&#8221; he said slowly, pausing for breath every couple of words. As the wall ahead of them neared, details revealed themselves in the half-light of the gloomy hangar, rivets studding the wall, fire extinguisers, safety signs, a door&#8230; The door slowly swung away from the wall, light trickling out through the narrow crack, becoming a cascade of illumination as the door opened fully. The shadow silhouetted resolved into a uniformed figure, passing through the portal, turning to close the door behind him, hearing the footsteps. He turned.

&#8220;Who´s th&#8230;&#8221; he managed before the crack of Arouin´s pistol sounded out across the hangar, echoing and reverberating amongst the insectile hulks of the black spacecraft. The man fell backwards, Arouin sprinting over his falling body without breaking his stride, crashing through the door and into the room beyond. Yyanis lept over the man´s corpse and looked up to see Arouin swing left, then right. There was a scream, Arouin´s right hand tracked a movement that was out of sight, then the pistol kicked in his hand once, twice, three times. Another movement, another brief blaze of fire as Arouin emptied the rest of the clip. Yyanis ran past him as he dropped to one knee and reloaded his pistol, pulling another one out of a pocket and repeating the process. Gun in hand, movements cautious and near-silent, she crept crab-wise into the next room, not sparing a glance for the blood-stained wall behind the table where the two technicians had been eating their meal. She sprang through into the next room, dropped into a crouch, quickly swung right, left, then looked back to Arouin.

&#8220;It´s clear &#8211; there´s a stairway here&#8221; she said, tersely. Arouin walked through the doorway, glancing around and taking in the contents of the room; a few storage lockers, a vending machine and a rack of coat-hooks. Yyanis moved closer to the bottom of the stairs, pistol grasped in front of her with two hands, covering the top.

&#8220;That should be the way up to the control deck&#8221; said Arouin, nodding, &#8220;Shouldn´t be too many people up there &#8211; it´s not in an active state.&#8221;

Arouin walked past Yyanis, guns held out in front of him, her pistol trained over his shoulder. He took the stairs carefully, one step at a time, keeping his gaze fixed at the top of the stairs. At the last step he paused for a moment, then leapt through into the room beyond the doorway at the top. There was a pregnant pause, complete silence for a few seconds and Yyanis held her breath, waiting for the blasts of firearms, but none came. Arouin´s head appeared again at the top of the stairs. Yyanis let out her breath and her gun dropped slightly.

&#8220;It´s clear, come on up. Looks like there´s no-one here.&#8221;

&#8220;let´s hope it stays that way,&#8221; said Yyanis, &#8220;I´m running out of ammo if nothing else.&#8221;

Arouin stepped through into the control room, Yyanis running up the last few stairs to catch up. The room was dimly lit, the better to see the glowing multitude of screens covering three walls and a control console in front of the wide window looking out over the hangar bay that made up the fourth. Five swivel chairs were scattered in front of the control console, head-sets hooked over the seat-backs, consoles before them blinking. Arouin relaxed after a few more seconds and looked around at the monitors showing a vast array of figures, diagrams and graphs. Then he turned to Yyanis. Yyanis, who had been following his gaze, took a step backwards and held out a hand.

&#8220;Don´t look at me!&#8221; she said hastily, &#8220;I don´t know what that lot means.&#8221;

&#8220;I don´t care what it means, I just want you to launch the fighters.&#8221; replied Arouin, calmly.

&#8220;_What?!_&#8221; she said incredulously, but before Arouin had time to reply there was a scream from down below. Arouin turned to look at the stairs, then looked back at Yyanis. There was a pause of three or four seconds, then the alarm went off. The speakers situated in the corners of the room hummed into life, whined with feedback for a moment, then started blaring out,

&#8220;Hangar Bay Two is being sabotaged! All pilots and security personnel report to Hangar Bay Two!&#8221;

&#8220;Oh dear&#8221; murmured Arouin, putting one hand to his forehead. There was the sound of running feet; someone coming up the stairs. Yyanis looked at him, face a mask of horror.

&#8220;**All?!** Isn´t that a _tad_ excessive?!&#8221;

Arouin gave her a withering stare for a moment as one hand came up with a pistol in it, pointing at the doorway. The other pointed at one of the consoles.

&#8220;Just do it. We haven´t got time to argue.&#8221;

There was a sudden intake of breath from behind Arouin as he looked at Yyanis, and without looking he pumped at the trigger, bullets slamming into the worker who´d found the bodies and raised the alarm.

&#8220;Now.&#8221;

Yyanis paused for a second, there was a thump as the body behind Arouin hit the floor, out of sight. She looked at him.

&#8220;Okay,&#8221; she conceeded, &#8220;tell me the plan as I go.&#8221;

*Yyanis walked over to one of the consoles and sat herself down at it, peering intently at the slightly flickering screen.*

&#8220;Pretty simplistic,&#8221; she said as her fingers danced across the keyboard, Arouin standing silently behind her, gaze alternating from the doorway to the window. &#8220;Of course you want the system to be simple,&#8221; she commented after a few seconds, &#8220;It wouldn´t do to have anyone too smart around here for extended periods of time. They might go digging, find out a few uncomfortable home truths. Get a little too nosy&#8230;&#8221; Arouin wasn´t moving any more; wasn´t paying attention to Yyanis´ idle chatter, just stood, staring out of the window.

&#8220;We may have a problem&#8221; he said slowly and deliberately. Yyanis looked up from the command console and stared through the glass. In the distance men ran from entrances in the hangar wall towards the ships.

&#8220;Launch the ones furthest from us first,&#8221; Arouin said urgently, &#8220;and fast.&#8221;

The line of ships furthest from them sank into the hangar floor, lift doors closing over them, sealing them away from the pilots who ran to try and board their ships. Yyanis looked up brightly.

&#8220;Oh, I get it! You launch the ships&#8230;&#8221;

&#8220;&#8230;with no pilots in them, then destroy them at will as they have to take time to ship pilots out to them. That takes about an hour a ship.&#8221; Arouin finished for her, a satisfied tone in his voice.

&#8220;That long?&#8221; said Yyanis, looking up at him, surprised, hands still tapping away at the keys.

&#8220;Oh yes,&#8221; he said, noticable relish in his voice, &#8220;modern ships don´t have airlocks and things like that &#8211; just a seal around the escape pod. Absolute hell to get a pilot into a drifting ship, risky too.&#8221;

Yyanis looked out of the window as the men, ant-like in their distant movements, ran for the ships which disappeared before them.

&#8220;You have also, I take it, thought up some subtle and cunning ruse to get us out of the sitation we´re about to find ourselves in?&#8221; she said, after a few more moments. Arouin looked slightly uncomfortable.

&#8220;Not as such, no.&#8221; he admitted after a few seconds.

&#8220;Start thinking?&#8221;

Arouin was still staring out of the window.

&#8220;The ships aren´t launching fast enough &#8211; the system can get the ships ready for launch faster than the launching system is sending them into space&#8230; I´m going to try and buy us some time. We can´t let any of these get away, and some of the pilots´ll get to their ships if they carry on at the rate they are.&#8221;

Yyanis looked unsure.

&#8220;Uh, are you sure that´s absol&#8230;&#8221; she started, but Arouin was gone, feet thundering down the stairs. As she looked out of the window, fingers a blur as she send ships, pilotless, out into the void, she saw Arouin running out towards the nearing line of pilots.

The line of grey-fatigued pilots ran with all their might towards their ships which were disappearing before them, a row at a time. The ships, the pride of the rebuilt Hyperian Navy, slipped away from them just as they approached, but they gained ground, slowly, surely. Sweat ran in trickles down their anguished faces, screwed up with the effort of the continued strain. Feet blistered, boots blurred, breath laboured. The row before them vanished, revealing the row behind, which vanished, revealing the row behind, which vanished, revealing the row behind, which vanished, revealing a black-trenchcoat wearing man, akimbo pistols gripped in white-knuckled fists, eyes glinting in the grey light of the hangar.

Feet planted firmly apart, a sleek, black weapon in either hand, trenchcoat billowing behind him in the wind created by the vacuum of space greedily accepting the ships of his enemies, Arouin opened fire. His hands tracked across the width of the hangar, trigger fingers spasming rhythmically, guns blazing with fire and noise, cartridge cases fountaining up in a shower of metal. Pilots twitched, knocked this way and that as bullets slammed home, falling like sickening dominos of flesh and blood and bone, striking the ground and lying there, immobile, as their life leaked away from them. The guns ran dry, the scream and whine of bullets ceased, there was a moment´s silence as the stream of pilots stared around them at the carnage wrought by Arouin, their friends and comrades scattered on the hard ground before them, bleeding, screaming, dying. Arouin span, coat swirling around him, and broke into a run, dropping the clips out of the pistols, sprinting for all his worth towards the disappearing line of ships. Bullets whined past him as his feet pounded on the grey metal floor, driving him ever onward. Of the next line of ships to sink into the floor before him, one did not, a safe haven of darkness. Arouin ran past it, then squatted on it´s far side, back against the cold metal, gasping for breath.

&#8220;Thank you Yyanis&#8221; me muttered, relieved. After a few moments of rest he hauled himself to his feet once more and started running again, reloading his pistols as he went. There was a shout behind him of an unfriendly nature, and he redoubled his pace, heading straight for the door into docking control. The welcoming rectangle of light drew nearer, erratic weapon fire starting anew behind him, bullets striking the wall and floor before him. With a quick glance back, Arouin squeezed off a couple of shots and was rewarded with a scream of pain and one of the pilots collapsing to the floor, screaming, clutching at his leg. He turned back again to see a silhouette taking up most of the doorway, raising a gun in one hand, bringing it to bear&#8230;

&#8220;**Get out the way!**&#8221; screaned Yyanis, Arouin quickly zipping off to one side as the gun in her hand flamed, snapped around to cover another target, blazed again and again as bullets screamed out and ripped into Arouin´s pursuers, knocking some from their feet in sprays of blood. Arouin almost knocked Yyanis to the floor as he sprinted through the door, then snapped around and put his back to the wall, reloading the single pistol he had used as Yyanis continued firing out of the doorway.

&#8220;How many ships got away?&#8221; Arouin shouted at her over the bark of the pistol. It ran dry, the trigger clicking twice before Yyanis sprang out of the way, changed the clip, then ran for the stairs.

&#8220;Only a couple, come on!&#8221; she yelled, running up the stairs, &#8220;The good news is it doesnt matter&#8230;&#8221;

Arouin reached the top of the stairs and ran into the control room.

&#8220;What?&#8221; he said, puzzled and out of breath.

&#8220;After the first three rows the ships started colliding into the ones that had already been launched. They´re one big lump of twisted, cooling metal outside the launch tubes.&#8221;

Arouin smiled wordlessly and turned back to the stairs.

&#8220;Any work on the getting-out-of-here-alive front?&#8221; she asked, scathingly.

&#8220;Getting out, no. Staying alive, yes.&#8221; said Arouin, grabbing her by the arm and dragging her over to the console.

## Part XXXII

They moved swiftly with practiced co-ordination and grace, cautious yet confident, weapons held tightly, aim unshaking. The fire team moved up the stairs, leap frogging their positions; one man running forward as his partner covered him, the reversing the roles, the first man standing frozen, alert, as his buddy ran past. There was a pause, a silence laden with the promise of violence, then a short hand signal from the lead. Afer a brief flurry of movement a stun grenade ricocheted off the wall and into the control room. Light blazed out for a split second, white, intense, throwing the room into sharp relief, then a thump as the secondary explosive went off, deafening and disorientating those inside. A thunder of feet preceeded the fire team as they stormed into the room, weapons sweeping theeir assigned sectors with geometric prescision. But silence fell; no clatter of automatic weapons, no screams of the dying. The fire team stood stock still, eyes flickering around the room, looking for a trap, a hidden figure, a suggestion of movement. The room seemed untouched, uninhabited, five seats were all tucked underneath the overhanging control console, monitor command prompts blinking balefully in the darkness. Scattered over the floor were four small screws, dull in the grey, dim light that the monitors cast over the room. Then, in the corner of the room, a glimmer of reflected light danced over polished metal. Instantly, as one man, the fire team turned on the glint, guns raised. The leader made a quick gesture with one hand, and one of the other troops moved cautiously forward, one careful step after another. When he was a meter or two away he crouched down and leant forward for a better look.

&#8220;It´s a grate,&#8221; he said, voice seeming unnaturally loud in the hushed silence of the room, &#8220;they´re in the ventilation shafts.&#8221;

The leader of the team shook his head slightly and grimaced,

&#8220;Cunning bastards&#8230; Hoskins and Turunae, stay here, make sure they don´t double back. Everyone else with me, seal off all the exits then we´ll move in, closing off sections as we sweep them. Call in the medic teams to clear up this mess.&#8221;

And with that, all save two of the troops left the room&#8230;

&#8230;The medic teams arrived less than a minute after the radio call to tell them that the area was secure, moving amongst the dead and dying, sorting those who might be saved from those who would surely die. Over the next twenty minutes they moved across the hangar floor, until they finally reached docking control. One medic checked the body nearest to the door, fingers to the throat. Was that a pulse? There it was again&#8230;

## 

&#8220;We´ve got a live one! Give me a hand over here!&#8221; he shouted, standing and waving, Off to his right one of his comrades lept to her feet.

## 

&#8220;Another one here!&#8221; she yelled, looking around and then kneeling back down beside the young woman with the blonde hair as two pairs of men ran over, bringing stretchers with them&#8230;

&#8230;Hoskins lay on the floor, gun still trained on the vent, Turunae kneeling beside him, still, quiet. They stayed there for ten minutes before Turunae broke the silence.

## 

&#8220;What do you know about this guy then?&#8221;

## 

Hoskins shifted his weight around slightly before replying,

## 

&#8220;He took out the Director.&#8221;

## 

&#8220;Shit&#8230; why?&#8221; said Turunae, concern and surprise in his voice.

## 

&#8220;He wasn´t briefed on the&#8230; Hyperian aspect of the project.&#8221;

## 

&#8220;He wasn´t told? He wasn´t with us?&#8221;

## 

&#8220;Nope, all behind his back. They bought him off with some equipment and didn´t give him the full story&#8221;

## 

&#8220;&#8230;And when he went off the rails no-one tried to stop him?&#8221; asked Turunae, shocked.

## 

&#8220;You obviously haven´t glanced over this guy´s CV&#8230;&#8221;


&#8230;&#8221;Okay, we´ve sealed all of the vent exits wuth regular security detatchments,&#8221; said Jarren, the fire team leader, pacing the briefing room, &#8220;Now it´s up to us. We need to move through the vent system and flush them out. They´re heavily armed and very, _very_ dangerous, so we´ll be moving in pairs. We can´t afford to let him get away. We´ve already lost the existing ships because of his actions, and the Director´s been killed. It´s up to us to avenge his death and stop this madman from doing any more damage. Command says there´s another one hundred ships en route that they had been saving for follow-up operations. When they get here we´ll re-start production of the new power systems, and then proceed with the plan. You know your places, we have work to do.&#8221; He stood up, and looked over the assembled faces of his men, &#8220;Move out troops&#8230;&#8221;

&#8230;The medics ran through the corridors towards the med-lab, pusing the two injured base personnel on trollies in front of them.

## 

&#8220;He seems stable, no obvious blood loss or physical injuries save for a few bruises here and there. Get an oxygen mask on him anyway, won´t do him any harm, might do some good.&#8221;

## 

The medic that had been looking into the eyes of the blankly staring man ran ahead of the trolley and further down the corridor to the next one, where they woman lay under the white sheets.

## 

&#8220;What about her?&#8221; he enquired, a concerned expression on his face.

## 

&#8220;Stable, dilated pupils, again no obvious signs of physical injury. Probably psychological trauma.&#8221;

## 

As the woman stared, unfocused at the ceiling above, lights passed overhead, glaringly bright. An oxygen mask from somewhere out of sight moved across her line of vision and settled over her face. Back down the corridor a medic meant over the man in the trolley.

## 

&#8220;He´s snapping out of it!&#8221;

## 

The man´s eyes focused on the medic´s concerned face.

## 

&#8220;You are very lucky to be alive my friend.&#8221; said the medic, with a smile. The face of the man looking back up at him broke into a grin.

## 

&#8220;I guess I am&#8230;&#8221;

&#8220;&#8230;This is Droden in sector A5. No movement.&#8221; The trooper stared ahead, tense, listening for the replies of the rest of the fire team.

## 

&#8220;Pinero, sector A6. Nada.&#8221;

## 

&#8220;Chaz, backing up Pinero. Fuck all at A6, over.&#8221;

## 

&#8220;Murver, B9, wth Genin. We´ve got nothin´ here.&#8221;

## 

Droden moved forward a little more, bent almost double in the cramped vent. Behind him his partner, a sleight man by the name of Shelton, or ´Shorty´ to his friends, moved up as well.

## 

&#8220;Bet you´re loving this Shorty.&#8221; said the taller man, grinning.

## 

&#8220;Not small enough, Dro´&#8221; he said in a high-pitched, whiney voice, &#8220;I´m just short enough that I think I can stand up. Just tall enough to crack my head on the bloody ceiling.&#8221;

## 

Droden laughed to himself quietly, but stopped suddenly when his radio crackled into life.

## 

&#8220;This is Pinero, A7, we´ve got movement, we´ve got morement!&#8221;

## 

Droden gripped his weapon more tightly and started moving forwards&#8230;

&#8220;&#8230;and then he blasted his way out of there, took on three Typhoons by himself in a Phoenix.&#8221; finished Hoskins with relish. Turunae was wide-eyed.

## 

&#8220;And what about this woman he´s going around with?&#8221;

## 

&#8220;No-one seems to know her name&#8221;, said Hoskins with a grin, &#8220;but from the camera footage I´ve seen she´s a nice piece of work.&#8221;

## 

Turunae laughed heartily at this, but didn´t relax his unwavering aim on the enterance into the vent system.

## 

&#8220;Well, I really don´t think that taking her alive is going to be an option.&#8221; he said with a smile. Hoskins grinned a stupid grin,

## 

&#8220;You never know, she might try and get us to spare her if&#8230;&#8221;

## 

There was a metallick clunk from the vent, and both men shut up immediately, concentrating on the vent, their weapons, their breathing slowing, measured. Another clunking noise came from the vent. And another&#8230;

&#8230;The medics moved the two injured personnel over to normal beds, kept their oxygen masks on, brought them water, and monitored them. After five or ten minutes of nervously glancing around the man lying in the bed beckoned to a nurse.

## 

&#8220;Could I&#8230; have my gun with me&#8230; I feel so unsafe&#8230; so vunerable&#8230;&#8221;

## 

A pleading look entered his eyes as the nurse frowned, looking dubiously at the man under the bedsheets.

## 

&#8220;I´ll have to ask Doctor Phaar,&#8221; he said, guardedly, then turned slowly, looking over his shoulder at the man in the bed, and then walked off. A few minutes later the nurse found Phaar sitting behind his desk behind a pile of patient charts, looking them over. After the man´s request was put to him, Phaar looked at the nurse with a quizzical stare.

## 

&#8220;Well, it would no doubt help his psychological state, but it´s a bit dangerous&#8230;&#8221; The nurse nodded in agreement with the older man as he spoke.

## 

&#8220;I think we can take care of it though.&#8221;

## 

&#8220;Hmmm&#8230; okay. I trust you know what to do with the weapon before handing it over.&#8221; The doctor grinned.

## 

&#8220;Of course,&#8221; said the nurse, looking slightly put out, &#8220;I´m not stupid you know.&#8221;

## 

A few minutes later, the nurse handed the cut-down rifle to the man who sat upright in his bed to accept the weapon. He backed off and gave a somewhat distasteful look at it before turning and walking off.

## 

&#8220;Thanks,&#8221; said the man to the retreating back of the nurse, smiling, &#8220;I feel better already&#8230;&#8221;

&#8220;&#8230;It´s close, it´s fucking close!&#8221; came the voice over the radio.

## 

&#8220;Stay cool, stay cool, we´re on our way&#8230;&#8221; came Droden´s steady voice.

## 

&#8220;Two traces&#8230; they´re closing!&#8221;

## 

Droden ran down the vent, doubled over, the metal sides a blur as he rushed through the enclosed space, Shelton moving fast behind him, staying close, weapon raised.

## 

&#8220;Stay cool, we´re almost there!&#8221; he said, running.

## 

&#8220;They´re right on-fucking-top of us!&#8221; came a paniced cry from his radio&#8230;

&#8220;&#8230;well, he´s asleep now. The gun wasn´t loaded anyway, not that it mattered &#8211; he dropped off minutes after he had it&#8221;

## 

The medic looked at the sleeping man.

## 

&#8220;And what about the woman?&#8221;

## 

&#8220;Diz Therran, a technician from sector V6, never seen combat before; scared out of her wits&#8230;&#8221;

&#8230;Droden stormed around the corner, Shelton close behind, to see two muzzles pointing directly at him. His mind immediately went into overdrive, assessing the situation, his reactions kicked in instantaneously, hurling himself to the ground. The guns before him blazed, fire licking out of the muzzles, bullets ripping overhead, smashing into the sides of the vent, sparks dancing in the air, falling to the ground in a shower of light. Shelton jerked spasmodically as the lumps of metal tore through his body, twitching, his finger jerked on the trigger as he fell and the limply-held gun blazed away, recoil spraying it around the inside of the vent, light and sound merging into a maelstrom of death.

## 

&#8220;**Cease fire, cease fire!!**&#8221; screamed Droden, looking up into the smoking barrels before him, &#8220;Pinero&#8230; you fucking idiot man&#8230; you´ve killed Shelton&#8230; you&#8230; you´ve been reading us on the tracker&#8230; you&#8230; fuck&#8230;&#8221;

&#8230;Turunae winced at the carnage and curses over the radio, and turned to look at Hoskins. His face was pale, eyes wide, aghast at what had happened. There was another clunk from the vent, and both men looked back at it, both shaking slightly. Turunae put a hand up to his ear.

## 

&#8220;Is anyone in sector C3? Sector C3?&#8221; he asked the squad, anxiously, gun in one hand, wavering slightly, still aimed at the vent.

## 

&#8220;Uh&#8230; yeah man&#8230; It´s Garris&#8230; we´ve, uh&#8230; taken a wrong turn somewhere. We´re gonna head back now. Shit, man, Shelton´s dead&#8230;&#8221;

## 

Turunae shook his head, and visibly relaxed a little. Hoskins sighed with relief, clambered to his feet, then stretched, arms held high above his head as he yawned.

## 

&#8220;That was fucking close buddy,&#8221; he said, quietly, &#8220;That was just too fucking close&#8230; almost ended up wasting Garris and his number two&#8230; Shelton dead&#8230; it´s all screwed up.&#8221; Hoskins nodded in silent agreement.

## 

Behind the two soldiers, in the dark space between the swivel chairs and the command console, there was a hint of movement, and gunmetal gleamed coldly in the grey light.

## Part XXXIII

## 

In the silence of the docking control room, in the darkness under the control console, metal scraped on metal; slowly, delicately, the sound barely audible from a few centimeters. As the thread turned the black, sleek cylinder edged down to eventually connect with the grey-silver gleaming metal of the gun, joining firmly, mated perfectly. Arouin smiled, teeth an ivory row of small, pale slabs seeming to glow softly in the darkness for a moment before disappearing again. His heand moved slowly, swinging around in the shadows to bring the deadly burden it carried to bear on the closer of the two troops who still watched the vent before them intently. The gun came to rest, deadly snout aiming for the back of the kneeling man´s head. He sawyed back and forth slightly, fiddling with the strap that his weapon was slung on. The hand with the pistol shifted back and forth with the man´s movement. Arouin relaxed, breathing in time with his unsuspecting target. The finger resting on the trigger shifted, tensed, squeezed. Despite the silencer the sound of the pistol firing seemed loud in the still air of the room, the sharp report muffled and reduced to a dull thump which seemed to carry abnormally well. The man gave a quiet grunt as the back of his head exploded, throwing him forwards and onto the back of his comrade who lay on the floor, closer to the vent. Blood spilt over the floor and sprayed up into the air before arcing back down and splattering onto the ground making a sound like rain.

## 

&#8220;What the fuck?!&#8221; shouted the man on the floor, writhing around to look back over his shoulder and into the face of his friend, blood running over the man´s frozen features. His eyes went wide, face a mask of panic, incomprehension and fear. Out of one corner of his eye he saw a movement within the darkness, a steely glint of light. Reactions honed through ten years of training and simulated combat instantaneously took over, legs thrust out to try and get him away from the body of his partner, weapon arm already swinging around to combat the threat, finger jamming down on the trigger as the gun swang around. The assault rifle blazed, cartridge cases spewing up into the air, muzzle flash describing an arc of yellow flame as it curved around. Arouin´s pistol snapped off two quick shots, the first shot slapping wetly into the top of the man´s forehead, sending his head snapping back as if he´d been hit in the face with a metal pole, blood and spittle spraying out. The body arced backwards, the second shot catching it in the shoulder and knocking it back into the air, spinning it around one and half times before it crashed back to the floor with a thud, face down. There was a moment´s pause before Arouin rolled off of the pair of seats he had been lying on underneath the console. Yyanis rolled off the three swivel chairs she´d been on and landed awkwardly, winding herself slightly. Arouin crawled out from the cramped space and watched Yyanis critically as she did likewise, then picked herself up off the floor.

## 

&#8220;Why did you get three chairs?&#8221; he said, sullenly.

## 

&#8220;It was your idea, I would have suggested something more comfortable,&#8221; she said, grinning, &#8220;besides, I´m a lady. You´re meant to be gracious and polite to me remember?&#8221;

## 

&#8220;Uh&#8230; yeah. Plan worked though didn´t it? One hundred percent success in my opinion; we´re both alive.&#8221;

## 

&#8220;Not the most original I´ve heard; ´Hide under the bed and hope they won´t notice us´&#8221;

## 

&#8220;Don´t knock it,&#8221; he said dryly, &#8220;it worked.&#8221;

## 

&#8220;Great, we´re still alive. We´re also still trapped in this damn station, so let´s work on the next bit; getting the hell out of here.&#8221;

## 

&#8220;I´m taking out the production facility first.&#8221; he said firmly, checking his pockets to see how much ammo he had left.

## 

&#8220;You´re taking out the production facility.&#8221; she said levelly.

## 

&#8220;Yeah.&#8221;

## 

&#8220;You´re mad.&#8221;

## 

&#8220;Well, I haven´t worked out the technicalities yet, but we can burn that bridge when we come to it.&#8221;

## 

&#8220;You´re the only person I´ve ever met who actually relies upon luck. Couldn´t we just get rid of the blueprints of the power system? It would have the same overall effect; they couldn´t make anyway.&#8221; she pointed out.

## 

&#8220;Yeah,&#8221; said Arouin nodding slowly, &#8220;I guess so&#8230; but how can we be sure that all trace of the thing´s been removed?&#8221;

## 

&#8220;Just a matter of getting high level access then deleting all copies of the blueprints on the database. Sod it,&#8221; she said thoughtfully, &#8220;we could just nuke the whole d-base.&#8221;

## 

&#8220;You´d better rephrase that ´we´,&#8221; said Arouin, glancing around, &#8220;if you think I can do anything to do with breaking into a secure system you´re woefully mistaken.&#8221;

## 

&#8220;Well yes, I can do it. I´ll need core access though.&#8221;

## 

&#8220;What? Can´t you do it from here?&#8221; Arouin pointed offhandedly towards the command console with a pistol.

## 

&#8220;No; they´ve got better security than that. I need to physically get to the core to do anything as far-reaching as deleting the whole database. Even then they´ll have backups and safeguards.&#8221;

## 

&#8220;A self destruct mechanism, perhaps?&#8221; said Arouin looking hopeful. Yyanis looked at him blankly.

## 

&#8220;This is not a holo series. People don´t build self destruct mechanisms into highly expensive computer systems.&#8221;

## 

&#8220;Okay, okay,&#8221; he said, holding his hands up and spreading them apart, &#8220;just a suggestion.&#8221; He looked down at the bodies again. &#8220;Whatever we do, we´ve got to do it fast; someone´ll notice they haven´t reported in sooner or later. Judging by today, probably sooner.&#8221; Arouin stepped over the bodies and walked out of the room, Yyanis followed him out of the room and down the stairs, checking the clip in her gun as she did so. He paused just inside the door which led out into the hangar, stuck his head out into the cavernous space, then looked left and right. The hangar stretched away from them, seeming to have grown by several orders of magnitude since the last time they had run through it. It was more brightly lit and completely devoid of cover; the floor featureless and flat save for cartridge cases and bloodstains scattered over it´s enormous expanse.

## 

&#8220;Looks like the clean-up crew´s gone&#8230; no-one on the hangar floor.&#8221; He withdrew and looked back at Yyanis who was staring over his shoulder, &#8220;What do you think?&#8221;

## 

&#8220;Eh?&#8221;

## 

&#8220;Run or walk?&#8221; he said, rubbing the stuble on his chin. He hadn´t shaved for what seemed like a week, and thinking about it, yes it was probably a week.

## 

&#8220;Running will attract attention if someone sees us&#8230; but, of course, we´re less likely to be seen on security cameras because we´ll get across it faster.&#8221;

## 

&#8220;If no-one´s meant to be in the area at all then it won´t matter if we run or walk.&#8221;

## 

&#8220;Hmmm&#8230;&#8221; said Yyanis, still gazing off into the distance.

## 

&#8220;Fuck it, let´s run.&#8221;

## 

&#8220;Okay&#8221; she said brightly, and walked over to the door. Arouin took one last all-encompassing look around the hangar, then started off across the expanse at a run. Side by side they ran, the distance before them steadily reducing, Arouin´s coat whipping around behind him, billowing in the wind. Arms and legs a blur they ran on. It took the best part of two minutes to cross the hangar; they arrived at the far side breathless, getting out of the hangar as fast as they could and into the comparitive safety of a camera-less corridor. They stood against the walls of the hallway, one on each side, backs against the cool walls, silently regarding one another for a few moments before letting their gazes drop in exhastion.

## 

&#8220;No alarms.&#8221; said Yyanis eventually. Arouin looked up from the patch of floor he had been staring dumbly at.

## 

&#8220;No.&#8221; he said simply, then a couple of moments later; &#8220;Not yet, at least.&#8221;

## 

&#8220;I don´t think we were seen.&#8221;

## 

&#8220;I hope we weren´t.&#8221; said Arouin, pulling himself upright and away from the wall. &#8220;We´ve got to move&#8221;. He loked back at Yyanis, who nodded slowly and pushed herself away from the wall. He drew a brace of pistols from his coat, and weighed them thoughfully in his hands before turning and starting to walk off down the corridor. After a few paces he stopped, then turned around and looked over his shoulder to Yyanis who was still standing there, checking over her pistol. She looked up and met his gaze.

## 

&#8220;All right; I´m coming.&#8221; She said, slamming a clip home and re-cocking the pistol before setting off after Arouin. Their pace was measured and cautious, checking corners with quick, stealthy glances before rounding them; sneaking past rooms which sounds came from behind their doors.

## 

&#8220;Where are we headed?&#8221; asked Yyanis after a couple of minutes of tension-laden pacing.

## 

&#8220;The core system´s at the central station core; the main module.&#8221; replied Arouin after a few moments, &#8220;From what you say, that´s where you should be able to access everywhere from and get rid of all trace of this powerplant.&#8221;

## 

&#8220;You have touching faith in my abilities.&#8221;

## 

&#8220;You haven´t failed me yet.&#8221; he said, smiling distractedly.

## 

&#8220;Any hint of a backup plan forming in that head of yours, just in case I can´t do it?&#8221;

## 

&#8220;If you can´t do it electronically I´d have to find out a way of nuking the entire production facility using two kilos of ´tex, which is all I´ve got on me. There´s some more in the ship, but I really don´t fancy our chances of making it to the ship, to the core, then back to the ship again.&#8221;

## 

Yyanis nodded silently, and they moved on.

## 

The station seemed to be in a controlled state of panic, the occasional alarm going off, personnel in the corridors unsure of what to do, rushing around trying to find out what was going on. Arouin walked with hands in his pockets, guns grasped firmly in balled fists. They moved slightly faster when they started to find crowds of people rushing around; ducking and weaving amongst the bodies, keeping on the move lest someone recognise them. At one point Arouin lept to one side of the corridor and ducked into a shadowy alcove, dragging Yyanis with him. They paused in the confined space for a few seconds as a troop of security personnel thundered past, weapons readied for action, then moved on again again as soon as the coast was clear.

## 

&#8220;Not far now&#8221; said Arouin steadily, glancing around warily. After another minute or so of walking they saw, suspended above their heads, a sign which read &#8220;Central Core&#8221;.

## 

&#8220;Wonderful; signposts.&#8221; said Arouin with a grin.

## 

&#8220;I guess we´re there&#8221; murmured Yyanis as they followed the direction the sign indicated, into a short corridor of a few metres in length. At the end of the corrdior there sat a forbiddingly large door, painted an off-white colour. Warning stripes covered a strip at the bottom of the door and a red-painted sign with white type gave a list of unpleasant things which would happen to anyone who wasn´t authorised to enter and tried to. A swipe card slot sat to one side of the door, a single orange light on it blinking slowly. Arouin regarded the door´s steely countenance for a moment before turning to Yyanis.

## 

&#8220;Any ideas?&#8221;

## 

Yyanis studied the door for a couple of seconds before looking sideways at Arouin.

## 

&#8220;What about the ´tex?&#8221;

## 

&#8220;We don´t know how thick the door is. Plus, of course, it´s probably a ´plug´ defence door &#8211; a blast from the outside will wedge it backwards and jam it there, preventing access.&#8221;

## 

&#8220;So not the ´tex.&#8221;

## 

&#8220;No, not the ´tex.&#8221;

## 

Arouin took a couple of steps forwards and ran his hands along the seal around the outside of the door, fingertips seeking a purchase on the smooth metal. Seeminly satisfied there was nothing of use he stepped back again and cast his gaze over a variet of pipes, wires and conduits that ran over the walls and floor, and into the surround of the door. Yyanis looked at them.

## 

&#8220;They wouldn´t be stupid enough to put the power for the door on the outside&#8221; she said.

## 

Arouin deflated slightly.

## 

&#8220;Any other ideas?&#8221; he said glumly.

## 

Arouin continued staring blankly at the door. Outside in the corridor there was the sound of running feet. Arouin looked up, then pushed his back against the wall as a technician rounded the corner and ran down towards the door. The man looked at Arouin strangely for a moment, then leant past him and hammered on the door with the base of his fist. There was a click, a whine, and the door started opening. Arouin looked past the back of the technician´s head at Yyanis, who pulled a face and shrugged. The door finished opening with a chunky metallic thump and the technician stepped through, and turned to his left.

## 

&#8220;Hi, sorry, forgot my card again.&#8221; he said smiling, then glanced back past the doorway at Arouin, who had turned to face him. &#8220;What´s with the new guards?&#8221;

## 

&#8220;Guards?&#8221; said a voice. Arouin was already moving, lashing out with a fist at the technician´s face as he ran past him into the room. The blow sent the man sprawling backwards into the room and sliding along the floor. Arouin span to his left, gun in hand, and fired into the face of a burly security guard who sat there, mouth open, coffee in hand.

## 

&#8220;Wha..?&#8221; he managed before the gun went off, the blast knocking him back off the chair, his flailing hands dragging the coffee mug and a magazine he had been reading with him to the floor. Arouin glanced around the room quickly. It was octagonal and double-layered; the centre of the room was sunk into the floor, filled with grey-black boxes with status lights playing over their surfaces. The door Arouin had entered by was on the upper level, the walls of which were covered by terminals and other technical-looking equipment. Yyanis stepped through the doorway and looked around the room as the door gave a faint click and then hummed shut behind them. Arouin´s gaze fell to the man who lay at his feet, eyes closed, body slack, white face, and jaw set at a funny angle. Yyanis brought her pistol around to bear on the unconcious technician and looked at Arouin, face questioning. Arouin shook his head.

## 

&#8220;No need.&#8221;

## 

Yyanis nodded absent-mindedly, then turned her attention to the wall consoles. She walked over to the nearest one and ran one finger down the screen, stopping at the bottom and holding it there, tapping her fingertip against the glass. Arouin grabbed a chair and dragged it over to Yyanis, who sat down on it without looking, then grabbed another two and dragged them over to the door. The wheels shreiked and skittered over the blue-grey floor tiles. Arouin knocked one chair over, then moved it so that the seat back was underneath the back of the other chair. Then he carefully knealt down and aimed his pistols at the door, resting his arms on the seat, body covered by the backs of the two chairs. In the background Yyanis tapped away a the keys, abserbed in the task at hand.

## 

&#8220;None of the terminals are logged in with high enough clearance to delete the backup files.&#8221; she said, after a minute or so.

## 

&#8220;What does this mean?&#8221; called Arouin, gaze not shifting from the door.

## 

&#8220;I can delete all the other copies &#8211; this terminal does have a high enough access level for that &#8211; but I can´t delete the remaining copy of it in backup unless I get can get complete control. It looks like the only way I can actually do that is try and bypass the security measures they´ve got installed&#8230;&#8221;

## 

&#8220;You don´t sound very hopeful.&#8221;

## 

&#8220;I´m not.&#8221;

## 

Minutes passed, the silence of the room only broken by the tapping of keys and the whirring of fans. Suddenly Yyanis shouted with frustration and pushed herself away from the console with both hands, sending the chair squealing backwards across the floor.

## 

&#8220;It´s secure! I can´t find any loopholes, no little bugs that can be exploited&#8230; doesn´t look like anyone´s been stupid enough to leave a log of entering the core system either&#8230; I can´t get into it.&#8221; she said, exasperated.

## 

&#8220;Well&#8230; what else can we do?&#8221; said Arouin, looking around.

## 

&#8220;I can try and crack it using brute force by feeding it a dictionary, if we can guess or find out a username to find a password for.&#8221;

## 

&#8220;Sounds good, how long would that take?&#8221;

## 

&#8220;Uh&#8230; could take a few weeks.&#8221;

## 

Arouin closed his eyes, and rubbed his forehead with one hand.

## 

&#8220;Yes. Very good. Yet not particularly useful.&#8221;

## 

&#8220;No&#8230; I suppose not.&#8221; she conceeded. Arouin carried on looking around, then stopped suddenly and turned back to face Yyanis.

## 

&#8220;The _only_ place it is stored is in the backup.&#8221;

## 

&#8220;Yeah, everything else I can delete but they´ve got it set up so that you can´t delete the backup and if it´s needed it´ll just re-propogate through the system.&#8221;

## 

&#8220;Propawhat?&#8221;

## 

&#8220;Don´t worry,&#8221; said Yyanis in a tired tone, &#8220;yes this is the only place the plans are stored.&#8221;

## 

&#8220;The only place&#8230;&#8221; he said, slowly, &#8220;in the entire universe&#8230; that these plans are stored&#8230; are in those boxes?&#8221;

## 

&#8220;**Yes, dammit!**&#8221; shouted Yyanis at him, &#8220;And I can´t do anything about it! Okay? Lay off for fuck´s sake!&#8221;

## 

&#8220;Calm down&#8230; I can do it.&#8221;

## 

&#8220;Oh you can, can you?&#8221; she said scathingly, turning around and grabbing the swivel chair from behind her. She pushed it towards Arouin. &#8220;Pray, have a seat and tell us mere mortals how it might be done.&#8221;

*Arouin laughed at her angry expression, then dug a hand in one pocket, fished around in it for a moment, then drew out a large off-white slab.*

&#8220;Do you really wish to delete this file?&#8221; he said with a grin and a glint in his eye. Yyanis held his gaze for a moment, then burst out laughing. She reached over to take the ´tex from his hand.

## 

&#8220;Hell yeah.&#8221;

## 

Arouin held onto the explosive for a moment, and a worried frown crossed his face.

## 

&#8220;Will there be any debris left they could retrieve the data from?&#8221;

## 

&#8220;No,&#8221; said Yyanis, shaking her head slightly, &#8220;The data´s stored on high density crystals. They´re fragile enough normally, never mind what´ll happen to them when a pile of ´tex goes off.&#8221;

## 

She plucked the slab from his outstretched hand, then walked over to the gap in the railings which circled the drop to the floor below, turned around, then climbed down the ladder. Arouin turned back to face the door, waiting. Behind and below him there sounded the slow and measured footsteps of Yyanis packing round the storage stacks. Every few footsteps there was a pause and a quiet, plasticy noise, as of putty being slapped against a metal wall. The work took less than a minute, then there was the metallic ringing of Yyanis climbing back up the rungs of the ladder to the upper floor.

## 

&#8220;Done.&#8221;

## 

&#8220;Okay, let´s get out of here.&#8221; said Arouin, starting to get to his feet.

## 

&#8220;Aren´t you forgetting something?&#8221; asked Yyanis, head cocked to one side.

## 

&#8220;Uh&#8230;&#8221; Arouin paused and glanced around uncertainly.

## 

&#8220;Detonators?&#8221;

## 

&#8220;Ah, yeah, good point.&#8221;

## 

Arouin fished around in a pocket for a second, then stuffed his hand into another, then patted down the length of the coat. Yyanis looked increasingly concerned.

## 

&#8220;You have got the detonators, haven´t you.&#8221;

## 

&#8220;Yeah&#8230; somewhere.&#8221;

## 

Arouin checked a pocket inside the trenchcoat, and the worried expression that had been building slowly on his face vanished as he grinned.

## 

&#8220;Got ´em.&#8221; he said, triumphantly, pulling a pair of metal cylinders out and tossing them one at a time over to Yyanis who caught them deftly. Arpuin turned his attention back to the door as she walked back over to the ladder and climbed down it. A few moments later Yyanis was back on the upper level.

## 

&#8220;_Now_ we can get out of here.&#8221; she said firmly. Arouin stood up and walked over to the desk, avoiding the spreading pool of blood from the body of the guard lying on the floor behind it. He reached out for the switch with one hand, but just before his fingers made contact with the white plastic the door hummed into life. Arouin snatched his hand back from the switch as if it had burnt him, and snapped his head around. The faint smile that had been on Yyanis´ face faded slowly. Light from the corridor outside painted a growing block on the floor, which stretched towards Arouin. Shadows obscured some of the light. Arouin was already letting himself fall back and to one side, guns seeming to leap into his hands as he fell. Yyanis was already throwing herself to one side, out of the line of fire. As Arouin landed on the floor, winding himself, he opened fire. The pair of legs closest to the door blew out at the shin, pieces of flesh and shards of bone blasting out and scattering over the floor leaving flecked trails of blood. There was a hideous scream of anguish, the legs buckled, and a guard hit the floor. Arouin carried on firing as the door raised, tracking shots up the bodies of the men outside which twitched and jerked inhumanly as the bullets smashed through them. One of the men near the back threw himself across Arouin´s line of fire, and hit the corridor wall heavily, pulling his gun up. Arouin rolled a split second before the laser carbine flared, the patch of tiles he had been lying on warping, spitting and crackling as a small explosion rippled over them, then quickly adjusted his aim and squeezed both triggers at the same time. The double-shot caught the man in his shoulder, almost tearing his arm off with the force of the two impacts and sending him cannoning back into the corridor beyond. The roar of the guns faded, Arouin´s grip on them relaxed slightly and he stood slowly, then paced cautiously over to the door.

## 

&#8220;Looks clear.&#8221; he said, turning back to face Yyanis, who stepped around a console and into view. Arouin saw her tense, the gun in her hand leap up to aim at his face, and, as his mind raced, his body moved to avoid the shot, but to late. The gun flared, yellow-white flame licked out from the barrel, the edges tinged red-brow. He felt a ripple of air past one cheek and a stray strand of hair tugged at his hair as the noise of the shot washed over him. Then he felt a fine spray on the back of his neck. He froze, stooped slightly, watching smoke rise in whisps from the end of Yyanis´ gun, and slowly lifted one hand to the nape of his neck. He drew it back, and looked down at it, sticky and red with someone else´s blood. He looked back up at Yyanis, then turned around and looked down at a body with half it´s throat and lower jaw missing.

## 

&#8220;Thanks&#8221; he said with feeling, staring down at the multilated body. Yyanis slowly untensed and stood up from the slight crouch she´d dropped into.

## 

&#8220;Can we actually leave now?&#8221; she said, keeping a close eye on the door.

## 

&#8220;Actually, while we´re here, there´s one more thing I´d like you to do.&#8221;

## 

Yyanis looked at him warily.

## 

&#8220;What?&#8221;

## 

&#8220;Tell the hangar bay 1 systems to take the _Lithe Shadow_ into a refitting bay, then get the automatic systems to install item ref number&#8230;&#8221; he closed his eyes, &#8220;&#8230;.3778359-A.&#8221;

## 

Yyanis looked at him blankly for a moment, then snapped out of it.

## 

&#8220;Okay.&#8221; she said with a shrug, then walked over to the closest console. She tapped out a quick squence of commands then took a step back.

## 

&#8220;Done&#8221;

## 

&#8220;_Now_ let´s get out of here.&#8221;

## 

## Part XXXIV

## 

The station shook; a rumble passed through it´s structure rattling those inside. Coffee mugs bounced along desks and desk lamps fell to the floor. People stopped in corridors and looked around uncertainly, wondering what caused the tremor. Inside the central operations complex, Jarren looked up.

## 

&#8220;What the fuck was that?&#8221; he said, calmly.

## 

The men gathered around him and the table he stood in front of glanced around at one another, faces carefully blank. The fire team had pulled back to the ops complex to try and find out where their prey had gone; for the last twenty minutes they had been studying reports overlaid onto the large electronic map of the station that was set into the central table in the room. After no-one moved for a couple of seconds one of the men at the table turned away and walked quickly over to a terminal. The tapping sounds of his fingers hitting the keys rang out curiously loud in the room, and although no-one turned to watch him it was quite obvious where everyone´s attention lay. The screen of the terminal that the man was typing at lit up blue with a smattering of white writing and an unintelligible error message. He paused for a moment, his face changing expression to one of increasing concern, before hammering out another sequence. The screen lit up blue again. He turned and walked back to the table, the colour draining from his face.

## 

&#8220;The&#8230; uh&#8230; system core is offline.&#8221;

## 

Jarren´s jam dropped a little way open and then hung there slackly.

## 

&#8220;**What?!**&#8221;

## 

&#8220;It´s down! It´s not responding&#8230; that blast must have been&#8230;&#8221;

## 

&#8220;Oh shit.&#8221; murmured one of the other men, covering his eyes with his hands and rubbing his face.

## 

&#8220;Check on the plans for the power system.&#8221; said Jarren hurriedly, walking around the table and following the man who turned once more and headed for the terminal again.

## 

&#8220;Nothing in the database&#8230; backup d-base is down&#8230;&#8221; there was more insistent tapping, &#8220;&#8230;and all traces of Project Divinity have been removed from local d-bases.&#8221;

## 

&#8220;**That bastard!**&#8221; shouted Jarren, casting his eyes to the ground, balling one fist and hammering it into the open palm of the other hand. He looked up again, teeth gritted. &#8220;What&#8230; about&#8230; the prototype?&#8221; he said slowly.

## 

&#8220;Uh&#8230; unknown sir&#8230; the logs show that it was removed from storage about three minutes ago&#8230;&#8221;

## 

&#8220;He´s going.&#8221; said Jarren, lips forming a thin line. &#8220;He´s going and he´s taking it with him.&#8221; He swung around to face the officers gathered around the table. &#8220;Well don´t just _stand there_&#8230;&#8221; he said, arms stretching wide, hands splayed, &#8220;**Get him!**&#8221;

## 

Arouin and Yyanis burst into the refitting bay from one of the doors on the seventh level in the bay´s back wall. The catwalk they stood on was three meters wide and ran along all four of the interior walls of the bay, ladders leading down to the six floors below and up to the two floors above them. The lowest five levels of catwalks only covered three of the walls; the wall furthest from them had a massive five-story door set into it which led to the hangar proper. Arouin walked to the edge, boots scraping on the metal grille of the catwalk, then leant forward bracing himself against the rusting metal railing with both hands. Heavy chains stretched past them from the ceiling and the floors above, lifting equipment attached to the ends below them, magno-clamps and hooks swaying gently and casting their moving shadows on the form of the _Lithe Shadow_ which sat five stories below Arouin. A cradle held the Phoenix class fighter in the air, supported by two massive oil-covered metal cylinders which disappeared into a small, dark pit in the floor. Fluro-strips hung from the ceiling high above, chains holding them up with power cables twisted around them like snakes around a bough of a tree, disappearing off into the darkness above where their light was not cast. Water fell from on high, distant cooling units condenstating the moisture out of the air, the droplets falling down, glinting in the light of the fluro-strips, before splattering on the floor twenty meters below and the hulking mass of Arouin´s fighter. Droplets formed, ran together, then flowed over the brown-painted armour of the ships in rivulets of silver that glittered like rivers of mercury in the glare from the overhead lights. Dust motes glittered, catching the light as they rose and fell and swirled around, contrasting sharply with the dark walls beyond.

## 

&#8220;Beautiful.&#8221; whispered Arouin under his breath, looking down upon the sleek lines of his ship. There were footsteps on metal behind him as Yyanis walked up to the railing.

## 

&#8220;Let´s move.&#8221;

## 

&#8220;Yup.&#8221; he grinned, still looking down, &#8220;Time to get out of here.&#8221; Arouin pushed himself upright, away from the railing, turned, and walked along the catwalk towards the nearest ladder downwards. He wove through the stacks of equipment and afterburner fuel, stepping over the occasional bit of piping or roll of cable that had been left lying around, and was only a few meters away from the ladder when there was a whine of servos and then the metallic thump of a door locking open. Arouin stopped slowly, and turned. Yyanis was already looking out across the refitting bay, at a now-open doorway one level above their own, set into the far bay wall. Two lines of troops ran out of the door, wearing black battle dress and plasteel combat jackets. They ran out along the catwalk, split evenly to each side of the door; twenty troops, ten each side. Another man walked out, hair greying slightly, grey eyes, an impassive expression on his face, a sub-machinegun hanging loosely from his right hand, a shotgun slung across his back. Slowly, his left arm came up, and one bony digit extended from his hand to point at Arouin.

## 

&#8220;You have too much luck for one man, Mr.Simmel.&#8221; he shouted across the bay.

## 

&#8220;Doesn´t look like it from here.&#8221; Arouin called back, then turned his head to Yyanis &#8220;Get ready to make a dash for the ship&#8221; he whispered urgently. The man on the far side of the refitting bay chuckled loudly.

## 

&#8220;Very observant, Mr.Simmel, very observant. You don´t _have_, you´ve _had_, past tense, too much luck for one man. That luck has now run out.&#8221;

## 

The sub-machinegun started to rise up in Jarren´s right hand, his left coming around to cradle the barrel just forward of the magazine. Arouin turned and pushed Yyanis out the way with both hands, throwing himself backwards behind a large packing crate who´s top was off revealing an array of expensive engine parts. Yyanis stumbled sideways, falling to the metal floor, bouncing, and then scrambling under the cover of a half stripped-down _Dream_ engine. The guns let rip. The plasteel crate Arouin was behind shuddered and quivered as bullets ploughed into it, shards of the material springing into the air and scattering across the metal grille of the catwalk, some pieces spilling over the edge and tumbling end over end down to the floor below. Arouin grabbed a pair of pistols, then squirmed around and up onto his feet, squatting on his haunches to keep his head down. Inside his head a counter was running.

## 

_25&#8230; 26&#8230; 27&#8230; 28&#8230; 29&#8230; 30 shots&#8230; 31&#8230; 32&#8230; 33&#8230; 34&#8230; **35**_

## 

Arouin sprang into action, launching himself to his right from a standing start into a sprint, tearing across the catwalk towards the nearest ladder. His arms were stretched out across himself, a pistol in each, head turned and watching the black-clad men as they fanned out across the catwalk above and across the bay, some already heading down ladders onto his own level. In the break in the firing as some of them reloaded, Arouin snapped off shots as he ran. Bullets whined out, drawing sparks which fell down like golden rain from the walls where they hit. One of the men on the level above caught a bullet through his calf as he ran for cover, the shot knocking his legs out from under him and sending him sprawling to the floor. Someone next to him finished reloading and sprung up from the packing crate they were behind to squeeze off a burst at Arouin as he ran. Bullets smashed through the metal behind him, he adjusted his aim, snapped off a pair of shots which sent metal shards flying from just in front of his assailant´s face, adjusted again, and started firing at one of the men who was half-way down a ladder. A bullet struck the ladder where one of the man´s hands hand just been; he snatched his hand away, then, hanging on with one hand, swung around with a blazing sub-machinegun in the other. In front of Arouin the wall errupted into a storm of flying metal which raced towards him flashing and rupturing. He instantly threw himself into a forwards roll, slipping under the stream of devestation before crashing into the side of a pile of drums labelled ´Afterburner Fuel´. There was a split second when Arouin just started dumbly at the writing, uncomprehending, then the bullets crashed into the pile of drums. Bullets punched through the thin metal of the drums in front of Arouin as he lay down quickly, franticly reloading both pistols. No fuel spilled from them; the seals lay abandoned on the floor around them.

## 

_Phew, empty&#8230;_

## 

As the bullets crashed around him the catwalk he lay on thrummed with the pounding of booted feet making their way towards him. Arouin finished reloading and moved crabwise along the line of drums towards a stack of packing crates at it´s end. Two pairs of footsteps came towards him, loud, fast. Arouin waited until they were a handful of meters away before leaping up onto one of the empty drums beside him, then up onto the stack of crates. Two black-clad troops in front of him looked up in surprise as he leapt off of the crates towards them, guns flaming, bullets screaming through the air around him and sending pieces of the wall spinning into the air. The men twitched and jerked under his fire, one of their guns going off in it´s owner´s hands as his muscles spasmed. Arouin landed hard, rolled sideways away from a stream of lead from above which chewed up the catwalk, then scrambled to his feet again and carried on running. A pair of legs appeared at the top of the ladder he was running towards, which lead both to the floor above and the floor below. Arouin fired, the legs shook and a screaming man fell down onto the floor which Arouin was on, starting to collapse to the floor, then twitching and silencing abruptly as the next bullet hit him in the temple. Arouin hit the ladder looking upwards, glanced down, then let himself almost drop right down to the floor below, squeezing off a pair of shots at a head which breifly appeared framed by the gap to the floor above before withdrawing in a hurry. On the left side of the bay Arouin could see Yyanis crouch behind a crate, still, as a pair of troops ran towards her position. When they a couple of meters away from her she slid out in front of them on the floor, then blazed away up at them with her pistol. The bodies arced backwards through the air, one sliding along the catwalk, the other bouncing off of the handrail before falling away out of view. Another man appeared from the ladder which Yyanis must have just passed and turned to face her as she scrambled to her feet. Arouin whipped around with both pistols and opened fire. The first shot hit the man in one shoulder; he shook from the blow, then calmly turned to face Arouin and opened fire. Arouin dropped to his knees just in time, the crates beside and behind him having fist-sized holes blown into them a split second later. Yyanis turned, faced the man who was firing at Arouin, then put a single shot into his head, throwing him off the side of the catwalk and to the floor below. Arouin jumped back to his feet, breathing heavily, then glanced about to find the nearest ladder down and ran towards it.

## 

_Four levels to go&#8230;_

## 

He looked across to the left side of the bay and saw Yyanis already a floor below him, her nearest pursuer two floors above, then turned back, grabbed the ladder before him, and quickly climbed down it. Arouin couldn´t see most of the troops any more; they were hidden by multiple catwalk layers,

## 

_But they´re there&#8230; and they´re still coming for me&#8230;_

## 

He glanced around the catwalk, then looked up suddenly when there was a rattling somewhere above him. In the distance there was a burst of machinegun fire, two pistol shots, and then a scream of pain. There was a rattle again from above him, and Arouin moved closer to the edge of the catwalk. He was about to lean out over the drop when out of the corner of one eye he saw a movement of something black and sleek. The darkness exploded into light again as someone three levels above at the back wall of the bay opened fire, flame from the muzzle flash lancing towards him. Arouin backpedaled quickly, firing his pistols as bullets ricocheted off of the handrail which hummed as the shots struck it. Arouin´s shots kicked up showers of sparks from the railing beside the black-clad figure, who quickly dropped back down out of view again. Arouin looked around, breathing laboured, and dropped to the ground beside a _Sport_ powerplant which was half-dismantled on the floor to reload. He dropped the clips out of both pistols, slammed a fresh clip into one, and was about to do the same with the second when there was a loud rattling noise right beside him. Arouin looked up to see one of the heavy chains shaking violently, water flying from it, and was half way to his feet again when a figure holding a sub-machinegun slid into view, holding onto the chain with one hand. Arouin launched himself out the way to his left, trying to put the dismantled powerplant between him and the other man, but he moved too slowly; the gun flamed, sweeping across to cover Arouin´s movement. In mid-air, Arouin felt the bullet smash into his left shoulder, spinning him around as he fell, his whole body shuddering with the impact. He hit the ground, shocked, and froze for a moment as the clamour of battle continued, face down to the deck, watching his own blood mingle with the water and drip down to the level below. There was a thump of someone landing on the catwalk, then the tread of heavy boots approaching. He felt a prod in his ribs, then a kick rolled him over onto his back, the pain adding to the cacophony of torment from his wounded shoulder. He watched the gun, hanging below from it´s owner´s hand, swing up, steady, and he looked down the darkness of the barrel which seemed to fill his vision. Arouin blinked, and focused on the face of the man who stood over him.

## 

_Bugger._

## 

Slowly, it seemed, the man brought the gun up, nestling the stock in his shoulder, and sighted on Arouin´s forehead. Arouin looked on, watching water and sweat mingle and run down the mans face; he could see the stubble where he hadn´t shaved, a scar on one cheek from who knows what injury in the past, and brown, hate-filled eyes. There was a shout from the far side of the bay, and the man standing over him turned his head, water spraying off of his hair as his head snapped around. Arouin dreamily brought up his right hand, the pistol still in it, and sighted on the man´s temple. He snarled something at someone unseen across the bay, then turned back to face Arouin, who fired. The shot punched a hole in the middle of the man´s face, blood and bone shrapnel spraying out over the catwalk as his body was launched backwards into the air. Arouin threw himself forwards off the ground, still firing as the trooper´s body shuddered again and again before landing heavily on the deck. Arouin spat a bit of bone out of his mouth, dropped his pistol, then stooped over the body and grabbed the sub-machinegun, slinging it´s strap over his neck before reloading it as quickly as he could, one-handed. The _Lithe Shadow_ was now only two stories below. Arouin made a stumbling run to the next ladder and clambered awkwardly down it, gun clattering on the metal of the rungs as it bounced between the ladder and his chest. He looked out across the bay.

## 

&#8220;Yyanis!&#8221; he shouted, straining his voice to it´s limit, shoulder screaming in agony, &#8220;We&#8230; are&#8230; leaving!&#8221;

## 

He took a few steps back, then ran forward and leapt over the railing, fell almost three meters, and landed heavily on the slope of the Phoenix´s right wing. The surface was slippery, and he fought to keep on it, feet scrabbling over the wet metal. There was a metal series of high-pitched thunking noises and a row of small indentations in the metal sprang up half a metre from Arouin, followed by a shout and the echoing crackle of a carbine. He stuck one foot in the crack by an inspection hatch to brace himself, then twisted around and loosed off a one-handed burst from the sub towards the figure high above before scrambling over the upper surface of the Phoenix to it´s cockpit. He glanced up to see someone sighting at him along a laser carbine from four stories above, leaning far out over the railing to get a clear shot. As he started to shift his weight to try and get into a position where he could aim at the man there was a flurry of shots which split the man´s head open like an over-ripe melon. Arouin slapped his hand down on the canopy controls and the black glass raised up, water running off of it, to let Arouin scramble over the edge of the cockpit and into the pilot´s chair.

## 

&#8220;Come on!&#8221; he screamed hoarsely to Yyanis who was two levels above and franticly scrabling down a ladder. He slumped back in the chair to try and use the control panel in front of him as cover, and rested the barrel of the sub-machinegun on the top edge of the radar display. One level above Yyanis, someone leant out over the edge of the catwalk to steal a quick glance at the situation, but moved too slowly; a burst of fire from Arouin sent them toppling backwards missing most of their neck. Yyanis made it to the level of the wing, ran, and jumped onto it. She almost slipped and fell off, but, with a great deal of frantic arm-windmilling, managed to regain her balance and run the handful of meters to the cockpit before falling over the side and landing heavily on the floor. Arouin slapped the canopy close button and sprayed gunfire from the sub at whatever he could see until it ran dry, before throwing it out of the cockpit just before the canopy clicked shut. There was a succession of muffled thuds as small arms fire struck the cockpit glass and the armour around it. Yyanis lay on the floor, watching blood drip from Arouin´s shoulder and pool on the decking. Arouin wasn´t hesitating; one-handed he powered up the ship´s systems, bringing everything online as fast as possible.

## 

&#8220;Disengage the cradle clamps&#8230; and we´re free.&#8221; he announced, voice tired. Below the Phoenix the yellow and black striped holding cradle dropped away, the metal cylinders supporting it sliding smoothly into the floor. Inside the cockpit Arouin grabbed the control stick. The Phoenix tilted slightly as the cradle lowered away from her, thrusters keeping her hovering in the artificial gravity of the refitting bay. Arouin hammered at the control to remotely open the door, and waited for a few seconds to see the result. Nothing happened. After a couple of seconds of glancing over the controls, Arouin shrugged and pulled the trigger. Quad Barraks flamed; two lances of blue fire leapt out from either side of the _Lithe Shadow´s_ cockpit and hammered into the door which screened off the main hangar. Giant holes were punched in the thin metal, then Arouin nudged the ship forward and accelerated out of the bay through the remanants of the doors, the Phoenix shuddering as it smashed through them, sending a horrific shriek of tortured metal through the air. The bay stretched out away in front of them and on both sides. The occasional ship sat here and there on the featureless grey floor, equipment and refueling nozzles lying around them. Arouin turned around in his seat, wincing a little as he did so.

## 

&#8220;Which side of the bay faces outward?&#8221;

## 

Yyanis closed her eyes, remembering a schematic seen half an hour but seemingly two decades ago.

## 

&#8220;Should be the one to our right&#8221; she said eventually. Arouin nodded slowly. Behind the Phoenix Jarren ran out through the shattered door and into the hangar, then stared up at the Octavian fighter which hung above him, shaking with rage and frustration.

## 

&#8220;Exit stage right&#8221; Arouin said, smiling faintly and thumbing the missile launch button. Four streaks of flame and smoke screamed away from under the wings of the *Lithe Shadow* towards the wall, growing smaller with distance before blossoming into flame. The flame broiled out from the impact point of the four, almost instantaneous, detonations, and a shockwave rippled through the bay, tearing up the floor and sending metal plates meters wide flying up into the air. The ceiling rippled like waves on an ocean, mirroring the reaction of the floor below, girders screaming and bending, snapping, hawsers whipping around. Pieces of equipment and bits of the structure started falling onto the floor of the bay, bouncing around before lying still, and above it all, above the crashing of metal on metal and the death throes of metal, rose the howl of the wind. The smoke blossomed out towards the Phoenix, paused, then, like a film being rewound, sucked back again to reveal the blackness of space beyond. Bits of debris started sliding along the floor, gaining speed and momentum, crashing into one another and bouncing up, heading for the oblivion of the void. Around the gaping hole in the side of the station were white contrails as the moisture in the air froze, rendering it briefly visible before the vacuum of space stole it away from sight. Arouin, fearful of debris hitting his fighter, glanced at the rear view screen just in time to see a grey haired, black suited body crash into the camera, then go spinning off past the cockpit before tumbling into the depths of space. After twenty seconds the storm became a gale, became a breeze, and died. Arouin accelerated the ship forwards towards the gap in the outer hull, the fused metal edges of which seemed to reach towards them before they rushed past and into the soothing darkness of space. He quickly targetted the only Jumpgate in the sector, and headed towards it, sighing with relief.

## 

&#8220;I hate to say it, but I wasn´t _absolutely_ sure we were going to fit through that.&#8221;

## 

Arouin smiled a satisfied smile, despite the pain, and slipped back into the comfort of the padded pilot´s seat.

## 

_Well,_ he reflected, _that wasn´t so bad. Got the powerplant. Got the ship. Still alive._ Arouin grinned broadly to himself. _Hell, even got the girl&#8230;_

## 

He turned to look at Yyanis. She was already on her feet, and looking at him in a slightly different way. The in her hand gun crashed, flame licking out from it´s barrel. Arouin shuddered, once, then collapsed back into the chair, blood running down his neck from the gaping wound in his forehead. Bits of red and grey flesh dropped away from the inside slope of the cockpit, splattering on the control panel. Yyanis leant past Arouin´s body and it´s incredulous, unbelieving expression, and tapped at the communications controls.

## 

&#8220;This is Agent Ulevi-09. I have the prototype&#8230;&#8221;

## 

## Epilogue

## 

In the cold of space, there was only the darkness, and the Phoenix.

```text

##Hope this is the stuff you wanted Arouin, I've tested it, and it shouldn't##
##malfunction on you; not that I expect you to come back and complain if##
##it does :) - Raquel##

mort=false
WHILE (mort=false) DO[
t=0
Body(get_hrate)
WHILE (hrate < 30) DO[     ##3 minutes should give anyone time to revive you##
t=t+1
delay(1000)     ##(Time in ms)##
Body(get_hrate)
IF (t=180) THEN mort=true
]
]
Shipsys(DefMech(Destruct))
```

## 

And then there was light.

## 

**THE END**
