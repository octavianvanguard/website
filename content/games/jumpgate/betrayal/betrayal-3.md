---
banner: "css/images/banner.png"
title: Parts XXI – XXVIII
author: fellblade
layout: page
date: 2018-04-07

---
## Part XXI

*Arouin wove through the mish-mash of pilots, technicians and spacecraft, looking warily from side to side, Yyanis trailing along behind him. Instead of a multitude of smaller passages from the hangar bay out to surrounding malls and corridors, the Octavian station had a single larger entrance straight into a large corridor leading to the centre of the station. About twenty <em style="color: #333333; font-weight: 300;">Minotaur</em> troops were standing around it, looking not-particularly alert in their heavy body armour. Yyanis hurried to catch up with Arouin who was striding confidently towards them and pulled on his shoulder to slow him down, wincing at the movement in her arm.*

*&#8220;How are you going to get past them?&#8221; she whispered to him. He turned to her with a slightly puzzled expression on his face.*

## 

&#8220;What do you mean? I´m going to walk past them like usual.&#8221;

## 

She glanced over his shoulder at the body-armoured, helmeted, heavily armed storm-troops cum security guards.

## 

&#8220;You mean they aren´t waiting for you? That isn´t more guards than usual?&#8221;

## 

Arouin let out a short laugh and turned, starting to walk towards the guards again.

## 

&#8220;No they´re not waiting for me&#8230; I´m guessing you haven´t been to the Core station before?&#8221;

## 

&#8220;Uh&#8230; no.&#8221; she said, walking to one side and slightly behind him, visibly more nervous than Arouin.

## 

&#8220;This is a pretty light guard,&#8221; said Arouin, &#8220;there aren´t any of them in the new _Demon_ exo-skeletons, and the Octavian government is keen to show the technology it´s developed to rival clans as well as other factions.&#8221;

## 

He nodded to the nearest guard as he walked past, who returned the greeting with a blank stare. A few meters ahead of them people leaving the hangar area were forming a wall as they were being held up by something obscured by the milling bodues. Arouin didn´t break his stride, but Yyanis began fidgeting with the zipper on her jacket. Then some of the crowd thinned out, revealing the reason for the hold-up. A series of desks were set up across the corridor with gaps between them, and alongside each gap there was a uniformed man with a black electronic ´wand´. As they approached the line of desks, one of the men in front of them walked forwards and put both hands behind his head. The nearest uniformed man frisked him with the wand&#8230; when it passed over the man´s left trouser pocket it beeped, and a small, previously invisible red light flashed. Yyanis tapped Arouin on the arm and he turned, irritated.

## 

&#8220;What is it _now_?&#8221; he asked, his growing annoyance showing clearly in his voice. Yyanis looked at him urgently.

## 

&#8220;_They´ll find your weaponry!_&#8221; she said in hushed tones. Arouin gave her a withering stare and shook his head despondently.

## 

&#8220;You don´t have a clue, do you? You really don´t have a fucking clue.&#8221; he said wearily, turning away. Yyanis stood there for a few moments in disbelief as Arouin walked away from her towards the line, then shook her head and followed him in confusion. Arouin watched as the man in front of him was checked for weaponry. After a few moments he was found to have none and was directed towards a corridor to one side of the main route with a green sign above it which read &#8220;Clean&#8221;. Arouin stepped forwards and the man with the wand ran it over him. It beeped, with the accompanying red flassh, at least fifteen times, the uniformed man giving Arouin an approving look before waving him on, then patiently waiting for Yyanis to step forward. Arouin turned and watched as the man gave her an impressed look-over, then took slightly longer than was strictly necessary to run the wand over her. It didn´t flash red once and Yyanis looked at Arouin, puzzled, as she was ushered towards the corridor under the &#8220;Clean&#8221; sign. Arouin walked quietly through the throng of people, down the main corridor, and into one of the main mall hallways. It was ten stories tall, the sides covered with shop fronts all the way up. Walkways allowed access to the shops, running along the wall in front of them and criss-crossing the gap between the two walls at multiple levels. The walkways had originally been made of fairly cheap metal gratings; the Octavian goverment, or what passed for one, was not too free with its money. Now the walkways had been piecemeal repaired over the years, the occasional piece of plastic bridged gaps where normal wear and tear, small arms fire, other miscillaneous damage or outright thievery had removed chunks of the original metal. Chains holding the walkways up streched down from on high, some more supporting lights or bright neon signs. The occasional one or two pieces of chain swung idly, attached to nothing at their lower end, swinging slowly as air currents from the massive overhead coolers disturbed them. Water dripped from the distant ceiling as mositure in the air condensated on the coolers, the falling droplets seeming to glow under the powerful gaze of overhead lights ten stories up, then fading to nothingness as they plummeted to the levels below. Arouin looked up at the people going about their business above him, then looked down and wiped the water from his face before walking on. After another hundred meters or so, he wandered over to the right hand side of the mall corridor and let the flow of people move past him. On the opposite side of the corridor two men stood, leaning against the walls, one either side of a blackened plexiglass door with no sign or other indication of what it led to. Arouin didn´t need any sign or indication &#8211; he already knew, as did the two people standing next to the door. And he knew what they were doing there as well, and what sort of person they were looking for. Then the door opened, a couple of men holding shiny new pistols came out, talking to each other animatedly, then Yyanis stepped out of the corridor and into the mall hallway, shaking her head. The men either side of the door who had been leaning against the wall, bored, noticed her and started to get up, but Arouin was already striding across the width of the mall hallway, ploughing through the crowd, reaching into his jacket, pulling out a pistol. The men had managed to each take a couple of steps towards a confused Yyanis when the gleaming pistol in Arouin´s hand barked twice. A scream of pain, two splatters of blood on the black-painted wall behind one of the men, a few flecks of red ichor sprayed over Yyanis´ jacket as she threw herself to the floor. The second man turned towards his companion´s assailant which meant that as the pistol barked again he took the round through his right eye instead of his temple. The crowd stirred, a couple of people screaming, the rest looking with interest to try and see what all the commotion was about, a few more looking for the dead or dying to loot their corpses. Arouin shouldered through the remains of the crowd that were between him and the now-cooling bodies, the slightly smoking pistol held loosely pointing at the floor in his right hand. There were no innocent bodies on the floor,

## 

_Something of a miracle._ thought Arouin, _Let´s see who these guys were&#8230;_

## 

He ignored Yyanis and stooped over one of the bodies, then pulled it´s coat open. A quick search revealed four wallets, only one with a matching ID card, and about twenty thousand credits in cash of various demonimations spread between them. As well as the 8mm pistol that was in the corpses pocket there was also a Burleigh-Thorkston pump action shotgun on a strap around the man´s chest. Arouin threw the pistol to Yyanis, who caught it neatly and stared at it blankly for a moment before putting it away in one pocket of her jacket, followed by two clips. Then he threw her the shotgun.

## 

&#8220;Where am I meant to hide this?&#8221; she asked.

## 

Arouin shrugged, &#8220;Why bother? Sling it over your back and let´s move. No sense in hanging around all day&#8230;&#8221;

## 

He got up and looked at the other body, only to find that he was too late; it had already been looted while he was going through the pockets of the first corpse. Arouin turned and walked away beckoning to Yyanis, who, after a moments hesitation, followed him. After a few minutes of walking in silence through the bustle of people going about their business, Yyanis spoke,

## 

&#8220;Why did you shoot those two men?&#8221;

## 

&#8220;Well, I thought it might be a good idea if they didn´t rob you.&#8221; he said, patiently.

## 

&#8220;How do you know they were going to do that?&#8221; she persisted. Arouin stopped suddenly and turned to face her.

## 

&#8220;You came out of the ´Clean´ corridor without a new weapon, so you were defenceless&#8230;&#8221; Arouin dug around in a pocket for a moment before producing three of the four wallets from the body.

## 

&#8220;If you take a good look at these three wallets you will notice that none of the IDs look even vaguely like our late pair of friends, so they weren´t spies with multiple ID cards. Besides, that would be exceptionally bad fieldcraft for an agent to carry his other IDs with him in so obvious a place. Can you think of any other reason why he had those wallets?&#8221;

## 

&#8220;No, I can´t&#8221; she admitted, &#8220;But&#8230; that place itself&#8230; I just can´t believe that they search you for weapons&#8230; but only to sell you one if you don´t have one.&#8221;

## 

&#8220;Good business sense,&#8221; said Arouin as he turned away and resumed his steady pace down the hallway, &#8220;you´re targetting the people who want the stuff you´re selling&#8221;

## 

After a few more minutes they turned a corner in the hallway and Arouin started paying more attention to the shop fronts on their level, looking for something.

## 

&#8220;What hold has Mr. High-and-mighty-Director got on you, anyway?&#8221; said Arouin suddenly.

## 

&#8220;Hold?&#8221; said Yyanis, pushing away someone trying to sell her some beef-flavoured soy-soup, &#8220;What do you mean hold?&#8221;

## 

&#8220;Well, he´s forcing me to do these stupid damn technology pickups for him. In return he´s going to remove the only two criminal offences on my file in TRI records, give me the ship that I´m using, and also not send out loads of his goons to try and kill me.&#8221; Arouin explained.

## 

&#8220;Ah, I see what you mean&#8230; Well, I was in training for the Solrain Espionage Forces and was heading towards then end of my training there when The Director picked me up and patiently explained to me that the SEF had placed a lump of ´tex in my head. If I disobeyed an order, went rogue, or wanted to quit the job, my head would have been turned pretty quickly into a pink mist. If I worked for him until those three technological wonders were captured for TRI, he would have the ´tex removed. Until then he is running a jammer to block the self-destruct transmission from the SEF.&#8221; she explained, quietly.

## 

&#8220;Charming&#8221; muttered Arouin with a grim smile. After a few more moments he spotted what he had been looking for, and made a bee-line through the crowd for it. The neon sign over the door that Arouin shouldered his way through proclaimed the shop to be the &#8220;Soy-Emporium&#8221;. Yyanis followed him and watched as he walked between two aisles of boil-in-a-bag soy products, grabbing packets from their places on the half-empty shelves.

## 

&#8220;I don´t like chicken in black bean sauce.&#8221; said Yyanis, as Arouin grabbed another packet off the shelf. He turned and looked blankly at her.

## 

&#8220;And?&#8221;

## 

He turned back and walked over to the counter where a weary-looking man with dusty brown hair cast an expert glance over the collection of transparent packets in Arouin´s hands.

## 

&#8220;One twenty&#8221; he said in tones that seemed almost as tired as he looked. Arouin opened up the wallet of one Jervis Thurr, grabbed one hundred and twenty credits in the form of six twenty-credit notes, slapped them down on the counter, and walked out of the shop. A couple of Minotaur security guards walked past, the crowd giving them a wide berth. Arouin watched them pass, then headed off in the opposite direction.

## 

&#8220;Where now?&#8221; asked Yyanis, trailing him.

## 

&#8220;We need supplies.&#8221; said Arouin, distractedly

## 

&#8220;We´ve already got supplies.&#8221; pointed out Yyanis, helpfully.

## 

&#8220;_Other_ supplies.&#8221; he replied.

## 

Their meandering path through the people on the dimly-lit lowest mall level wound them through a couple more corridors until they emerged into a large cavern, where the main lighting bank flickered from time to time as falling water short-circuited the giant fluro-strip. There was a large pool of faintly glowing liquid on the floor, a couple of technicians walked slowly and carefully through it in heavily shielded clean-up suits. A couple of pedestrians waded through it in normal shoes, shaking them off on the far side before entering a curios shop.

## 

&#8220;Looks like a reactor leak again&#8221; said Arouin, glumly, staring up at the flickering light overhead.

## 

&#8220;The reactors are _above_ the mall?!&#8221;

## 

&#8220;Well&#8230; yeah. Otherwise none of these leaks would be noticed or reported, would they?&#8221;

## 

Yyanis didn´t reply, but stood there looking vaguely horrified as Arouin walked over to a ladder up to the level above them, and started to climb it.

## 

&#8220;No grav-lifts?&#8221; asked Yyanis, looking up at him. Arouin paused and turned, holding onto the ladder with one hand, then laughed.

## 

&#8220;I really wouldn´t risk it&#8221; he said, turning back and continuing up the maroon-painted rungs.

## 

On the next level they didn´t have far to go before Arouin stopped suddenly and ducked into a alcove. Yyanis went around the corner cautiously, then watched Arouin typing access codes into a public information terminal for a few seconds before taking a couple of steps closer.

## 

&#8220;Not often you find a working one&#8221; he explained to her without turning, &#8220;This one has just had all the keys spray-painted. Might be a good idea to find out if anything is going on here which we coupld take advantage of&#8221;

## 

Arouin selected ´News´, then selected his preferred publication from the available list. The screen filled with type in neat columns under the heading ´Mercenary Today´. Arouin scrolled the pages upand down for a few seconds, his eyes flickering quickly from side to side, scanning the news for anything of interest. He paused, then beckoned to Yyanis to move closer.

## 

&#8220;Listen; ´The Quantar Church has accused the Octavian Government of technology theft and say they are attempting to provoke a war between the two factions while maintaining a political high-ground. This follows a recent spate of violent incidents in Quantar Core that culminated in an attack on a Quantar research facility. _Mercenary Today_ can exclusively report that the Octavian Government is recruiting pilots and hiring mercenaries at an alarming rate through a number of proxy companys and organisations. All our loyal Octavian readers are reminded that their clan allegiance is secondary to their bond with all other Octavians; saddle up and buy some beefy weaponry, boys and girls, things are about to get interesting.´&#8221;

## 

&#8220;Faction war?&#8221;

## 

&#8220;Well&#8230; it hasn´t happened too often but it isn´t unprecedented. What surprises me is that the Quantar Church is already blaming the Octavian Government for it&#8230; and The Director doesn´t seem to be doing anything to try and calm the situation down.&#8221; Arouin rubbed the stubble on his chin thoughtfully, &#8220;Surely TRI doesn´t want the situation to inflame any more?&#8221;

## 

Yyanis shrugged, &#8220;No doubt he has good reason&#8221;

## 

&#8220;No good reason that I can see.&#8221; said Arouin, with more than a hint of suspicion in his voice as he thumbed the terminal power button and turned away.

## 

&#8220;Well, he knows background information that we can only guess at.&#8221; she said as Arouin pushed past her back onto the walkway.

## 

&#8220;Speak for yourself.&#8221; he muttered, stepping past a group of four people talking amongst themselves and gesticulating wildly.

## 

Another two levels up Arouin found what he was lookng for, a shop with a plexi-glass window so stained and dirty it was hard to see what was behind it.. Yyanis waited outside for a moment as Arouin roughly pushed the door open and went inside. The sign overhead read ´Yuri´s Small Arms´ under a red neon pistol with a yellow and orange neon muzzle flash that blinked on and off every few seconds. The door opened again and Arouin´s head poked out.

## 

&#8220;Get inside before you get mugged or something&#8221; he said, vanishing again. Yyanis sighed and followed him in. As soon as she stepped into the shop a small hatch opened in the ceiling and a turret with four large-calibre machine-guns dropped down and swivelled to aim at her. There was another nearby it which was already aiming at Arouin, who was standing by the counter watching her, as was the man behind the till. He was thick-set with short-cropped black hair and piercing green eyes. Yyanis stood still, looking up at the turret that was aimed at her, then looked over at the counter where there was a large red button marked ´Active Defence: No Not Touch´.

## 

&#8220;That´s a nice piece of woman you´ve got yourself there, Vackston. Come on in lady, it won´t do anything unless I hammer that.&#8221; the man behind the counter pointed casually at the button.

## 

&#8220;Not my woman Yuri, don´t ask. We need some of your&#8230; back-stock.&#8221;

## 

&#8220;Sure, my friend&#8221; said Yuri, grinning, as he turned around and stuffed a key into an antique lock in the door behind him.

## 

&#8220;_Vackston?_&#8221; mouthed Yyanis, silently, puzzled. Arouin shook his head quickly and put his finger to his lips. Yuri finished fighting with the lock and opened the door, walk into the room behind it and waved Arouin and Yyanis in. Yyanis went through the door first and stood in the entrance with her mouth open, staring at the room full of munitions. After a few moments of patient waiting Arouin shoved her out the way and walked past her into the room.

## 

&#8220;Why are all _these_ guns hidden,&#8221; said Yyanis, encompassing the hundreds of pieces of military hardware that were on display with a sweep of her arm, &#8220;when all the ones out _there_ aren´t?&#8221;

## 

&#8220;Well the guns in here are illegal, the ones back there weren´t.&#8221; Arouin aka Vackston explained.

## 

&#8220;Oh&#8221; she said, mollified but uncomprehending. Yuri walked over to Arouin and offered him a pistol. Arouin took it off the heavily built man and weighed it thoughtfully in one hand before looking back up at Yuri.

## 

&#8220;I need to get rid of three 9mm caseless, some ammo for then, and an 8-milli.&#8221;

## 

&#8220;How much ammo?&#8221; asked Yuri, fiddling with a pen he had pulled from his shirt pocket

## 

&#8220;Twelve clips for the nones, plus the three in them, two for the eight, plus one in it.&#8221; replied Arouin, pulling guns and ammunition out of his cavernous trenchcoat pockets and laying them on a metal transport crate beside him.

## 

&#8220;Okay, I´ll swap a pair of T&P _Bullroarers_ for that lot.&#8221;

## 

&#8220;Sounds good. Give me twenty clips with them and ten clips of thirteen point five milli by nine for mt large-cals.&#8221;

## 

&#8220;Okay.. I´ll do you that lot for&#8230; hmmm&#8230;&#8221; Yuri wiped the palm of his hand down his leg, &#8220;call it four point five, cash&#8221;

## 

&#8220;Four point five just for ammo?&#8221; said Arouin with a pained look on his face..

## 

&#8220;I thought you wanted the good stuff?&#8221; said Yuri, grinning.

## 

&#8220;Of course I want the good stuff&#8230; but&#8230; how good _is_ this exactly? Four point five K´s worth?&#8221;

## 

&#8220;Take a look&#8221; said Yuri, handing him a clip. Arouin thumbed out a round from it and held it between his finger and thumb.

## 

&#8220;What the hell is this?&#8221; he said, turning it this way and that, reflections dancing on the curved, bronze-coloured shell casing.

## 

&#8220;The big problem with the _Bullroarers_ has always been the recoil when you use a high-powder round, or the lack of penetration power ´cause of the low muzzle velocity when you use a medium-powder round.&#8221; Yuri paused, cradling the pistol in his hands, then looked up at Arouin, who nodded. &#8220;Well, someone´s found a solution. These babys have a low-powder cartridge, but the bullet itself has a rocket motor in it. Diamond tipped for penetration with tungsten carbide outer shell and a depleted uranium core to give it weight.&#8221;

## 

Arouin raised an eyebrow.

## 

&#8220;And how reliable are these?&#8221;

## 

&#8220;Well, I´ve been told there´s half a percent chance that the roket motor doesn´t fire properly&#8230; but nothing catasrophic.&#8221;

## 

&#8220;Not bad,&#8221; said Arouin, nodding, &#8220;I´ll go for it.&#8221;

## 

He opened another wallet and started shovelling cash out of it, then handed a hansomly-sized wad to Yuri.

## 

&#8220;Actually, come to think of it, I´ll take a couple of clips of that ´Diablo´ stuff for the Burleign-Thorkston. On the house.&#8221;

## 

Yuri paused mid-way through grabbing the second box of shotgun ammunition, then smiled and carried on.

## 

&#8220;There ya´ go. Have fun.&#8221;

## 

Arouin grinned, turned to go, then turned back.

## 

&#8220;I don´t suppose you know where I could get one of those _Demon_ exo-skeletons do you?&#8221; he said, with a malicious smile. Yuri laughed, a guttral belly-laugh, shook his head, then looked at Arouin again.

## 

&#8220;You´re serious as well, aren´t you?&#8221;

## 

&#8220;Am I anything but?&#8221;

## 

&#8220;I´ll speak to people who´ll speak to people, but I can´t guarantee anything.&#8221;

## 

&#8220;Thanks&#8221; said arouin, leaning over and putting another two hundred credits into the big man´s hand, then turned and started putting clips into his various pockets. After a few moments he turned and handed Yyanis the two boxed of shotgun ammunition, then looked to Yuri again.

## 

&#8220;Yuri, we´re outta here, not that we were here at any point in time anyway, okay?&#8221;

## 

&#8220;I hear you. Maybe I didn´t. Who the hell are you, anyway?&#8221; Yuri grinned.

## 

Arouin nodded to him then pushed Yyanis towards the door.

## 

&#8220;Time to go&#8221; he murmured. They walked out of the shop and onto the walkway outside, Arouin looked over the side of the railing to the floor, three levels below them, didn´t notice anything amiss, and turned back to Yyanis.

## 

&#8220;Time to go to work&#8221; he said grimly.

## 

## Part XXII

## 

Arouin and Yyanis walked quietly along increasingly deserted corridors as they neared the Research and Development module of Octavius Core station. The interior decor was improving as they went futher; sometimes officials from other factions visited this type of place, part of official tours and suchlike.

## 

_And wouldn´t it be terrible if they got the wrong impression of what Octavian stations are like?_ thought Arouin, bitterly.

## 

The walls were beige and, for a change, didn´t actually look as though they were made from the cheapest sheet metal that was available at the time. There were occasional pictures on the walls, and most of the lighting was actually working. Signs on the wall pointed down the corridor towards the ´Imperial Centre for Technological Investigation´, and after a few more minutes of walking, following the signs, the corridor was almost completely devoid of people.

## 

&#8220;How are you planning to&#8230; uh&#8230;&#8221; Yyanis bit her lip, &#8220;tackle security?&#8221;

## 

&#8220;I thought you were the one with all the ideas?&#8221; he said after a few moments.

## 

&#8220;Okay.. have you looked up any design information on the place? Thought about bribing the guards?&#8221; she suggested.

## 

Arouin walked over to one side of the corridor, turned around, and leant back on the wall.

## 

&#8220;Bribing the guards won´t work,&#8221; he explained, &#8220;because most of them don´t do it for the pay anyway; they do it for the action. Then there´s the problem that there´s a contingent of about 60 troops in there; even if they were all corruptible, which they won´t be, we haven´t got enough money for that many bribes.&#8221; Arouin pulled out one of the two new pistols and worked the action a couple of times as he spoke, &#8220;As for plans of this facility; who do you think you are dealing with here? If there´s a decently-size vent enterance into this place it´s only there to serve as a trap.&#8221;

## 

&#8220;So what are you going to do?&#8221; she said, looking at him seriously.

## 

&#8220;Well, I don´t see too many options myself&#8221; he said, pulling out a clip and checking it over, then replacing it. &#8220;We could always ask nicely if they would let us in to take away the power generator. Doing that would certainly reduce the body-count.&#8221;

## 

&#8220;It would?&#8221;

## 

&#8220;Yeah&#8221; he said wryly, &#8220;There would be only two bodies.&#8221;

## 

&#8220;Look,&#8221; said Yyanis, narrowing her eyes, &#8220;violence _can´t_ be the only way.&#8221;

## 

&#8220;It´s not _always_ the only way, but it does seem to be at the moment, doesn´t it? Be sure to tell me if you spot some other method. For the moment stay out of the fight and watch for anyone coming up behind us, okay?&#8221;

## 

&#8220;I´m not happy about this&#8221;

## 

&#8220;Oh no,&#8221; said Arouin, pulling one of the new pistols out of his pocket and slamming a clip into it, &#8220;whatever shall I do?&#8221;

## 

He started walking down the corridor towards the research facility, hands in his pockets. Yyanis stayed ten paces behind him, looking wary and more than slightly nervous as they walked around the last bend of the corridor and arived before the doors of the Imperial Centre for Technological Investigation. There was a distinctly different feeling to the Octavian R&D area compared with, say, the facility in Quantar Core. Sliding plexi-glass doors opened into a large hallway with a heavily manned security desk and detectors on either side of the single corridor leading away from it. Behind each of the desks were four or five heavily armoured_Minotaur_ guards; they doubled as shock troops for the Octavian government whenever boarding actions were needed, and as a result had a healthy reputation as the toughest security or police force anywhere in TRI space. As well as the men behind the two desks there were two others, standing in front of the metal detectors. Wearing _Demon_ class exo-skeletons. The _Demon_ was created by a healthily paranoid Octavian government, who knew that they only retained power by being slightly stronger than any other major clan who could vie with them for the leadership. The _Demon_ was created to make them more than _slightly_stronger. A miniature power field, designed around the same ´Pod´ class shield that was used in many smaller space-going vessels, encased the network of titanium that surrounded the pilot of the suit; beyond the shield inch-thick titanium plating. All coupled together with a negative-feedback system for control and a miniaturized fusion generator made the _Demon_ a very, very nasty thing indeed.

## 

_Oh shit._ thought Arouin.

## 

The guards and the two Demon pilots had turned to look at them when they walked through the doors, and Arouin found himself the center of a great deal of largely unwanted attention. Yyanis quietly padded up behind him.

## 

&#8220;When do you start shooting?&#8221; she hissed into his ear.

## 

&#8220;When I can do it without being killed faster than I can blink&#8221; he muttered back.

## 

_Here goes._

## 

Arouin walked towards the closest security desk, nodding at one of the Demon pilots as he went past, and leant over it, looking at the most senior of the security guards present.

## 

&#8220;Hi there,&#8221; he grinned, franticly looking over the paperwork on the desk in front of the man. &#8220;My associate, Ms Hurrein, and I have just arrived here from Solrain Core station. We are from the Bure have a certain item that Professor&#8230; Duvein needs.&#8221;

## 

The man raised an eyebrow.

## 

&#8220;What is it? We´ll have it sent up to him&#8221;

## 

_Well, I managed to read the name upside-down. That´s something. Doesn´t count for much, but I tried._ thought Arouin, reaching into his pocket for a pistol,_might as well go out with a bang_.

## 

Yyanis saw Arouin´s hand moving down into his pocket, wrapping around something bulky, starting to move up. She grabbed his hand by the wrist and pushed it back down into the pocket, leaning forward to get the attention of the guard.

## 

&#8220;We can´t tell you what it is&#8230; or show you. I can´t confirm anything&#8230; but you might have heard some&#8230; rumours of late. About certain espionage operations against the Quantar?&#8221;

## 

The officer grinned, &#8220;Yeah, I´ve heard one or two things&#8230; one of our guys is supposed to have bust in there and&#8230; uh&#8230; ´retrieved´ some things that the brass wanted.&#8221;

## 

&#8220;Well, I can´t confirm anything out here, as you may have guessed&#8230; but you might be getting some more rumours about incidents in Sol Core.&#8221; Yyanis said slyly.

## 

&#8220;Is that so? Well, I´ll take you to the professor myself&#8230; might even get a look-see at this fancy new technology.&#8221; the man grinned a broad grin and started walking around the desk.

## 

_Nice one Yyanis,_ thought Arouin, _we´re home free_

## 

The officer looked over at one of the Demon pilots.

## 

&#8220;Padre, fall in, we´re taking these guys to Doc Duvein.&#8221; he turned to Yyanis with a smile, &#8220;Sorry miss, but we´ve got to be safe, in case you´re not who you say you are&#8221;

## 

&#8220;Oh no problem, we just want to get this to the professor as soon as possible&#8221; replied Yyanis, smoothly.

## 

_Maybe we´re not home free_

## 

Arouin looked at Yyanis smiling a 24-carat smile out of the corner of his eye, then held his hand out in front of him.

## 

&#8220;Lead the way, commander&#8221;

## 

The man swelled visibly at the senior, if inaccurate, rank verbally bestowed on him by Arouin, and walked through the detectors with Arouin and Yyanis in tow. As he went through the detectors an alarm went off, a horrible screeching sound accompanied by rotating red lights. The officer turned around quickly, &#8220;Shut that damn thing off&#8221;

## 

&#8220;Sorry sir&#8221; muttered one of his subordinates, punching at the relevant controls in front of him. After another couple of seconds the horrific noise shut off, only for the entire episode to be repeated, with an even more irate officer, when the Demon pilot went through the detectors in his powered exo-skeleton.

## 

&#8220;Sorry about that,&#8221; said the guard as they walked down the corridor, &#8220;can´t get the bloody staff these days&#8221;.

## 

Arouin looked over his shoulder at the Demon pilot´s face as they walked. The man was staring down the corridor, obviously bored out of his mind. Arouin casually slipped one hand into his right pocket. The ´click´ sounded much louder than it should have, but none of the others seemed to have noticed, in fact the guard officer was giving a thoroughly boring monologue on how he ended up in command of the soldiers at the facility. And there was no tell-tale shimmering in front of his face showing that the force-field was activated. Arouin turned to the Demon pilot, stopping suddenly.

## 

&#8220;Excuse me&#8221; he muttered.

## 

The man seemed to snap out of his trance-like state and leant over to bring his face three feet down and on a level with Arouin´s.

## 

&#8220;Pardon?&#8221;

## 

There was a blur of movement as Arouin whipped the pistol out and fired twice into the pilot´s face, then span to face the officer who was turning towards him. Yyanis was standing frozen in shock; he hadn´t given any warning to her of what he was going to do, but she was thankfully clear of the line of fire. Another pair of shots roared from the muzzle of the pistol, knocking the man to the floor as they slammed into the chest-plate of his armour.. Arouin stepped forward quickly and pumped another five rounds into the man as he struggled to get up, being beaten down by the impacts again and again until he finally lay still in a spreading pool of blood. Yyanis was staring at him, but he didn´t have time to deal with her.

## 

&#8220;Get under cover you stupid bitch!&#8221; he shouted at her as he span and ran to the fallen _Demon_. As Yyanis kicked at a door Arouin undid the catches keeping the exo-skeleton fastened closed, heaved the chest plate off, then struggled to remove the body of the pilot from inside. Shouts were beginning to echo down the corridor as Arouin pulled himself into the suit, brought the chest plate back down, then secured it.

## 

&#8220;Systems initialised&#8221; the suit informed him in quiet feminine tones, the HUD powered up, Arouin felt the suit around him change from an immobile hulk of scrap metal to a responsive machine under his command. Yyanis finally managed to kick the door down and scrambled into the room behind it, leaving Arouin inside the_Demon_, lying on his back in the middle of the corridor. He got up carefully, tearing handholds in the walls of the corridor to help him, looking down the length of it at a mass of guards charging down the corridor towards him. The first shots whined around the suit, then he felt the impact as they started to find their mark on the hulking mass of animated metal.

## 

_Where the hell are the controls for this thing? It´s built for guards, for fucking grunts, it can´t be that hard to control&#8230;_ he thought desperately.

## 

Another fusilade of shots hammered into the suit, shaking him about, others missed and chewed up pieces of the walls, throwing lumps of metal and sheet plastic into the corridor.

## 

_No hand controls&#8230; feedback system&#8230; so&#8230; must be&#8230;_

## 

Aruin looked up, a glint in his eye.

## 

&#8220;Shield on&#8221;

## 

&#8220;Shield.. activated.&#8221;

## 

The troops running towards him slowed, spreading out, firing constantly, but the shots were no longer striking metal and cratering the armour. Small green hazy patches of air marked where the projectiles hit his shielding system, ricocheting off into the walls, floor, and ceiling, the roar of small arms fire was deafening. Arouin turned and walked through a doorway into an adjoining room to get some cover, pieces of metal and plastic distorting out of shape and falling to the ground around him as he did so, the suit too large for the small door into, as it turned out, an empty cafeteria.

## 

&#8220;Suit&#8221;

## 

&#8220;Yes?&#8221;

## 

&#8220;What weapons do I have available?&#8221; he asked, the chatter of weapon fire dying down in the corridor outside.

## 

&#8220;On the MkIV _Demon_ class exo-skeletal suit there are two _Chainsaw_ 3.35mm chain-cannon, one high-power welding laser, one Xeon-based laser cannon, and one_Vorry_ anti-personnel grenade launcher&#8221; the voice informed him.

## 

&#8220;Time to rock&#8221; he murmured, then in a louder voice, &#8220;Select Chainsaw&#8221;

## 

Two pods on the undersides of the lower arms of the suit opened and a circular arrangement of nine barrels smoothly moved out. Arouin turned and walked out of the room, back into the fire zone.

## 

The dust that had been raised in the short and inconclusive firefight in the corridor was beginning to settle, the group of Minotaur troops firing off an occasional burst into the hole where the suit was last seen, in an attempt to keep Arouin pinned down. The first ´crunch´ of the suit stepping on the layered rubble just out of sight from the troops sent them scurrying to try and find cover, a couple of them finding a nice spot where a whole wall section had caved in, others taking cover in doorways or behind pillars that were set into the walls down the corridor. Then Arouin moved into sight, and there was a whine as the motors span the barrels up to speed, the barrels blurring into a seemly solid cylinder of gunmetal. The troops held their ground, badly disciplined but eager for action, then the chainguns opened up. The twin flares were as bright as burning magnesium, a hot, white flare lighting up the corridor and the men who were about to die. Arouin, sitting inside the suit, watched as wherever he pointed twinned lines of destruction cut through anything he aimed at, men virtually torn in half by the hail of lead. The ammunition counter in the heads-up-display in front of Arouin´s eyes was a blur, the individual figures vanishing faster than the eye could take in their presence, individual shots blurred into a constant roar that battered at the senses relentlessly. After three seconds it was over.. Bodies lay strewn throughout the wreckage, often lying in more than one place. There were no individual bullet-holes, just lines of torn metal and plastic where the destruction had been wrought. The chainguns wound down, the only noise that he could hear, and then, for a moment, everything was eerily silent. Arouin turned to the doorway which Yyanis had bolted for cover through and looked into the darkness inside. Yyanis was there, sitting in one corner of the room with the shotgun aimed at the door.

## 

&#8220;You´re still alive?&#8221; she said, eventually.

## 

&#8220;I got in the suit.&#8221; he said simply, &#8220;No wonder the government isn´t worried about another clan making a take-over bid when they have these.&#8221; Arouin turned back to look at the corridor, then turned away again, shaking his head.

## 

&#8220;Suit, give me a map of the facility.&#8221;

## 

An image of a hexagonal area appeared on the HUD in front of Arouin´s eyes as Yyanis edged around the semi-destroyed doorway and looked down the corridor.

## 

&#8220;Arouin&#8230;&#8221; she turned back, looking appalled, &#8220;that thing is just wrong&#8230; any sort of civilian uprising, any arguement with the government&#8230;&#8221;

## 

&#8220;Yes.&#8221; Arouin nodded slowly, &#8220;They will deploy the _Demon_ class suits.. Why do you think there haven´t been any riots lately? Everyone here is scared of what these can do.&#8221; Arouin went back to studying the map of the research facility on his HUD as Yyanis stood in silence.

## 

&#8220;We need to stop these things&#8221; she said quietly, after a minute or so.

## 

&#8220;Well, I´m not getting paid for it, so it´s nothing to do with me&#8221; said Arouin, distractedly, eyes locked on the glowing green lines etched in the air inces in front of his face. After a few more moments he stirred.

## 

&#8220;Found it. Stay behind me, I´m going to go straight to the high security lab; that´s where they will be storing the power generator.&#8221;

## 

&#8220;Straight to the lab?&#8221;

## 

&#8220;Oh yes,&#8221; said Arouin, turning to face the nearest wall at a slight angle, with a glint in his eye, &#8220;_straight_ there.&#8221;

## 

## Part XXIII

*The firing had been going on for quite a while, most of the scientific staff had hurried for shelter in the areas of facility away from the labs &#8211; what any intruder would be heading towards. Dr Svaris Uzen was franticly grabbing his latest batch of readouts from a hard-copy printer in the deserted lab, looking around in a panic, waiting for the printer to finish spitting out the last piece of paper. In the distance, the horrendous roar of weaponry ceased suddenly, and Dr Uzen froze again for a second as a quietness fell upon the lab for the first time in a few minutes.*

_Obviously,_ he thought, _the intruders have been neutralised_, and he took a little more time to wander around the lab, picking up loose pens and discarded scraps of paper from the worktops, waiting for the announcement over the tannoy. After a few seconds there was a distant sound of tearing metal, and the doctor looked up from a stack of paper that he had been shuffling through trying to find the results of an earlier test, and froze again. He slowly turned. There was another sound of tortured metal, a loud clang, and a scream. It was noticeably closer. The doctor grabbed the last few pieces he needed, and began to head for the labaratory door, looking much more nervous than before. It dawned on him that the stop of the automatic weapons fire didn´t necessarily mean that the security detachment had stopped the invading force, and he broke into a run towards one of the lab doors. There was another screeching sound as metal was deformed, and it was much closer. The doctor ran to the retinal scanner by the door, took off his glasses, and looked into the visor attached to the scanner. As the mechanism whirred quietly in front of him there was a series of metallic thuds which made the walls reverberate slightly.

## 

&#8220;Please hold still while the retinal scan takes place&#8221; a quiet, synthesised voice told him, &#8220;retinal scan re-starting&#8221;

## 

There was a horrible crash and screech of metal to the doctor´s right hand side as the machine intoned.

## 

&#8220;Thank you.&#8221;

## 

The doctor hurriedly backed away from the scanner and moved to the now-opening door, but glanced back for a second at the source of the noises. There was a fist stuck _through_ the metal of the wall, gleaming in the light, flexing, then withdrawing. The doctor ran through the open door and the temporarily-deactivated defence mechanisms, down the corridor, franticly trying to get away from the lab and whatever was trying to get in.

## 

Arouin stood inside the office in the _Demon_ powered exo-skeleton for a moment, then stared kicking at the buckled metal around the area of the wall which he had just punched through. Sparks flew as metal smashed against metal, the wall tearing and warping, showing a gleaming silvery sheen along the freshly created edges. After a few seconds of energetic bludgeoning, there was a large enough hole for Arouin to force himself and the exo-skeleton through. Yyanis followed a few seconds behind him, and gazed around the interior of the room as she stood amongst glittering fragments of the wall which were strewn all over the floor. Behind them streched a long, untidy tunnel of destruction, a dozen or so rooms with their walls unceremoniously smashed through, leading all the way to the corridor in which Arouin had fought the facility´s guard. The lab´s low granite-coloured tables were covered with paper, pens, and delicate looking instruments and tools, virtually none of which Arouin recognised, some papers lay on the floor scattered and looking slightly mangled where running people had trampled them. The walls were a faded yellow, smudged with dirt and scorch marks, the floor tiled with an off-putting shade of orange. In the approximate center of the room lay two two octagonal tables, a few meters away from each other. The sides of the tables had various instruments attached to them and leads running from the instruments and attached in various places to the the objects in the centers of the the two tables. Arouin stood there, looking at them as Yyanis edged past.

## 

&#8220;_Two_ artifacts here?&#8221; said Arouin, staring at them. One was boxy, but fairly small, about the size of a shoebox, with a grey-silver sheen and glowing blue embedded filaments. The other was a brown-red toroid with two sockets on either side of it.

## 

&#8220;Well,&#8221; said Yyanis, &#8220;one isn´t a relic, it´s made with our technology, so technically no.&#8221;

## 

Arouin looked at her.

## 

&#8220;Okay, you can call it a relic. So there are two here, someone managed to get hold of the other one.&#8221;

## 

&#8220;Looks like it.&#8221; said Arouin, surprised, &#8220;Which one´s which?&#8221;

## 

Yyanis walked over to the octagonal desks, looked at a terminal attached to one, and deftly tapped at the keyboard.

## 

&#8220;This is the regulator&#8230;&#8221; she said, walking to the other desk, &#8220;Which means that this one should be the positron coupling&#8221;

## 

&#8220;Uh&#8230; no&#8221; said Arouin, with a troubled expression.

## 

&#8220;What?&#8221; said Yyanis, puzzled, and turned to look at him as the terminal behind her flashed up a new screen. She turned. The title at the top said &#8220;Pre-collapse Power Generator, _Titus_&#8220;. She turned back, one hand on her hip. Arouin was looking uncomfortable as he remembered something.

## 

&#8220;Arouin&#8230; you were meant to get the power generator at Quantar Core, weren´t you?&#8221; she said, slowly.

## 

&#8220;Uh&#8230; I thought I brought back the positron coupling&#8221; he replied, unpleasant memories surfacing&#8230;

## 

_&#8220;&#8230;the Quantar now had the plans for the power regulator and the only prototype, and the positron coupling&#8230; &#8230;Unfortunately for them, it seems that they don´t _actually_ have it &#8211; they have a replica&#8230; &#8230;The Quantar Heavy Transport Vessel &#8220;Methodical&#8221; which brought the coupling back&#8230; &#8230;seems to have had a traitor on board&#8230; &#8230;disguised the power coupling as a coffee machine&#8230; &#8230;and sent it off via normal recorded delivery to his Solrain masters&#8230;&#8221;_

## 

&#8220;Oh shit.&#8221; whispered Arouin, face going slightly paler than normal.

## 

&#8220;You must have brought back the replica. The fake.&#8221; Yyanis said, shaking her head, &#8220;look, let´s worry about that later. We´ve got two of the items he wants here&#8230; lets get out of here. What defence mechanisms are there here?&#8221;

## 

Arouin slowly shook his head murmuring &#8220;&#8230;_fucking idiot, **fucking**_ idiot why didn´t I remember?&#8221;

## 

&#8220;Will you _please_ snap out of it?&#8221; she said, impatiently.

## 

&#8220;Oh there won´t be anything there&#8221; said Arouin, loking at her blankly, &#8220;lab defence mechanism will be based on stopping people getting in. We don´t do high tech laser traps,&#8221; he smiled slightly, &#8220;we do guns.&#8221;

## 

Yyanis shook her head, and reached over the instruments to grab the power generator, then having to yank it until all the leads connected to it fell off. She set it down on the floor, then retrieved the power regulator from the other table and put it down beside it. She looked up at Arouin,

*&#8220;Where can I carry them?&#8221; she asked.*

&#8220;Don´t look at me,&#8221; said Arouin, looking pointedly at the exo-skeleton surrounding him, &#8220;I can´t fit anything in my pocket when I´m wearing this thing.. You´ll just have to carry it I guess&#8221;

## 

Yyanis sighed, stooped, and picked up the generator and the regulator, one under each arm.

## 

&#8220;Weapons lock detected&#8221;

## 

&#8220;Pardon?&#8221; she said looking up at Arouin. Arouin looked at her for a moment.

## 

&#8220;Ah. Shit.&#8221; he said, and turned around already spinning up the chainguns as he did so. The roar of the same _Chainsaw_ chainguns came from from other end of the self-made corridor, and the shots started smashing into Arouin´s shield for a few seconds before his own chainguns echoed the scream of the rapid-fire weapon being targetted on him as they were brought to bear on the other _Demon_ suit. The other pilot´s shield lit up green, flickering with amazing rapidity as Arouin´s blurred chaingun barrels spat out hundreds of rounds a second, the return fire lighting up Arouin´s shield likewise. Yyanis was crawling across the floor, she got behind cover, set the two artifacts down, and pulled the pistol from her pocket before going to one knee and pumping the trigger. The blazing destruction of the guard´s chainguns swept clear of Arouin for a second and tried to bear on Yyanis, who grabbed the artifacts and threw herself out the way as the lines of high-velocity metal chewed through the desk she had been hiding behind, cutting it in half. Arouin walked forward, his teeth gritted, taking up the DIY entrance into the lab, not allowing any rounds past into the lab.

## 

&#8220;Shields at 50 percent&#8221; chimed the feminine voice in his ear. The targetting information showed the guard´s _Demon_ on fifty-nine percent shields, going down equally as fast as Arouin´s.

## 

*Shit. These are exactly the same. No advantages.* thought Arouin, realising his plight as he held down the triggers. Then he started walking towards the other_Demon, chainguns still whirling faster than the brain could comprehend, stepping through the smashed walls, shield flickering constantly. After another twenty seconds of the incessant barrage of lethal projectiles, Arouin was close enough to see the other pilot´s face; contorted with rage as he stood over the bodies of his fallen comrades. Arouin broke his aim away from the other _Demon_ and aimed at the ceiling. He made a circling motion with the chainguns, a curved line arcing along the ceiling as the bullets ploughed through the metal, the other pilot looking up suddenly as he kept his fingers jammed on the triggers, still aimed at Arouin. The arc, in a split second, lengthened, reached it´s zenith, came back, reached another zenith, then joined with it´s own start. There was a scream of metal, heard even above the deafening roar of the four chainguns, and the ceiling fell in. The oval-shaped block of steel, aluminium, and reinforced concrete ceiling fell in, a cloud of dust enveloping the scene from which metal fragments shot out as if from a grenade. Arouin stepped forward slowly as the dust cleared, his chainguns winding down with the characteristic whine. There was still a muted roar from the cloud of settling dust; then the dust cleared. There was a crumbled heap of debris with one battered metal foot poking out from the side of it, and a steady, quiet roaring. Yyanis, back in the lab, picked herself up and dusted herself off, then, with the artifacts in hand, slowly edged down the facility´s newest corridor. Suddenly the muted roar sprang into the full-bodied scream of the chainguns and a block of concrete on one side of the pile of debris, about half way up the heap, shattered. A hail of bullets shot out of it, then chewed a hole through the ceiling. On the other side a piece of sheet metal suddenly flew off of the pile, thrown into the air and then chopped up mid-flight into two separate pieces which clattered to the ground. The chainguns roared on, pieces of the pile falling down into the holes made by the passage of the bullets, then flying out as they themselves were hit. Arouin looked on, amazed. After a few more seconds, the screaming abruptly stopped and there the most horrible sound Arouin had heard started, repeating again and again at the same rate as the firing.

## 

The click. You fought. You did your best, you learnt your craft and honed it to the ultimate, final, stage; perfection. You thought like lightning, you _fought_ like lightning. No fear. Adrenaline pounding through your veins. Reactions faster than a snake´s strike at it´s prey. You became one with your weapons, felt their balance, maintained them, took care of them. You had your rituals, you knew them and stuck to them, you had your own faith; a faith in yourself. Your gun kicked in your hand. But all for nothing, if you heard that click. _No bullet in the chamber_.

## 

The noise almost sounded like a gun itself; the clicks were in such rapid sucession; Arouin had to scream to Yyanis over the sound,

## 

&#8220;Let´s get the hell out of here!&#8221;

Nothing; silence.

Arouin turned, and ran back down the corridor. Half-way down Yyanis was stitting at a desk in one of the offices they had carved through, franticly hammering away at the keyboard in front of a terminal screen.

## 

&#8220;What the hell are you doing?&#8221; asked Arouin incredulously, far enough away from the sound that he didn´t have to scream at the top of his lungs.

## 

&#8220;I´m not letting any more of those get built!&#8221; she said, looking at him with a hatred in her eyes, &#8220;They´re inhuman!&#8221;

## 

&#8220;How easily can you do it?&#8221; he asked.

## 

&#8220;This guy,&#8221; she said, pointing at a sawn-in-half body on the floor, &#8220;must have been working at this terminal when the fight was going on; it´s still logged on. I´m looking for a way to stop the production&#8230; permanently.&#8221;

## 

&#8220;Sounds like fun. Why don´t I just wander in there with a load of ´tex and blow everything up?&#8221; Arouin pointed out.

## 

&#8220;You would need a _lot_ of ´tex for that&#8221;

## 

&#8220;You could wait here and I´ll go and get some&#8221;

## 

&#8220;I think that would be a bad idea. There are going to be some people arriving who are going to br pretty pissed off at loosing their _Demon_ production facility.&#8221;

## 

&#8220;Hmm&#8230; I see your point.&#8221; conceeded Arouin. Yyanis´ hands flickered over the keyboard, tapping away at the keys, schematics and controls systems popping up briefly on the screen before being replaced by something else.

## 

&#8220;Catastrophic experimental faliure, code Osidian?&#8221; she said, turning to look at Arouin.

## 

Arouin shrugged, the exo-skeleton moving with him perfectly, then paused.

*&#8220;Suit&#8230; what is a Catasrophic experimental faliure, code obsidian?&#8221; he asked, tentatively. Yyanis stood up and walked over to Arouin to listen.*

&#8220;Code Obsidian Experimental Faliure is an experiment going critical. The section of the research and development module of Octavius core which the catasrophic experimental faliure has taken place has all bulkheads leading to it sealed and is then jettisoned into space. This is to prevent large scale damage to the station which may result from a catastrophic experiment faliure taking place. This method of protection is code Obsidian. If only the bulkheads are sealed, and the section of the research and development module is not jettisoned, then the method is code Jade.&#8221;

## 

Arouin looked at Yyanis. &#8220;Jackpot.&#8221; he whispered. Yyanis turned to the terminal and hurriedly tapped at the keys. After a few seconds she paused, and looked up from the screen, expectantly. Nothing happened. She looked back down, hit the ´accept´ button again, and looked back up expectantly again. Suddenly the lighting flickered to red and those speakers mounted in the ceiling which were not damaged hummed into life.

## 

&#8220;Warning&#8221; announced the sythesized voice, &#8220;Code Obsidian Implemented in Section Five. Demon production facilities and storage bays will be jettisoned in thirty seconds. Twenty nine. Twenty eight. Twenty seven&#8230;&#8221;

## 

Yyanis stood up, looked around wildly, then back at Arouin.

## 

&#8220;Let´s get out of here, quick!&#8221;

## 

&#8220;Why such a rush. It isn´t going to blow up.&#8221;

## 

&#8220;Oh yeah&#8230; uh&#8230; sorry, you know, the count-down and all.&#8221;

## 

&#8220;Ah.&#8221;

## 

Arouin opened the release catch on the outside of the exoskeleton, the chest plate swung up, the fuzziness caused by the active shields faded, and he climbed out.

## 

&#8220;Well,&#8221; he muttered, hand reaching into the pocket of his trenchcoat, &#8220;wasn´t that fun?&#8221;

## 

He pulled out a large calibre pistol in one hand, held it at arm´s length, then emptied a clip into the unarmoured inside of the exo-skeleton. There was a satisfying crackle of electricity and a smell of ozone wafted past Arouin as he turned to Yyanis.

## 

&#8220;Now we go.&#8221;

## 

He turned and walked through the gaping row of holes in the walls then picked his way carefully around the pile of debris and winced at the high-speed clicking, Yyanis following him. They walked back down the corridor towards the doors into the foyer, Arouin slowing down a little so Yyanis could catch up.

## 

&#8220;Guess what?&#8221; he asked.

## 

&#8220;What?&#8221; Yyanis replied, tiredly.

## 

&#8220;We´ve just started a civil war.&#8221;

## 

As Yyanis turned to look at him with startled questions on her lips, the count concluded,

## 

&#8220;Zero.&#8221;

## 

There was a faint shudder and a triangular slice of the research and development was spewed into space, spinning slowly out into the void.

## 

_&#8220;Oh **shit**! Sir, sir, come and take a look at this!&#8221;_

## 

&#8220;What is it, private?&#8221;

## 

_&#8220;The research and development module has had a catasrophic faliure in the _Demon_ production and storage sector &#8211; it´s been jettisoned!&#8221;_

## 

The man behind him smiled, out of sight of the private.

## 

&#8220;And what does that mean, young man?&#8221;

## 

&#8220;It&#8230; it means that Clan Torriaus no longer has the technological advantage&#8230; there´ll be a power struggle again sir! We´re going to loose power!&#8221;

## 

_&#8220;Wrong, private,&#8221; the older man said, smiling, as he finished screwing the silencer onto the end of his pistol and aimed at the back of the other man´s head, &#8220;it means that _you´re_ going to loose power&#8221;_

## 

Yyanis looked worriedly at Arouin.

## 

&#8220;What do you mean?&#8221;

## 

&#8220;The Octavian government is made of the strongest clan, which was, up until now, clan Torriaus. We´ve just jettisoned thier strangle-hold on Clan Praetus; Clan Praetus will take control.&#8221; explained Arouin

## 

&#8220;Take&#8230; control?&#8221;

## 

&#8220;Oh yes. The registered Clan activists, those who would make up the clan´s army if it was in government, will be arming themselves as we speak. Anybody they´ve managed to infiltrate into government installations will be helping, disrupting Clan Torriaus communications.&#8221; Arouin explained, calmly, as they continued walking.

## 

&#8220;They fight?&#8221;

## 

Arouin laughed, &#8220;Oh yes, two or three thousand casulaties usually, maybe another thousand civilians.&#8221;

## 

_The main lights flickered in Mall 4, then died, civillians ran for cover in nearby shops, trying to make their way home in the near-darkness. They knew what was coming; it had been broadcast over the main speakers seconds earlier. Clan Praetus were making the first moves against the primarily Torriaus area of Mall 4. It was not a good time to be on the streets. Footsteps hammered at the ground as people made their way to their homes before the fighting started. A few_Demon_ troops wandered the mall &#8211; three of the only five that weren´t on the research and development module when they were jettisoned. People ran along the walkways, the metal twanging from the footsteps, evryone was glancing around nervously. Glimmers of light appeared from the east hallway; lights mounted on the combat armour of Praetus troops, bobbing up and down with the men´s movement as they came around the corner. Those people still on the ground ran for the nearest ladders to try and get out of the area. One of the three _Demons_ noticed the lights, alerted his comrades, then radioed in for support. A hazy field sprung up around each of the three suited Torriaus fighters, then there was a sudden flare of light as a shoulder-mounted rocket laucher was fired, a split second of peace, a blur of movement, then the blast as the missile hit, throwing burning bits of debris up high into the air. The shockwave knocked people to the floor, even those high above the action, and there was a renewed fervour to the movement of people as they struggled to their feet and tried to get away from the area. There was another flare and a second rocket shot down the hall, slamming into one of the _Demons_ and sending the other flying. The last suited fighter managed to get his chainguns firing and the stream of death from the twin guns cut down six of the Praetus troops before a third rocket went flying down the hallway, impacted, and blew the exo-skeleton into three pieces. There was a moments pause as pieces of exo-skeleton and human clattered to the ground in the half-light, then a thunder of feet as the Praetus troops stormed past the remains of the Torriaus clan soldiers who had so briefly opposed them&#8230;_

## 

The door into the foyer opened with a swish and Arouin poked his head around quickly, then pulled it back.

## 

&#8220;Looks clear&#8221; he muttered.

## 

He took a longer look, then paced around the corner slowly, a pistol in each hand, raised, ready for some opposition.

## 

&#8220;Looks all quiet, let´s get out of here and fast&#8221; he said, turning to Yyanis.

*She nodded her agreement and they ran out of the main doors and down the corridor away from the research and development module as small-arms fire clattered in the distance.*

&nbsp;

## Part XXIV

## 

The man ran out of a side corridor, stumbling as he turned the corner, bouncing off the opposite side of the corridor as the sharp crack of a pistol send bits of the wall flying into the air. He had his hands over the back of his head and almost doubled over as he ran, shaking with fear, towards Arouin. Yyanis pulled the shotgun into her hands from behind her back, then dashed to one side of the corridor, working the pump action and kneeling behind a handy pillar. Arouin watched with interest as the man run past him, down the corridor, seemingly less worried about where he was running to than what he was running from. Arouin pulled out a large calibre pistol as a stampede of footsteps approached, and took a few careful steps to the entrance into the corridor which the man had run from. Then, as the footfalls came steadily closer and increased in volume, he edged his shoulder around the corner and pointed the pistol out backwards, then started pumping at the trigger. There was a shout just before the first gunshot, then the sounds of shots, the tinkle of shell casings falling on the floor like brass rain, and the scream of those hit by the blind firing all melded into a cacophony of noise. Then there was a moment of silence, the metallic ring and clatter of the last casing completing its parabola, and a click as Arouin dropped the clip out of the pistol. Yyanis edged forward, shotgun trained on the corner, with a quietly determined look on her face. Arouin, face blank, slammed another clip home and recocked the pistol, paused, then, holding the pistol in his left hand, swung himself around the corner with his right, letting himself fall as he did so. There was a sharp crack and Arouin felt the wind of the bullet´s passing on his face as he slid beneath it, training his pistol on the source of the muzzle flash and working the trigger once, twice, three times. There was a shadowy movement down the badly-lit corridor as Yyanis edged around the corner, looked down at Arouin, lying on his back next to a body with a hole through it´s throat. Arouin heard her footstep and looked backwards, Yyanis´ figure towering over him, inverted. He heard another footstep, Yyanis froze, looking down at him as his face contorted.

## 

&#8220;**GET DOWN**&#8221; he screamed, lashing out at her leg with his right fist, making contact with her shin and knocking her legs out from under her. Yyanis lurched to one side as the submachinegun opened up, bullets tracking from left to right across the corridor. The smack and metallic twang of bullets slamming into the wall and ricocheting filled the air as pock-marks raced along the corridor wall towards Yyanis´ falling body. Arouin´s head snapped around, his right hand coming up to cradle the base of his pistol as the trigger became almost a blur, spitting out the magazine´s clip in just over a second. Yyanis hit the floor beside him heavily, then scrambled back up to her feet as Arouin used a large hole in the panelling of the corridor to help him get off the floor. The shadowy figure was kneeling at the end of the corridor, one hand on the wall, the other fighting to lift the submachinegun it still held in one dark hand. Yyanis brought the shotgun up to eye level, aimed it, then pulled the trigger. There was a loud click. The sub was slowly moving up, shaking furiously. Arouin leaned forward, grabbed Yyanis´ left arm by the wrist, then moved it back and forth, cocking the shotgun. Yyanis pulled the trigger again and the corridor lit up for a slit second, highlighting the figure´s stained, sweaty face before hurling the body backwards onto the floor.

## 

&#8220;Always remember to cock it. But don´t cock it before you put it on your back again, or you´re likely to blow your head off.&#8221; he explained. Yyanis was still looking down the corridor at the silhouette of the body on the floor, back-lit by a fallen, yet still working, fluro-strip.

## 

&#8220;So I see.&#8221; she replied with surprising calmness as she lowered the shotgun.. Arouin looked back at Yyanis for a moment longer, her lips a thin line, then glanced back along the corridor, strewn with bodies.

## 

&#8220;We need to get back to the ship, then get the hell out of here. Octavius Core station will not be any more pleasant than it is noramlly if there´s a civil war going on, although it should only go on for a few days at most&#8221;

## 

&#8220;How far to the ship?&#8221; she asked, looking at him once more. Arouin shrugged,

## 

&#8220;Twenty minutes, give or take; we´ve got to get down about five levels and cross about half the station. Then we´ve got to get to the ship and get it, and us, out of this station in one piece.&#8221;

## 

&#8220;Sounds like fun&#8221; said Yyanis, smiling slightly.

## 

&#8220;Oh,&#8221; replied Arouin, drawing out a second large-calibre pistol and looking down the corridor with a steely gaze, &#8220;I wouldn´t miss it for the world&#8221;.

## 

They walked cautiously down the corridor towards one of the main mall hallways, the doors to the appartments on either side all shut. As they walked past there would be an occasional shuffled movement behind a faceless grey-green door, the person behind it looking fearfully out into the corridor behind the barricade defending them from the harsh and violent world outside, guessing as to the intentions of the people who strode the well-worn paths in this time of chaos. Arouin paused at the entrance into the hallway which lead out onto a fifth-story walkway and carefully, slowly, peered around the corner, first one way and then the other. He withdrew, then looked at Yyanis and put a finger to his lips before turning back to the walkway. There was a shout below, and Arouin froze in the entrance for a few moments before slowly moving backwards, one careful step at a time.

## 

&#8220;What is it?&#8221; whispered Yyanis as Arouin turned slowly to face her.

## 

&#8220;Bad news.&#8221; he replied in quiet tones. There was another cry from below them, followed by an ominous, echoing ´click´ that carried strangely well in the still air. There was a patter of footsteps, then the bark of a pistol being fired.

## 

&#8220;We´re going to have to move before it gets much worse,&#8221; said Arouin urgently, looking Yyanis in the eye, &#8220;this is going to be dangerous, keep low, try to avoid making any noise.&#8221; There was another fusillade of pistol shots, a scream, and then a burst of submachinegun fire.

## 

&#8220;Okay&#8221;, said Yyanis, nodding slightly. Arouin edged forward again, a pistol still clenched in each hand, bent almost double. There was another burst of submachinegun fire, followed by pistol shots, which, this time, didn´t fade away. The barking pistol was joined by bursts of submachinegun fire, then more, thundering submachineguns. Arouin moved carefully along the walkway, keeping to the edge where piles of litter would camoflauge him from below.. Yyanis walked out carefully, and looked down through the grating she stood on. Five levels below, two groups of soldiers fired fought over makeshift barriers, muzzle flashes stabbing out towards each other, tracers flying from one side to another. The bursting submachineguns were suddenly rendered impotent by the deep report of a heavy machinegun opening up. Down below a wirey man wearing goggles stood behind an antique machinegun mounted on a tripod grasping it by its twin triggers, leaning back against it and swivelling it to rake fire across the top of the other barricade. Cartridge cases spewed forth from the gleaming black-painted, oil covered gun, a short man crouched beside it, feeding it a belt that led from a massive ammunition canister by his feet. When she looked back up, Arouin was about twenty meters ahead of her, and beckoning for her to hurry up. Yyanis moved more quickly, picking her way amongst the litter and stepping around gaping holes in the metal of the walkway as Arouin waited impatiently for her to catch up. When she finally came within arm´s reach, Arouin turned back around and headed off again, Yyanis following more closely now. As she started moving, Yyanis knocked a beer can that rolled along the walkway with a metallic tinkling noise. Arouin paused, mid-step, and swivelled around to watch as it rolled toward a large hole in the walkway. It fell, gaining speed, and ricocheted off a couple of chains, clanging, falling down and down, before landing a few meters in front of the barricade that they were almost directly over. A couple of people behind the barricade noticed it in the midst of the chaos, looked upwards, then got the attention of their comrades and started pointing up at Arouin and Yyanis.

## 

&#8220;Shit&#8221; breathed Arouin. The men below started aiming weapons, bringing them to bear&#8230; fingers moved towards triggers&#8230; Arouin looked around wildly, then took a risk.

## 

&#8220;**GO PRAETUS!**&#8221; he bellowed down to the people below, walking to the edge of the walkway and leaning over the rail, punching the air with a pistol in his clenched fist. Then men below him paused, then lowered their weapons, waved, and cheered, turning back to the fight. Arouin turned back to Yyanis with a grin on his face.

## 

&#8220;That went quite well&#8221; he said, looking pleased with himself. His grin slowly faded as he realised that Yyanis was staring, white-faced, past him. Arouin turned, looked down. The group of people behind the other barricade were looking up at him, shouting, faces angry, weapons being swung around to point at him.

## 

&#8220;I don´t think shouting ´Go Torriaus´ will help us at this point.&#8221; he said, breaking into a run, &#8220;let´s bug out&#8221; Yyanis broke into a run as the first muzzle flashes lanced out in their direction, spitting fire and death at them. Arouin´s feet pounded the walkway as he ran along the walkway, incoming rounds sending sparks fountaining up as they struck the underside of the metal grille. A burst of machinegun fire chewed into the supports of the walkway, a couple of the chains of the section of walkway that Arouin was on snapping.

## 

_Shit!_ thought Arouin as he felt the walkway begin to give way beneath his feet, and threw himself forwards onto the next section, skidding along it as bullets smashed into the metal around him. He turned to see Yyanis stepping onto the collapsing section, the end nearest him giving way and dropping with a crash onto the walkway below. She lost her footing and fell over, sliding and rolling down the slope to the level below, hitting it, then scrabling to her feet and looking around franticly for Arouin.

## 

&#8220;**Run!**&#8221; he shouted down to her, her face partially obscured by the metal grille. She nodded and started to run again, Arouin overtaking her overhead, looking for a ladder. After another fifty meters he gave up and, with bullets still striking the metal around him, he dropped down to the floor winding himself, then threw himself sideways under the battered safety railing. Yyanis slowed as she saw Arouin hang from the level above trying to hold onto his pistols and the walkway at the same time, then shudder as his leg was hit by a round from below, a red puff of blood appearing in the air and bits of tattered cloth and flesh briefly fountaining out. He grimaced in pain, swung himself over the level she was on and let go, landing on her level with a thump, wincing and giving a shout of pain. Yyanis ran towards him.

## 

&#8220;**_You fucking sons of fucking BITCHES!_**&#8221; he shouted, limping to the walkway with hands wrapped around his pistols still, knuckles white, smeared with blood. He aimed at the people below and started pumping rounds from the pistols alternately, spraying them with death from on high, punctuating the dialogue of flying lead with curses.

## 

&#8220;**FUCK YOU FUCK YOU FUCK YOU**&#8221; he screamed, fingers spasming until the pistols both clicked, empty. Yyanis cannoned into him, hurling him to the floor as a hail of returning fire punched through the walkway where he had been standing.

## 

&#8220;Leave your grieviances until later, I want to live to see tomorrow you bloody idiot!&#8221; she shouted at him, crawling along the ground and trying to drag him with her. He rolled over onto his back, stuffed his pistols back into his pockets and pulled out the newly-bought pair of Bullroarer pistols. Yyanis pulled him slowly along the floor, holding his trenchcoat by its collar as Arouin, eyes on fire with rage and pain, started firing back down at the Torriaus troops. The Bullroarer rocket ammunition punched through the grating, making fist-sized holes in it, each shot leaving a steak of fire across the vision, lancing down into the armed crowd below, drawing forth screams. As Yyanis dragged Arouin around a corner, he stuffed the pistols back into his trenchcoat, and, with the aid of the safety rail, clambered to his feet, jaw locked, teeth gritted against the pain.

## 

&#8220;Let´s get the hell out of here.&#8221; he muttered, turned to look at Yyanis, then lowered his eyes from her gaze, &#8220;and thank you.&#8221;

## 

&#8220;Time to go&#8221; she agreed, nodding.

## 

&#8220;Wait one moment&#8221; he said, holding up a finger, then pulling out his pistols from his pockets, one at a time, and reloading them. Then he aimed one at the handrail of the walkway, put four shots into it, then blasted at another point of it about one and a half meters away. After a few moments of pulling and twisting at the metal, it came free, and Arouin was left with a length of metal pole that he promptly tucked under his left shoulder to support his injured left leg. They moved away, more slowly now, towards the hangar bay, encountering little in the way of fighting, and taking considerable time to navigate the four ladders which took them down to the level of the hangar bay.

## 

The hangar bay entrance was no longer crowded with throngs of people, nor did it have people checking for any weapons that new entrants had. All the desks that were there before were still there, but tipped onto their sides to form a rudimentary barricade that looked so flimsy it was more use as a psychological barrier than a physical one. Eyes peered out from the cracks between the tables, and gun barrels poked out.

## 

&#8220;Who goes there?&#8221; shouted a slightly quavering voice.

## 

Arouin paused, looked thoughtfully at Yyanis, then reached into a pocket of his trenchcoat with his right hand and pulled out a pistol. Yyanis looked at him dubiously, then went wide-eyed with alarm as he rammed the barrel of the gun against her temple.

## 

&#8220;It´s not true I work for him!&#8221; she shouted.

## 

&#8220;Nice thinking,&#8221; whispered Arouin into her ear, &#8220;I hadn´t thought of that.&#8221; Then he raised his voice,

## 

&#8220;Let me through or she gets her brains splattered all over the wall!&#8221; he shouted at the men behind the barricade. There were muffled whispers.

## 

&#8220;Who is she?&#8221; came the uncertain reply. Arouin paused for a moment.

## 

_Damn. Should´ve thought this out a little further in advance_

## 

&#8220;She is the&#8230; chief scientist in charge of the&#8230; uh&#8230; _Demon_ project. Yes, yes, that´s right, the _Demon_ project. You know what that means?&#8221; Another muffled, yet considerably more urgent discussion went on behind the barricade.

## 

&#8220;_Stop looking bored and look scared_&#8221; hissed Arouin into Yyanis´ ear. Yyanis quickly went wide-eyed again and tensed up, replacing the blank-expressioned slouch she had been in a few moments earlier.

## 

&#8220;No, no, we don´t actually know what that means. Sorry.&#8221; came the shaky reply. Arouin shook his head.

## 

&#8220;**If she dies,**&#8221; he shouted, &#8220;**you will all be executed, so let me through!**&#8221; There was a shuffle of movement and one of the desks slid sideways, producing a decently-sized gap.

## 

&#8220;**Wider!**&#8221; shouted Arouin. The desks moved again, and he limped though the gap warily, gun still rammed into the side of Yyanis´ head.


## Part XXV

## 

The canopy of the Phoenix slammed down, raising a little dust that had settled on the nose of the fighter as it did so, sealing the cockpit. Inside, Arouin, still bleeding from his leg wound, collapsed heavily into the pilot´s seat as Yyanis sat down on the floor next to him.

## 

&#8220;Tower, this is the Phoenix in bay 27-A4, do you read, over?&#8221; said Arouin, holding the transmission button down as he did so.

## 

&#8220;This is Tower&#8230;&#8221; there was the chatter of an automatic weapon in the distance, &#8220;do you support the oppressive Torriaus regieme or support the Praetus in their bid for power?&#8221; Arouin looked down at Yyanis, who shrugged, then edged away from a growing pool of blood on the floor.

## 

&#8220;We support the Praetus of course.&#8221; he replied, unconvincingly.

## 

&#8220;Ah-ha! My earlier talk was a cunning ruse! No escape for you, Praetus dog!&#8221; Arouin looked stupidly at the mike for a moment, then shook his head.

## 

&#8220;Okay, I have four Lance torpedoes on this Phoenix. Either you let me out of here or I trigger tham all and blow up the hangar including you, me, and everyone else in it.&#8221; There was a moments silence from the speaker.

## 

&#8220;Okay, how about I let you go and you don´t kill everybody?&#8221;

## 

&#8220;Sounds good to me.&#8221;

## 

&#8220;Let me just&#8230;&#8221; there was a sharp ´crack´ from the speaker, a scream, then a thud. Arouin looked down at Yyanis, who had just opened a floor locker and picked out an emergancy medical kit, then turned back to the microphone.

## 

&#8220;Hello? Tower?&#8221;

## 

&#8220;This is the **new** tower authority! State your alleigance!&#8221;

## 

&#8220;This is the Phoenix in bay 27-A4, the Torriaus wouldn´t let us leave because we are Clan Praetus supporters.&#8221;

## 

&#8220;Ah, comrades!&#8221; said the voice at the other end in exaultation, then the tone differed slightly, &#8220;Why do you seek to leave us?&#8221; Arouin paused, mouth open, about to reply.

## 

&#8220;Torriaus command members have been leaving the station on transports, we are leaving to engage and destroy them! Quickly!&#8221; Arouin looked down at Yyanis again; she was already looking at him with one eyebrow raised slightly and a sceptical expression on her face.

## 

&#8220;Of course comrade! Good luck!&#8221; came the voice as the lift they were stationed on began to move downward. The ceiling moved up away from them, the floor of the hangar gaping wide, swallowing them whole into its dark interior. Arouin winced as he felt Yyanis tear the hole in his left trouser-leg a bit wider, then shine a torch on the bloody mess of his calf.

## 

&#8220;What a mess&#8230; we´ll need the Director´s medical facilities to speed up the healing on this, or it´ll take a good three weeks to heal.&#8221; she said quietly as the lift hummed around them.

## 

&#8220;Forgive me if I´m not overjoyed&#8221; replied Arouin dryly as the lift shuddered to a stop. Then they felt the lift, and the ship with it, turn, and the darkness in front of them parted to reveal a star studded blue-black spacescape beyond it. Arouin relaxed slightly, then hammered the throttle forward as the station´s eject mechanism propelled them out into the void, the Phoenix flying out of the station like a bullet from a gun. Arouin slowly rolled the Phoenix while pulling up, reading his radar display as it flickered into life, reporting new contacts as it found them.

## 

_Fifteen _Phoenix_, loads of light fighters, a couple of transports&#8230;._ thought Arouin, _and lots of missiles._

## 

&#8220;Hold on&#8221; he murmured to Yyanis whilst strapping himself in, &#8220;this might get a little rough&#8221; Yyanis looked around for a moment, mid-way through unrolling a sterile white dressing.

## 

&#8220;Hold on to _what?_&#8221; There was a loud beep from the onboard computer.

## 

&#8220;I don´t know,&#8221; said Arouin testily, bringing the Phoenix around in a sweeping turn to avoid a firefight that was breaking out around a transport, &#8220;use your initiative.&#8221; Then the comms system crackled into life once more, on the sector broadcast channel.

## 

&#8220;Phoenix, registration RX-15, unknown pilot, state your alliegance.&#8221; came a heavy voice

## 

&#8220;Can I say neutral?&#8221; Arouin wondered aloud into the mike.

## 

&#8220;That is not an option&#8221; responded the disembodied voice.

## 

&#8220;Wonderful&#8221; muttered Arouin under his breath, without transmitting, then louder with the key depressed, &#8220;We fly under the colours of Clan Praetus!&#8221; there was a chorus of cheers and an equal cry of threats from the many sparring ships in the sector. Arouin quickly hammered in a few strings of code into the programmable targeting computer; it replied with an electronic beep and then the tri-d radar display collapsed in on itself and re-formed with the ships colour-coded by their clan allegiance.

## 

&#8220;Well, that makes things a little easier at least&#8221; he muttered, his eyes flickering over the radar display to assess the situation. Clan Praetus easily had the upper hand &#8211; they had two more Phoenixes in the field than Torriaus, and as Arouin watched one of them swooped past, Barraks blazing at a Clan Torriaus Raven which was desperately trying to avoid the incoming fire. Arouin maxed the throttle and set off in pursuit, the green glow of the Praetus Phoenix hovering in front and above him, spitting blue bolts of destruction. The Raven´s shields were already out as Arouin aligned the targetting computer aiming point projections and grabbed at the trigger, his own Barraks flaring either side of the cockpit, the rounds ploughed into the Raven´s armour as it tried to turn, and he saw it wobble from the impact. The Raven slowed down too rapidly for Arouin to keep a bead on it and he overshot, struggling to bring the Fighter to bear on his prey. As the crosshairs aligned again he jammed the trigger on his flight stick down, his finger almost slipping off because of the sweat on his hands. As he smoothly turned the Phoenix to compenstate for the Raven´s turn, and the Phoenix he was alongside engaged it´s afterburner, another Raven tore from left to right across Arouin´s vision, Straker machine-guns blazing. The Torriaus light fighter shuddered from the impacts, a piece of armour plating springing loose and tumbling past Arouin has he jinked the ship suddenly to avoid it, throwning Yyanis to the floor.

## 

&#8220;Watch out!&#8221; she shouted at him, getting back to her knees, holding onto the side of the cockpit for support.

## 

&#8220;I am watching out, that´s why we haven´t got a huge hole in our shields!&#8221; he yelled, turning back just in time to see the afterburner fuel tanks on the Raven get hit and blossom into a firey explosion from which the grey-green streak of an escape pod sprung.

## 

&#8220;Nice kill&#8230;&#8221; Arouin glanced down at his radar display, &#8220;&#8230;Whetstone, nice kill.&#8221;

## 

&#8220;Thanks Lithe Shadow&#8221; came the drawled reply from the comms system.

## 

&#8220;I´ve gotta split, guys,&#8221; said Arouin into the mike, depressing the transmission button, &#8220;didn´t manage to rearm my guns after the last fight, and I don´t fancy trying to dock in this shit. Good luck and have fun&#8230;&#8221;

## 

&#8220;Okay Lithe Shadow, we hear you, and we´ll cover your retreat.&#8221;

## 

&#8220;Thanks Big Bear, Lithe Shadow out.&#8221;

## 

Arouin headed for the Imperial Causeway jumpgate, hammering at the afterburner toggle as a few rounds from a pursuing Raven light fighter hit his shields. He glanced down to his rear-vision VDU to see a Phoenix full in behind the Torriaus clan light fighter and open fire with linked Featherfires and Thorns, lighting up the Raven´s shields as it broke off the chase to engage its hunter. Twenty seconds later, Arouin engaged the jump drive as they entered the glowing blue-white sphere of the jumpgate.

## 

They re-emerged in the Imperial Causeway sector, and Arouin quickly set their course for Versalus Hook before swivelling around in the chair so that Yyanis could better attend to the wound in his leg. She carefully inspected it, trying to avoid touching the wound too much,

## 

&#8220;Looks like it´s gone straight through&#8230; I´m just going to put a sterile dressing around it, then leave the rest for the doctors and the accelerated-healing chamber back at the TRI base.&#8221; Yyanis reached down for the dressing as she said this, then started to wind the white bandage tightly around Arouin´s leg, being careful to avoid getting any creases in it. Arouin steadfastly gazed out into the perpetual night as he guided the ship back along the long route to The Stith and it´s hidden jumpgate.

## 

The wait for the scout to lead them to the jumpgate didn´t take as long as the last time; a mere five minute wait instead of twenty or twenty five minutes of boredom. Arouin´s leg had stopped bleeding severely, but spots on the outside of the bandage were turning a rust-red colour from drying blood. The space outside the grey-white station was now almost clear of the debris that had been strewn throughout it when Arouin had last been in the area, and now he was looking carefully over the station´s exterior as he came into dock. There was little sign that anything had been damaged previously&#8230; Arouin noticed one or two blemishes on the paintwork, or lines where metal plates had been welded into place then painted over, but there seemed to be nothing obviously wrong with it any more &#8211; the module that had been fragmented before was back in place, as if nothing had happened.

## 

&#8220;Nice clean up job&#8221; Arouin remarked quietly to Yyanis as the shadow of the docking tube swallowed them whole.

## 

A minute or so later, Yyanis helped Arouin off of the final rung of the ladder that a technician had wheeled to the side of the Phoenix, then picked a bundle covered with her jacket off of the floor, and turned to face a crowd of people who were walking towards them, with the Director at their head.

## 

&#8220;Hello Mr Simmel, Ms Tenan,&#8221; he nodded to each of them in turn, &#8220;I have recieved news that you have something for me. Or, hopefully, more than something. Two somethings.&#8221; He was staring at Arouin, who was looking decidedly uncomfortable.

## 

&#8220;Yes, we have&#8221; Arouin replied, &#8220;I&#8230; forgot&#8230; about who ended up with which artifact.&#8221; He said slowly. The Director looked at him, then cocked his head to one side before continuing,

## 

&#8220;It wouldn´t have mattered if you had realised that you brought back the fake positron coupling and needed to go back for the power regulator; and Octavian mercenary force bribed a Solrain lab worker at Quantar Core into getting it for them; it wasn´t there when you reached the station.&#8221; The Director turned, and began to walk slowly away from them. Arouin paused for a moment as Yyanis brushed past him, then hurried to catch up before settling to a steady pace along side her. They walked the winding maze of corridors to the Director´s office, where he sat down stiffly in the chair behind his desk before dragging it forward a little and resting his hands on the expensive-looking desktop.

## 

&#8220;You´ve done well, although you seem to have caused a little trouble on your way. No doubt the situation will settle down in a few hours with a new Octavian government in power. Also, I notice you seem to have been shot, Mr Simmel.&#8221; he said, peering around the two now-uncovered artifacts on his desk to look down at Arouin´s leg, &#8220;We´ll see what we can do about that. Ms Tenan, please show Mr Simmel to the infirmary and get him fast-tracked for the accelerated-healing process. These,&#8221; he said, indicating the two objects, &#8220;will go to the scientists immediately. Meanwhile, get rested, get well. And don´t get shot again, Mr Simmel, some people will think you are starting to get sloppy.&#8221; He then looked down at some paperwork on his desk, seeming to suddenly loose any pretence of interest in them. Yyanis stood up, tapped Arouin on the shoulder, and walked out of the room. With a last glance back at The Director behind his desk, Arouin followed her down the corridor towards the infirmary.


## Part XXVI

## 

Arouin awoke, leg screaming in agony as if the bullet wound had been caused anew, and jerked upright, eyes snapping open and hand grasping reflexively for a gun that wasn´t there. Accelerated healing, as both the doctors and Yyanis called it, was not pleasant. It had started off with a variety of injections into the open bullet wound in his left, the first of which was meant to dull the pain, but Arouin suspected they had messed up somewhere and ended up giving him that one last. After the injections his leg was submerged in a tank while he sat strapped to a heavy metal chair with less-than-adequate padding, and then Yyanis cheerfully told him that they were about to put him into a coma. And that was the last thing he could remember before drifting into dark unconciousness. Now he looked around, at the curtains drawn around his bed, a pile of his clothes on the table beside him, a second table with his weapons on it, and a third table with the rest of his weapons on it. He swung his legs over the side of the bed, and stood up, testing weight on his injured leg. It held, without any problems, and although the flesh was red and raw, there was no other sign of where the bullet had smashed through his leg. Arouin quickly pulled on his clothes as the cold floor began to chill him through the soles of his feet, then stood for a few moments, savouring the warmth of his coat and checking his various armaments were arranged about his person to his satisfaction. He pulled out a pistol, worked the action a few times, slapped a clip in, then stood cradling it in his hands for a couple of seconds. Out of the corner of his eye, Arouin saw the curtain twitch, and snapped his pistol around to bear on the movement. Yyanis pushed her way through the material, smiling until she found herself looking down the barrel of a gun.

## 

&#8220;You´ve _really_ got to cool down a little bit&#8221; she said quietly, slowly reaching out with one finger and pushing the barrel of the gun to point away from her. Arouin let out a breath at last,

## 

&#8220;I don´t like people sneaking up on me.&#8221;

## 

&#8220;So I see.&#8221; she said, reaching over the bed and pulling the covers back over where Arouin had disturbed them, &#8220;How are you feeling?&#8221;

## 

&#8220;Odd,&#8221; replied Arouin, &#8220;it hurts as if the wound was still there,&#8221; he moved his leg slightly, &#8220;but it works fine&#8221;.

## 

&#8220;The same as my shoulder&#8221; Yyanis smiled slightly.

## 

&#8220;Hmmm&#8230;&#8221; said Arouin, awkwardly, trying to avoid looking at her face to face.

## 

&#8220;Come on,&#8221; she said, turning to walk away, &#8220;things haven´t been going well since they put you under.&#8221;

## 

&#8220;How long have I been unconcious?&#8221; asked Arouin, alarm in his voice.

## 

&#8220;About twenty hours. Not all of it under drugs &#8211; it looks like you´ve been needing some sleep.&#8221;

## 

&#8220;Yeah&#8230; I suppose things have been a little&#8230; uh&#8230; _busy_ recently.&#8221; Arouin followed Yyanis as she walked away, through a pair of double doors away from the infirmary, and down a few of the corridors that made up the maze that was the TRI space station. She pushed open a closed door that, to Arouin, looked like any other in the rabbit warren, and stepped through the entrance into the room beyond. Arouin followed cautiously, sticking his head through and jamming the door open with one hand before relaxing slightly and walking through. It was a canteen, devoid of any other inhabitants save for a couple of maintenance personnel who were watching the same Tri-D screen that Yyanis was looking at as she sat on a bench by a table. Arouin sat beside her and looked at the screen, not paying too much attention to it at first, then becoming increasingly confused as the broadcaster continued her speech. He watched as the news-presenter´s more-perfect-than-life glowing pink lips formed the words.

## 

&#8220;Faction War&#8221;

## 

Arouin looked at Yyanis, and she slowly turned to him and looked him in the eyes.

## 

&#8220;What&#8230; the hell&#8230; has happened?&#8221; he said, slowly, disbelievingly.

## 

&#8220;Just watch,&#8221; she said softly, still looking at him, &#8220;they´re repeating the story every few minutes.&#8221; Arouin turned back, shaking his head. The news presenter, unsure of herself, ploughed on through the news, her job demanding that she look bright, cheerful and attractive, the news in hand requiring she was sombre, composed and reserved.

## 

&#8220;Following a relatively bloodless coup of the ruling Torriaus clan in the Octavian Empire, the Praetus clan, now in control, contacted the Quantar government over accusations of technology theft. CTRIN can exclusively report that the new Octavian Government revealed to the Quantar Church that it had itself lost a number of crucial technologies in the same incident that brought it into power. CTRIN is now displaying footage of the attack that rendered Clan Torriaus defenceless to the coup by Clan Praetus, courtesy of Oct Core Local News Network.&#8221; the screen flickered and showed grainy security camera footage of a corridor, down which came walking three men and a woman, the man at the rear in a power suit. Arouin glanced round at Yyanis again, but she was staring at the screen, lips forming a tight line.

## 

&#8220;The man who you just saw kill the security guard in the power suit is believed to be one Arouin Simmel, a notorious pirate who until now has shown no interest in clan politics, a change of ethos that seems to have shaken up the whole of the Octavian power structure, effects surely too large to be caused by the actions of only one man.&#8221; Arouin raised his eyebrow. &#8220;The woman he is seen here with is, as yet, unidentified. The Quatar Government until three hours ago were requesting permission to send in a special forces team to find Mr. Simmel and bring him in for questioning. After talks between the Octavian and Quantar governments, there was a sudden and unexpected development. Faction war.&#8221; The two ominous words faded into the Tri-D screen, span around a few times, then settled above the presenter´s right shoulder.. &#8220;Octavian and Quantar research stations have both lost what we can now reveal to be two pieces of a powerful, pre-collapse artifact, the third piece of which is held by the Solrain. The Octavian and Quantar governments have concluded, since they do not have the pieces themselves, which independant arms inspectors have verified, that the Solrain have obtained the other two pieces for themselves in what must be an attempted coup. With only one faction with this advanced technology the balance of power within TRI is dangerously upset, and as I speak a large fleet of both Quantar and Octavian fighters and troop transports are forming up, preparing to launch an assault on Solrain space.&#8221; Arouin stood up, and Yyanis looked up at him.

## 

&#8220;This is not good.&#8221; he said, quietly, &#8220;let´s move&#8221;

## 

&#8220;Move where?&#8221; said Yyanis, voice strained, &#8220;There´s a faction war going on!&#8221;

## 

&#8220;Well, it hasn´t started properly yet, has it?&#8221; said Arouin quickly, &#8220;If we get to Solrain Core and get the positron coupling before the fleet arrives, the Solrain government can show that there´s nothing there, and as far as everyone is concerned, it will have just disappeared.&#8221;

## 

&#8220;Okay, I´m in.&#8221; said Yyanis, standing up next to him. Arouin nodded, then walked to the door,

## 

&#8220;I need to speak to the Director.&#8221; he said, looking back at her, &#8220;meet me at the ship.&#8221; She nodded, and he turned and walked down the corridor to the Director´s office. After a fast two knocks at the door, Arouin walked in to see the Director behind his desk, still, smiling slightly.

## 

_Odd,_ thought Arouin, _considering TRI´s going to shit he seems pretty happy_.

## 

The smiled disappeared rapidly from the older man´s face as he looked up at Arouin.

## 

&#8220;Yes?&#8221; he said, eyebrows raised.

## 

&#8220;I´m going to be doing this last job with an untried companion, in a combat situation. There´s going to be tightened security and there´ll be a fleet of ships loaded with troops coming to do the same thing as me. I need an edge.&#8221; said Arouin, firmly. The Director looked at him thoughtfully, and drummed his fingers on his desk.

## 

&#8220;An edge.&#8221;

## 

&#8220;Yes.&#8221;

## 

&#8220;What did you have in mind?&#8221;

## 

&#8220;I don´t know!&#8221; said Arouin, exasperated, &#8220;Anything! A new gun? Shield? Something that I can take in there that will improve my chances a little.&#8221;

## 

&#8220;There may be something&#8230; wait one moment.&#8221; The Director leaned over his desk, and tapped a finger on a touch-sensitive spot, which lit up a pale gold colour.

## 

&#8220;RDX division. Bring up a pair of ID suits to my office. Immediately.&#8221; the finger tapped down again, the golden glow vanished.

## 

&#8220;ID suits?&#8221; said Arouin, confused, &#8220;Make me look like someone else?&#8221;

## 

&#8220;No&#8230; ID stands for Impact Distribution, not Identification.&#8221; The door behind them opened and a sergeant hurried in with a pair of thick, black, one-piece suits.

## 

&#8220;Looks stretchy. Latex isn´t my style.&#8221; said Arouin looking back at the Director. The Director returned the glance with a withering stare that made Arouin feel like he was seven years old and had been caught stealing a sweet.

## 

&#8220;Sorry&#8221; he muttered, looking back at the suits.

*&#8220;Impact Distribution. The force of any impact on the suit will be evenly distributed, as well as the suit can manage, across the whole body.&#8221; The Director explained, &#8220;They haven´t been field-tested, but initial results have been promising. They don´t, of course, work on energy weapons.&#8221; Arouin nodded.*

&#8220;Thank you. Now it´s just that little bit more likely that I´ll come back in one piece and bring that damned Positron Coupling with me.&#8221; Arouin grinned.

## 

&#8220;Duly noted, Mr. Simmel. You will not be allowed to keep the suits after this operation.&#8221; said the Director, as Arouin´s grin faded slightly.

## 

&#8220;Okay. I´ll take my leave.&#8221; Arouin turned and opened the door. He was half way out when the Director spoke again in his thin voice.

## 

&#8220;One other thing, Mr. Simmel. She sat by your bed for eighteen hours while you were unconcious. Don´t let what you have, or may gain, slip away through pride.&#8221; Arouin stopped for five or six seconds, then nodded slowly and walked away, the door hanging open behind him.

## 

Arouin was unusually quiet as he walked across the hangar bay towards the _Lithe Shadow_ and Yyanis, standing below it´s nose, looking at him. He was already wearing his ID suit on underneath his clothes, and as a result they were tight-fitting and rather uncomfortable. He had his t-shirt and Yyanis´ ID suit over one arm and was wearing his black trenchcoat.

## 

&#8220;What´s the new outfit for?&#8221; she asked as he drew nearer.

## 

&#8220;You can put yours on in the cargo bay, we don´t have time for you to go and find some changing rooms.&#8221; he said, starting to climb up the side of the Phoenix and slapping his hand on the ID-panel to unlock and open the cockpit.

## 

&#8220;Don´t worry I´ll be okay&#8221; she said, smiling up at him. Arouin jumped into the pilot´s chair, waited until Yyanis had gained a footing inside the cockpit, tossed her the suit, then hit the canopy control and it lowered again over their heads. Yyanis moved around behind the chair as he opened a comms channel to the station launch control system.

## 

&#8220;This is _Lithe Shadow_ requesting immediate launch.&#8221;

## 

&#8220;This is tower, we have you down for an emergancy launch ASAP, adios.&#8221;

## 

The lift the Phoenix was parked on shuddered and began its descent, the light fading. In the darkness there was a rustle of clothing behind Arouin´s chair.

## 

&#8220;Could they have made this thing any more difficult to get into?&#8221; muttered Yyanis behind him, &#8220;You still haven´t told me what it does.&#8221;

## 

&#8220;The Director called it an ´ID´ suit, it´s meant to equally distribute any impact across the whole suit.&#8221;

## 

&#8220;Hmmm&#8230;&#8221; said Yyanis, &#8220;&#8230;wonder how they managed that?&#8221;

## 

&#8220;Don´t look at me, I only work here.&#8221;

## 

The lift shuddered to a stop, and the comms channel crackled into life.

## 

&#8220;_Lithe Shadow_ this is tower, you are go for launch.&#8221;

## 

The doors snapped open in front of the fighter, moments before it was shot out into the void. In the half-light of the stars, Yyanis walked over to him and squatted down next to him, wearing her ID suit.

## 

&#8220;Suits you.&#8221; he said, grinning. She smiled back, saying nothing.

*&#8220;Time for the stars.&#8221; murmured Arouin, ramming the throttle forward and heading for the jumpgate out of the hidden sector.*

&nbsp;

## Part XXVII

## 

The yellow drive flare from the Phoenix blazed out behind it, propelling it onward through space, towards Solrain Core station. The _Lithe Shadow_ was a hunted ship now, and Arouin had changed the registration codes a few times en route, staying away from the large formations of Quantar and Octavian picket ships which hung in the void like vultures hovering above potential prey, vigalent and uncaring. The trip had taken them past streams of troop transport ships, some waiting, under guard from a myriad of fighters, others moving slowly towards the Oupost Station, which was seemingly the staging post for the upcoming assault. And now they approached Solrain Core station, the last jumpgate spitting them from Wanderer´s Pond out into the deep-blue stained depths of inner Solrain space. The symbol of Solrain power in space, Solrain Core station, hung before them, vast and magnificent, shining silver-white, warning lights blinking on and off, the central fusion core blazing its bright blue light out into space. Arouin and Yyanis did nothing for a moment, just stared at the station taking in its immense size and majesty. Slowly, Arouin moved his hand over to the comms panel, his fingers flickered over it, then there was a crackle from the speaker.

## 

&#8220;This is Solrain Core Docking Control hailing the Octavian Vessel _Charming Tune_, state your purpose here.&#8221; came the slightly distorted, extremely strict-sounding voice. Arouin was staring at the radar screen, adjusting a couple of controls.

## 

&#8220;This is _Charming Tune_ we have come here seeking to obtain medical supplies to take to Klatsches Hold station where there has been an outbreak of Vessy Fever.&#8221; said Arouin carefully, keeping his voice neutral. He adjusted the radar screen a little more, then moved closer to it and craned his head around, moving from side to side.

## 

&#8220;What is it?&#8221; hissed Yyanis.

## 

&#8220;Well they´re about to be attacked, yet there aren´t any fighters out here.&#8221; said Arouin, with a far-away, distracted tone to his voice.

## 

&#8220;This is Solrain Core Docking Control. You are clear to dock. If you are still in station when the Quantar-Octavian fleet arrives, your ship will be impounded and you will be imprisoned until the emergancy is over.&#8221;

## 

&#8220;Charming.&#8221; muttered Arouin, then, into the comms system, &#8220;We hear you, Control, we´re coming in to dock now.&#8221;

## 

The green holographic docking rings sprang into life before them, and Arouin let the Phoenix slide into the centre of one of the rings before spinning the Phoenix through 180 degrees and applying full burn to bring it to a sudden stop. The fighter turned, engines flared, then died, and it coasted into the docking tube, lit by rows of pulsating white lights.

## 

The lift with the dubiously registered _Charming Tune_ on it slid smoothly to a stop as Arouin hit the canopy release button, then climbed out, Yyanis following. The hangar floor gleamed; no scorch marks, no rusty gratings or oil-covered metal, just a grey, faintly metallic surface stretching out to the far walls. Overhead the lights glowed softly, no flickering, harsh-white, cheap fluro-tubing, and they didn´t sway, buffetted by an unseen breeze from a ventilation duct or circulation fan. Arouin turned to Yyanis, who was holding her shotgun by the top of the barrel, resting the other end on the floor.

## 

&#8220;Pass me that&#8230; they don´t like such obvious weapons here.&#8221; he took the shotgun from her and slipped it under his trenchcoat, pulling an arm out of one sleeve and hooking the shotgun´s strap over it, so it hung just out of sight below his arm. Arouin dug into his pockets and drew out a pair of pistols.

## 

&#8220;Use these,&#8221; he said, passing them to her, &#8220;less kick than the heavier ones I´m using now, but they´ll take someone down. Here´re a couple of spare clips. We´ll probably need them.&#8221;

## 

&#8220;How can you possibly expect to get this thing?&#8221; she asked, not unkindly.

## 

&#8220;Well, as I see it, the R&D area is going to be very very heavily defended.&#8221;

## 

&#8220;So a frontal assault would be pointless&#8230;&#8221; Yyanis said firmly as the canopy on the Phoenix behind them shut.

## 

&#8220;Now I didn´t say that.&#8221; said Arouin midly, starting to walk towards the nearest exit from the hangar. Yyanis walked up to him, then kept pace beside him.

## 

&#8220;You can´t be serious&#8221; she said eventually as they drew near to the exit from the cavernous, spacecraft-filled hangar.

## 

&#8220;We´ll see how it goes. If the worst comes to the worst and no other course of action presents itself, then yes, we´ll have to.&#8221;

*They walked through a number of corridors until Arouin found a man dressed reasonably smartly who was using a public information terminal, and they waited patiently until the man finished browsing through the locale news.*

&#8220;Excuse me, sir,&#8221; said Arouin, smiling, &#8220;could you tell us where the Solrain Government´s primary research facility on this station is? The young lady and I have something that may be of some interest to them&#8221;

## 

&#8220;Of course I could,&#8221; said the man, evenly, and held out his hand. Almost immediately, a wad of ten-credit notes was dropped into the outstreched palm.

## 

&#8220;Level 37, Ring Arm 2&#8221; he said, in a friendly tone of voice, then turned and walked away.

## 

&#8220;I love Solrain stations,&#8221; said Arouin to Yyanis as he started off again, &#8220;a little bit too pacifist for my liking, but hey, you can get anything for a price.&#8221; Arouin looked into the window of components shop as they passed it, &#8220;Even if the price is always a little too high.&#8221; They walked for a few minutes through the corridor until they came to a grav lift. Arouin pressed a warmly-glowing button and the polished metal doors slid open to reveal a slick, mirrored cubicle within. He walked through the doors and Yyanis followed him, walking past then leaning on the rear wall of the lift.

## 

&#8220;Level 37&#8221; said Arouin clearly. There was a faint hum and a slight sense of acceleration for a few seconds, then Arouin felt himself become fractionally lighter for a moment, then the doors slid open.

## 

&#8220;Level 37, thank you.&#8221; intoned the lift solemly. They walked out from the lift into a spacious hall, a fountain in the centre, everything white-painted and glass. Tall columns stretched up to the ceiling where they ended in intricate carvings. Around the fountain was a mosaic inlaid in the flooring.

## 

&#8220;Showy&#8221; muttered Yyanis disdainfully.

## 

At the far end of the hall there was a set of glass and chrome double-doors with a holographic sign over them. The sign was unreadable from where they stood because of the playing waters of the fountain, but they new sure enough what it was. There were also about twenty armed guards in front of it, most of whom were fidgeting with their uniforms or weapons, looking around warily.

## 

&#8220;For once,&#8221; said Arouin, with a faint smile playing over his lips, &#8220;I think you may be right on the frontal assault idea. Not too feasiable&#8230; and I don´t trust these suits.&#8221;

## 

&#8220;I´ve got an idea&#8221; said Yyanis brightly, after a moment´s pause, &#8220;We need an emergancy terminal.&#8221;

## 

&#8220;Level 36, thank you.&#8221; said the lift as they stepped through the still-opening doors. Arouin pointed over Yyanis´ shoulder to the red box of an emergancy terminal that was mounted on the wall a few meters to their right. Yyanis walked over to it, and lifted the reciever.

## 

&#8220;What _is_ the idea?&#8221; asked Arouin. Yyanis paused with the receiver half way towards her ear.

## 

&#8220;We tell them there´s a fire &#8211; they´ll evacuate the place.&#8221;

## 

&#8220;What about the slight problem that any emergancy terminal that is connected to the network automaticly sends its location to the system in case the person calling collapses before they can tell them. Let me do this&#8230; I can rig it up.&#8221; Yyanis put the receiver back in it´s place, and moved aside to let Arouin get to it. He pulled out a knife and levered off the panel on the wall beside the emergancy terminal, then deftly stripped the insulation off of a bundle of cables. Six wires were revealed. The clang of the panel dropping the floor attracted some looks from passer-bys but nothing too dangerous.

## 

&#8220;This one and this one,&#8221; he said, pointing at two of the wires with the tip of his knife, &#8220;carry the voice data. The other four,&#8221; he indicated each in turn to Yyanis, who was observing over his shoulder, &#8220;carry video, sensor readings, and location data.&#8221; He cut the four lines with his knife, put the blade away, then lifted the receiver to his ear. Arouin hit the emergancy button in the box.

## 

&#8220;Hello&#8230; yes&#8230; we´re on level 37&#8230; there´s been a fire&#8230; an experiment´s gone wrong&#8230; get help here quickly&#8230; yes&#8230; the terminal´s been damaged&#8230;. help us&#8230;&#8221; he said tersely, then pulled out the knife and cut the other two lines. He up and quickly searched the ceiling for a fire detector, spotting one after a few seconds. He climbed out on top of the emergancy terminal box, levered the fire detector off its mount on the ceiling, and dragged it down to the ground, trailing cables behind it. Sticking the knife into its casing he levered the detector open and looked at the electronics inside.

## 

_Let´s see&#8230; heat detection with the bi-metal strip, smoke detection from the radiation source and detector&#8230;_ he thought to himself. He quickly reached down and bent the bi-metallic strip around so it touched a contact. A red light went on. He cut off a piece of the casing, stuck it between the radaition source and the detector, blocking the alpha particles&#8230;

## 

&#8220;We have smoke and we have fire&#8221; said Arouin in satisfaction as a second light went red, the pair blinked, and then a siren went off overhead. &#8220;Let´s move.&#8221;

## 

They ran back to the lift, hit the button, and waited for the few moments it took one to arrive.

## 

&#8220;Level 37&#8221; said the lift. Arouin looked out of the lift, check the area, then quickly ran to a large white column that rose up to the ceiling, dragging Yyanis with him. Sirens were going off in the distance and there was a commotion of shouts, crashes of things being dropped, and the thunder of feet running. After twenty seconds or so a bunch of scientists, some wearing lab coats others suits, rushed past them and into the lift, faces flushed and alarmed, eyes wide and startled. The doors closed and they sank out of view from Arouin and Yyanis.

## 

&#8220;They´re going to have to evacuate it,&#8221; whispered Arouin, &#8220;they can´t risk it being destroyed. Make sure your weapons are ready.&#8221;

## 

Yyanis didn´t speak, but pulled out the brace of pistols that Arouin had given her, checked them over, then cocked them one at a time and put one in each hand. Then she looked up and him and nodded. There was a sound of many heavily booted feet hammering on the hardened floor, and a hoarse voice shouted out above the stampede,

## 

&#8220;**MOVE! MOVE! Come on get it outta here!**&#8221;

## 

Arouin looked back to Yyanis.

## 

&#8220;That´s our cue.&#8221; he said, grimly, &#8220;Enter stage right.&#8221; gun in each hand, he ran out across the hallway in front of the fountain, and in front of a group of about thirty security troops manhandling a heavy, armoured, hover-pallet. His feet pounded at the ground as he ran, body twisted to his left, arms outstreched, a heavy pistol in each. Flames billowed out of the guns, cartridge cases poured out of them skywards like the water in the fountain behind the first line of falling guards. Three men screamed, twisting, falling, limbs flailing as they collapsed to the ground, great, bloody holes torn through them like someone had been punching through sheets of paper. The rest of the security men let go of various pieces of equipment they had been carrying and their hands reached for the automatic rifles slung on their backs. As the first of them were raised up to eye level, Arouin launched himself through the air, guns still flaming, bullets lancing out like blows from the fists of a demon. The first clatter of automatic rifle fire drew Yyanis out of her position behind the column, one pistol held high, other pointing at the ground. The security guards tracked Arouin through the air, fighting the recoil of their blazing weapons to aim at the dropping figure. As Arouin hit the ground and rolled, dropping the now empty clips from his pistols, Yyanis pumped at the trigger of the raised pistol, it kicked with unleashed power in her hand, a puff of blood came from the torso of one guard, he screamed, some of his colleagues heard the cry, span, firing. Yyanis dropped to one knee and brought up the second pistol alongside the first, bullets whistling above her head, smashing into the column above her. Great chunks of metal and plaster were torn out of the tall pillars by the high velocity weapons, white dust spraying out over the scene as Yyanis opened up with her second pistol, the knees of one of the guards vanishing in an explosion of flesh and bone, then lowered the first as it ran dry, spinning back under the cover of the pillar as a cascade of bullets hammered the other side of it. Arouin, hearing the noise, dropped his large-cal pistols and pulled out the pair of _Bullroarers_, whipping around the corner of the pillar and firing precisely aimed shots at the guards left standing as they turned back once more to face him. He sprinted towards them as they turned, increasing the speed of his fingers pumping on the triggers, each contraction of a muscle bringing a flare of light followed by a bright yellow-white streak as the rocket motor in the bullet ignited, propelling it to it´s final, grisly destination. The first guard to bring his weapon to bear was struck in the shoulder by a rocket-bullet, blowing his arm off, the weapon letting off a burst of fire as the now-detached limb holding it spasmed, propelling the grey-black weapon into the air for a moment before it dropped to the floor. Arouin threw himself forwards, pistols blazing, lances of light finding their marks, blue-uniformed men crumpling as if made of paper-mache instead of flesh and bone. Another sustained burst of rifle fire blazed out as Arouin hit the floor, knocking the wind out of himself, sliding along the floor amongst the dust and the debris that had fallen. Bullets chewed into the floor around him, throwing up pieces of tiling and sparks as the metal underneath them was hit. The line of bullets raced along behind Arouin as he slid, drawing closer split-second by split-second as the last man standing brought it to bare. There was a sudden flurry of sharp cracks and the man standing in front of Arouin was plucked from the ground and thrown bodily over him, weapon still clattering, by the deluge of bullets blasting their destructive way into his frail body. There was a sickening cracking noise as the body hit the still-floating pallet and lay there, arched over the domed armour. Arouin looked up, face covered with white dust, lines of flesh showing where rivulets of sweat had run, washing away the pale grime. Yyanis was stood over him, a pistol in each hand, covered with sweat. One of the pistols slid through her grip a little, hung there in her hand for a moment by the trigger, swaying, then clattered to the floor. She offered the empty hand to Arouin, who took it gratefully, then pulled himself up.

## 

&#8220;Thank you.&#8221; he managed. She coughed, raising dust from her face,

## 

&#8220;Any time&#8221; she croaked. Arouin looked around them, at the scene of devestation. Security personnel lay all over the floor, over one another, bodies contorted in anguish. Some breathed. Many did not. Blood was splattered over them, the whiteness of bone showing through the bloody mess of some of the injuries. Arouin turned to Yyanis, leaned forward to her, and delicately brushed a droplet of blood from her brow with his fingertip. Then he turned back to the casket.

## 

&#8220;After that lot I bloody well hope this is the right thing.&#8221; he said, gingerly picking his way over a couple of bodies to get to it. He looked at the complicated locking system, and experimentally tapped a few of the numbers on a keypad set into it. There was a dull beeping noise which sounded like the electronic equivalent of ´fuck off´. Arouin took at step back and looked at it critically.

## 

&#8220;Any ideas?&#8221; he asked.

## 

&#8220;Uh&#8230; no.&#8221; replied Yyanis shakily.

## 

&#8220;Oh well.&#8221; he said, shrugging, pulled out the shotgun from under his trenchcoat, aimed it, and pulled the trigger. The blast echoed through the hall, and tiny pieces of metal flew through the air and then scattered over the floor, making hundreds of tinny little scraping noises. The electronic control panel was a complete mess, blackened and gutted. Suddenly, the anti-grav generators on one end of the pallet shut off, and it dropped to the floor, making a cracking noise as it landed on the ribcage of a fallen security guard. Arouin pulled at the lid until it came open, then fell off.

## 

&#8220;Yep, that´s the one.&#8221; he said, &#8220;looks just like the first one I took back the the Director. The&#8230; uh&#8230; wrong one.&#8221;

## 

&#8220;When you shot me&#8221; said Yyanis, evenly.

## 

&#8220;Uh&#8230; yeah,&#8221; said Arouin, uncomfortably, &#8220;sorry about that.&#8221;

## 

&#8220;I´ve been waiting for you to say that&#8221; Yyanis grinned, but in the body-strewn hall the smile faded quickly. &#8220;Let´s get out of here&#8221;

## 

&#8220;I am inclined,&#8221; he said, pausing for a breath, &#8220;to agree.&#8221;

## 

&#8220;Level 5&#8221; said the lift, in a grave tone of voice. Arouin stepped out first, Yyanis after him,

## 

&#8220;Why do they have such a depressing lift voice here?&#8221; asked Yyanis, a lumpy, cloth-covered item under one arm.

## 

&#8220;Ours is not to reason why.&#8221; said Arouin, broodingly, looking warily around them, &#8220;Let´s get back to the ship. We don´t have much time before the fleet gets here.&#8221; They started walking away from the lift, down the corridor back to the hangar bay.

## 

&#8220;What´s going to happen?&#8221;

## 

&#8220;With what?&#8221;

## 

&#8220;Well, the joint fleet is going to arrive here, kill loads of people, and then assault the station. They´ll get to the R&D site, break in, kill some more people, then find it isn´t there. They´ll probably think that the Solrain have hidden it somewhere, and start looking everywhere for it, doubtless killing more innocents.&#8221; Arouin walked for a few moments before replying,

## 

&#8220;Quite possibly.&#8221;

## 

&#8220;So what could we do to stop it?&#8221;

## 

&#8220;They have security camera footage of the pair of us wasting about thirty guards and making off with the positron coupling. Someone in the Directors Board is probably having a fit right now.&#8221; Yyanis nodded slowly in reluctant agreement. As they rounded to corner to the hangar, the overhead speakers hummed into life.

## 

&#8220;A joint Quantar-Octavian battle fleet has just entered Solrain Core sector, Solaria is now in a state of war. All defence pilots to their ships. All defence droids are being launched. All gunners to their turrets. All foreign factionals please head to your nearest security station. Thank you.&#8221;

## 

Arouin turned to Yyanis, who was already looking at him.

## 

&#8220;Now this is _not_ good.&#8221; he said in humourless tones, &#8220;Come on, we´re going to have to move fast.&#8221;

## 

They broke into a run, feet pounding on the clean deck, weaving through a flood of security guards and maintenance personnel, pilots and their ships, heading for Arouin´s Phoenix fighter. As they rounded the bow of a Solrain Traveller-class light transport, they saw the graceful lines of the Phoenix, and the rather ugly lines of a burly security guard standing beneath her prow. Arouin, unfazed, walked over to the man, who was about 4 inches taller than him. The guard looked down at Arouin as if he was an ant only fit to be crushed. Arouin looked up slightly, to the man´s face.

## 

&#8220;Hello there. Could I have my ship back please?&#8221; he asked in a pleasant tone of voice. The guard laughed at him,

## 

&#8220;I don´t think so.&#8221; he said, threateningly. Arouin turned to Yyanis.

## 

&#8220;Now, you can´t say I didn´t try.&#8221; he said, accusingly.

## 

&#8220;Let me into my ship right now or I will blow your fucking face off.&#8221; he said to the guard in the same, cheerful, tone of voice which was considerably more menacing than it should have been, especially because there was now a gun pointed at the man´s forehead whose bore was only slightly smaller than his increasingly widening eyes.

## Part XXVIII

## 

Arouin watched the man who had been guarding his Phoenix look up at him as the canopy sealed shut, then turn and run towards the launch control station.

## 

&#8220;He´s going to cause trouble&#8221; remarked Yyanis, standing beside Arouin, hand on his shoulder as he seated himself and busied readying the Phoenix fighter for flight. Arouin changed the comms channel to transmit to the launch control station, then depressed the transmit button.

## 

&#8220;This is the Phoenix class fighter _Charming Tune_ demanding immediate launch clearance.&#8221; he said, urgently.

## 

&#8220;This is tower,&#8221; came the reply, &#8220;you have a negative, repeat, a negative on launch clearance. Stand down and prepare to surrender yourself.&#8221; Arouin leant back into his chair, clasped his hands, and tapped his thumbs together a few times. Then he leant forward, and tapped in a sequence of commands into the control console, disabling the fake registration that he had previously set up.

## 

&#8220;This is the Phoenix class fighter _Lithe Shadow_, piloted by Arouin Simmel, demanding immediate launch clearance. I´ve got a Lance torpedo under one of my wings, and complete override control so I can do what I want with it. If you don´t let me out of this shit-hole right now I´ll fire this thing right into the launch control station.&#8221;

## 

There was a pause on the other end of the line.

## 

&#8220;Ah&#8230; that won´t be necessary. We will engage the launch system now.&#8221; came the eventual, shakily spoken answer. Arouin looked at Yyanis,

## 

&#8220;They´re going to try something else, getting them to give in to that demand was far too easy.&#8221;

## 

Yyanis nodded an unspoken agreement, then sat down to one side of Arouin, facing towards the back of the Phoenix. The lift hummed, then smoothly dropped away below them, the lights inside the lift seeming to move up past them, casting a multitude of moving shadows. A door closed and locked above them, then there was a sensation of movement as the Phoenix was brought into line with one of the docking tubes. Arouin looked around outside of the cockpit warily, then grasped the flight stick with one hand and got ready for the launch. He waited for a few more seconds, nothing happened. Then a red light came on inside the cockpit, immediately drawing Arouin´s attention to it. Yyanis followed his gaze to the lone, baleful light.

## 

&#8220;What does that mean?&#8221; she asked quietly.

## 

&#8220;They´re trying to use the external override to open the canopy and vent us into the vacuum. I got a friend of mine at the Outpost Station to hard-wire it so they can´t.&#8221; Arouin paused for a moment, then leant forward and hit the transmission button again.

## 

&#8220;Hello tower,&#8221; he said pleasantly, &#8220;that really isn´t very nice of you. Breathing vacuum just isn´t my style. As a repayment I´m going to blow a Phoenix-sized hole in your launch bay doors. Have a nice day.&#8221;

## 

Arouin leant over the control panel and started hurriedly activating a number of overrides, before looking back up, arming the weapons systems, and selecting a Lance torpedo. He looked down at Yyanis.

## 

&#8220;I hope you´re holding on tight down there.&#8221;

## 

He thumbed the missile launch button on the stick, and the Phoenix shuddered slightly as the heavy torpedo left the launch rack. There was a moment´s pause, then a flash of light and a rumbling explosion, followed by a shock wave that battered the shields of the Phoenix and threw around the two people inside, Arouin barely managing to maintain his grip on the flight stick. Pieces of metal flew past, richoceting around the holding chamber they were in, and the firestorm from the blast faded slowly, the flames broiling as they lost intensity, became increasingly transparent, then vanished, leaving a cloud of floating debris. The launch doors were buckled outwards, ruptured, the metal twisted and distorted from the heat and power of the blast. Arouin rammed the throttle forward as Yyanis got unsteadily to her feet and turned to face the front of the fighter again, holding onto the side of the cockpit to support herself.

## 

&#8220;Is that big enough for a Pheo&#8230;&#8221; she began, then was knocked off her feet as the Phoenix was shaken as if stuck by the fist of an angry god and the screech and smash of tortured metal screamed at them. Suddenly, space filled the view, and they were clear of the station. Arouin turned quickly and helped Yyanis up from the floor,

## 

&#8220;The answer to that would be ´not quite´,&#8221; he said firmly, &#8220;but it is now&#8221;. Arouin turned back to the consoles and stared at the radar screen. There was a large cloud of blue traces of varying sizes, and another of mixed red and green traces, the other side of the blue cloud. Arouin saw in his mind´s eye hundreds of pilots squinting down at their radar display at the single red dot that had appeared behind them, cycling targets to find it, looking at the name of the ship, turning their fighters around to bear&#8230;

## 

&#8220;You know,&#8221; he said quietly, &#8220;I think we´ve just entered a world of hurt.&#8221;

## 

The Phoenix shot forward at the head of a yellow spear of fire, rolling slowly, heading for the Wanderer´s Pond jumpgate. Arouin stared rigidly ahead as one hand kept a firm grip on the flight stick and the other flickered over the controls, setting up a new fake registry. The blue mass on the radar image started to curl in around the edges as Arouin barrelled towards the centre of the ships.

## 

&#8220;I´m going to try and punch straight through&#8230; I really don´t know if this is going to work.&#8221; he said through gritted teeth. There was the characteristic whine of Barrak rounds flying past at high speed as the cloud of ships collapsed in around the _Lithe Shadow_. The backdrop of space was twinkling, not only from the stars but from hundreds of drive flares, light from distant stars reflecting off of polished armour, and the blazing of weapons as the ships fell into place around the Phoenix. The shields glowed red, the firing of weaponry around them reaching a crescendo of sound, blurred, glowing rounds from a multitude of weapons flashing past as the Phoenix dove, ducked, swooped and jinked furiously around. A pursuing cargo tow outfitted as an interceptor ignited a flashfire to try and catch up with them, but the pilot misjudged it and smashed through a couple of Interceptor class fighters, shields glowing, before crashing bodily into an Intensity which ignited in a fireball, billowing into a much larger secondary explosion as the ammunition and missile stocks went up. Arouin glaced down at the radar again, at the red-green cloud which he was racing towards, and which was racing towards him. Streams of flying death threaded their way around the Phoenix as Arouin pushed the throttle harder and harder, as if he might somehow urge more power from the engine with his desperation, and the shield glowed as yet another salvo of weapon fire smashed into it. The Intensities, faster than the heavier Phoenix, were catching up, the shields being hit more and more often, power draining from them as the generator pumped as much energy into them as it could. Arouin, getting desperate, ignited a Flashfire. The drive flare flashed white-purple and the spacecraft shuddered, Arouin was crushed back into the pilot´s chair, and Yyanis was thrown bodily across the floor of the cockpit before getting pushed up against the back wall with the force of the acceleration. Suddenly the green and red mass was upon them, ships flashing past them on either side, guns blazing out in a cacophony of multi-coloured light and harsh, violent sound. The sound of the roaring engines was tremendous, distorted as they flashed past, the scream of rocket motors setting off as missiles were loosed from their racks and sent lancing out into the Solrain formation, ships disappearing in a firey blast and a cloud of debris. Arouin wove and ducked around the fighting ships, some Solrain fighters still on his tail, came around in a long curve, guass rounds flashing past his vision as the pilots chasing him tried to bring their weapons to bear on the smoothly changing flightpath of the Octavian fighter. Arouin switched the comms channel to faction broadcast with one hand, as he fought the impact of a pair of Barrak rounds finding their mark on his shields with the other.

## 

&#8220;This is the Octavian Phoenix _Lost Serendipity_ we need help, got three or four Intensities on me, and I can´t take ´em out, shields already down to thirty percent&#8230;&#8221;

## 

&#8220;This is the _Madman´s Wrath II_, I see you buddy, coming in&#8230; hold em steady&#8230; missiles away!&#8221;

## 

Arouin stared at the rear view VDU, as eight curving contrails reached out towards the pursuing ships. One of the Intensities noticed the incoming missiles, and quickly broke off, but the missiles trailing him still aimed for his ship, roared towards it, then smashed their devestating warheads into the Intensity, which was thrown off course by the explosion, shields in tatters. The second Morningstar missile cluster slammed into another Intensity, sending it lurching forward, then breaking up into a fireball as its already-damaged shields gave up and let the last warhead of the four bore into the afterburner fuel tanks before detonating and tearing the ship apart. The Typhoon fighter which had loosed the missiles was steadily gaining on one of the two Intensities which were still doggedly on the_Lithe Shadow_´s tail, and after a few more seconds opened up with quad-linked Hammers, the blue bolts reaching forward and lighting up the shields of the pursuer. On this cue, Arouin increased the speed of the turn, dropping off the throttle and snapping the ship around, bringing the guns to bear, and triggering his own quad Barraks. The Intensity which was being fired upon by the _Madman´s Wrath_ had turned, slowing, to meet its new assailant while its companion over shot Arouin, guns blazing, scoring a number of hits on the already battered shielding of the Phoenix. Arouin ignored the dangerously low state of his shields, and as the Typhoon flashed past he depressed the trigger, fighting the kick of the guns to hold the reticule steady on the still-turning and now almost stationary Intensity. The pilot realised his mistake, and throttled up quickly, but Arouin carefully adjusted his aim as the Intensity grew larger in his reticule, the shields giving out with a blue flare of light, then sparks being thrown up as the guass rounds found their mark on the bare armour. The Intensity was at full throttle now, trying to put distance between it and the Phoenix pursuing it, but it couldn´t do it fast enough; Barrak rounds smashed into the left of the Intensity´s two engines, and it exploded in a dull red explosion. The unbalanced thrust span the Intensity round, ammunition ploughing into it as it span until a Barrak round hit one of the two _Kataka_ warheads slung beneath it, whereupon the whole thing disintegrated in an eruption of flames. Arouin´s Phoenix shot through the expanding cloud of burning gasses leaving a swirling void in the flames, then came around in a long, slow curve while the Typhoon pilot engaged his afterburner and came in close behind the Intensity that was still on Arouin´s tail. The guns flared again, blue Hammer rounds whipping around the Intensity which rolled to avoid as many of the shots as it could, it´s pilot now aware of the hostile intent of the fighter behind it. Hits began to appear on the shields, but the Intensity pilot jinked and weaved while still gaining on Arouin, then opened up with his own guns. The shields on the Phoenix finally gave out, flaring red a couple of times before dying, and the odd round started to strike the armour on the Octavian fighter, knocking it around and making Arouin fight the controls just to keep the ship even vaguely stable. Another couple of rounds hit home, a large chunk of armour was sent spinning from the exterior of the red-brown ship and an alarm went off inside the cockpit, the pulsating red light illuminating Arouin´s sweat-streaked face as he fought to avoid the weapon fire directed at his suddenly-frail ship. As the Intensity gained, there was a stroke of luck out of the melee around them, a Raven light fighter pilot swung in behind the Intensity from above, raking it with Straker chaingun fire, then, as the shields lowered and the Intensity started to take more evasive maneuvres, he unleased a pair of Hellrazor missile packs. The four missiles shot out with the range at less than a kilometer giving the hapless Solrain ship no time to evade. The first pair of missiles slammed into one side of the ship, sending it into a skid, the pilot trying to bring the ship around to regain control. Then the second pair of missiles slammed home, blowing off one of the characteristic ´wings´ of the Solrain fighter, which tumbled end over end as it flew off into space. Crippled beyond hope, the pilot ejected as the Typhoon came in for a final pass, guns tracking fire over the shattered remanant of the fighter, armour buckling and spewing out into space, then the ammunition stocks exploding and tearing it apart from the inside out..

## 

&#8220;Time for me to leave this one folks,&#8221; he said over the comms channel, &#8220;thank´s for the help, I owe you guys one.&#8221;

## 

&#8220;No problem, good luck&#8221;

## 

&#8220;Thanks, out.&#8221; Arouin turned to Yyanis, who had pushed herself further back into the corner of the cockpit where she had fallen, and was now looking at him with a smile on her lips.

## 

&#8220;Sorry&#8221; he said apolegeticly, with a dry smile.

## 

&#8220;No need to apologise, we´re alive&#8230; you did magnificently.&#8221; Arouin, turned, flushing red slightly and brought them around to head for the Wanderer´s Pond jumpgate, and when the glowing blue field encompassed them he engaged the jump drive, flinging them far across space.

