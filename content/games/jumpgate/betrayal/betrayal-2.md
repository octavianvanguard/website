---
banner: "css/images/banner.png"
title: Parts XI – XX
author: fellblade
layout: page
date: 2018-04-07

---
## 

## Part XI

## 

Shouldering his way through the crowds, Arouin entered the hangar entrance area, turned a corner into the hangar proper, and moved towards his ship. Passing a couple of Solrain light fighters, he noticed the guy in the blue shirt from in the mall working under one of them; he must have needed some information from the terminal in the mall to fix his ship. Raquel wasn´t working on the _Lithe Shadow_&#8230; although he could see she had been; the cockpit locking system had been added and replaced, he could see. Raquel was standing beside it, looking at him, tapping her foot as he approached. When he finally came within arm´s reach of her, she grabbed him by the shoulder, pulled him towards her, and whispered urgently in his ear,

## 

&#8220;There´s a stack of Lance warheads arranged in a starburst cluster around your powerplant and engine&#8230; where the hell did you get this ship from?&#8221;

## 

Arouin took a step back, looking suspicious.

## 

&#8220;Really?&#8221;

## 

&#8220;Yeah, really.&#8221; she confirmed, stepping aside to reveal a pile of munitions on the floor behind her. Arouin knealt down beside the warheads, picked one up in his hands, and tossed it up and down thoughtfully.

## 

&#8220;Would you mind not doing that?&#8221;

## 

&#8220;Oh, sorry&#8221; He carefully put it back down on the pile. &#8220;How were they rigged to blow?&#8221;

## 

&#8220;Radio transmitter, simple, I know, but whoever it was could blow you up at will anywhere there´s a TRI comms beacon&#8230;&#8221;

## 

&#8220;Which is everywhere.&#8221;

## 

&#8220;Right. Who would want to do this to you?&#8221; she looked at him, apalled.

## 

He grinned, Raquel sighed, &#8220;OK, stupid question, who wouldn´t want to do this to you, right? But who wanted to and _had the means_?&#8221;

## 

&#8220;Oh, I have a pretty shrewd idea who wanted to do this, and who had the means. A guy just over my height, think he´s from Hyperial. Grey hair, likes to wear black clothes a lot.&#8221;

## 

&#8220;Personal?&#8221;

## 

&#8220;Oh no, strictly business.&#8221;

## 

&#8220;That doesn´t sound good.&#8221;

## 

&#8220;Well, no, it´s not exactly ideal.&#8221;

## 

He stood up again.

## 

&#8220;How much do I owe you for removing that pile?&#8221;

## 

&#8220;Well, I´m guessing you´re pretty short on cash right now, so whatever you can spare. I´m not exactly going to put them back, and it really wouldnt look too good for me if your ship blew up after I had carried out work on it, would it now?&#8221;

## 

&#8220;No, I suppose not. Well, tell you what, I´ll give you my pocket change&#8221;

## 

&#8220;Oh, I had hoped for a little more than tha&#8230;.&#8221;

## 

Arouin pulled out the big wadge of cash from his pocket, smiling mischeviously

## 

&#8220;This do?&#8221;

## 

&#8220;Uh&#8230; I´m not going to ask where you got that from, but I´ll take it. There is more, by the way. When I was running through the computer systems on that thing, I found two small additions to the escape capsule computer system; one is a transmission that the system is programmed to send out every second to the nearest TRI beacon; it´s highly encrypted, and I haven´t managed to break the encryption, but I guess it isn´t good news.&#8221;

## 

&#8220;No, I think not. You can remove it?&#8221;

## 

&#8220;Yes, I´ve isolated it, all I need to do is literally hit the delete button. The second addition that is coded into the escape capsule computer&#8230; it´s programmed to open the escape capsule hatch&#8230; while in deep space.&#8221;

## 

Arouin put his hands behind his head, whistled, and looked at the ceiling.

## 

&#8220;Son&#8230; of&#8230; a&#8230; bitch. Son of a _bitch_. This guy is going down. He is going DOWN.&#8221;

## 

&#8220;Thought you wouldn´t appreciate it. I´ve got that rigged for removal too.&#8221;

## 

Arouin had an idea, &#8220;Don´t remove it, I´ll pick up an EVA suit from the parts store just outside the hangar.&#8221;

## 

Raquel grinned, &#8220;You are a fiendish bastard aren´t you?&#8221;

## 

&#8220;Yeah, yeah, so sue me&#8221; he said, walking towards the hangar exit again.

## 

A few minutes later, he returned with an EVA suit, and dumped it into the cockpit; the cockpit sealed itself seperately from the rest of the ship, in the event of an emergancy, and headed for the nearest friendly station. Raquel stood outside, watching him. &#8220;Anything else?&#8221;

## 

&#8220;Yeah honey, one more thing. Can you go up to launch control and arrange for me to be launched in 90 seconds, from when you get there.&#8221;

## 

&#8220;Sure thing, you´ve deposited the money in my account.&#8221;

## 

&#8220;Yeah,&#8221; he smilied, &#8220;don´t spend it all on booze&#8221;

## 

&#8220;I hear ya´&#8221;

## 

Raquel headed for the entrance into launch control, as Arouin prepped the ship, then walked into it. It would take her 10 seconds to get up to launch control, and another 10 to organise the launch. 110 seconds.

## 

Arouin vaulted out of the open cockpit, and walked towards the man with the blue shirt, in his brown jacket as well now. The man was at the ship beside Arouin´s, a Quantar Typhoon, working on some power couplings under one wing.

## 

Arouin tapped him on the shoulder.

## 

&#8220;´Scuse me, what are you doing?&#8221;

## 

&#8220;I´m fixing up my ´phoon mate.&#8221; The man nodded and grinned, his black hair swining back and forth as he did so.

## 

&#8220;So what about the Interceptor earlier?&#8221; A flicker of indescision in the eyes of the other man

## 

&#8220;That was my friend´s, Pather.&#8221; 90 seconds.

## 

&#8220;Ok,&#8221; Arouin rubbed the stubble on his jaw, &#8220;that still doesn´t explain why you are using a plasma blowtorch to fix power couplings. You need a wrench mate, you need to unscrew here,&#8221; Arouin indicated with one, accusing index finger, &#8220;not fuse it together.&#8221; 70 seconds

## 

&#8220;I.. uhm&#8230;.&#8221;

## 

&#8220;You&#8230; uhm&#8230; what?&#8221;

## 

&#8220;I was just looking, wasn´t starting to repair it yet.&#8221;

## 

&#8220;Ok, so how long have you spent on this, and your friend´s Interceptor?&#8221;

## 

&#8220;Oh, I´ve been the past 3 hours, yeah, the past 3 hours easily&#8221; 50 seconds

## 

&#8220;Well, that Interceptor wasn´t even here when I docked, 2 hours ago, and I saw you in the mall not 45 minutes ago.&#8221;

## 

&#8220;I&#8230; uhm&#8230;&#8221; 40 seconds

## 

&#8220;You&#8230; uhm&#8230; die?&#8221;

## 

Arouin whipped out the pistol and stuck it in the man´s mouth, flicking off the safety and cocking it in one swift movement.

## 

&#8220;_Mffffff! MMMfefffffff! mmmmfffmfffmmmmm!_&#8221;

## 

&#8220;Sorry, I didn´t quite catch that&#8221; Arouin pulled the gun out of the man´s mouth, punching him in the stomach with the other hand. He doubled up with a grunt, the Arouin pulled him up by his hair. A few people around nearby ships were paying attention now. Street theatre rarely got better. 30 seconds

## 

&#8220;_Why are you spying on me?_&#8221;

## 

&#8220;I&#8230; I&#8230; I´m&#8230; not!&#8221; he answered in the strained tones of the hypnotically conditioned, sweating feverishly

## 

&#8220;_Wrong answer amigo_&#8221; The lift the Phoenix was on started powering up.

## 

Arouin aimed the pistol at the man´s chest at arm´s length, then pumped the trigger six times. The shots rang out and echoed across the hangar, the light from the blasts savagely illuminating Arouin´s impassive face with a cold, yellow light.

## 

As the body finished twitching, Arouin span, reloading, and sprinted for the side of the _Lithe Shadow_. He was half way up the ladder´s rungs to the cockpit when there was a rush of movement and two armed hangar guards pushed through the crowd around the body, pointing their weapons at him. Without hesitation he started firing at them as he continued to climb. The first shot hit a guard in the shoulder, sending him spinning to the floor. His second shot missed and ricocheted off the Typhoon fighter´s armour. The second guard was moved into a crouch and started firing before Arouin could adjust his aim, so the third shot went high, ploughing into the hip of a bystander, knocking them to the ground, screaming. The fourth and fifth shots hit the guard square as the lift started to descend, sending her sprawling. He jumped into the cockpit and slammed the hatch down as the Phoenix dropped beneath the level of the hangar floor. The light from above was cut off again as the doors closed over him, and there was a senstation of movement as he was brought into line with the launching tubes. The doors before him opened, and the Phoenix was spewed into space.

## 

A few moments after the _Lithe Shadow_ launched, a pair of station defence droids were sent in pursuit. Arouin decided not to engage them &#8211; he had enough pure speed to outrun them and he couldn´t spare the time to destroy them. He had other business to attend to. Possibly a bad idea to remain in Octavian space though, so it looked like he would have to leave his business here for later. His ship was modified; he could change his ship´s electronic and physical serial number at will, and he was ready for any nastyness The Director could pull on him in his own ship&#8230; so, time to head for Quantar space.

## 

The Director looked over his desk at the young Lieutenant.

## 

&#8220;Your spy was killed?&#8221;

## 

&#8220;Err, yes sir, I.. have the report right here.&#8221;

## 

He handed over a data-wafer to The Director.

## 

&#8220;And the electronic tracker removed as well you say?&#8221;

## 

&#8220;Report on the same wafer sir.&#8221;

## 

&#8220;Very well.&#8221;

## 

He drummed his fingers on the desk before him.

## 

&#8220;Well, the other trackers are still operational, aren´t they?&#8221;

## 

&#8220;Indeed they are sir.&#8221;

## 

&#8220;Well, we have no immediate problem then. Assign another spy to him. Someone slightly more subtle than the late Mr.Iffre.&#8221;

## 

&#8220;Yes sir, very good sir&#8221;

## 

The Lieutenant walked stiffly out of the room. The Director regarded the door he walked out of quietly for a few moments, then tapped a control set into the sleek, black desk, and the tracking screen slid out of it. He leant forward.

## 

_Quantar space, eh? Very interesting._

## 

It took Arouin 30 minutes longer than the normal trip from Outpost station to Quantar space; he had to lie low in an asteroid field for quarter of an hour to avoid some unwanted attention from a TRI-bounty-induced manhunt for a multiple murderer. Notoriety wasn´t as good as fame, but it tended to spread faster.

## 

Jumping into Quantar Core sector, he accelerated rapidly away from the jumpgate, turning swiftly to avoid a large rock that was right beside it. Checking the radar he could see hundreds of asteroids of all sizes and types spread throughout the sector, many of the larger asteroids with mining ships sitting beside them, leeching their mineral wealth. he surveyed the beige mass of the station as it hove into view. It really was damn ugly, spikes housing transmitters and mineral processing plants stretching out from it, asteroid impact craters spread all over it, the green Quantar factional insignia plastered here and there for good measure. And all beige; the Quantar colour of purity; the same colour robes that their infernal priests wore. The place was hell in a dogfight too&#8230; smaller opponents who had flown around the station for years loved to exploit their knowledge of it by flying fast and tight around it´s structural supports, weaving around the asteroids liberally scattered around it. And the damn greenies had even towed a ´roid into position near the docking rings, so their Quantar bretheren learning to dock could steady their nerves by taking a glance at the soothing big rock. Supposedly the rock showed the strength and steadfastness of the Quantar religious faith. But that rock scared the shit out of Arouin &#8211; what right minded person would stick a lethal lump of rock _in_ the docking rings of a major space station&#8230; nutters&#8230;

## 

After waiting for a few minutes for an Octavian cargo tow to complete it´s docking, Arouin moved the _Lithe Shadow_ forward into the docking tube. The hatch closed behind him, blocking off the light from the stars, and a sincere voice boomed out over speakers in the airlock,

## 

&#8220;Welcome, heathen, to Quantar Core station. Let Roh guide you through your troubled life. Take the One True God into your Soul, embrace Him as he is willing to Embrace you&#8230;&#8221;

## 

The voice boomed on, and lights came on in the airlock as the lift started up.

## 

_That speaker,_ thought Arouin, _is really beginning to piss me off._

## 

Checking that the lift was filled with air, he over-rode the cockpit release, and pushed it open.

## 

&#8220;&#8230;he will cherish you, and embrace you as the True Quantar embrace the Rock. For he knows that is not your fault that you were born a heathen, and he wants&#8230;&#8221;

## 

Three shots rang out, reverberating off of the closed walls, and there was a whine from a ricochet. The speaker buzzed in a very broken way. Arouin sat back down in the cockpit. There was a crackle again from off to his left. He looked. There was another speaker.

## 

&#8220;Do not forsake the Great God Roh, for He has not forsaken You. Love him with your heart&#8230;&#8221;

## 

Arouin looked incredulously at the speaker before unleashing another four rounds, it descending into electrical hell along with it´s sibling. Arouin pulled out a clip from his inside jacket pocket, hammered it into place, and recocked the pistol. He looked to his right. Another speaker. He sat there for a moment. It crackled into life. He raised the pistol again, raising one eyebrow with it. The operator of the system evidently thought it best not to push his luck, and there was another crackle as power was cut. Arouin relaxed, holstered his pistol, and sat down.

## 

The lift continued upwards for another twenty seconds or so, before the doors above him snapped open and the lift locked into place in the hangar floor. he swung his legs over the edge of the cockpit, closed it, then had second thoughts and opened it again. Rummaging around in his locker beside the base of the chair, he pulled out another four pistols one after another. Two went into his belt at the back, one into each inside jacket pocket. He took another handful of clips for good measure and stowed them in pockets about his person. Closing and locking the cockpit, he climbed down the ladder and made a run for the closest hangar exit as three priests converged on him from different angles, desperate to convert him to the One True Religion. Making it to the exit without too much priestly attention, he headed for the central mall. He was looking for some Chaplains.

## 

There was a noticable difference from the Octavian mall that made up the center of Outpost station; in a way the mall was the station. The only reason that anyone went to a station was to buy or to work. And, with the exception of the hangars, the malls were where everyone bought stuff or worked. the interior of the cavernous space was painted beige, the entrance to any shop was an arch, there were actual waste receptacles for rubbish instead of dumping it wherever you felt like. And there were Peacekeepers &#8211; security guards with a very strict view on any misdemanor. Now&#8230; the Green Chaplains&#8230; where would he find them. He looked around the mall at the various entrances into establishments that it offered. Pick a church, any church. There were four on this level alone. Each catering to the same faith, but with a slightly different spin or emphasis on the same teachings&#8230; the four here were the Church of the Undying Rock, the Church of the Pure Soul, the Church of the Ebon Night, and the Church of the Smiting of Infidels. The Green Chaplains were Quantar factionalist hardliners&#8230; so&#8230;

## 

Arouin turned to the Church of the Smiting of the Infidel, and walked into the gaping maw of the entrance.

## 

Inside there was a congregation of twenty to twenty-five people sitting on pews, facing the pulpit, where a beige-robed priest stood, reading from a chained lectern, preaching on how evil the ways of the other factions were; how they threatened to corrupt the souls of all Quantar. Arouin cleared his throat loudly, and the priest looked up at him.

## 

The people in the pews turned, many with a glint in their eye; a glint Arouin was not too sure he liked. He felt distinctly out of place.

## 

Arouin made eye contact with the priest.

## 

&#8220;I need&#8230; to see&#8230; a Chaplain.&#8221;

## 

The priest relaxed, and most of the congregation turned back. The robed man at the pulpit gestured off to one side of the front row of pews.

## 

&#8220;Through here, converted one&#8221;

## 

Arouin bowed slightly, then, with his head down, walked past the congregation and shouldered his way through the wooden doorway that was before him.

## 

Behind the doorway there was a small room with a desk, bookshelf, and computer terminal. The wizened man behind the terminal looked up at Arouin distastefully from behind a pair of glasses. Although laser treatment was readily available, some Quantar churches forbade their members to have any surgery unless the problem was life threatening; altering the body was dangerously close to altering the soul.

## 

&#8220;Yes?&#8221;

## 

&#8220;I´m here to see a Chaplain.&#8221;

## 

&#8220;I can see that, you had to have been; otherwise you wouldn´t have made it through the church alive.&#8221;

## 

&#8220;Ah.&#8221;

## 

&#8220;First time?&#8221;

## 

&#8220;Uh&#8230; yes. I need to speak to someone about a spy in your organistation.&#8221;

## 

The old man´s brow creased into a frown.

## 

&#8220;A spy?&#8221;

## 

&#8220;Yes.&#8221;

## 

&#8220;Hmm&#8230; well&#8230; you had better follow me.&#8221;

## 

He stood up shakily and walked unsteadily towards the second door in the room. Arouin followed him out into a high-ceilinged corridor, where they stopped outside a wooden door. The man held a finger to his lips, then knocked three times, quietly, on the door.

## 

&#8220;Enter, brother.&#8221;

## 

The door slid open quietly, well oiled hinges not letting out so much as a murmur. Inside there was another desk, with another, younger, bulkier man sitting behind it. he was wearing the same beige robes, but with body around, two holsters, a rifle slung over the back of the chair, and a shaved head. There were pieces of paper framed on the walls behind plexi-glass coverings, and bookshelves cluttered with a mix of religious texts and books on maintenance and use of a variety of firearms, some of which Arouin had never even heard of. And he had heard of a lot of different firearms.

## 

&#8220;This&#8230; _Octavian_,&#8221; the old man almost spat out, &#8220;wishes to speak with you&#8230; about a spy.&#8221;

## 

That drew his attention away from Arouin´s somewhat thuggish attire and to his face.

## 

&#8220;A spy?&#8221;

## 

&#8220;Indeed&#8230; Chaplain.&#8221;

## 

&#8220;You may leave us, brother Methrew.&#8221;

## 

Arouin watched the little man bow slightly, walk backward out of the office, and shut the door quietly behind him. He turned back to the man behind the desk.

## 

&#8220;You have information on a spy?&#8221;

## 

&#8220;Yes, yes I do. But I want payment.&#8221;

## 

&#8220;That can be arranged,&#8221; said the man, smiling at him, &#8220;but money is trivial compared to faith. And you have information on one of us whose faith is somewhat&#8230; lacking. But I´m not going to take your word for it&#8230; I´m going to need proof.&#8221;

## 

&#8220;Okay&#8230; you deposit half a million credits into this account,&#8221; Arouin passed over a small card with a number scrawled on it, &#8220;and I will then give you the name of the spy, and go to get the evidence for you. Then, when I have the evidence, you will deposit a further million credits into my account. Are these terms acceptable?&#8221;

## 

&#8220;They seem acceptable to me; you may watch me deposit the money now.&#8221;

## 

Arouin stood, and walked around the desk to view the terminal screen. A few deft taps later, half a million credits were transferred into Arouin´s personal fund. He smiled as the man turned around to him on his swivel chair.

## 

&#8220;The name?&#8221;

## 

&#8220;Thorest Vippen&#8221;

## 

&#8220;Hmmm&#8230; let me check the database.&#8221; He turned back to the screen, then looked irritably over his shoulder at Arouin, &#8220;Do you mind?&#8221;

## 

&#8220;Oh, sorry.&#8221; Arouin walked back around and seated himself again.

## 

&#8220;Yes, we have him registered, he is preaching here in Quantar Core&#8230; on the forty seventh level, G sector. Door to door conversion of heathens; he has an appartment in the same area, serial 47-G-7482-A. Well, at least part of your story is true&#8230; we do have someone under that name. And the evidence.&#8221;

## 

&#8220;I´ll be back here with it in two days time, maximum, hopefully only one.&#8221;

## 

&#8220;May the Great God Roh go with you&#8221; said the Chaplain, standing.

## 

&#8220;Uh.. thanks I guess.&#8221; Arouin, smiled, nodded, opening the door and walking out, the closing it behind him.

## 

_A spy?! In the Green Chaplains?_, thought the Chaplain, _If the Octavian heathen is right&#8230; it could be very dangerous, and best if the problem was&#8230; exorcised. If he was wrong&#8230; he could be.. taken care of._

## 

The Chaplain smiled grimly to himself.

<h2>
Part XII
</h2>

<h2>
</h2>

<p>
Arouin walked back down the corridor, occasionally glancing toward the door he had left behind him, then pushed open the door into the small ante-chamber. The old man was there again sitting behind his desk and looking very annoyed, a massive furrow across his brow which was contorted into a scowl. He barely glanced up as Arouin passed, eyes focussed on the terminal screen before him. Arouin barely spared him a glance as he passed into the church proper, avoiding eye contact with the congregation, walking quickly and fidgeting with his large-cal pistol out of sight under his jacket.
</p>

<h2>
</h2>

<p>
<em>The screen was showing a view of the interior of the office within which Arouin had talked to the Chaplain. The Chaplain looked pleadingly up at the camera.</em>
</p>

<h2>
</h2>

<p>
Arouin looked around suspiciously as his left the church, wary of anyone who could be watching his movements. It wasn´t exactly common for an Octavian to walk right into a Quantar church, let alone into a Church of the Smiting of the Infidel. It was even less common for one to come out. Well, walk out. He pushed through a throng of people looking at and listening to a beige-robed figure on a wooden packing crate, blank looks on their faces as his shadow passed across them.
</p>

<h2>
</h2>

<p>
<em>The old man looked furiously at the figure on the screen.</em>
</p>

<h2>
</h2>

<p>
<em>&#8220;One and a half million credits? We don´t have that sort of money to throw around!&#8221;</em>
</p>

<h2>
</h2>

<p>
<em>Now&#8230; to find a certain verdent preacher.</em> thought Arouin to himself as he strode with seeming confidence through the middle of the mall towards the nearest grav-lift. His eyes flickered over faces in the crowd.
</p>

<h2>
</h2>

<p>
<em>Who have I seen before? Who haven´t I seen before&#8230; but has seen me?</em>
</p>

<h2>
</h2>

<p>
<em>The technician summoned his superior to his monitoring console in the dimly-lit room with a swift beckoning of his hand.</em>
</p>

<h2>
</h2>

<p>
&#8220;He´s left the Church alive&#8221;
</p>

<h2>
</h2>

<p>
The senior officer leant over the other man´s shoulder.
</p>

<h2>
</h2>

<p>
&#8220;Well, so he has. Bring him in, we will want to find out what he is doing&#8230; Well&#8230; someone will.&#8221;
</p>

<h2>
</h2>

<p>
<em>&#8220;Very well,&#8221; the technician paused for breath, switching comms channels, &#8220;this is the Rock to Pebble three seven niner. Subject </em>Charmed<em> is to be brought in immediately.&#8221;</em>
</p>

<h2>
</h2>

<p>
Arouin had picked up his shadow now. He was good, he knew his craft; but Arouin had been hunted for the past 5 years. You learnt fast as a pirate, or you stopped being one a lot faster than you anticipated. The man was thirty, perhaps thirty five, Mr. Average, as every good field agent should be. His hair was starting to grey a little, light blonde colour though, easily dyed, no amazingly obvious facial features, a reversible jacket, had a pair of shades in one pocket, and Arouin had seen him wearing glasses at least once. Turning off to one side, Arouin popped into a small shop and picked up a magazine, spending a good two or three minutes browsing the shelves. As he walked off again with the mag tucked under one shoulder, his shadow appeared about five meters behind him, and continued tailing.
</p>

<h2>
</h2>

<p>
<em>&#8220;Well,&#8221; said the old man, &#8220;he´ll have to take two hundred thousand instead of the million. We need the proof that Vippen is a spy&#8230; and we won´t get the proof ourselves with violence&#8230; you know as well as I do that any decent spy has at least three way´s to suicide. This Octavian might be the only way we have of uncovering any heretics that the Chaplains are harbouring. We´ll wait it out&#8221;</em>
</p>

<h2>
</h2>

<p>
Arouin was looking at his shadow´s reflection in a show window when the unobtrusive stalker put his hand to his ear, and tilted his head to one side. Then he nodded, and started striding quickly towards Arouin. Arouin started moving towards one side of the mall area, and when he walked past an alleyway, he darted inside it´s welcoming shadow. A few seconds later, glancing from side to side, his shadow followed him.
</p>

<h2>
</h2>

<p>
There was a gleam in the darkness, and then polished metal pressing itself against the underside of the shadow´s chin. Arouin´s face emerged slowly from the darkness alongside his shadow´s.
</p>

<h2>
</h2>

<p>
&#8220;I don´t know who you are,&#8221; he said, addressing the agent´s controllers, &#8220;and I don´t know why you are following me. But don´t. Training agents is expensive. I´d hate to waste any more of your money.&#8221;
</p>

<h2>
</h2>

<p>
<em>The technician jumped up and shouted involuntarily, knocking his headphones away as the gunshot overloaded them, hurting his ears. He scrambled back to his seat and put the headphones on again in a hurry as everyone else in the room turned to look at him</em>
</p>

<h2>
</h2>

<p>
&#8220;This is The Rock to Pebble three seven niner, come in please.&#8221;
</p>

<h2>
</h2>

<p>
His superior started to walked over to him.
</p>

<h2>
</h2>

<p>
&#8220;This is The Rock to Pebble three seven niner, come in please, repeat, come in please, over.&#8221;
</p>

<h2>
</h2>

<p>
The technician wiped the sweat from his brow with a slightly grimy hand, and turned to his superior.
</p>

<h2>
</h2>

<p>
&#8220;We´ve lost three seven niner&#8221;
</p>

<h2>
</h2>

<p>
<em>&#8220;So I hear. I want this man. I want him yesterday.&#8221;</em>
</p>

<h2>
</h2>

<p>
Arouin stepped out of the alleyway, glanced around again, and hurried off.
</p>

<h2>
</h2>

<p>
<em>One more player in the game&#8230;</em>
</p>

<h2>
Part XIII
</h2>

<h2>
</h2>

<p>
<em>The Controller looked over the shoulder of the technician, at the vital signs of the agent which had a close encounter of the violent kind with Arouin. They were all flat, and a small red light blinked on and off balefully in the corner of the screen. The Controller was not a man who easily gave into his feelings, but he had just lost an agent, and he was not happy. He grasped the back of the technician´s swivel chair, and turned it around until the seated man was looking up into his eyes.</em>
</p>

<h2>
</h2>

<p>
&#8220;Code Orange.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Yes sir.&#8221;
</p>

<h2>
</h2>

<p>
The technician swiveled back to face the screen.
</p>

<h2>
</h2>

<p>
<em>&#8220;This is the Rock. All Pebbles, Code Orange on subject </em>Charmed<em>&#8220;</em>
</p>

<h2>
</h2>

<p>
<em>&#8220;Our friend is about to have a rather bad day.&#8221;</em>
</p>

<h2>
</h2>

<p>
Arouin walked calmly away from the scene in the alley, past a couple of Peacekeepers wearing armour and weaponry almost intimitdating as the hard gaze of the reflective faceplate on their helmets. He studiously ignored them and headed into a secondary mall area off of the main one. He needed some sleep&#8230; he hadn´t slept for at least 40 hours, and it was beginning to take it´s toll.
</p>

<h2>
</h2>

<p>
<em>Could do with a toilet and a shower while I´m at it.</em> Arouin mused.
</p>

<h2>
</h2>

<p>
Arouin didn´t have his own quarters at Quantar Core, real estate was expensive on space stations, and as time went on it became more and more expensive, as the pressures of an expanding population took their toll on society in the forms of increasing population density, decreased living space per person. Arouin did, however, own a small place on the 16th level, 3rd outrigger at Octavius Core station, and another decently-sized pad at Lothar´s Landing, both under assumed names, of course. Not that those helped him now; if he wanted a place to stay he would need to rent it, and he needed to contact Thorest Vippen, and then actually get the positron coupling thing, oh, and stay alive, too.
</p>

<h2>
</h2>

<p>
<em>Piece ´o cake.</em> he thought, bitterly.
</p>

<h2>
</h2>

<p>
After a few minutes walking he came across a decent-looking place next to a Che-tak resturant, and he ducked inside. The owner was a tall, wirey, balding man, about 70 years old, who looked up from a magazine as Arouin´s face appeared over the counter.
</p>

<h2>
</h2>

<p>
&#8220;Hi there, how much for a room?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;How long?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;12 hours. Do you have insurance?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Yeah, I have damages insurance, costs a lot, but you need it these days. Why, you planning to cause trouble?&#8221; he said, looking at Arouin suspiciously with slitted eyes and lips pushed into a thin, harsh line.
</p>

<h2>
</h2>

<p>
&#8220;No,&#8221; he smiled, &#8220;I don´t plan to cause any trouble&#8221;
</p>

<h2>
</h2>

<p>
<em>Although I´m fairly sure that someone will want to cause some trouble to me</em>
</p>

<h2>
</h2>

<p>
The old man nodded and smiled, handing Arouin a key as he said,
</p>

<h2>
</h2>

<p>
&#8220;Four hundred credits&#8221;
</p>

<h2>
</h2>

<p>
Arouin though it was a bit steep, but he couldn´t be bothered to haggle, so the old man happily took four hundred credits from Arouin´s wallet in cash, buying himself more trouble per credit than he could have obtained anywhere else.
</p>

<h2>
</h2>

<p>
Arouin walked down the corridor, sparing a glance for a framed certificate authorising the proprietor to provide lodgings for money. It was about 20 years old. He went past 4 doorways before he came to his own, stuffed the electronic wafer-key in the lock, then pushed the door open. It was sparsely decorated, but then, what did you expect? It had a bed, a toilet, and a shower. But here he only needed two of those. After relieving himself and having a very relaxing, warm shower, Arouin felt considerably refreshed. Some sleep would be nice, but it would have to wait another half hour or so. Arouin pulled a fist-sized lump of plastic explosive from one pocket, neatly wrapped in plastic film, a detonator, IR sensor, and timer, and laid them all on the bed. Then, he went into the en suite bathroom, turned on the tap just enough to make an audible trickling noise, then turned on the light and the radio. Then he went to work.
</p>

<h2>
</h2>

<p>
Ten minutes later he looked carefully out of the front door of the hotel at the closest security camera. As it´s sweep passed away from the doorway, he sprinted over to the wall under the camera, waited a few moments, then ran around a corner. Five minutes later he was at another hotel, signing in as Jezenth Hurr, with a fake ID and a second account. This hotel didn´t have showers. The corridors were painted black, so as to not let the grime show, and rubbish was piled up in the corners, graffiti on the walls. After paying the reasonably extortionate price of two hundred credits, he went inside the room and pulled out a bottle of synthetic whiskey. He took a very small swig, then put some on his hand and daubed it over his face. Running his hands along the wall of the corridor turned up enough grease to keep a cleaner at work until retirement, and he sighed inwardly before rubbing his hands over his face. Then he took a pillow from the bed, and walked into the corridor outside, locking the door behind him. He walked over to one corner of the corridor, threw the pillow down, then fell on it heavily. After pouring most of the whiskey onto the floor around himself, leaving the bottle open on it´s side, he dragged some rubbish over himself and went to sleep.
</p>

<h2>
</h2>

<p>
<em>&#8220;Ok, are they in position?&#8221;</em>
</p>

<h2>
</h2>

<p>
&#8220;Yes sir, in position.&#8221;
</p>

<h2>
</h2>

<p>
<em>&#8220;Ok, two minutes, pass the word&#8221;</em>
</p>

<h2>
</h2>

<p>
<em>A group of six men in black suits were lined up inside the corridor, backs against the wall.</em>
</p>

<h2>
</h2>

<p>
&#8220;We´re hot, one minute to go&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Check&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Check&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Check&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Check&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Check&#8221;
</p>

<h2>
</h2>

<p>
The men readied their weapons, cut down assault rifles for room clearing duty. Grenades, explosive charges and motion trackers were hanging from them and their uniforms like some sort of twisted jewelery.
</p>

<h2>
</h2>

<p>
&#8220;Ok, we are go. Code Orange, we need to keep his brain undamaged, no headshots. Don´t care what you do to his body but you mustn´t hit the brain, or they can´t hook it up and interrogate it&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Check&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Check&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Check&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Check&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Check&#8221;
</p>

<h2>
</h2>

<p>
The officer moved around the corner of the corridor, his five men following him. They passed the doors until they reached RD-242-05.
</p>

<h2>
</h2>

<p>
&#8220;Sort the lock&#8221;
</p>

<h2>
</h2>

<p>
The third man in the team moved forward, and brought up a small electronic device which he secured in place over the lock, then depressing a small black button on it. After two seconds a light on the device flashed, and he removed it again.
</p>

<h2>
</h2>

<p>
&#8220;Fibre check&#8221;
</p>

<h2>
</h2>

<p>
The fourth man moved quietly forward, then put a piece of fibre-optic under the door whist examining the room via his helmet eye-piece.
</p>

<h2>
</h2>

<p>
&#8220;Looks like he´s in the bathroom. Light on, door partially open&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Roger that. Ok team. Five, six, take point. Two, swing right, I provide back-man support.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Check&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Check&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Check&#8221;
</p>

<h2>
</h2>

<p>
&#8220;We´re hot. Go go go.&#8221;
</p>

<h2>
</h2>

<p>
They moved like a well oiled machine. The first two men knocked the door open as they charged through it, the third breaking off to the right. The officer moved in swiftly, after a split-second glace around the appartment all four snapped around to face the bathroom door. Five kicked it in, two and six already firing low to cut up the target´s legs. The movement by the bathroom door triggered the IR sensor. The electrical pulse from the IR sensor shot down the wire to the detonator in the commode at lightspeed. The detonator in the explosive in the commode detonated. The explosive in the commode, reaching a critical temperature, exploded. The fireball blew out the walls of the room, melting the flesh from the four people inside, then consuming the bones, leaving behind a thin calcium residue that the forensics team, three days later, would even outside in the main mall corridor. The people in the six rooms adjacent to Arouin´s died from the blast, most from incidental damage such as furniature smashing into them from the shockwave, a couple from the fire afterwards. The old man running the hotel was out that night at a local inn, and didn´t die in the blast. Three pedestrians were killed when the wall from the hotel blew out across the street, all three crushed against the far wall of the mall corridor by high-velocity pieces of Arouin´s room. And the blast also woke up Arouin.
</p>

<h2>
</h2>

<p>
&#8220;Sir, the ops team was wiped out by a large explosion.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;I thought you said the camera didn´t pick him up leaving that place!&#8221;
</p>

<h2>
</h2>

<p>
The Controller was mad now, face red
</p>

<h2>
</h2>

<p>
&#8220;I&#8230; he&#8230; didn´t, you viewed the footage yourself!&#8221;
</p>

<h2>
</h2>

<p>
&#8220;DAMNIT. Run a computer search on all cameras for the nearest 6 sub-malls.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Yessir&#8221;
</p>

<h2>
</h2>

<p>
Moments passed.
</p>

<h2>
</h2>

<p>
&#8220;We´ve picked him up, he must have evaded the camera and then taken another lodging&#8230; here&#8221;
</p>

<h2>
</h2>

<p>
The technician stabbed one finger at the hotel showing in the view from a security camera with Arouin walking into it, 5 hours ago.
</p>

<h2>
</h2>

<p>
&#8220;Ok, send op teams two and three over there now.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Code Orange?&#8221;
</p>

<h2>
</h2>

<p>
<em>&#8220;Yes, still Code Orange.&#8221;</em>
</p>

<h2>
</h2>

<p>
Twelve men. Same gear. Same guns. They moved down the corridor slickly, covering zones and keeping tight, watching all angles. They burst into the final corridor suprisingly quietly. Eight covered the door to Arouin´s room, four swang to the other side of the T-junction to see a drunk lying in the corner, stinking of whiskey and covered with rubbish. They turned to face the door as the drunk let out a loud snore.
</p>

<h2>
</h2>

<p>
&#8220;Nine, Lock, Ten, Fibre&#8221;
</p>

<h2>
</h2>

<p>
Nine and ten moved forward and set about their tasks.
</p>

<h2>
</h2>

<p>
Arouin opened an eye.
</p>

<h2>
</h2>

<p>
&#8220;Lock sorted&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Fibre; something in the bed.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Ok, we move in fast, cover all angles. This guy took out the whole of team one, so be real careful.&#8221;
</p>

<h2>
</h2>

<p>
Arouin carefully, slowly, screwed on silencers to a pair of 9mm caseless pisols.
</p>

<h2>
</h2>

<p>
&#8220;Ready?&#8221;
</p>

<h2>
</h2>

<p>
A chorus of quiet &#8220;Check&#8221;s came from the assembled team.
</p>

<h2>
</h2>

<p>
&#8220;Check&#8221; murmured Arouin as he rose to his feet, a silenced pistol in each hand, one arm hanging by his side, the other raised. He sideled towards the ops team. One kicked in the door, Arouin fired, the pistol making a hollow ´thup´ noise, as Twelve collapsed with the back of his head turned to a mush. Eleven looked around at his squadmate, who was mid-way to the floor, blood hanging in the air from the impact of the shot. He looked back
</p>

<h2>
</h2>

<p>
´thupthup´
</p>

<h2>
</h2>

<p>
Ten and nine turned
</p>

<h2>
</h2>

<p>
´thup´
</p>

<h2>
</h2>

<p>
Nine took a shot through the faceplate of his visor, the clear plexi-glass turning red with jagged white lines running through it.
</p>

<h2>
</h2>

<p>
<em>&#8220;WHAT THE?&#8221;</em>
</p>

<h2>
</h2>

<p>
´thupthup´
</p>

<h2>
</h2>

<p>
One, Two and Three were all inside the room now. The squad began to turn.
</p>

<h2>
</h2>

<p>
Arouin raised the other pistol at arm´s length, and walked through the dying ops team members
</p>

<h2>
</h2>

<p>
´thupthupthup thupthup´
</p>

<h2>
</h2>

<p>
Seven convulsed as he went down, his finger jerking on the trigger as he fell. The roar of the cut-down assault rifle brought the attention of the first three team members to the events outside. Five dived out the way of a hail of bullets from the akimbo pistols, slamming into the corridor wall, then into the floor. Arouin jumped back into the main corridor, out of the T-junction, and threw a fragmentation grenade around the corner as he reloaded the twin pistols. There was the hollow ´crump´ of an explosion, and a hail of metal shards flew past the end of the T-junction, making tinny ricocheting noises as they hit the metal wall at the other end.
</p>

<h2>
</h2>

<p>
Arouin launched himself sideways into the corridor again, guns spitting out flame, metal and death. Five went down quickly, bullets ripping through his shoulder, chest and gut, Four took a round in the knee and a couple in each leg, shattering them and sending pieces of blood and bone flying. Arouin hit the ground, rolled across the corridor firing down it at the shadowy forms of One, Two and Three as the trio scrambled for cover. Two was caught on the heel by a shot, then one of Arouin´s pistols made the ominous click of a gun with no rounds left. After another two shots the pistol in his left hand ran dry.
</p>

<h2>
</h2>

<p>
&#8220;He´s out, go go!&#8221;
</p>

<h2>
</h2>

<p>
One and Three´s rising forms filled up the doorway ahead as Arouin dropped both pistols and pulled out his large-cal, clasping it in two hands as he lay on the floor. The fierce bark of the pistol sounded four times, flame roaring out, Three was knocked off of his feet by a double hit to the chest, the shots making wet smacking noises like someone hitting a steak with a baseball bat as they slammed home. One´s left leg was blown off at the knee, then as he toppled screaming he took another round through the shoulder. Arouin leapt off the floor and sprinted to the doorway as Two tried to drag himself into the bathroom, standing over the man he put a round into each of his legs. He screamed, and Arouin dropped the large-cal pistol, and pulled out another pair of 9mm caseless guns. He rolled Two over with one foot, and knealt down beside the screaming man.
</p>

<h2>
</h2>

<p>
&#8220;We need to talk.&#8221;
</p>

<h2>
Part XIV
</h2>

<h2>
</h2>

<p>
The black suited special operations team member looked up at Arouin in agonised fear. Blood seeped from three gunshot wounds in his legs and his breathing was laboured, fogging up his mask. Arouin ripped the mans helmet off and looked at him, put his head on one side and then pointed a pistol at his eye.
</p>

<h2>
</h2>

<p>
&#8220;Who are you and your recently demised friends working for?&#8221;
</p>

<h2>
</h2>

<p>
The man looked into Arouin´s eyes. Arouin moved the pistol closer until half the troop´s vision was occupied by the black tunnel and its potential death.
</p>

<h2>
</h2>

<p>
&#8220;Who?&#8221;
</p>

<h2>
</h2>

<p>
The ops team member was staring into the barrel, sweating furiously. Gradually his gaze became less focused. He convulsed, then slumped. Arouin stared in disbelief, and used his pistol to open the man´s mouth, a faint grey whisp emerging as he did so. Inside, there were the shattered remains of a hollow tooth. Arouin squatted on his haunches, tapping the gun on the floor idley as he thought of what to do.
</p>

<h2>
</h2>

<p>
<em>Someone wants me dead. Not new news. Someone wants me dead enough to send two teams of special forces after me. And they have the resources to track my movements with startling accuracy. This is not good. Time to get out of here</em>
</p>

<h2>
</h2>

<p>
Arouin looked at the helmet an arm´s reach away, leant over and picked it up. He tapped the mike tentatively.
</p>

<h2>
</h2>

<p>
&#8220;Hello?&#8221;
</p>

<h2>
</h2>

<p>
<em>&#8220;Hoskins, Hoskins, you´re still alive? Your monitor must be broken, the system thinks you´re dead&#8221;</em>
</p>

<h2>
</h2>

<p>
&#8220;Sorry to disappoint, but Hoskins is very, very dead.&#8221;
</p>

<h2>
</h2>

<p>
A moment of silence on the other end of the line. Muttered converstation.
</p>

<h2>
</h2>

<p>
<em>&#8220;This is the Controller. If you give yourself up we will not take any further action against you. If you choose to remain at large you will be killed.&#8221;</em>
</p>

<h2>
</h2>

<p>
Arouin raised an eyebrow.
</p>

<h2>
</h2>

<p>
&#8220;Well, that isn´t very nice of you is it? You still haven´t answered my question, have you?&#8221;
</p>

<h2>
</h2>

<p>
Silence.
</p>

<h2>
</h2>

<p>
&#8220;Hello?&#8221;
</p>

<h2>
</h2>

<p>
Arouin slowly got to his feet and looked around, then scrabbled for his guns that he had dropped inside the room and the corridor outside it, reloading each as he picked them up, then grabbing one of the sawn-off assault rifles for good measure. He walked down the blood-splattered corridor, stepping carefully to avoid bodies on the floor, shell casing from the assault rifles skitting over the metal grating with quiet tinkling noises. He turned right around the corner to the hotel entrance and headed for the mall corridor outside. He drew just short of the entrance, and paused.
</p>

<h2>
</h2>

<p>
Silence.
</p>

<h2>
</h2>

<p>
Nothing was happening.
</p>

<h2>
</h2>

<p>
<em>Nothing</em>
</p>

<h2>
</h2>

<p>
No tramp of pedestrians, no chatter or bustle that occured at every time of synthetic day or night upon the brooding metal hulks that were the space stations. A loud voice boomed out from inside the mall corridor.
</p>

<h2>
</h2>

<p>
<strong>&#8220;Throw your weapons onto the floor outside and walk out with your hands above your head&#8221;</strong>
</p>

<h2>
</h2>

<p>
Arouin grinned. He knew how to deal with people playing it <em>this</em> way. He had done it <em>so many</em> times before. he took off his jacket, and pulled out his first pair of 9mm guns, throwing them outside the entrance. He slung the assault rifle on over the back of his dirty black t-shirt.
</p>

<h2>
</h2>

<p>
<strong>&#8220;The rest&#8221;</strong>
</p>

<h2>
</h2>

<p>
Arouin couldnt help smiling as he threw out the next pair of 9mm pistols, then put his jacket on over the cut-down assault rifle, making sure the barrel didn´t poke out below the bottom of the jacket.
</p>

<h2>
</h2>

<p>
<strong>&#8220;Move out now, slowly&#8221;</strong>
</p>

<h2>
</h2>

<p>
Arouin jammed his remaining pistol under the strap for his assault rifle at the nape of his neck, put both hands behind his head with the pistol grip within easy reach, and walked out calmly to meet the enemy.
</p>

<h2>
</h2>

<p>
Arouin looked left. Arouin looked right.
</p>

<h2>
</h2>

<p>
<em>Wow,</em> thought Arouin, <em>I didnt know they had</em> that<em> many Peacekeepers on this station. Well, I know who the other player in the game is then; the Quantar Government.</em>
</p>

<h2>
</h2>

<p>
Looking at the two lines of beige-uniformed peacekeeping troops cutting off any escape down the corridors, it slowly dawned on Arouin.
</p>

<h2>
</h2>

<p>
<em>I can´t&#8230; actually&#8230; kill them all. More Peacekeepers than I have bullets. Shit.</em>
</p>

<h2>
</h2>

<p>
<strong>&#8220;Remain where you are.&#8221;</strong>
</p>

<h2>
</h2>

<p>
The speaker was a Peacekeeper with heavier body armour than usual; an officer in the crops. A group of four people in black fataigues carrying restraining straps and a stretcher started moving towards him at a quick jogging pace, while the seventy or so Peacekeepers looked on impassively.
</p>

<h2>
</h2>

<p>
<em>This don´t look good.</em>
</p>

<h2>
</h2>

<p>
As they reached him, one went for his hands with a restraining strap, another bent to put one around his legs. He pulled out his pistol from behind his head while kicking the face of the man reaching for his legs, grabbed the man going for his arms around the throat, and stuck the gun barrel at one side of his head.
</p>

<h2>
</h2>

<p>
&#8220;I am getting out of here now or this man dies!&#8221; shouted Arouin, looking towards the Peacekeeper officer. As the man on the floor coughed teeth and blood the officer cocked his head to one side and put a hand to one ear, listening to his superiors. He muttered a command. Arouin looked on in astonishment as the Peacekeepers hefted their weapons into firing positions. As fingers jammed onto triggers, Arouin threw himself back into the hotel entrance, dragging the man with him. Bullets smashed into the frail tissue of the hapless medic, his corpse being torn out of Arouin´s grasp by the hail of metal. Arouin scrabled backwards along the floor, incoming fire eating into the corners of the entrance, seeking and probing for his warm flesh. He span, onto all fours, scrambled to his feet whilst shrugging off his jacket and bringing the assault rifle around into a useable position. He ran down the corridor to the corner, then crouched beside it, breathing heavily.
</p>

<h2>
</h2>

<p>
<em>Shitshitshitshitshit</em>
</p>

<h2>
</h2>

<p>
As the first Peacekeepers stormed into view Arouin held down the trigger, and franticly fighting recoil, hosed the assault rifle over the incoming law enforcement troops. The roar of the gun was pheonomenal, shell casings flying out at a ridiculous rate, the flame of the muzzle flash highlighting the beads of sweaton Arouin´s face. Slightly more than a second after he first pulled the trigger, the ammunition ran out. He threw himself out the way as the troops ran towards him firing down the corridor. He grabbed his pistol and threw the rifle to the floor as he reached the T-junction, stumbling over the bodies of the special ops team. Panting heavily, he picked up another rifle and a couple of clips which he stuffed into his belt. He went to grab a lump of explosive, then realise that it was in his jacket&#8230; which was lying on the floor somewhere back down the corridor and probably being trodden on by a large number of people all in a hurry to kill him. Arouin ran into his room and slammed the door shut, a futile gesture possibly, but at least it offered the suggestion of protection, which nothing else in the immidiate vicinity seemed to.
</p>

<h2>
</h2>

<p>
Arouin looked around, desperately thinking of something to get him out of the rather untenable situation he was in.
</p>

<h2>
</h2>

<p>
<em>Steal a uniform from an ops team member and pretend to be dead? Not enough time. Hide under the bed? Don´t be a bloody idiot.</em>
</p>

<h2>
</h2>

<p>
Arouin looked at the wall. The heavy thumping of a multitude of boots hammering against the floor, propelling the owners towards him.
</p>

<h2>
</h2>

<p>
Arouin lifted his assault rifle, grabbed at the trigger, and made a rough circling motion with the death-dealing weapon as it sprayed its leathal contents agaist the wall before him. Metal ruptured, Arouin kicked at the metal, knocking it through into the next store in the mall at the back of the hotel. It was a food store, with a hefty number of customers. Being the next mall along, the Peacekeepers had obviously not bothered to clear it since it was well out of the line of fire. Arouin pushed through the shoppers, some of which started screaming at the sight of the assault-rifle carrying sweating madman who had blow n through the shop wall, and ran out into the mall. He turned and ran, and ran. Through the screaming people towards the nearest grav-lift.
</p>

<h2>
</h2>

<p>
<em>I need to get that positron coupling or I really am dead&#8230;</em>
</p>

<h2>
</h2>

<p>
More screams behind him, gunfire. A young man walking with his girlfriend dropped to the floor as Arouin ran past, his head splitting open. Ricochets cut through bystanders. Arouin turned around to see the Peacemakers shouldering through the crowds, dropped to one knee and squeezed off a burst that sent two of them sprawling on the floor, tripping one behind them and staining their holy beige robes red with their own blood. Arouin dropped the rifle, and ran for the lift, only about a hundred meters away now, with a group of people waiting in front of it.
</p>

<h2>
</h2>

<p>
&#8220;OUT THE WAY!&#8221; he screamed as he charged into them, a Solrain youth near the front turning and looking at him and said
</p>

<h2>
</h2>

<p>
&#8220;What do you think you´re doing you fuc&#8230;&#8221;
</p>

<h2>
</h2>

<p>
Arouin pushed him aside, the young man grabbed his shoulder and span him around.
</p>

<h2>
</h2>

<p>
&#8220;I <em>said</em> what the hell do you think you´re&#8230;&#8221;
</p>

<h2>
</h2>

<p>
Arouin shot him in the kneecap with his pistol, as he screamed with pain the nearest members of the crowd paniced and began to push away from the lift which had just opened it´s doors. Arouin jumped inside, hearing a metallic whine as bullets hit the closing doors, and a couple of screams as crowd members were caught in the fire. The lift moved upwards, towards level 47.
</p>

<h2>
</h2>

<p>
After about twenty seconds the door opened, the lift chiming &#8220;Level 47, have a nice day&#8221; as Arouin stumbled out of it, panting.
</p>

<h2>
</h2>

<p>
<em>Sector G&#8230; 7482-A&#8230;</em>
</p>

<h2>
</h2>

<p>
Checking a nearby terminal, Arouin found directions, and walked quickly through the corridors, looking nervy. They could track him, it wouldn´t be instantaneous, but they knew which floor he was on for sure. He paused outside the door marked 7482-A, then knocked on it. A young man with stubble covering his chin answered, yawning.
</p>

<h2>
</h2>

<p>
&#8220;Hello?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Thorest Vippen?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Yah, me.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;We have a mutual aquiantance. A&#8230; Director&#8221;
</p>

<h2>
</h2>

<p>
The man raised an eyebrow.
</p>

<h2>
</h2>

<p>
&#8220;You had better come in.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Ok&#8230; but make it fast.&#8221;
</p>

<h2>
</h2>

<p>
The door closed behind him, and he took a seat on the bed in the cramped quarters. The only other furnishings were two cupboards set into the wall, a table with a terminal on it, and a chair. All a vaguely sickening lime-green colour, save for the desk which was painted dark brown in a failed attempt to look like real, good wood.
</p>

<h2>
</h2>

<p>
&#8220;What do you need.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Firstly, I need proof that you really are working for the Director, I need to know I can trust you.&#8221;
</p>

<h2>
</h2>

<p>
The man looked at him for a second, then nodded.
</p>

<h2>
</h2>

<p>
&#8220;Ok, I guess so. My orders are sent the old fashioned way, he leaves information in the features area of the floor 47 weekly bulletein, it´s a grid coded with a one time key, got the key on wafer right here.&#8221;
</p>

<h2>
</h2>

<p>
he flourished the thin, semi-transparent data wafer.
</p>

<h2>
</h2>

<p>
&#8220;Show me one.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Okay&#8230;&#8221;
</p>

<h2>
</h2>

<p>
the man leant over the terminal and brought up that weeks ads. Sure enough, about a third of the way down the page of links, between a recipe suggestion for synthetic meat stew and a short story there was a ´word search´ sent in by a reader, with several thousand letters on each side of the grid.
</p>

<h2>
</h2>

<p>
&#8220;Seems ok, guess I´ll have to take your word for it. Where is the positron coupling?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Well,&#8221; the man sat down on the chair and faced Arouin, &#8220;the Quantar government have it in an R&D establishment down on level 6. Won´t be easy to get it out of there&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Why do you think I´m here, a bloody holiday?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Fair enough.&#8221;
</p>

<h2>
</h2>

<p>
Arouin stood up, and made to go,
</p>

<h2>
</h2>

<p>
&#8220;Oh, by the way, where is the central security command post on this station?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Level 10, it´s signposted.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Thanks&#8221;
</p>

<h2>
</h2>

<p>
Arouin looked around as he left the appartment, and heard the hammering of feet in the distance. He ran down the corridor in the opposite direction to the one he had come from, and took the next grav lift down, fiddling idley with the data wafer he had palmed in the appartment.
</p>

<h2>
</h2>

<p>
<em>&#8220;Where is he, damnit?&#8221;</em>
</p>

<h2>
</h2>

<p>
&#8220;It´s level 47, sir, you know that the cameras there are always getting damaged. Bloody Octavians. We haven´t got complete coverage, we have units searching now&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Right, I want at least four man squads, order them not to split up. Get the other three ops teams on the sweep, we can´t let him get away. He has been enquiring about something that some people with higher ranks than me don´t want him to find&#8221;
</p>

<h2>
</h2>

<p>
&#8220;All very cryptical sir.&#8221;
</p>

<h2>
</h2>

<p>
<em>&#8220;Indeed.&#8221;</em>
</p>

<h2>
</h2>

<p>
&#8220;Level 15&#8221; chimed the lift. Arouin stuck his head out and checked the surroundings before going anywhere, then moved with apparent calmness down the corridor into one of the minor malls. He walked past a couple of shops and then looked up at the sign above him in satisfaction. The neon tubing telling the casual passer that Venn´s Oddbits was open for trading. Arouin pushed the black plexi-glass door open and walked inside. Curios and bits of old antiquities lined shelves of the tiny shop, which was a great deal smaller than most of the shops in the mall. The man behind the counter was shorter than the norm, had a shaved head, a network of scars across his face, a pierced nose and ear, and a large tattoo of a snake entwining a dagger on one arm.
</p>

<h2>
</h2>

<p>
&#8220;Do you wish to purchase something sir? We have a great many bits and bobs, the odd pre-collapse relic fragment even.&#8221;
</p>

<h2>
</h2>

<p>
Arouin stared at the man in the eye, past the ill fitting visage and words.
</p>

<h2>
</h2>

<p>
&#8220;I wish to buy a ring.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;What sort of ring?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;It must be gold. With seven emeralds.&#8221;
</p>

<h2>
</h2>

<p>
The man smiled.
</p>

<h2>
</h2>

<p>
&#8220;I´ll see what we can do&#8221;
</p>

<h2>
</h2>

<p>
Hitting a button underneath the counter, he turned to the door behind him and motioned Arouin to follow, as the neon outside flickered and died, and heavy bars closed across the entrance. Arouin closed the door behind them, walked down a corridor which was only a few meters long, then through another door, and into Venn´s Den. Crates lay on the floor, lids off and styro-foam packing spilling out, racks covered the walls, and shelving covered the small areas that weren´t already occupied by the racks or the crates. Black, menacing shapes lay under the styro-foam, on the racks, and on the shelving, with a very metallic feeling prevailing. Venn turned around to face around, and held his arms apart expasively.
</p>

<h2>
</h2>

<p>
&#8220;What do you want. If I haven´t got it, it either doesn´t exist, or I´ll be taking a delivery tomorrow&#8221;
</p>

<h2>
</h2>

<p>
Arouin grinned.
</p>

<h2>
</h2>

<p>
&#8220;Two silencers, two 9mm caseless snub-guns.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;There has to be more&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Of course there is more. I need another 13.5mm large-cal slugthrower&#8230;&#8221;
</p>

<h2>
</h2>

<p>
&#8220;You like the first one I sold you?&#8221;
</p>

<h2>
</h2>

<p>
Arouin showed him the large-cal pistol
</p>

<h2>
</h2>

<p>
&#8220;Yeah, look at the wear and tear on that thing. Love her.&#8221;
</p>

<h2>
</h2>

<p>
Venn smiled, &#8220;I aim to please&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Well, I need about 4 kilos of ´tex, 4 detonators, a remote, couple of flashers, a couple of frags&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Okay&#8230; you want to check these out? Rivo´s nicknamed em Det beans&#8221;
</p>

<h2>
</h2>

<p>
Venn scooped a handful of black coffee-bean sized pellets out of a small plastic container and showed them to Arouin
</p>

<h2>
</h2>

<p>
&#8220;Any testimonys?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;None yet.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;I´ll take 10, I want a third off them, they´re untested.&#8221;
</p>

<h2>
</h2>

<p>
Venn hesitated for a moment, thinking, then smiled again, &#8220;For you, done.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;How´d they work?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Grab one, squeeze it with 4 fingers like so&#8230;&#8221;
</p>

<h2>
</h2>

<p>
Venn demonstrated, pushing on 4 sides of the ovoid at the same time. A small light glowed a dull red, and Venn squeezed it again, the light turned off.
</p>

<h2>
</h2>

<p>
&#8220;&#8230;then you have 4 seconds. Squeeze again in that time interval and it dis-arms&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Gotcha&#8221;
</p>

<h2>
</h2>

<p>
Arouin followed Venn around as he grabbed the pistols and the explosives.
</p>

<h2>
</h2>

<p>
&#8220;How many clips do you want with those pistols?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;I´ll take fourteen fifteen´s, twelve seven´s for the 13.5´s&#8221;
</p>

<h2>
</h2>

<p>
Venn picked out twenty-six clips out of boxes lying around and dumped them in a plastic box nearby, then grabbed a pair of 9mm matte black silencers and dumped them into the same box. Arouin looked around as the other man took the selected weapons of devestation out of their storage places. His eye fell on an odd-looking weapon on some shelving next to the ´tex that Venn was loading into the box. He picked it up.
</p>

<h2>
</h2>

<p>
&#8220;What&#8230; the hell&#8230; is this?&#8221;
</p>

<h2>
</h2>

<p>
Venn turned and looked at the weapon Arouin was cradling in his arms. It was the same length as a submachinegun, but it had three barrels and an odd protrusion below the barrels of the gun.
</p>

<h2>
</h2>

<p>
&#8220;It´s a little something that the Solrain have cooked up for their special forces guys to use.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Yeah&#8230; okay&#8230; so, we have a sub, with.. onetwothree barrels on it, at what has got to be at least 10mm.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Yeah, bit chunky, no?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Well&#8230; you could say that&#8230; or you <em>could</em> say that the recoil would stick the <em>barrel</em> of the gun into the <em>roof</em> and the <em>back</em> of the thing <em>through</em> your <em>shoulder!</em>&#8221;
</p>

<h2>
</h2>

<p>
Venn laughed loudly, &#8220;That is why this is special. You know the artificial gravity they use on these space stations? Some tech-head has managed to miniaturize one to the degree that it can be mounted in the body of the gun. The specific gravity around the gun changes when you depress the trigger, feels a bit wierd&#8230; there is <em>no</em> recoil.&#8221;
</p>

<h2>
</h2>

<p>
Arouin looked at him incredulously.
</p>

<h2>
</h2>

<p>
&#8220;How much?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;One hundred K´s&#8221;
</p>

<h2>
</h2>

<p>
Arouin looked at the gun nestled in his arms.
</p>

<h2>
</h2>

<p>
&#8220;Uh&#8230; okay. How much ammo per clip?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Round about two hundred rounds, uses depleted uranium pellets with shaped ´tex charges in the back, the grav-generator that powers the anti-recoil mechanism is what gives it the velocity.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;What´s the firing noise like, the rate of fire?&#8221;
</p>

<h2>
</h2>

<p>
Venn scratched his head, &#8220;It´s really, really wierd. I mean&#8230; it´s definately distinctive. Not as quiet as a standard railgun, but then you don´t get handheld railers. No muzzle flash, but I wouldn´t call it stealthy. The rate of fire is about 50% faster than a standard sub, the barrels are that size because the outside of the barrel needs to house the field inside it, and they can´t be re-fired too quickly, that´s why there´s three, see?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Okay, I´m game&#8230; I´ll need&#8230; call it ten clips for this baby too.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Sounds good. How do you want it packaged.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Take-away&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Smart or laid back?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Got anything in between?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Hmm&#8230;&#8221;, Venn scratched his head, &#8220;&#8230;you need quick access I take it?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Yeah.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Well&#8230; I haven´t got a bag. Where did your jacket go?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;It&#8230; got left behind.&#8221;
</p>

<h2>
</h2>

<p>
Venn wandered off to the other side of the Den and rooted around in a green metal chest.
</p>

<h2>
</h2>

<p>
&#8220;What do you think?&#8221;
</p>

<h2>
</h2>

<p>
Venn was holding up a grey-black trenchcoat. Arouin raised an eyebrow.
</p>

<h2>
</h2>

<p>
&#8220;What? It will hold everything, I am sure. Maybe not the ´tex&#8230;&#8221; said Venn, somewhat mollified by Arouin´s expression.
</p>

<h2>
</h2>

<p>
&#8220;No, I don´t think a trenchcoat can hold 4 kilos of ´tex.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Okay, I´ll box the ´tex for you and you can pick it up before you leave?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Guess so. Got any more of those?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Yeah&#8221;
</p>

<p>
&#8220;Got a black one?&#8221;
</p>

<p>
&#8220;This is black&#8221;
</p>

<p>
&#8220;Bullshit is that black. That´s black after it´s been used as a duster for five years.&#8221;
</p>

<p>
Venn sighed, and walked to another chest, and opened it. He braced his arms against the sides, and looked sideways at Arouin
</p>

<p>
&#8220;You understand, I´m only doing this because you are such a good customer. This is new.&#8221;
</p>

<p>
He held out a black one which was, indeed, new. Arouin grinned broadly.
</p>

<p>
&#8220;Now you´re talkin´&#8230;&#8221;
</p>

<p>
A few minutes later a black-trenchcoated figure hurried out of the doorway and to the grav-lift.
</p>

<p>
&#8220;Level 10.&#8221; Arouin looked out again, then moved quietly out, staying near the wall as he followed the gently curving corridor around, ignoring the turnings into living quarters and factories, following the signs for the Security Centre.
</p>

<p>
<em>&#8220;Ok, level 47 is swept, all clear. A woman in sector N says she saw a man fitting his description taking lift 17 to a lower level about fifteen minutes ago. I´m checking the camera´s on the lift exits now, it will take a while, 15 minutes of footage on 55 cameras.&#8221;</em>
</p>

<p>
The technician´s superior took a sip of synthetic coffee from a plastic mug.
</p>

<p>
&#8220;Right. Harrow, Filps, give him a hand.&#8221;
</p>

<p>
&#8220;I´m covering floors 1-22&#8221;
</p>

<p>
&#8220;Ok you two, dive the rest up between yourselves, play it at two or four times normal speed. I want to find out where he is in a few minutes, not tomorrow&#8221;
</p>

<p>
There was a loud, fastly modulating warbling noise outside the room.
</p>

<p>
&#8220;What the hell was that?&#8221;
</p>

<p>
A shout.
</p>

<p>
<em>"Harrow, what the hell was that noise, have you heard anything like that before?"</em>
</p>

<h2>
Part XV
</h2>

<h2>
</h2>

<p>
The three barrels on the gun span, the gunmetal blurring into a faded cylinder, a high pitched warbling noise emitting from Arouin´s new-found tool of destruction. Arouin kicked over the remains of the table in the mess hall of the security center, dove behind it, then rolled out to one side and aimed at kneecap-height as he sprayed ammunition liberally across the corridor.. Five or six Peacekeepers´ legs buckled as their shins and knees were blown out, and they fell to the floor howling in agony. Another, taking cover on the corner of the corridor leading away from the mess hall, leant around the corner and let out a long burst at Arouin, who was already rolling back behind the cover of the overturned table. Shifting the sub into one hand, he drew a 9mm pistol as he moved up to one knee, then waited a moment. After a few seconds he heard running footsteps, and quickly leant out from behing the table and let off three shots at each of the two Peacekeepers running towards him, they were knocked about by the force of the impacts, one bouncing off the wall of the corridor en route to the body-covered floor. Arouin dropped the half-empty clip out of the pistol, slammed a new one home, then stuffed the pistol back into one of the cavernous trenchcoat pockets. He emerged cautiously from behind the cover of the table, stumbling over one of the bodies that littered the mess hall. He had caught them at lunch, and it wasn´t as if he could have left them alone, they would have just picked up weapons and come after him.
</p>

<h2>
</h2>

<p>
<em>And I can´t be having with that.</em> thought Arouin.
</p>

<h2>
</h2>

<p>
He walked carefully towards the corner of the corridor, swinging around and firing down the length of it, this time at nothing, a few sparks being produced by the rounds as they hit the end of the corridor, twenty or thirty meters distant. It never hurt to be cautious. He moved down the corridor following signs for the Operations Centre, no further opposition manifesting itself along the way, despite the sirens blaring out their warning that an intruder had entered the base. When he reached the door whose label proclaimed it to be the way into the operations centre, he hefted the gun and fired it at the door at point blank range. Swinging it back and forth over the thin metal of the door, a long, thin line of ruptured metal appeared and a stifled scream came from behind it as Arouin kicked it open. The room was very dimly lit, and monitors shone their light on the faces of technicians who were either slumped forward with the tops of their head blown open or left cowering after a line of depleted uranium pellets had passed inches or less above their heads, depending on their height. In the center of the room there was a body in two pieces; he must have been standing right next to the door when Arouin had sprayed it with ammo. A coffee mug lay on the floor beside the upper half of the body, its contents running steaming over the floor. Arouin swiftly reloaded the clip in the tri-barrelled gun, then started firing at the banks of electronic equipment that lined the walls. Chunks of metal casing and circuit boards flew, sparks being sent up as the pellets slammed home at incredible speed, the technicians diving for the floor and staying there&#8230; except for one. Arouin caught the movement out of the corner of his eye; one of the technicians´ hands was moving inside his jacket.
</p>

<h2>
</h2>

<p>
One tech yelled &#8220;Don´t do it Harrow!&#8221;, but the other man carried on regardless, pulling out a small-calibre pistol and aiming it at Arouin. Arouin dropped himself to one knee, too late, the technician, aiming at Arouin´s head, pumped at the trigger, which refused to budge. Arouin froze, looking at the technician. The technician, wide eyed, looked down at the gun, then started fumbling for the safety. Arouin grimaced, and squeezed the trigger on the sub, the body of the unfortunate man spasming as the depleted uranium pellets slammed into him. Arouin aimed at the next technician, who scrambled backward away from him, whimpering, as the three-barrelled gun was brought to bear. Arouin looked at the man, cowering away from him, and shook his head, lowering the weapon, standing up, and walking out the door.
</p>

<h2>
</h2>

<p>
<em>He hasn´t done anything, not anything he should be killed for.</em>
</p>

<h2>
</h2>

<p>
Arouin headed back the way he came, running past the bodies in pools of blood at the reception desk, not sparing them a glance. With the central security command centre out of operation there would be no way for anyone to keep very close tabs on his movements, which, with a bit of luck, meant he wouldn´t have to kill anyone else.
</p>

<h2>
</h2>

<p>
<em>Well, not for a while, anyway. I´m running out of ammo too</em>
</p>

<h2>
</h2>

<p>
&#8220;Level 20&#8221;, informed a pleasent feminine voice.
</p>

<h2>
</h2>

<p>
Arouin stepped out of the lift, slightly less cautiously than had lately been his norm, and headed through the arched mall corridor to the central mall area, and The Church of the Smiting of the Infidel. Things were somewhat quieter now, and the lights were a little dimmer than was normal; the station was turning off some of the lighting to conserve power while the majority of the population slept.
</p>

<h2>
</h2>

<p>
<em>Well,</em> thought Arouin as he entered the silent church and headed for the side doorway, <em>either someone is awake in here now or someone is going to be woken up</em>
</p>

<h2>
</h2>

<p>
Arouin reached the door, hammered on it a few times, then pushed it open. The old, bespectacled, man was still behind the desk, the terminal lighting his face, casting stark highlights and deep shadows over his visage. He looked up.
</p>

<h2>
</h2>

<p>
&#8220;Oh, it´s you again, heathen. I expect you will want to see our Chaplain again.&#8221;
</p>

<h2>
</h2>

<p>
Arouin smiled faintly before replying, &#8220;Yeah, hope I´m not disturbing him&#8221;.
</p>

<h2>
</h2>

<p>
&#8220;He will be awake, I´m sure&#8221;
</p>

<h2>
</h2>

<p>
The old man hit a couple of keys on his terminal, shutting off the screen, then lead Arouin through the door and down the passageway again before knocking on the door for Arouin, and leaving him standing outside. After a few moments the door was opened by a very sleepy Chaplain, rubbing his eyes and yawning.
</p>

<h2>
</h2>

<p>
&#8220;You had better come in&#8221; he managed, between yawns, motioning Arouin inside, then pointing him to the chair. The Chaplain sat down opposite Arouin, took a swig of something in a plastic mug, then pushed aside some paperwork and looked at him.
</p>

<h2>
</h2>

<p>
&#8220;You have evidence that Thorest Vippen is a spy?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Yes&#8221;
</p>

<h2>
</h2>

<p>
&#8220;And who he is a spy for?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Yes&#8221;
</p>

<h2>
</h2>

<p>
&#8220;And he is still <em>alive</em>?!&#8221;
</p>

<h2>
</h2>

<p>
&#8220;I <em>think</em> so. I left him alive&#8221;
</p>

<h2>
</h2>

<p>
The Chaplain nodded, &#8220;So, let me see the evidence.&#8221;
</p>

<h2>
</h2>

<p>
Arouin fished in a pocket for the data wafer, it took a good three or four seconds to find it, mixed in with a pile of clips and a 9mm pistol. Arouin flourished it in front of the Chaplain.
</p>

<h2>
</h2>

<p>
&#8220;This is a data wafer with a one-use encoding algorithm on it. Use it to translate the ´word search´ section of the floor 47 weekly bulletien with it, every week for quite a way back I guess.&#8221;
</p>

<h2>
</h2>

<p>
The Chaplain took the wafer from Arouin´s hands, inserted it into his terminal, and paused for a few moments, flicking through pages on-screen.
</p>

<h2>
</h2>

<p>
&#8220;This, I think, will suffice. And now to the issue of payment.&#8221;
</p>

<h2>
</h2>

<p>
Arouin grinned, &#8220;The account number to transfer it into is&#8230;&#8221;
</p>

<h2>
</h2>

<p>
&#8220;&#8230;Well, we won´t be able to give you as much as we originally&#8230; dicussed.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;<em>WHAT?</em> Now listen here&#8230; that was not a ´discussion´, that was an agreement. I <em>am</em> having my money.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;I´m afraid that isn´t an option.&#8221; said the Chaplain firmly, &#8220;it´s 200 thousand or nothing. And I guess that you don´t want nothing.&#8221;
</p>

<h2>
</h2>

<p>
Arouin sighed.
</p>

<h2>
</h2>

<p>
&#8220;Okay, 200 thousand will have to do.&#8221; he said, sitting back and looking rather put upon. The Chaplain seemed both pleased and surprised that Arouin hadn´t made too much of a fuss, and turned back to the terminal. Arouin stood up as the other man typed, and paced around to the back of his chair, then stood looking at the screen as the Chaplain typed in the details for the Green Chaplain´s account, the password appearing as a greyed out block. He typed in the amount to be transferred, then turned asking,
</p>

<h2>
</h2>

<p>
&#8220;What is the account num&#8230;&#8221;
</p>

<h2>
</h2>

<p>
The shot tore off half of the left side of his head, spraying it over the paperwork which was scattered over the desk. Arouin put the pistol on the table, and pushed the body off of its office chair and onto the floor. Selecting the account number to transfer the funds to, he typed in his own, then he went back and changed the amount to be transferred, adding another couple of zeros to the end of the 200000 that was up on the screen, then hammered the ´accept´ button. The sound of boots hammering on the floor echoed down the corridor outside, then stopped.
</p>

<h2>
</h2>

<p>
<em>&#8220;Chaplain? Sir?&#8221;</em>
</p>

<h2>
</h2>

<p>
Arouin grabbed the pistol from the desk in one hand, the other grabbing the other large-cal pistol from a pocket. As the door started to move aside he brought the pair of pistols up, and started firing into it as the terminal glowed with the message ´Transfer complete´.
</p>

<h2>
</h2>

<h2>
Part XVI
</h2>

<h2>
</h2>

<p>
The wooden door shook violently as pistols rounds smashed through it, the thin timber growing ragged holes and long splinters of wood flying off it onto the floor. Arouin dropped the large-calibre pistols to the floor and grabbed a 9mm from the deep pockets of the trenchcoat as there was a scream from outside the door and the muffled thud of a body hitting the ground. He pumped at the trigger while making a vague circling motion with his pistol arm, blowing out a large chunk of the door into the corridor outside. Holding the pistol firmly in one hand, he grabbed a pair of frag grenades from his belt with the other, pulled out the pins with his teeth, waited a second, then threw them out into the corridor through the hole in the door. Arouin quickly flattened himself against the wall next to the door, and reloaded his pistol. A split second after the clip slammed home, the grenades went off, blowing the tattered, shredded remains of the door into the room, breaking it up further and knocking the terminal off the desk and onto the floor behind it. Arouin rolled out into the corridor, franticly swinging to the left, then to the right, looking for any threats. There were the bloody remains of four Green Chaplain &#8220;Inquisitor&#8221; troops outside the door; body armour studded with pieces of metal shrapnel from the frag grenades, and two of them riddled with large calibre pistol ammunition. Arouin darted back into the office, grabbing his large-calibre pistols from the floor and stuffing them into his belt; he had to move quickly, the blast from the grenades would have attracted some unwanted attention. He ran back out into the corridor, running towards the room with the ever-present old man and his information terminal, barged through the door which slammed loudly back against the wall, almost being torn from its hinges, then drifting closed again behind him. Through the sighting notches on his pistol held at arms length, Arouin swept his gaze over the room. No old man. The terminal screen was still alight, and Arouin risked a moment to glance at it. His own face stared back at him from one of the active panels, with reams of information below it, other panels showing views from the security cameras inside the Church, including the office he had been in earlier, the corridor, the entrance to the church&#8230; and the Green Chaplains armoury and barracks room. Onscreen about twenty men ran into the armoury, hurriedly donning body armour whilst grabbing weaponry from racks set into the walls, then charging out, into&#8230; Arouin looked at the next panel.
</p>

<h2>
</h2>

<p>
<em>Into the corridor. Hmm.</em>
</p>

<h2>
</h2>

<p>
Arouin switched the pistol to his left hand, aimed vaguely at the door without looking and pumped four or five rounds into it, while ejecting the data-wafer in the terminal and pocketing it with the other hand. On the screen the foremost Inquisitor fell to the ground clutching at his arm, where one of the shots had found a mark, the others dropped to the floor, one grabbed a long tube from his back, and brought it up to one shoulder&#8230;
</p>

<h2>
</h2>

<p>
<em>SHIT</em>
</p>

<h2>
</h2>

<p>
Arouin vaulted the desk, already firing at the lock on the door into the main hall, half a second later smashing through it bodily as the roar of a rocket motor started. The rocket propelled grenade punched through the thin metal of the door and into the room, exploding as it made contact with the far wall, a bright plume of flame mixed with darker spots of assorted debris being funnelled out of the room and into the main hall with Arouin. He dove for the nearest pew, the shock wave from the blast deafening him and throwing him much further than he originally intended, landing on the concrete imitation-stone floor at high speed, drawing blood from his palms and scraping the skin from the knuckles of the hand desperately holding the pistol. Cursing, Arouin clambered to his feet with the assistance of the plastic pew, turned and ran out of the church at high speed, sounds of pursuit behind him as he emerged into the expanse of the central mall. His boots hammering on the smooth flooring of the cavernous and mostly deserted mall he picked up the pace, running past confused pedestians as the Inquisitors behind him began to open fire. Screams came from civilians as they ran for the sides of the mall, the nearest shop fronts providing nearby and welcome cover for them as the chatter of automatic small-arms fire began to echo through the vaulted halls. He turned a corner, trenchcoat flapping behind him, and sprinted for the next welcoming T-junction. There was a whine as a bullet came a little too close for comfort, Arouin aimed backwards without looking and started firing wildly and erraticly at his pursuers, still running like mad. As he skidded around the next corner, he headed for the grav-lift at the end of the mall corridor, an idea beginnning to form rapidly. The doors of the grav lift drew closer, but, as he drew close enough to see the numbers on it, he saw the lift was at level 43 but headed down. Arouin spent a second trying to dump the now-spent pistol back into a pocket, but struggled as it caught on the edge.
</p>

<h2>
</h2>

<p>
<em>Sod it</em>, he thought dropping the pistol and reaching under the trenchcoat to his left side where the tri-barreled pellet gun was slung beside his body. He brought it out and span, letting his boots loose their grip on the smooth flooring as he turned to face the Inquisitors behind him, dropping him below the hail of bullets that leapt from their weapons as they opened fire once more. Arouin hit the ground hard, knocking the wind out of him, but managed to hold himself together enough to squeeze down hard on the trigger, drawing a line of sparks as the depleted uranium rounds hammered into the shops fronts to his left. He adjusted his aim as the Inquisitors ran towards him, their own weapons spitting fire and metal, chunks of the surfacing on the floor being thrown up into the air around him. The three rotating barrels replied with their strange warbling noise, the oddly disconcerting lack of muzzle flash belying the power of the weapon. The Inquisitors broke up, diving for cover in different directions as he swung the gun over them, one of the front runners taking a cluster of rounds in the chest, sending him sprawling backwards with the force of the multiple impacts. Arouin swung the weapon wildly from one group of Inquisitors to another, firing at their positions of cover, trying to keep their heads down. He looked back at the lift´s level; 30, then back at the Inquisitors who were now giving return fire, then staring in horror as the man who had taken the shots in his chest got back up onto one knee and started firing back again.
</p>

<h2>
</h2>

<p>
<em>Damn! Kinetic body armour!</em>
</p>

<h2>
</h2>

<p>
Arouin pushed himself backwards along the floor, firing in sporadic bursts at the Inquisitors, before quickly getting to one knee and pressing the lift call button and dropping back down again. A ricochet threw up a piece of plastic flooring in front of him, and he rolled out the way of the hail of fire as rows of impact marks stiched themselves across the floor towards him.
</p>

<h2>
</h2>

<p>
&#8220;Level 20&#8221; chimed the lift as the doors opened and a couple walked out, talking animatedly to each other, unaware for a second or so of what was happening, then rushing out the way as Arouin barged through them and into the lift, and the sound of gunfire impressed itself upon them.
</p>

<h2>
</h2>

<p>
Arouin hit the ´6´ button, then stabbed franticly at the ´close door´ button. There was a pause for a few seconds as he stared at the lift doors, sweating profusely, then flattened himself against the lift wall as the door became pockmarked with impacts from small arms fire. After what seemed like an eternity of bullets hammering at the doors, the lift jolted into movement, Arouin feeling suddenly light with the acceleration. After a few seconds of normality, Arouin felt heavier, then the sensation resided and the lift chimed,
</p>

<h2>
</h2>

<p>
&#8220;Level 6&#8221;
</p>

<h2>
</h2>

<p>
Level 6 was one of the lower, mainly goverment or faction-controlled facilities, in Quantar Core level 6 being reserved exclusively for research and development both government and privately funded. And since the government owned the space station itself, the majority of the research establishments were government run, the Quantar Church both funding them and recieving any technical innovations that they produced. There was no mall complex on level 6, there was a simple hall with seven entrances leading off it, two into unused and locked research establishments, four into commerial R&D facilities, and the largest, a pair of double-doors, leading into the government funded &#8220;Jade Technology & Research&#8221; company. There were five armed guards outside, another six inside beside the reception desk and the receptionist behind it, then a corridor leading into the bowels of the facility. It wouldn´t be long until the Inquisitors found out where he had left the lift, and then they would be there to. Arouin looked at the guards, one or two of which were beginning to eye him suspiciously, then looked back at the lift doors.
</p>

<h2>
</h2>

<p>
<em>Yeah&#8230; yeah, that´ll work. Hopefully.</em>
</p>

<h2>
</h2>

<p>
Arouin walked towards the guards, putting his hands in his coat pockets. There was a click from each pocket as he eased empty clips out of the two large-calibre pistols, and carefully slid new ones into place. As he neared the guards they began to pay attention, and the five outside turned to look at him. He walked up to the closest one, and looked at him carefully. The guard´s face portrayed the epitome of boredom that emerges from any menial guard duty anywhere in the known universe.
</p>

<h2>
</h2>

<p>
&#8220;Excuse me, I´m&#8230; uh&#8230; new here, could you tell me where the public information office is?&#8221;
</p>

<h2>
</h2>

<p>
One of the guards laughed, turned back to his colleague, and carried on talking. All save the one Arouin was speaking to turned away, but the closest guard smiled, shaking his head, and then looked at him again.
</p>

<h2>
</h2>

<p>
&#8220;Christ, you had us worried there, we´ve heard some madman wearing a trenchcoat is causing all types of havoc aroun&#8230;.&#8221;
</p>

<h2>
</h2>

<p>
The blast blew the guard about a meter backwards, his arm left hanging on by a thread as he tumbled to the hard, unforgiving floor screaming. The rest of the guards scrabbled at their holsters for their pistols, as Arouin span, blasting at them at close range, the four other guards outside Jade Research and Technology falling to the ground in cries of pain mixed with fear and surprise. The guards inside ran for the doors as the receptionist slammed the red panic button under the desk, sirens starting up inside the facility. Arouin, standing firmly with legs apart and arms braced, let rip with the two pistols, the running guards being slammed around as the shots hit home. Slabs of plastic used for the interior panelling shattered under the force of the impacts, and the glass in the facility fronting blew out, scattering hundreds of thousands of tiny crystalline shards over the floor. One of the guards managed to bring his pistol to bear and let off a shot which whined past Arouin, who, with reflexes like a cat, snapped his aim around and let off a flurry of three quick shots which smashed the mans ribcage apart, leaving a red stain on the floor he was thrown down onto. Arouin ran through the remains of the shattered glass doors, looked at the receptionist, head blown half open by a stray shot, then ran down the corridor. Two guards were running up it towards Arouin, who snapped off a pair of shots at each, sending them tumbling to the floor, then opened the door they had just passed through, and ran back up the corridor. Running outside the research establishment, he sprinted for the shadows by the entrances into one of the unused facilities, squeezing himself into one corner. A minute or so later the lift chimed again,
</p>

<h2>
</h2>

<p>
&#8220;Level 6&#8221;
</p>

<h2>
</h2>

<p>
There was a patter of feet as the squad of Inquisitors deployed, then a voice.
</p>

<h2>
</h2>

<p>
&#8220;Yeah, we´ve got a pile of bodies here, looks like he is headed into the facility. Ok. Yeah. Ok, we´re after him. I hear you, crush resistance.&#8221;
</p>

<h2>
</h2>

<p>
There was a click of a radio being turned off.
</p>

<h2>
</h2>

<p>
&#8220;Ok, we´ve got to follow this guy into the facility, kill him, then bug out. Anyone tries to stop us, we´re going to put em down, but try to use non-lethal force if you do. This man has got himself a big weapons cache and it sure as hell looks like he knows how to use it. I want this to go smoothly. Let´s go!&#8221;
</p>

<h2>
</h2>

<p>
The squad of the remaining nineteen Inquisitors ran past him, guns at the ready.
</p>

<h2>
</h2>

<p>
&#8220;Remember folks, this guy is <em>dangerous!</em>&#8221;
</p>

<h2>
</h2>

<p>
<em>Right on. He´s behind you too.</em>
</p>

<h2>
</h2>

<h2>
Part XVII
</h2>

<h2>
</h2>

<p>
Arouin waited until the footsteps had faded a little, then quietly stepped out from the shadowed doorway. The green robes on the backs of the Inquisitors shook about as they ran away from him, towards the center of the facility. Arouin walked after them carefully, alert incase they doubled back, and began to reload his weapons, dropping the empty clips onto the floor where they clattered for a second before lying still. After the reloading was complete, and the sounds of screams and gunfire echoed down the corridor ahead, he put one of his large-calibre pistols in each hand, made sure that the safetys were off, and then picked up the pace to a light jog. He passed through the doors where the bodies of the last guards he had killed lay, then followed the winding of the corridor around, avoiding pools of blood from slumped and groaning guards. It looked like they were trying to use non-lethal force; they were aiming for the limbs instead of the head or chest of the hapless guardsmen. Arouin ignored the cries of those asking for help, dodging away from their grasping arms and continuing down the corridor. Pockmarked walls showed where small arms had spread their deadly fire, scorches on the floor where flashbangs, stunners, and the occasional fragmentation grenade had gone off. As Arouin cautiously stuck his head around a corner, the sounds of battle became suddenly more intense and a bullet ricocheted off the wall a meter or so away from him. He gazed down the corridor; the closer group were the remaining Inquisitors, now only numbering seven, crouched in doorways along half the length of the corridor, a couple lying behind the bodies of their foes or comrades in the middle of the corridor, returning fire over the top of them. At the other end of the corridor, hiding behind a makeshift barricade of a couple of chairs and a three heavy tables on their sides, were a group of twenty or so turquoise-uniformed guards, many in light battle armour, firing with a mixture of handguns and light submachineguns. The roar of automatic weapons and the tinkle of shell casings from the less-advanced weapons blended with the screams of the injured and dying, the shouts of those alive and afraid, into a hell-storm of sound. Arouin could see the the Inquisitors weren´t going to get much closer to the positron coupling at the current rate, they were pinned down by superior firepower, and it wouldn´t be long until someone managed to come around behind them&#8230; and then they would be well and truely screwed.
</p>

<h2>
</h2>

<p>
<em>Well, time to lend a hand</em>, thought Arouin.
</p>

<h2>
</h2>

<p>
He fished in his pocket for some of the det-pellets that he had bought from Venn, squeezed four fingers around a couple as the arms dealer had shown him, then threw them one after the other down the corridor. In the midst of the tracer fire and muzzle flashes the two black bean-sized objects went unnoticed, sailing through the air, one sliding under a table, the other bouncing off it and landing just in front. Arouin backed around the corner of the corridor, and braced himself for the blast. It came with a deafening roar, the floor shaking and the panelling on the walls buckling as Arouin moved back around. The table had been blown through one wall of the corridor into the room within, of the other two one was matchsticks on the floor and stuck into the ceiling, the other torn asunder into two charred and smoking pieces. The Inquisitors lay still for a moment, recovering from the unexpected noise, light and shockwave.
</p>

<h2>
</h2>

<p>
&#8220;What the hell was that?&#8221; asked a confused Inquisitor, looking up to his commander from behind a body on the floor, then back towards Arouin, who quickly ducked back before the man saw him.
</p>

<h2>
</h2>

<p>
&#8220;Looks like a couple of their own grenades went off on them. Probably using TP 420´s&#8230; you know how the pins on those worked loose&#8230; Roh favours us today, lets move out. We need to get to the coupling before he can.&#8221;
</p>

<h2>
</h2>

<p>
The seven green-robed warriors picked themselves up and moved cautiously down the corridor, as Arouin moved up behind them, moving carefully from one doorway to another, screwing the silencers onto two of the 9mm caseless pistols as he went. After a few more twists of the corridor, only encountering sporadic resistance from the occasional one or two facility guards, the team of Inquisitors came to a pair of plexiglass doors forming an airlock into a lab; after a few attempts to open them using the nearby keypad, the commander of the team turned to a subordinate,
</p>

<h2>
</h2>

<p>
&#8220;Breach it.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Yes sir&#8221; nodded the younger man, kneeling beside the first door. He fished in a pouch attached to his belt, and pulled out a flexible ´tex strip which he worked into place along the bottom, sides and top of the door, then sticking a detonator in and ushering the team away. Arouin quickly moved back, and darted inside a nearby, unoccupied office. A few moments later there was the distinctive ´crump´ of a muffled explosion as the ´tex went off. Arouin stayed where he was as people moved about outside, then there was another rush of feet as the Inquisitors ran for cover. Another exploision. Arouin moved around the corner as the Inquisitors moved through the shattered doorways into the lab. The man who had been setting up the explosives turned and looked at his superior.
</p>

<h2>
</h2>

<p>
&#8220;Sir&#8230; if this guy is ahead of us&#8230; how did he get through the doors?&#8221;
</p>

<h2>
</h2>

<p>
The older man paused for a moment, then turned around.
</p>

<h2>
</h2>

<p>
&#8220;You know, I´m not sure on that one&#8230; maybe he had someone on the ins&#8230;&#8221;
</p>

<h2>
</h2>

<p>
The wet <em>smack</em> as a bullet tore into the man´s throat punctuated the sentance, and he collapsed bleeding profusely, gargling and trying to scream but missing too much of his windpipe to make the sounds. The rest of the team span, looking for the unseen assailant, guns up, but too slow as Arouin strode out into the corridor, the odd sound of the silenced pistols tiny compared to the cries of pain from those the bullets hit. The guns spoke softly, but with no less ferocity than before, Inquisitors span to the ground, hands moving to their wounds in un-armoured places; heads, throats, limbs.. The last spun firing, Arouin dropped to the floor firing, the man´s legs buckling as bullets shot through his knees and shins, then his screaming ended as his fall into the twin lines of blurred metal death brought his head into them.
</p>

<h2>
</h2>

<p>
Arouin stepped over the bodies with their riddled body armour and bleeding wounds, into the lab proper. There were a series of workbenches in the white-walled room, clear of any rubbish or clutter, tools hanging above them on extensible power cords, and powerful fluro-strips suspended over them. At the other end of the lab there was a small, white-blue podium with a cuboid sitting atop it in a glass case, railings cordoning off that end of the lab and laser beams criss-crossing the ground between. Infra-red sensors covered the walls to the ceilings, more lasers positioned around the podium all the way up. Arouin looked at the elaborate setup, unfazed.
</p>

<h2>
</h2>

<p>
<em>Well, that´s a little excessive, isn´t it?</em>
</p>

<h2>
</h2>

<p>
Arouin sauntered over to the far end of the lab, hands in his pockets, and leant over the railings, looking down into the menacing red glow of the lasers. In addition to the criss-crossing laser beams there were another set of ten or so, oscillating quickly to catch anyone trying to tip-toe through the lasers by the floor. The cuboid within the plexi-glass casing was studded with glowing lights and had odd prostrusions covering it, lumpy and organic-looking.
</p>

<h2>
</h2>

<p>
<em>A bit showy really</em>, thought Arouin, still staring at the podium.
</p>

<h2>
</h2>

<p>
Arouin looked up to the roof of the lab. It was studded with ventilation and filtering systems, vital for keeping the now-violated cleanroom free of dust. Arouin grinned, then a second later scowled as there was a metalic thump from the ventilation system above him. One of the pieces of it flexed.
</p>

<h2>
</h2>

<p>
<strong>Whump</strong>
</p>

<h2>
</h2>

<p>
The next one made the sound, bulging outward slightly. Arouin quietly moved away from the podium, lasers and railings, and crouched behind one of the white work benches.
</p>

<h2>
</h2>

<p>
<strong>Whump</strong>
</p>

<h2>
</h2>

<p>
Whump
</p>

<h2>
</h2>

<p>
Whump
</p>

<h2>
</h2>

<p>
<strong>Whump</strong>
</p>

<h2>
</h2>

<p>
The movement in the vents was now right over the podium. It stopped&#8230; then there was a faint clatter, a hiss, and a plasma blowtorch flame cur through the metal of the vent. It moved around, cutting a large rectangle out of the venting, then the removed metal was hoisted up, out of view. A moment later a pair of legs appeared through the gap, and a lithe woman clothed in a black, overly well-fitting Solrain stealth suit dropped through on a pair of wires. Arouin watched as the supple form edged downwards over the laser trap towards the podium, then stopped slowly and carefully over the plexi-glass casing.
</p>

<h2>
</h2>

<p>
<em>The stealth suit doesn´t let the IR sensors see her. Nice.</em>, thought Arouin
</p>

<h2>
</h2>

<p>
She lowered a set of mirrors on another wire; they were arranged to deflect a laser beam around an area and then send it on its original course, not breaking the laser. Another set. Another. Another. After five or ten minutes of careful positioning, there was a laser-free area around the podium and the plexi-glass case atop it. The black-suited infiltrator winched herself up, and retrieved a slightly larger open-topped plexi glass box from the vent, then winched herself down again. Arouin watched her as she grasped the top of the plexi-glass casing, then dropped the larger one over it <em>just</em> as she lifted the one covering the positron coupling. She reached into a bag on her belt and drew out a small, rounded, black object that she set on the same pedestal as the positron coupling. It floated above it, then, as she grasped the coupling and lifted it slowly, the black object descended, finally coming to rest on the pedestal. The woman winched herself up again, then swung out over the railings and released herself from the wires, landing quietly and gracefully on the floor, holding the positron coupling in one hand. She took off her stealth suit helmet with the other land, long brown hair slipping out of it as she did so. Arouin carried on staring.
</p>

<h2>
</h2>

<p>
<em>Nice.</em> he thought.
</p>

<h2>
</h2>

<p>
<em>Shame</em>, he thought, after a moment´s reflection.
</p>

<h2>
</h2>

<p>
Then she noticed the carnage in the doorway. She pulled out a pistol from her pocket, and moved towards the door. As she got to the nearest Inquisitor, Arouin stood up silently. She knelt over the man, put the positron coupling down, and turned him over to look at his face. Arouin took a few steps forward. She heard one of the footsteps, swung around as fast as a cat, trying to aim at any threat. Arouin was already pre-aimed.
</p>

<h2>
</h2>

<p>
The blast from Arouin´s large calibre pistol hit her in the shoulder, ripping off her arm with the pistol in it and throwing her against the wall she was standing next too, smearing blood all over it as she collapsed, moaning.. Arouin walked over and picked up the positron coupling from its position on the floor next to the fallen Inquisitor. As the woman lapsed into unconciousness, Arouin walked out of the lab with the positron coupling under one arm, humming quietly to himself.
</p>

<h2>
</h2>

<p>
<em>[NOTE: Ah-ha! Had you going there didn´t I? Thought I would lapse into cliché-mode didn´t ya? Wrong! 😉 ]</em>
</p>

  <h2>
    Part XVIII
  </h2>
  
  <p>
    Arouin walked out of the lab, carrying the positron coupling under one arm, a pistol in his other hand.<br /> <em>Might be important&#8230;</em>#
     he thought, breaking into a run, <em>Might be clever&#8230; But it´s bloody uncomfortable</em><br /> He lept over the smouldering remains of the guard´s barricade, and plunged on down the corridor, trenchcoat flapping behind him as he ran. As he rounded a corner of the corridor, a guard leapt out from a doorway, shouting<br /> &#8220;<em>Stop righ..</em>&#8221;<br />
   </p>
  
  <p>
    His words blended into a scream as Arouin´s pistol roared its reply, Arouin barely breaking his stride to fire at the man. Skidding around a corner, he ran past the reception desk with it´s half-headed receptionist, blood now beginning to dry on the floor and desk, and out of the door headed for the lift. When he was about twenty meters from the lift the lift doors opened, and a five-strong squad of Peacekeepers looked up in surprise. Arouin let himself fall backwards as he dropped the positron coupling and grabbed franticly for a pistol in a trenchcoat pocket. He landed on his back and slid along the floor firing at the group of men as they milled, confused, the men in front wanting to get away from Arouin and his twin spears of death, the men behind trying to get out to see what all the fuss was about. The men closest to Arouin fell, blood streaming out of them, the men behind saw him, blazing death and destruction and tried to get back into the lift, too late.
  </p>
  
  Arouin stepped over the corpses and nudged one that was lying in the
  lift door out with his foot, the blood leaking from his gunshot
  wounds making a red streak over the metallic lift floor. Arouin
  stabbed hurriedly at the button marked ´15´, missed, and jabbed at
  it again to send the lift on its way. He steadied himself against the
  wall as the lift jerked suddenly into motion, moving up towards the
  second hangar bay level. After a few moments of silence save for the
  gentle creak of straining metal the lift shuddered to a stop and the
  lift chimed ´Level Fifteen´ in plesant feminine tones, and one of
  the doors opened. The other one tried to move out the way a couple of
  times, but was closed by the metal that made up the door, twisted and
  battered from mutliple weapon impacts. Arouin stepped out of the lift
  crabwise, gave a quick glance around the faces of the group of people
  staring at the chewed up lift, looked down and hurriedly got out of
  the way. Arouin felt the gaze of the inquisitive crowd on his back as
  he left the lift, and he looked quickly for the fastest way back to
  his Phoenix. He walked swiftly down the corridor, managing to keep in
  rein the temptation to start running, lest it draw attraction to him.
  As if he wasn´t suspicious enough&#8230; a tired, sweaty and
  bloodstained individual in a large black trenchcoat with a cube under
  one arm that looked like nothing he had ever seen before. The
  corridor was dark and dank, a couple of the fluro-strips in a row had
  failed; one was blinking sparodicly&#8230; the other was completely
  dead. A few turns in the corridor, past an engine-fittings shop and a
  used weapons salesman, Arouin was out in the second hangar bay. It
  was now getting on for early morning, station time, purely artificial
  of course, and activity in the main bay was beginning to pick up.
  Arouin hurried over the metal decking, the occasional nut, bolt or
  screw skittering away noisily over the floor as his boots knocked
  them. The bay his Phoenix was parked on was obscured by a pair of
  Solrain cargo tows with a group of five technicians working on each
  in baggy blue overalls. Arouin walked between the pair of cargo tows,
  pushing past a couple of technicians fixing a power coupling between
  the cargo pod and the tow itself, and glancing into their toolbox as
  he did so.<br /> <em>Now that <strong>is</strong> odd isn´t
  it&#8230;&#8230;</em><br /> Arouin moved cautiously out from between
  the two cargo tows and looked at his Phoenix. There was a technician
  busy working on the armour plating on the top of one wing, another
  couple by the front landing skid.<br /> <em>&#8230;technicians don´t
  usually have TP laser-subs as standard power tools.</em><br /> Arouin
  stopped, looking at the techinicians working on the <em>Lithe
  Shadow</em>, then turned around and walked back out of sight to the
  group of technicians he had passed. He approached the closest one
  from one side, took careful note of the <em>tiny</em> earphone in
  one ear, almost invisible, put a pistol into each hand, and tapped
  him on the shoulder.<br /> &#8220;Excuse me&#8221;<br /> &#8220;How
  can I help y&#8230;&#8221; the ´technician´s eyes flared wide for a
  split second before Arouin squeezed the trigger on one pistol, the
  muzzleflash scorching the eyebrows off the ´technician´, the bullet
  taking most of his head. Arouin adjusted his aim to the next man, who
  was turning and getting up from the floor as the first man´s body
  began to crumple and fall to the floor, the blood still pumping out
  of the gunshot wound. The second man looked up, confused and with
  apprehension showing in his face as Arouin brought the other pistol
  to bear, the pair of guns jerking twice with recoil and cartridge
  cases fluttering up into the air, catching the light and glowing gold
  before falling back down to clatter away into the darkness. The body
  skidded back across the floor, Arouin span expecting the phoeny
  workers on his Phoenix to come up behind him, but no-one appeared. He
  crept out, hugging the side of one of the cargo tows, and peered
  aroung the corner. A quick glance let him see the running figures
  fading into the middle distance amongst the muddle of the ships, and
  people looking towards him with alarm on their faces. Arouin looked
  around, shrugged, and ran towards his Phoenix.<br />
  
  <p>
    <em>Well, they didn´t last long</em> he thought as he climbed up to the cockpit.
  </p>
  
<em>The officer hesistated outside the beige door with its simple, black label, then knocked on it quickly. It was an ungodly hour and he wasn´t too sure that waking The Director at this sort of time was too good an idea if you wanted to have any prospect of promotion. Despite the time, he received a quick reply,<br /> &#8220;Come in, come in.&#8221;<br /> He opened the door, stepped through, and closed it carefully behind him. The Director was sitting behind his desk nursing an early morning black coffee, the folding bed already back inside the wall.<br /> &#8220;What is it then? Speak already!&#8221;<br /> &#8220;Sorry sir&#8230; it´s uh, Mr Simmel sir.&#8221;<br /> &#8220;We´ve lost him again?&#8221; said The Director, taking a sip of coffee from his mug.<br /> &#8220;Nooo.. no not as such.&#8221; replied the officer hesitantly<br /> &#8220;Not as such?&#8221; The Director put his coffee down on the table and gave the officer his full attention. The poor man really would rather that he didn´t, but ploughed on regardless.<br /> &#8220;The group of people I sent to guard Mr Simmel´s ship&#8230;&#8221;<br /> &#8220;Who attacked them? I take it someone has attacked them?&#8221;<br /> &#8220;Uhmm&#8230;&#8221; the officer shrugged slightly, &#8220;Mr Simmel did sir.&#8221;<br /> &#8220;Ah.&#8221; The Director pushed his chair back with his feet and folded his hands together over his chest, &#8220;I see. Did our field operatives inform Mr Simmel of what they were doing?&#8221;<br /> &#8220;No&#8230; it doesn´t look like they got a chance to. He just arrived, killed two of them right in the middle of the hangar bay, then took off.&#8221;<br /> &#8220;And what did I tell you about Mr Simmel?&#8221;<br /> &#8220;That he could take care of himself&#8230; but I thought that he was up against quite a lot and it might be an idea if we.. lent him a hand.&#8221;<br /> </em>

<p>
<em>&#8220;Let that be a lesson to you&#8221; chided The Director softly, shaking a lone finger at the officer, who watched it mesmerised, &#8220;Don´t underestimate our friend Mr Simmel.&#8221;</em>
</p>

<em>
The launch had gone well, after Arouin had threatened to let off all his weapons inside the hangar bay unless they let him out.<br /> <em>But now</em>, thought Arouin, as he battled g-forces slamming him around in his pilot´s chair as he ducked and wove around Quantar Core Station, <em>&#8230;now things are going less than well</em><br /> A defence droid slammed into the station beind him, showing up on his rear view monitor as a shower of sparks and flame as oxygen from within the staiton ignited and leaked out into a firey plume. Hitman rounds buzzed past him from the remaining four station defence droids as he thrashed the flight stick around, searching around the cockpit desperately, jinking to avoid incoming rounds. The three Typhoon fighters pursuing weren´t giving any ground, but he didn´t expect them to; they had honour to uphold, the station´s defence droids helping them, <em>and</em> a big fat bounty to claim if they took him out. Arouin dove his ship arouin another jutting pylon, putting it between the Typhoons and himself as he screamed through space inside the broken cavern of metal that made up Quantar Core. One Typhoon broke out wide to the right, then nipped back around it, another went around the outside of that piece of the station structure, the third and final Typhoon rolling 90° to avoid having one side of itself ripped off by the metal construct.<br /> <em>Damn, they don´t want to kill themselves</em>, Arouin turned in a sweeping arc and headed back for the docking area, <em>&#8230;how very inconvenient.</em><br /> The Typhoons were a little more spread out now, just out of effective gun range for their Hammers, but the station defence droids were still in weapon range, and were firing a constant stream of ammunition even though the chance of hitting was miniscule. Arouin passed the central docking area, span the ship through almost 180°, opened up the throttle to maximum and engaged the afterburners. His previous screaming speed disappeared suddenly, and with a second of thrust and brake, he was hiding beside the large, stupidly-placed rock that the Quantar kept beside their docking tubes. The defence droids, being little more than radars with guns, happily assumed he was destroyed and headed back to dock, but the Typhoons kept on coming.<br /> <em>Radar shadow should buy me a little time&#8230;</em><br />

<p>
Arouin craned his neck around, looking for the Typhoons, and after a few seconds he saw the green engine glow appearing from one side of the rock as they neared it. He armed a Hellrazor missile, checked that the Barraks were ready for firing, then the three Typhoons shot past him one after another, two to three seconds apart. As the last one tore past, afterburners blazing, Arouin gunned the engine and brought the throttle to full, hammering on his own afterburner as if treading on the ignition pedal harder would give him that <em>little</em> extra bit of speed.
</p>

<em>The last Typhoon to go past was slowing, the pilot looking at his radar for the disappeared radar image of the Octavian´s Phoenix<br /> </em>Where are you&#8230; come on&#8230;<em><br /> &#8220;This is Green two, I can´t see him anywhere&#8221;<br /> The Typhoon pilot thumbed the transmit button.<br /> &#8220;Maybe he hit the station&#8230; I can´t see him on my&#8230;&#8221;<br /> &#8220;<strong>GREEN THREE HAUL ASS HE´S RIGHT ON TOP OF YOU</strong>&#8221;<br /> Green three looked up from his comms system to see the blow glow of barrak ammunition speeding towards him<br /> </em>

<p>
<em>&#8220;<strong>SHIT SHIT MISSILE LOCK SHIT!</strong>&#8220;</em>
</p>

<p>
The Typhoon pilot began to turn and accelerate away as Arouin´s first Barrak rounds chewed through his shields, the green glow dissapating rapidly as sucessive shots battered the protective energy. As the distance closed and the Typhoon began to accelerate, Arouin carefully adjusted his aim, the barrak rounds beginning to smack the other ship around as their power ruptured armour plating, sending pieces of metal and showers of sparks into the void. A second later the Hellrazor ploughed into the softened Typhoon, the warhead penetrating deep into it before exploding, full blowing open the rear two thirds of the ship in a fireball that lit up Arouin´s shields as he shot through the expanding wreckage cloud. At the corner of his vision he saw an escape pod emerging and heading back to the station, raising a grim smile as he glanced back at the radar. One of the two Typhoons was headed back and was only just out of sight&#8230; the other was around the other side of the central docking area, but turning as he looked at it.
</p>

<em>&#8220;Okay I´m on him Green one, give me some support here, Green three´s down&#8221;<br /> &#8220;This is Green one, I´m heading around the other side of the station to cut him off&#8230; hold him there&#8221;<br /> Green two was sweating as he curved around the station, the Phoenix slowly coming into his sights,<br /> </em>

<p>
<em>&#8220;I hear you Green one, I´ll do my best&#8221;</em>
</p>

<h2>
</h2>

<p>
<em>Ten seconds until the other Typhoon gets around the station</em> thought Arouin as he triggered his Barraks, the guns flaring their blue-white death at the Typhoon in his sights. The Typhoon was coming head on head throwing out Hammer rounds at him, but the Hammer muzzle velocity was notoriously low, and Arouin ducked and weaved around the streams of ammunition, only occasionally aligning his ship for a quick burst of gunfire at the fast-closing Typhoon. His shields lit up red as a sucession of rounds slammed into his shields, he spun the Phoenix around as the ammo, aimed at a <em>moving</em> Phoenix, went wide. Dead in space, he fired at the Typhoon as it shot past, franticly trying to spin and bring its own guns to bear on him, then he accelerated as it slowed, spinning himself around the grey and green spacecraft, pummelling it with his quad Gauss cannon. Watching his radar, he saw the other Typhoon clear the curve of the station and bring it´s own guns to bear&#8230;
</p>

<em>&#8220;This is Green two, I´m in trouble&#8230; he´s&#8230; damn good&#8230;&#8221; the Quantar pilot spluttered into his mike, wrestling the flight stick as he accelerated away to try and get away from the deadly Barrak fire.<br /> </em>

<p>
<em>&#8220;Green two, I´m on him, firing now, firing now, hold on there buddy&#8221;</em>
</p>


<p>
The Typhoon´s shields were out, it accelerated away from him, twisting away from the lines of death that his quad guns drew out across the darkness, spiralling around an antenna protruding from the station to avoid it. Arouin throttled up, watching his rear view monitor out of the corner of one eye, sticking close to the interior curve of the station as Hammer rounds smashed into it in before and behind him, throwing up small plumes of sparks and flames as metal flash-vaporised. The Typhoon pilot ahead followed the station curve, and headed for an antenna patch, a fine network of metal that could seriously impede the progress of a fighter trying to get through it. The Typhoon ahead slowed slightly and started to jink around the antennas, something that Arouin in his larger, bulkier, and definately less nimble Phoenix could definately not do. Arouin paused for a moment, a few Hammer rounds striking his shields and making them glow, then jammed the trigger down, the Barrak rounds ploughing through the delicate antennas, questing, seeking for the sweet taste of the shields of the fleeing ship. Eventually they found it, and the small amount of newly-recharged shielding on the Typhoon evaporated like morning dew.
</p>

<em>&#8220;This is Green two, I can´t take much more damage&#8230; shields are completely out&#8230; I´m gonna pull up sharpish, get ready to get on him.&#8221;<br /> </em>

<p>
<em>&#8220;I heard you Green two, on him.&#8221;</em>
</p>

<p>
The Typhoon made a sharp 90° turn upwards and blasted away from the station´s interior surface, Arouin checked his radar then pulled up after it, barrel rolling. As he rolled a flurry of blue Hammer rounds streaked past and a handful smashed into his shielding, making him jink suddenly and steal a glance at the rear view monitor. The other Typhoon was right up behind him&#8230; Arouin whipped the <em>Lithe Shadow</em> around 180 degrees, and let rip again with his Quad Barraks, triggering his remaining three Hellrazors as he did so. The missiles shot out, the Typhoon pilot turned but had too much inertia to get away from the closing missiles, the first slamming into his ship and sending it spinning. The spin put him out of the way of the second missile, which shot off and began to curve around back towards it. The Typhoon was accelerating slowly, its engines had obviously taken some damage from the blast, and Arouin tore through space towards it, strafing Barrak fire over it´s hull as he screamed past. A second after he had passed it he looked on his rear view monitor to see the returning missile drive home into the ship, which lost one engine and a healthy portion of it´s center hull. The Typhoon spiralled off into space&#8230; after a few seconds an escape pod emerged and the remains of the ship self-destructed in an orange-yellow ball of firey power.
</p>

<p>
Arouin smiled as his radar lost contact with the last Typhoon as it fled towards Outer Roh Cloud, and aligned his ship with the Omni V gate, bringing up the throttle and permitting himself a few seconds of relaxation&#8230;
</p>

<h2>
Part XIX
</h2>

<p>
The stars wheeled around <em>Lithe Shadow</em> as she curved toward the Omni V Jumpgate in the Quantar Core sector, the pilot inside guiding his flight through the darkness of space idly with one hand, the other cradling a cup of coffee. He flicked through the comms channels, listening to the speakers, most of whom seemed overly stressed. A couple of Quantar pilots requesting assistance because they had heard that a notorious pirate was about, a Solrain cargo tow pilot screaming impatiently at a rookie pilot who was attempting his second ever docking, but on all of them the small symbol in the lower right of the screen which meant there was a priority transmission. Arouin didn´t need to listen to the transmission to know what it was about, he just needed to get away from it. Arouin aimed at the gate keeping the throttle high and after a minute or so the blue glow surrounded him and he felt the peculiar wrench of the journey through space-time as he triggered the jump drive.
</p>

<p>
On the far side of the gate he pulled away quickly out of the path of an oncoming convoy of Octavian cargo tows and their Phoenix fighter escort, and headed for Ekoo´s Stop&#8230; and from there&#8230; onwards to The Stith. The journey was largely uneventful, on the whole a rather disappointing lack of juicy pirate targets, but Arouin ploughed on through the void of space, making the occasional detour around an asteroid cluster which blocked his path, careful to give the brown-grey lumps of deadly rock a wide berth. All the time he was watching his radar, looking for anything that he could make a meal of&#8230; or anything that might make a meal of him.
</p>

<em><em>The Director ignored the glowing screen which was displaying the flustered face of a young woman in a light grey suit making a public broadcast from one of the few private news agencies on Quantar Core Station. He was too busy, trying to make sense of what had just happened/<br /> &#8220;Over the past twenty hours an outburst of violence from factionalist groups and other unknown forces has wreaked havoc on Quantar Core Station. Close to one hundred civillians have been killed in a string of vicious firefights in the corridors of the station, most of them in the main mall areas. Unconfirmed reports state that the station has lost over half of it´s resident Peacekeeper law enforcement troops, and a number of special operations personnel. The station authorities and the Quantar Government have both declined to comment on the spate of killings, and our reporters have been unable to find any pattern to the fighting. One figure seen here in security camera footage&#8230;&#8221;<br /> The screen faded to a grainy video image of the back of a man in a black trenchcoat with a brace of pistols, walking backwards towards the camera while firing them. As the camera tracked him, a hand suddenly appeared in one corner of the screen, the camera panned across to an image of a Peacekeeper in shattered armour trying to pull himself along the ground, then blanked out into snow as a stray round caught it. The screen jumped back to the newsreader,<br /> &#8220;&#8230;is believed to be responsible for the majority of the deaths. Here at QCINN we have managed to find through a string of contacts that this man is believed to be the notorious pirate Arouin Simmel, seen here in a TRI court defending his actions after killing a Quantar miner who he claimed was threatening Octavian security&#8230;&#8221;<br /> A much higher-resolution picture faded into view now, and the man it showed behind the dock was clearly Arouin, expressively explaining to the prosecutor why he had found it necessary to kill an unarmed miner. After a few seconds the presiding judge brought down his gavel, and declared Arouin guilty of the unprovoked murder of Jess F´thar. Arouin nodded quietly to himself, and turned to look the public gallery. There was a blur of movement as something was thrown down to him, then he span with a machine-pistol and shot the judge, the prosecution, four guards, all three witnesses, and his own defending lawyer before storming out of a door, and the image fading back to the newsreader once again,<br /> </em></em>

<p>
&#8220;&#8230;Mr.Simmel is notoriously good at covering his own tracks, and this is believed to be one of the few crimes that TRI has evidence of him committing. There are reports, also unconfirmed, that a government research agency in the lower, high-security decks of the station came under attack for unknown reasons. We now bring you the weather&#8230;&#8221;
</p>

<em>He tapped his fingers on the desk, digesting the news, then brought up the tracking console through a few deft taps on the controls set into his desk. Sure enough, the overlapping traces were in The Connexion and headed for The Stith. The Director tapped a button on his desk, then put his hands behing his head and looked up at the ceiling as he spoke,<br /> &#8220;Allow scout Rhapsody to escort Mr Simmel to the jumpgate&#8221;<br /> There was a faint clatter as someone, surprised, dropped something on the floor, then stumbled on it to their way to the microphone, then a communications officer picked up the mike and replied, wearily,<br /> </em>

<p>
<em>&#8220;Yes sir, right away, sir&#8221;</em>
</p>

Arouin sat in his pilot´s chair, looking bored. He had entered The Stith twenty minutes ago, and had expected The Director´s cronies to be pretty sharpish when it came to getting him to&#8230; wherever that place was&#8230;<br /> <em>Is this thing important or not?</em> wondered Arouin, checking the radar for what seemed like the twentieth time.<br /> His wait was not in vain; after a few more minutes spent industriously chewing a protein-bar, there was a crackle from the comms systems, then a man spoke;<br /> &#8220;Mr. Simmel, this is the stealth scout <em>Rhapsody</em> hailing you on a closed channel. We are taking down our ECM systems; please follow my radar trace to the jumpgate you&#8230; require.&#8221;<br /> Arouin looked around, shrugged to himself, then flicked the transmission button on his control panel.<br /> &#8220;This is Arouin Simmel hailing <em>Rhapsody</em> from the <em>Lithe Shadow</em>, lead the way.&#8221;<br />

<p>
He sat back heavily into his chair, and stared at the radar until a yellow trace appeared on it. After a few seconds it started moving away and Arouin accelerated his ship after it, cycling targets and noting down the distance from the jumpates into The Stith, Dark Crossroads and Ring View. He glanced up at the view before him and drew a brief sketch of it, then smiled and stuffed the crumpled piece of paper into a storage locker under the command console.
</p>

<em>
The journey took a good half hour, the lone radar trace ahead of Arouin keeping a steady, unwavering course into the depths of space. Eventually, his radar picked up an indistinct contact at nineteen kilometers or so, and after another thirty seconds it was revealed as a jumpgate, coming into view as a bright blue-white speck in Arouin´s view. The scout pulled out of the way, then the contact faded from radar,<br /> &#8220;<em>Rhapsody</em> to <em>Lithe Shadow</em> you are cleared for entry into the system&#8221;<br /> Arouin cleared his throat quickly before replying &#8220;Okay <em>Rhapsody</em>, I´ll be seeing you again in a while I guess&#8221;<br /> &#8220;You hope.&#8221;<br /> &#8220;Uh&#8230; yeah. Thanks for the confidence boost there&#8221;<br /> There was a laugh from the other ship, &#8220;Don´t worry, see ya later&#8221;<br /> Arouin entered the glown, encased by it, flooding his vision&#8230; intensifying&#8230; fading&#8230; and&#8230;<br /> <em>Well, here I am.</em> he thought to himself, somewhat glumly.<br /> The same turquoise nebula glowed softly in the interstellar distance, illuminating the scene before Arouin. The TRI station sat before him, a grey-white monstrous spider, the central hub connecting with large, bulbous modules&#8230;<br /> <em>Housing who knows what&#8230;</em><br /> But one of them was missing. One of the arms connecting the central core with the modules was ruptured and twisted, showing blast marks from energy weapons and impact craters from torpedo and missile impacts. Debris floated throughout the area, metal panneling, distorted remains of equipment, one or two clouds of glittering red and white with large chunks of frosted flesh that were all that was left of people who had suffered exposition to the vacuum of space and the power of weaponry designed for combat in the void. A couple of transport ships were flying slowly through the debris field, scooping it up and into their cargo bays. Arouin stared in disbelief. The remaining station was scarred and charred from multiple weapons impacts, and in some of the craters left by the larger munitions, Arouin could see the extent of the armour on it; it was over three meters thick, easily. Arouin jammed a finger on his transmission button,<br /> &#8220;This is the <em>Lithe Shadow</em> requesting docking clearance&#8230; I&#8230; what´s&#8230;?&#8221;<br /> There was a crackle from the comms system, then a strained and obviously weary woman spoke,<br /> &#8220;There was a&#8230; weapons malfunction during a routine death. Please dock using tube 3, normal approach.&#8221;<br /> &#8220;<em>Weapons malfunction</em>? What?&#8221;<br /> &#8220;You heard me. Proceed to dock&#8221;<br />
</em>

<p>
Arouin turned his ship around and headed for docking tube 3, thinking, <em>Weapons malfunction?!</em>
</p>

<em>
He maneuvred the <em>Lithe Shadow</em> into the docking rings at very low speed, avoiding a large chunk of wreckage that sat there. As he moved towards the docking tube, a frozen, clothed arm bounced off the cockpit glass and ricocheted off into space. Arouin stared at the spinning arm is it became a spec in the distance, the dark shadow of the docking tube cutting off his view after a few tens of seconds. The docking clamps attached and drew him in, the lighting in the light flickering on as he was drawn up into the bowels of the station. The lift doors above parted and the lift jerked to a stop, then clamped in place.<br /> Arouin looked out over the hangar bay, and the first thing that became apparent was that there weren´t as many ships lying in dock as there had been before. The ones that remained all seemed to be damaged, scorches on them, gouges in the thick armour plating, and, noted Arouin, they didn´t have any missiles on their racks. A pair of grey-faced technicians in ragged clothing ran over to the ship and helped him down as a group of five security officers walked across the hangar bay to him. He reached the hangar floor, and turned to the officer in charge of the security detachment, who surveyed him critically.<br /> &#8220;Have you got it&#8221;<br /> &#8220;Yes I have&#8230; what the hell has happened here?&#8221; questioned Arouin<br /> The officer looked slightly put out, but rallied quickly<br /> &#8220;There was a malfunction during a routine weapons test.&#8221;<br /> Arouin stared him in the eyes for a few seconds, holding his gaze.<br /> &#8220;Hmm. Right, well, you had better lead the way, I suppose.&#8221;<br /> &#8220;You can give it to us&#8221;<br /> &#8220;Nooo&#8230; I give it to The Director only.&#8221;<br /> The officer looked slightly miffed, but submitted, &#8220;Very well, follow me.&#8221;<br /> He turned smartly and walked back in the direction he had come from, gesturing Arouin to follow him. Arouin fell in behind him in studied silence, looking around and taking in everything that he could. The corridors weren´t damaged or scarred with weapons fire&#8230; there were no bloodstains&#8230;<br /> <em>Looks like the fighting was confined to space&#8230; they weren´t boarded.</em> thought Arouin, as he turned another corner in the corridor and they arrived outside the door with the black label bearing ´The Director´ in plain white type. The security detachment took up positions outside as Arouin knocked on the door.<br />
</em>

<p>
&#8220;Come in&#8221; said a surprisingly tired voice from behind the door. Arouin glanced at the face of the nearest member of the security detachment, who failed to return it, staring impassively at the far wall instead. Arouin mentally shrugged, pushed the door open, stepped inside, and closed it neatly behind him.
</p>

<em>
The Director was sitting behind his black desk, as if it was his shield against the world. He looked up at Arouin with a stare that said &#8220;No matter what you have been through, no matter what you have endured, I have had it worse&#8221;. Arouin was suitably mollified, and sat down in the chair opposite The Director in silence.<br /> &#8220;I hear&#8230; that you have it.&#8221; he said to Arouin, quietly.<br /> &#8220;Yes&#8230; yes, I do.&#8221;<br /> &#8220;Well, pass it over.&#8221;<br /> Arouin reached under his trenchcoat and pulled out the deformed cuboid, holding it out over the desk. The Director reached for it with both hands, and tried to take it from Arouin´s grasp, but Arouin didn´t release it. The Director stared him in the eyes for a few seconds.<br /> &#8220;What´s the matter?&#8221; he asked.<br /> &#8220;I need more money. I need it just to get the job done. It was hard enough this time, and I had to.. uh&#8230; ´appropriate´ my own funds.&#8221;<br /> The Director let out a bitter half-laugh, and and grim smile escaped his lips<br /> &#8220;Money is the last of our problems, we´ll give you ten million in expenses&#8221;<br /> Arouin was slightly startled by the unexpected generosity, but let go of the positron coupling. The Director took it from him carefully, and laid it on the desk next to him. As he looked up Arouin was staring at him.<br /> &#8220;What went on here?&#8221; asked Arouin.<br /> The Director looked away for a moment, then returned to stare at Arouin´s eyes, before slowly and deliberately replying,<br /> &#8220;There was a weapons malfunction during a routine test. I heartily recommend that you do not enquire further, Mr Simmel.&#8221;<br /> Arouin felt irritated at being brushed off again,<br /> <em>Well, they obviously have something to hide,</em> he thought, <em>well&#8230; in addition to all other <strong>other</strong> things they have to hide</em><br /> The Director looked away.<br /> &#8220;You may go Mr Simmel. You know what you have to do.&#8221;<br /> &#8220;Yes, I guess so.&#8221;<br /> Arouin stood up, straightened the chair he had been sitting on, and walked out of the room. The security detail wasn´t there any more&#8230;<br /> <em>Wonder where they´ve gone&#8230;</em> thought Arouin, starting off down the corridor.<br />
</em>

<p>
After a few minutes of walking he found himself thoroughly lost, in a white-walled section of the station. From behind a set of double doors on one side of the corridor there was a scream, and Arouin pushed his way through them carefully to investigate.
</p>

<h2>
</h2>

<p>
There were a number of people lying on beds with various wounds, most of them no longer bleeding. On closer inspection Arouin noticed some of the people were actually corpses, and those that weren´t didn´t warrant close inspection lest you wanted to loose the contents of your stomach. There was a white-coated doctor with an electronic tablet kneeling beside one who was screaming in pain, talking quietly into his ear. Arouin pushed through another set of double doors and into another sterilised-white room. This one had people lying in beds down the length of the ward, most of them with see-through moulded casts around their arms or legs, and one of the people was a young, attractive lady with long brown hair who appeared slightly shorter than the norm. She had a plastic cast over her right shoulder and a red, raw area aroun the top of her right arm where skin was being forced into re-growing over a massive rupture. She was reading a fashion magazine.
</p>

<em>
Arouin pulled out a pistol from one pocket of his trenchcoat and aimed it at her, walked over to the end of her bed, then kicked it. She looked up at him, her face going from surprise and fear to anger.<br />
</em>

<p>
&#8220;What the <em>fuck</em> are you doing here? asked Arouin, choosing his words carefully.
</p>

<h2>
</h2>

<h2>
Part XX
</h2>

<h2>
</h2>

<p>
She stared at Arouin in silent shock for a few moments with her mouth open slightly, uncomprehending, then realisation dawned.
</p>

<h2>
</h2>

<p>
&#8220;What the fuck am <em>I</em> doing here?&#8221; she asked, incredulously. Arouin took a step forward and cocked his pistol with his thumb.
</p>

<h2>
</h2>

<p>
&#8220;No, I asked you that one.&#8221;
</p>

<h2>
</h2>

<p>
There was a scuffle behind Arouin, and he turned around to see the four-man secutity team barging through the double doors into the ward and bringing their weapons to bear. He turned back to see the woman in the bed propped up with one hand, a pistol in the other, pointing at him, with the bed-side drawer open. The tableau stayed frozen for a few seconds, then two of the four-man security team turned to bring their weapons to bear on the brown-haired woman in the bed instead of Arouin. The only sound apart from the breathing of the six armed people and a multitude of scared patients was the tap of slowly approaching footsteps. The double doors parted a second time, and the Director slowly paced in. He looked first at Arouin, then slowly around to the woman in the bed.
</p>

<h2>
</h2>

<p>
&#8220;If you two would mind putting your weapons away, I shall, under the cirumstances, explain myself.&#8221; he said, tiredly.
</p>

<h2>
</h2>

<p>
&#8220;I am not going to put my gun away&#8221; said Arouin quietly, &#8220;until everyone else´s is on the floor. I like living, I like it very much, and I intend to continue doing it for as long as possible. One of my prime survival traits is not to put my gun away if someone else still has theirs.&#8221;
</p>

<h2>
</h2>

<p>
The Director sighed slightly before replying,
</p>

<h2>
</h2>

<p>
&#8220;Well, since Mr.Simmel seems unwilling to comprimise, Lieutenant Voxx, Yyanis, put your weapons down.&#8221;
</p>

<h2>
</h2>

<p>
The movements that the security team made were slow and steady, Yyanis, the woman in the bed, did likewise but with slightly more hesitation, and a slight wince as she moved her arm. Arouin looked at the two other parties, then slowly put his pistol away in his pocket. Then he turned and looked angrily at The Director, who returned his look with an impassive stare.
</p>

<h2>
</h2>

<p>
&#8220;So would you mind telling me exactly what is going on?&#8221;
</p>

<p>
A few minutes later in The Director´s office Arouin was leaning against the wall in one corner of the room studying Yyanis, who was sitting in a chair by the opposite wall, while The Director shoveled a small stack of paperwork into an incinerator. She was wearing what Arouin would class as ´normal´ clothes now, a pair of denim-subsitute trousers and a light blue short sleved shirt with a black jacket over it. After a few seconds of work, he looked up at them, glancing from one to the other.
</p>

<h2>
</h2>

<p>
&#8220;Evidently, you have already met, and I know where from Yyanis´ report on her last assignment.&#8221; here he looked at Arouin, &#8220;You, of course, don´t give me reports on what you do.&#8221;
</p>

<h2>
</h2>

<p>
Arouin looked annoyed at him, his brow creasing slightly,
</p>

<h2>
</h2>

<p>
&#8220;I thought you were buying me off with the promise of blanking my criminal record to get the job <em>done</em> not spend my time filing bloody reports.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Granted. One or two notes on any items of interest would not, however, go amiss.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;That was an item of interest? Some silly, teched-up girl in a Solrain stealth suit appears, nabs it, and I shoot her? Have you any idea how many people I have ended up shooting over the past few days?&#8221;
</p>

<h2>
</h2>

<p>
The director pointedly looked at another pile of paperwork on his desk before replying, &#8220;Yes, I do. That is a complete list. Well, I´m told it´s complete, one or two were probably missed. One thing, why did you kill the receptionist in the research facility?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;It´s not as if I tried to, she was in the line of fire, these things happen&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Also, I haven´t heard from Thorest Vippen for the past few days&#8230;&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Yeah&#8230; uh&#8230; I think he was going to be rather busy for the next few days..&#8221;
</p>

<h2>
</h2>

<p>
The Director looked at him for a moment, &#8220;Really? Oh. Never mind&#8230; anyway; your shoot-on-sight policy is getting you into more trouble than you think, or getting others into trouble.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;What a diabolical shame.&#8221; muttered Arouin, kicking the wall behindhim with one heel.
</p>

<h2>
</h2>

<p>
&#8220;The group of mechanics which you executed two of were actually at Quantar Core to protect your ship from any tampering.&#8221;
</p>

<h2>
</h2>

<p>
Arouin was silent for a moment, then replied, &#8220;I don´t need any help&#8230; if I did, I&#8230;&#8221;
</p>

<h2>
</h2>

<p>
The Director interrupted him, &#8220;..You <em>wouldn´t</em> ask for it. When you finally got to the research facility, according to Yyanis´ report, you would have been unable to obtain the positron coupling if she had not been there to retrieve it from the elaborate technological defences that were in place.&#8221;
</p>

<h2>
</h2>

<p>
Yyanis, sitting over the other side of the room, smiled slightly. Arouin looked annoyed,
</p>

<h2>
</h2>

<p>
&#8220;Actually, you´re wrong. That silly bitch with her fancy suit just made it a little faster for me&#8221;
</p>

<h2>
</h2>

<p>
Yyanis, scowling, got up and started to walk purposefully towards Arouin. The Director motioned her to stop, then looked at Arouin and in a chiding tone of voice said,
</p>

<h2>
</h2>

<p>
&#8220;You might have shattered her arm, and shoulder, she might have a cast on it, but she still excels at unarmed combat.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Not very useful if someone has a gun though&#8221; pointed out Arouin, utterly unfazed.
</p>

<h2>
</h2>

<p>
&#8220;But you don´t have a gun,&#8221; said The Director smiling again, &#8220;and now, if I can&#8230;&#8221;
</p>

<h2>
</h2>

<p>
There was an omnious click and The Director looked up to see Yyanis standing with a shocked and not unfrightened expression staring down the barrel of Arouin´s pistol.
</p>

<h2>
</h2>

<p>
&#8220;That is where you´re wrong&#8221;
</p>

<h2>
</h2>

<p>
The Director got up and looked at him throughtfully.
</p>

<h2>
</h2>

<p>
&#8220;I thought I told you to put down your weapon? Yyanis, do sit down&#8221;
</p>

<h2>
</h2>

<p>
&#8220;I did. This is another one.&#8221; said Arouin, as Yyanis backed off.
</p>

<h2>
</h2>

<p>
&#8220;Put it away. Now, Yyanis was there as a back-up plan. My sources didn´t know what the security was like at the facility. We knew they had their own security troops, which is where you came in,&#8221; he said, looking at Arouin again, &#8220;&#8230;but we were unsure what other defences they had in place. Which is why we sent Yyanis.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;And you neglected to tell me?&#8221; questioned Arouin
</p>

<h2>
</h2>

<p>
&#8220;What you don´t know you can´t tell others.&#8221; said The Director firmly.
</p>

<h2>
</h2>

<p>
&#8220;Thanks for the vote of confidence there&#8221; replied Arouin with bitterness in his voice.
</p>

<h2>
</h2>

<p>
&#8220;We can´t take risks&#8230; too much is at stake here; if any of the governments of the three factions knew that we were getting the pieces of the power system together, they would all try and get it, to get an edge over the other two. We can´t let that happen.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;I suppose not,&#8221; conceeded Arouin, &#8220;where do we go from here?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;I would suggest going for the part of the power system that is currently being held in Octavius Core station. The Octavians are unlikely to have any high-tech equipment protecting the piece that they have.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;So I don´t need to have her tagging along behind me?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;I don´t want her to see action for a while,&#8221; The Director turned to Yyanis to silence her already-forming protest, &#8220;you need to rest, heal. Instead, Yyanis will accompany you in your Phoenix, and act as a restraint. I wish to try and minimize any further civillian casualties, she may be able to spot alternative ways out of a situation that you would use brute force to overcome.&#8221;
</p>

<h2>
</h2>

<p>
Yyanis smiled, Arouin looked vexed, &#8220;What, exactly, is the matter with brute force?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Nothing&#8230; provided it is applied at the right place and the right time. Yyanis´ job is simply to ensure that you <em>are</em> applying it at the right place and the right time. You are to take any orders that she gives you as if they were from me&#8230; if they aren´t followed I <em>will</em> know about it, and I will <em>not</em> be happy.&#8221;
</p>

<h2>
</h2>

<p>
Arouin was looking decidedly not pleased, &#8220;I have got to take orders from her?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Yes&#8230; well, within reason. If you think that anything she tells you to do would comprimise your mission there, then obviously do something else,&#8221; here The Director looked at Yyanis, &#8220;although I expect her to use discretion.&#8221;
</p>

<h2>
</h2>

<p>
Arouin pushed himself away from the wall he had been leaning against, and moved towards the door. Yyanis got up to follow him, and he shot her a venom-laden look before glancing back to The Director.
</p>

<h2>
</h2>

<p>
&#8220;You I have been lumbered by this silly bitch if I hadn´t poked my nose around?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Actually, yes. She was going to be shipped out to Octavius Core in tow days time, you have just accelerated the process somewhat.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Wonderful.&#8221; Arouin said, sarcasm dripping from his voice. He walked out of the door, slamming it behind him and catching Yyanis´ leg in between the door and it´s frame. He ignored her shout of pain and carried on down the corridor, past the security detail and towards the hangar.
</p>

<p>
As he walked purposefully across the hangar bay, uneven footsteps followed him, gaining slowly, until Yyanis appeared beside him, hobbling slightly.
</p>

<h2>
</h2>

<p>
&#8220;I know we haven´t got off to a very good start, but with time&#8230;&#8221;
</p>

<h2>
</h2>

<p>
&#8220;With time,&#8221; interrupted Arouin, &#8220;you will hopefully be shot and killed and I can get on with doing my job my way&#8221;
</p>

<h2>
</h2>

<p>
Yyanis looked away for a moment, shocked slightly as the pair carried on across the hangar towards the Phoenix, then looked back at Arouin´s face.
</p>

<h2>
</h2>

<p>
&#8220;What have I done to deserve this hostility?&#8221;
</p>

<h2>
</h2>

<p>
Arouin ignored her, and started climbing up to the cockpit, slapping the cockpit release control as he did so. The canopy raised and he clambered over the side as Yyanis slowly and painfully made her way up the side of the Phoenix´s nose. The canopy was already closing as Yyanis hauled herself over into the cockpit, and looked around for somewhere to sit as Arouin´s hands flew over the control board.
</p>

<h2>
</h2>

<p>
&#8220;Where do I sit?&#8221;
</p>

<h2>
</h2>

<p>
&#8220;Tower, this is the <em>Lithe Shadow</em> requesting clearance to leave.&#8221; he said to the comms system.
</p>

<h2>
</h2>

<p>
&#8220;This is Tower, Mr Simmel we are starting the lift now, see you around.&#8221;
</p>

<h2>
</h2>

<p>
&#8220;See ya Tower.&#8221;
</p>

<h2>
</h2>

<p>
The Phoenix shook for a moment, then there was a jolt as it began to sink into the floor, and Yyanis stumbled, steadying herself with a hand against the inside of the glass of the canopy. She looked around for a moment, then gave up and sat cross-legged on the floor, holding on to one side of the cockpit with one hand as darkness closed over the Phoenix.
</p>

<p>
The journey that followed was long and uneventful; Arouin didn´t make any stops at any interim stations, he flew the <em>Lithe Shadow</em> straight to Octavius Core Station. Any attempts that Yyanis made at starting a conversation were brushed aside by Arouin by ignoring them completely. The core sector was unusually busy, shuttles fliting back and forth betwen the main space station and its newly-forming child; a tooling centre with large numbers of automated factories to help the main station with its manufacturing industry. It was mainly a framework at the moment, but there were a few areas where modules had been put in place and power was running. Hazard lights flickered over the struts of the station, warning travellers of the new construction that was partially invisible in the darkness of space. Arouin could see, when he strained his eyesight a little, a few wreckage clouds scattered here and there in the vicinity of the new station; where some poor unforunates had not paid enough heed to the baleful red glow of the hazard lights or there had been a power faliure in the lighting system itself. Arouin smoothly piloted the spacecraft into the glowing green docking rings leading to the docking tube, waited a few moments for a Solrain Interceptor-class fighter to dock, then accelerated into the docking tube. Yyanis awoke as the lift moved them up into the hangar and got up from her prone position on the floor. She yawned and looked down at Arouin, who was still seated in the pilot´s chair.
</p>

<h2>
</h2>

<p>
&#8220;So we´re here then?&#8221; the asked, cheerfully, and recieved a non-comittal grunt in reply. He looked annoyed, and walked around the chair so she was partially in front of Arouin, and looked him in the eyes as the light from the hangar bay moved over the Phoenix.
</p>

<h2>
</h2>

<p>
&#8220;What the hell is the matter with you? You <em>shot</em> me on Quantar Core station, and I´m not mad at you for that &#8211; you were just doing your job to the best of your abilities. Why won´t you speak to me at all, whatever it is, just discuss it with me and we´ll see what we can do.&#8221;
</p>

<h2>
</h2>

<p>
Arouin looked at her, sighed, and stood up, hitting the cockpit released button as he did so. The canopy opened, and he vaulted over the side and began to climb down.
</p>

<em>
&#8220;I work,&#8221; he paused as he dropped off onto the hangar floor, &#8220;alone. That means by myself, e.g.: not with others. All you are going to do is get in my way and tell me not to do stuff that I deem necessary to do.&#8221; Arouin watched the canopy lower as Yyanis hit the hangar floor, then turned and walked away from the Phoenix, weaving through the ships towards the center of Octavius Core Station.<br />
</em>
