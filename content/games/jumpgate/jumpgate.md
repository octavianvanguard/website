---
banner: "css/images/banner.png"
title: Jumpgate
author: Lupus
layout: page
date: 2018-04-07

---
The Octavian Vanguard\`s mission is to aggressively further the interests of the Octavian Empire throughout TRI space.

OV will also accept mercenary contract bids if they do not conflict with our primary goals.

* **Octavian Vanguard:** [Map Of Sectors]({{< relref "jg-sectors.md" >}})
* **Spacecraft:** [Griffon]({{< relref "jg-griffon.md" >}})
* **Fiction: ** '[Betrayal]({{< relref "betrayal.md" >}})' by Fellblade
