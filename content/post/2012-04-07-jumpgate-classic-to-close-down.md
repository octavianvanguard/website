---
banner: "css/images/banner.png"
title: Jumpgate Classic to Close Down
author: divvur
date: 2012-04-07
categories:
  - Jumpgate
tags:
  - JG
  - JGC
  - Jumpgate Classic
  - MMO
  - Octavian Vanguard
  - OV

---
It&#8217;s the game where it all started, a space combat MMO that saw squadrons of fighters engage in PvP combat across a vast three dimensional universe. One such squadron was Octavian Vanguard, who have endured since the early days of Jumpgate Classic, which was launched in 2001.

The game itself, however, will be shutting down at the end of April, leaving no traces of its existance except for the communities and friendships that were created during it&#8217;s 11 year run. A message from the team behind the game has been left on the <a title="Jumpgate Classic" href="http://www.jossh.com/" target="_blank">Jumpgate Website</a>.

![Jumpgate Classic Screenshot](/wp-content/uploads/2012/04/normal_200212102.jpg)

A sequal to the original game, Jumpgate Evolution, had previously been announced. However, it is believed that this game is longer in development.

Even without the Octavian Empire to protect, you can be sure that Octavian Vanguard will continue to thrive in other online games. Should some smart forward thinking games developer think to make another game featuring high quality first person space combat, in depth logistical challenges and a degree of team based action, you can be sure that OV will be there to once again challenge for the position of best squadron out there.

