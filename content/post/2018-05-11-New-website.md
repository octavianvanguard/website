---
banner: "css/images/banner.png"
title: New web-site launched!
author: Specto
date: 2018-05-11
categories:
  - General


---
Welcome to the new and improved OV website!

The old word-press site was starting to look a little dated, so we've given the site a face-lift.

Don't worry - all the old content has been imported, so take a look around and if you feel like it join us on our discord server.
