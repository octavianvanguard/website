---
banner: "css/images/banner.png"
title: City of Heroes/Villains – Issue 13 is live!
author: ohms
date: 2008-12-03
categories:
  - City of Heroes

---
Issue 13: Power And Responsibility has arrived on the live servers and is now open!

Some of the main features are:

  * Midnight Squad Story Arcs (for Level 20-25 Heroes, Level 30-35 Villains)
  * Cimerora Story Arcs (for Level 40-50 Heroes and Level 35-50 Villains)
  * The Merit Rewards System
  * Day Jobs
  * Leveling Pact
  * New Shield and Pain Domination Powersets
  * Multiple Builds
  * PVP Revamp
  * Plus a whole host of tweaks, new invention sets, QoL changes and bug fixes

Read the full patch notes [here](http://eu.cityofheroes.com/game-guide/game-patches/issue_13_power_and_responsibility/)

![](/wp-content/uploads/2011/11/ov-coh-issue13.jpg)

