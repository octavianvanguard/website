---
banner: "css/images/banner.png"
title: City of Heroes/Villains – Now on Mac too!
author: ohms
date: 2009-01-09
categories:
  - City of Heroes

---
What’s the only thing as good as City of Heroes® on the PC? City of Heroes on the Macintosh, of course! NCsoft is proud to announce the availability of our super-powered MMO on the Mac for those players out there who choose to make it their gaming console of choice. The Mac Special Edition is a digital only download and includes special in-game items that you’re not going to want to miss out on!

Check out the official press release [here][1].

![](/wp-content/uploads/2009/01/ov-coh-mac.jpg)

 [1]: http://eu.plaync.com/eu/news/pressrelease-full/ncsoft_opens_city_of_heroes_world_to_mac_gamers/
