---
banner: "css/images/banner.png"
title: 'City of Heroes : Spring Fling is near!'
author: ohms
date: 2012-02-01
categories:
  - City of Heroes
tags:
  - City of Heroes
  - MMO
  - Spring
  - Valentines

---
Spring is just around the corner, and Cupid&#8217;s arrows are nocked&#8211;it&#8217;s almost time for the annual Spring Fling celebration! This time, the matchmaking cherub has gone all out with new missions, badges, and temporary powers!<a href="http://na.cityofheroes.com/en/global/includes/images/news/Cupid1_logo.jpg" target="_blank"><img src="http://na.cityofheroes.com/en/global/includes/images/news/Cupid1_logos.jpg" alt="" border="0" /></a>

In addition to the yearly co-op tasks of fighting Arachnos to retake tainted water from the Well of the Furies, retrieving Aphrodite&#8217;s Girdle for the god Eros, and stopping that dimension-hopping Red Cap, Snaptooth, this year&#8217;s Spring Fling introduces two brand new holiday missions, with new rewards!

**Widow in Red**

Ms. Liberty (Heroes) and Arbiter Hawk (Villains) have recruited you for a secret mission involving the infamous Red Widow, Lord Recluse&#8217;s long-deceased love. This mission is available for characters level 30 or higher, and awards the _**Widow Maker badge**_ for completion!

<a href="http://na.cityofheroes.com/en/global/includes/images/news/Cupid2_logo.jpg" target="_blank"><img src="http://na.cityofheroes.com/en/global/includes/images/news/Cupid2_logos.jpg" alt="" border="0" /></a>

**Valentines**

No Valentine&#8217;s Day would be complete without the exchange of love letters, but many of these heartfelt confessions have gotten lost throughout Paragon City, the Rogue Isles, and Praetoria! DJ Zero has charged you with finding these Valentines and delivering them to their intended recipients, but that doesn&#8217;t mean you have to: if you&#8217;re feeling mischievous, you can misdirect the letters!

These Valentines drop from eligible mobs in Paragon City, the Rogue Isles, and Praetoria during Spring Fling. Players must be level 20 or higher, and have completed an attunement mission from Ganymede (Heroes) or Scratch (Villains). Completing a Valentine mission rewards you with new temp powers _**Arrows of Romance**_, which increases regeneration of allies and placates foes; or _**Arrows of Jealousy**_, which increases damage dealt by friends and confuses enemies. Completing Valentines missions will also award the _**Match Maker**_ or _**Missed Connection**_ badges!

The Spring Fling event begins Wednesday, February 8, 2012 at 7am PST (10am EST / 3pm GMT / 4pm CET) and ends on Monday, February 27, 2012 at 7am PST (10am EST / 3pm GMT / 4pm CET)!

