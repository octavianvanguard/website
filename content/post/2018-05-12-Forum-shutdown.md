---
banner: "css/images/banner.png"
title: OV Forums shutting down
author: Specto
date: 2018-05-12
categories:
  - General


---
After many years of use, we're sorry to announce that the OV Forums will be shutting down. It's been a journey, with the forums facilitating many a rampage throughout digital galaxies.

OV still have active members and we now predominately connect using Discord. Feel free to join us on our discord channel: [https://discord.gg/fYQ5Cf2](https://discord.gg/fYQ5Cf2)