---
banner: "css/images/banner.png"
title: City of Heroes – Issue 10 Invasion!
author: ohms
date: 2007-07-24
categories:
  - City of Heroes

---
_The skies over Paragon City are filled with dark, grey thunderheads. It is the same in the Rogue Isles, Europe, and across the globe. Small ships dart through the heavy weather&#8230; And there are so very many of them._

_The Rikti have returned._

![](/wp-content/uploads/2011/11/ov-coh-issue10.jpg) 
  
_Major Features_

_Rikti World Invasion: The Rikti have been fought off before. But now, prepare for a desperate battle to save Earth from the new Rikti Invasion Force! Aggressive Rikti forces can attack Paragon City and Rogue Isle areas at any time and with little warning!_

_Rikti War Zone: Heroes and villains are asked to put aside their differences and co-operate to assist Vanguard, the volunteer force funded by the United Nations Security Council, to stem the flood of Rikti and their attempts to salvage their downed Mother Ship. Earn Vanguard Merits to obtain new costume items and other cool stuff!_

_Rikti Co-op Task Force: Lady Grey, leader of Vanguard, has a series of missions for a proven team of superpowered individuals, security clearance 45 to 50, designed to take the fight to heart of the Rikti Invasion!_

_Combined Notes (CoH and CoV)_

_Invasion!_

_&#8220;The Rikti Return! The Vanguard needs YOU! All super powered beings are encouraged to join the effort! This isn&#8217;t about your history, but about OUR future. Join now.&#8221;_


  * The Rikti have somehow found a way to bridge the dimensional gap between their world and ours once again, and are staging raids on Paragon City and the Rogue Isles.
  * When Zones within the cities are under attack, a world-wide alert will be issued by Vanguard. This alert will show up in your Chat window, warning you in which zone attack is imminent.
  * While the invasion is occurring, players do not receive debt while outdoors in an invaded zone.
  * Those in that zone that do not wish to be at risk of Rikti attack are asked to take shelter immediately. They can do so by leaving the zone, or moving inside buildings or under cover (overpasses, parking garages, etc.)
  * Rikti bombs appear to be unstable and not 100% reliable. Some do not explode on impact and only embed themselves in the ground. These bombs have been noted to detonate after a time, so take caution when you see these. They can be disarmed or delayed by inflicting damage upon them.

_Rikti War Zone_
  
_The region previously known as the Crash Site has erupted into a War Zone! All non-combatants are advised to stay well clear of the area!_


  * This zone is now co-operative &#8211; Heroes and Villains can both enter into the zone and team together while here. Heroes and Villains cannot fight in this area. They will both arrive into the zone in the Vanguard Base that has been established there.
  * Entry points can be found in Vanguard Bases in the following zones: 
      * Paragon City: 
          * Atlas Park
          * Founders Falls
          * Peregrine Island
      * Rogue Isles: 
          * Cap au Diable
          * St. Martial
          * Grandville
      * The SWAT entrance from Crey&#8217;s Folly is now sealed.
  * The zone is now scaled for levels 35 to 50.
  * While in the Rikti War Zone, debt obtained will be 50% of normal.
  * When you enter the zone, Levantera, one of the directors of Vanguard, will be auto-issued to you as a contact. Complete her missions to get a feel for the area, as well as be inducted as an honorary member of Vanguard and be able to obtain merits.
  * Borea has repeatable missions for you in the Rikti War Zone.
  * Other characters that have missions available are Gaussian, Dark Watcher, and Serpent Drummer. They can all be found in the Vanguard Base.
  * By joining Vanguard you will earn Vanguard Merits for defeating Rikti as well as completing missions for Vanguard. These Merits can be exchanged for Costume Pieces (of the Vanguard Armor), as well as some other cool stuff. The devices for purchasing these items are special crafting tables located within the Vanguard base. Vanguard Merits can not be used at other crafting tables.
  * You can view Vanguard Merits earned from the Salvage window under the &#8216;Special&#8217; tab.

_Rikti Co-op Task Force_
  
_The UNSC backs Vanguard for billions. With the second Rikti invasion bearing down upon us, the United Nations Security Council has essentially given Vanguard a blank check to cover the costs of fighting the Rikti. &#8220;We don&#8217;t have time to haggle over budgets,&#8221; says a senior level official in the UNSC. &#8220;Vanguard has always been about defeating the Rikti, no matter what. We fully support Lady Grey and her team.&#8221;_ 

  * Lady Grey gives out a Strike/Task Force for 8 super powered individuals with security clearance from 45 to 50. The mission attempts to stop the Rikti before they can get a foothold in our dimension. Be wary, the Rikti are single-minded in the pursuit of their objectives!

_Take the fight to the crashed Rikti ship!_


  * The Rikti have erected strange new energy pylons throughout the area surrounding their crashed ship in the Rikti War Zone. Vanguard engineers have discovered a way to detect Rikti Pylons. To view waypoints to these pylons, check the &#8216;Special&#8217; option in your map display window.
  * Destroying all pylons before the Rikti can replace them will lower the shield on the Rikti Mother Ship in the zone.
  * Heroes and Villains are asked to plant explosives inside the ventilation systems of the ship to cripple the Mother Ship repair effort the Rikti have in progress.
  * Once enough bombs are planted, the Master of War will arrive to clear out non-Rikti interlopers!
  * Characters that participate with this will be rewarded with Vanguard Merits.

_Rikti Villain Group Revamp_

  * The Rikti Villain group has undergone an art and powers revamp.
  * The new art vastly upgrades the look of the Rikti and now allows us to use more animations with the Rikti soldiers.
  * One major change is the Rikti swords now have a ranged energy weapon in them, allowing them to use both melee and ranged attacks without switching weapons.

Full patch notes are available [here](http://uk.cityofheroes.com/game-guide/game-patches/issue_10_invasion/).
