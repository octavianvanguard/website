---
banner: "css/images/banner.png"
title: 'City of Heroes : Issue 22: Death Incarnate is now Live!'
author: ohms
date: 2012-03-06
categories:
  - City of Heroes
tags:
  - City of Heroes
  - Issue
  - MMO

---
The fog has cleared from the streets of Dark Astoria, revealing a horror so terrifying that even the restless spirits dare not roam its streets. Heroes and Villains alike seek to master the powers of darkness that rest with the Demon God, Mot. Experience the all new revamped Dark Astoria, the new Dilemma Diabolique Incarnate Trial and the all new, free for VIPs Power Set: Darkness Control! Issue 22: Death Incarnate has arrived to your favorite super heroic MMORPG, City of Heroes Freedom!

 <a class="thickbox" href="http://na.cityofheroes.com/media/global/includes/images/Issues/issue-22/Screenshots/sentinel_elite_logo.jpg" rel="gallery"><img class="mt-image-none" src="http://na.cityofheroes.com/media/global/includes/images/Issues/issue-22/Screenshots/thumbs/sentinel_elite_logo_t.jpg" alt="Sentinel Elite" /> </a>

With tons of new content, including the all new mid level trial Drowning in Blood, new Powersets, the new Olympian Guard costume set (free for all VIP subscribers), new low level missions found in Steel Canyon, and the much anticipated solo Incarnate Path in the newly revamped Dark Astoria, Issue 22: Death Incarnate is a great time for you to take to the skies in City of Heroes Freedom! So log in today and experience what&#8217;s new to Paragon City!

 <a class="thickbox" href="http://na.cityofheroes.com/media/global/includes/images/Issues/issue-22/Screenshots/Diabolique4_logo.jpg" rel="gallery"><img class="mt-image-none" src="http://na.cityofheroes.com/media/global/includes/images/Issues/issue-22/Screenshots/thumbs/Diabolique4_logo_t.jpg" alt="Diabolique Incarnate Trial" width="240" height="150" /></a>

You can find an overview  for Issue 22: Death Incarnate <a title="Issue 22 Overview" href="http://na.cityofheroes.com/en/news/game_updates/issue_22/overview.php" target="_blank">here</a> and the Patch Notes <a title="Issue 22 patch notes" href="http://na.cityofheroes.com/en/news/patch_notes/issue_22_death_incarnate_-_march_6_2012_live.php" target="_blank">here</a>!


