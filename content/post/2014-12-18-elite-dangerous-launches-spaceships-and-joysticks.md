---
banner: "css/images/banner.png"
title: 'Elite Dangerous Launches: Spaceships and Joysticks!'
author: ohms
date: 2014-12-18
categories:
  - Games
  - General
tags:
  - Elite

---
<img src="http://www.lard.me.uk/ED/cariolissmall.png" alt="" width="579" height="450" />

* * *

&nbsp;

Joystick Wagglers\* old and new are now able to take part in the release version of the online laser-fest that is Elite Dangerous. Those oldies who remember Jumpgate\** have probably already splooged the cash and bought into the Beta but as of two days ago, Elite Dangerous is live and stable. Pretty much. The game is rather pretty, and doesn&#8217;t need a ridiculous PC to play but that&#8217;s no excuse to starve your eyeballs of such beauty as can be seen in <a title="4k" href="http://www.lard.me.uk/ED/cariolis.png" target="_blank">4K</a>

At the moment the on-line cooperation is limited, unsurprising considering the strong bias towards off-line/solo play that existed in the Backer/Alpha community. However, squad mechanics are promised Soon &#8482; along with many expansions to come in the future. The game costs <a title="£40" href="http://www.elitedangerous.com" target="_blank">£40</a> and does not require a subscription. Future expansions are not included in the initial purchase, but will not be mandatory. Initial reports range from &#8220;OMG it&#8217;s full of (200bn) stars!&#8221; to &#8220;How the hell do you even BOOM!&#8221;. Helpful hints will be available from experienced OV pilots, as soon as we have some.

*No joystick required, but most of us play in the dark &#8211; so why not?
  
**One former Jumpgate pilot is report to be running round in small circles and biting the furniture. Already.

