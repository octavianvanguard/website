---
banner: "css/images/banner.png"
title: 'City of Heroes/Villains – Issue 14 live & 5 Year Anniversary'
author: ohms
date: 2009-04-24
categories:
  - City of Heroes

---
Issue 14 goes live!

> <div>
>   First City of Heroes® set a new standard for player creation and customization in MMOs with its Character Creator. Now, with Issue 14, this game takes another giant step, allowing players to design their own missions and story arcs to share with the entire City of Heroes community across all of our live servers. Using an intuitive interface, players can browse through other player created missions and create their own missions from the ground up. Players will determine details ranging from environments, mission objectives, and enemies, to written fiction and character dialogue; giving their stories nearly infinite depth and personalization.
> </div>

Read more [here][1]

5 year anniversary celebrations!

> <div>
>   Five years ago a trumpet call rung out for heroes…</p> 
>   
>   <p>
>     From around the globe that call was answered and the streets, rooftops, and skies of Paragon City™ were filled with the widest variety of costumed crusaders ever seen. These stalwart players brought with them the courage, fellowship, and dedication that made Paragon once again a true City of Heroes®!
>   </p>
>   
>   <p>
>     Since that fateful day much has changed. Enemies have fallen and new ones have risen in their place, proving that evil never sleeps. Threats came from alternate dimensions, mystical realms, other times, and even from our own world itself. Lord Recluse™ loosed an army of villains into the world, turning the Rogue Isles™ into the largest den of villainy in recorded history. His destined ones showed remarkable will, cunning, and determination in this City of Villains®. Heroes and villains came into direct conflict with PvP battles, uncovered new powers, found new worlds to explore…or conquer, broke through with new inventions, and flashed back through time. The most recent development has provided the tools for players to architect all new adventures and help take us into the future.
>   </p>
>   
>   <p>
>     Now it is a time to celebrate all the triumphs of the last five years and so Paragon Studios™ has cooked up some special gifts for all our loyal fans.
>   </p>
>   
>   <p>
>     Please join us for a month of celebration starting on our birthday, April 28th and continuing on throughout the month of May!
>   </p>
> </div>

Read more [here][2].

 [1]: http://eu.cityofheroes.com/en/news/game_updates/C208
 [2]: http://eu.cityofheroes.com/en/news/article/city_of_heroes_5_year_anniversary_celebration
 
