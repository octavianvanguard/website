---
banner: "css/images/banner.png"
title: City of Heroes – Issue 9 Breakthrough
author: Zeshin
date: 2007-05-01
categories:
  - City of Heroes

---
![](/wp-content/uploads/2011/11/ov-coh-issue09.jpg)Issue #9: Breakthrough, the next free expansion to the City of Heroes universe, introduces an entirely new level of depth and gameplay. Players acquire and invent new items and have the option to buy and sell them using the new hero and villain auction houses. Along with Issue #9’s other great features, this brand new system results in a wealth of new options for heroes and villains alike.

Invention System (Heroes and Villains)
  
Using the highly anticipated Invention system, players acquire and collect “loot” in the form of Salvage and Invention Recipes. These are used together to invent uber-enhancements, costume pieces, and more. This creates a world of new activities, rewards and character evolution!

Auction Houses (Heroes and Villains)
  
Where better to buy and sell all of these new items than in the new auction houses! Wentworth’s Fine Consignments have sprung up throughout Paragon City™ while the villains prefer to do their bidding alongside “black-market” trucks placed throughout the Rogue Isles™.

Statesman’s Task Force (Heroes only)
  
This high-level PvE content allows eight heroes to team up in order to face and defeat Paragon’s greatest enemies! Completing this difficult challenge results in some of the game’s rarest loot!

New Hamidon Encounter (Heroes and Villains)
  
Not only do villains get their own end-game raid encounter with the “Hamidon,” but the new experience is also brought to the Heroes side where players must use new tactics to bring down one of the “City of”s greatest threats.
