---
banner: "css/images/banner.png"
title: City of Heroes Closing down
author: ohms
date: 2012-09-01
categories:
  - City of Heroes
  - Uncategorized
tags:
  - City of Heroes
  - coh
  - MMO

---
### [Farewell, from all of us at Paragon Studios][1]

This morning we announced that Paragon Studios will be taking to the skies of City of Heroes for the last time.

In a realignment of company focus and publishing support, NCsoft has made the decision to close Paragon Studios. Effective immediately, all development on City of Heroes will cease and we will begin preparations to sunset the world&#8217;s first, and best, Super Hero MMORPG before the end of the year. As part of this, all recurring subscription billing and Paragon Market purchasing will be discontinued effective immediately. We will have more information regarding a detailed timeline for the cessation of services and what you can expect in game in the coming weeks.

The team here at Paragon deserves special praise for all that we have accomplished over the last 5+ years. These developers are some of the most creative and talented people in the gaming industry. By now, we&#8217;ve all been given this news internally, but to anyone who may be reading this message after the fact; know that your hard work and dedication has not gone unappreciated or unnoticed. To any potential studios looking to grow your team; hire these people. You won&#8217;t regret it.

To our Community,

Thank you. Thank you for your years of support. You&#8217;ve been with us every step of the way, sharing in our challenges, encouraging us to make City of Heroes better, more than everyone else thought it could be. We couldn&#8217;t have come this far without you. I implore you all, focus on the good things of CoH and Paragon Studios. Don&#8217;t dwell on the &#8220;how&#8221; or the &#8220;why&#8221;, but rather join us in celebrating the legacy of an amazing partnership between the players and the development team.

Thank you, and I&#8217;ll see you in the skies, one last time.

Andy Belford

Community Manager

Paragon Studios.

<a title="Official announcement" href="http://na.cityofheroes.com/en/news/news_archive/thank_you.php" target="_blank">http://na.cityofheroes.com/en/news/news_archive/thank_you.php</a>

The game will be closing for good on 30th November.

 [1]: http://na.cityofheroes.com/en/news/news_archive/thank_you.php
 
 
