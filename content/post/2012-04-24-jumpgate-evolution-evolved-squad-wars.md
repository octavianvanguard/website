---
banner: "css/images/banner.png"
title: 'Jumpgate Evolution evolved : Squad Wars'
author: ohms
date: 2012-04-24
categories:
  - Jumpgate
  - Upcoming Games
tags:
  - EndGames
  - Jumpgate
  - NetDevil

---
Take control of a state of the art spacefighter in SQUAD WARS™, the new **free-to-play** team based space combat game from END Games Entertainment.  <a title="End Games" href="http://endgames.net/games/squadwars" target="_blank">The team behind Squad Wars</a> includes the creative forces behind the classic groundbreaking space combat MMOG, _Jumpgate_, and numerous other online games from the last 15 years.

<div class="template" data-href="http://static.endgames.net/squadwars/HeaderImage02.jpg">
  <img src="http://static.endgames.net/squadwars/HeaderImage02.jpg" alt="" />
</div>

Form a Squad and fly into harrowing dogfights and zero-G furballs as you vie for dominance of the stars against competing Squads. Gain rank, experience and cash to upgrade your ship. Hack your CoBot™ to upgrade in-flight support abilities, and rack up achievements or purchase unlocks for new fighters and ever more devastating weapons. Forge your Squad’s unique identity and battle other Squads for control of entire systems (in the name of the Interstellar Conglomerates, of course).

#   &#8220;Create an elite squad of mercenary space pilots with your friends to seek fortune and glory across the galaxy.&#8221;

Squad Wars is space combat the way it&#8217;s meant to be; furious, uncompromising and totally intense. This is no on-rails space game, this is true hardcore PvP space combat. Space is stone cold dangerous; here in the fields, it’s get to flying or get busy dying.

<a title="Squad Wars Kickstarter page" href="http://www.kickstarter.com/projects/1065318294/squad-wars" target="_blank">Learn more at their Kickstarter page</a> or join the community at the games <a title="Squad Wars" href="http://squadwars.endgames.net/" target="_blank">official website</a>.

