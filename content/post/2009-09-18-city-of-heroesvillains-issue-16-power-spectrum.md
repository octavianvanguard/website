---
banner: "css/images/banner.png"
title: City of Heroes/Villains – Issue 16 Power Spectrum
author: ohms
date: 2009-09-18
categories:
  - City of Heroes

---
The excitement is palpable among heroes and villains alike. Issue 16: Power Spectrum is now available across Paragon City and the Rogue Isles for all to make heroic (or villainous) use of! With this much-anticipated update, players can use the all new character creator and customize most of their powers by selecting themes or changing the colors and animations of specific powers.

Whether you feel the need to match your hero&#8217;s powers with their bright outfit or want your villain&#8217;s powers to look even more nefarious, the palette is yours!

But that is far from being all that Issue 16 has to offer. Take a look at the [Issue 16 Release Notes][1] to familiarize yourselves with the list of additions in City of Heroes. These include new Mission Difficulty settings for players seeking the ultimate challenge and the all new Super Sidekicking system, making Sidekicking easier and more fun than ever with the convenience of never having to find and wait for someone else to sidekick you again! Powerset Proliferation has also brought select powers to several new Archetypes.

Don&#8217;t take our word for it though; it&#8217;s all happening now on a live City of Heroes server near you!

See the Issue 16 trailer [here][2]!

 [1]: http://boards.cityofheroes.com/showthread.php?t=191169
 [2]: http://www.youtube.com/watch?v=DWhF_fSH76c
 
 
