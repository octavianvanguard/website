---
banner: "css/images/banner.png"
title: MMO Update, with Vids!
author: divvur
date: 2012-08-05
categories:
  - Games
  - Guild Wars
  - Planetside
  - Star Trek Online
  - The Old Republic
tags:
  - Guild Wars
  - MMO
  - Octavian Vanguard
  - OV
  - Planetside
  - Star Trek Online
  - The Old Republic

---
It has been a busy month in the world of MMOs, here is just some of the latest news that has caught the interest of OV members.

<a title="Planetside 2" href="http://www.planetside2.com/" target="_blank">Planetside 2 </a>is a game that is certainly being closely followed after OV spent a lot of time as an Outfit in the original. They have currently entered closed Beta and at the same time have released a <a title="Planetside 2 Cinematic" href="http://youtu.be/41QFL4QB3NE" target="_blank">cinematic trailer</a>. As good as this trailer is, it is a little unbelievable that a Smurf could kill anything without a Jackhammer&#8230; Loyalty Until Death!

<img class="alignnone" title="Planetside 2" src="http://www.planetside2.com/uploads/dcsclient/000/000/000/445.jpg?v=151.4" alt="Planetside 2" width="576" height="388" />

Another game that is currently in Beta is <a title="Guild Wars 2" href="https://www.guildwars2.com/en-gb/" target="_blank">Guild Wars 2</a> , a sequel that looks like it may define the next generation of Sword and Sorcery MMOs, and all for zero subscription fee. It promises to be a living, breathing fantasy world with personal storylines, a dynamic event system and world-class PvP. The pre-order headstart begins on 25th August.

<img class="alignnone" title="Guild Wars 2" src="https://d2vn94glaxzkz3.cloudfront.net/wp-content/uploads/2012/02/elementalist-static-field.jpg" alt="Guild Wars 2" width="576" height="324" />

<a title="Star Wars: The Old Republic" href="http://www.swtor.com/" target="_blank">Star Wars: The Old Republic</a> has also announced some upcoming content, complete with a <a title="TOR HK-51 Trailer" href="http://www.swtor.com/info/news/news-article/20120716" target="_blank">cinematic teaser</a> featuring a droid design that may be familiar to those who have played the previous Knights of the Old Republic games. Not only that, but Bioware have also announced that they will be expanding Star Wars: The Old Republic to include a <a title="TOR Free to Play" href="http://www.swtor.com/info/news/press-release/20120731" target="_blank">Free-to-Play option</a> later this year.

<img class="alignnone" title="Star Wars: the Old Republic" src="http://www.swtor.com/sites/all/files/en/ss/gu_1-4/ss07-1600x900.jpg" alt="HK-51" width="576" height="324" />

In addition, <a title="Star Trek Online" href="http://sto.perfectworld.com/" target="_blank">Star Trek Online</a> has also recently launched <a title="Star Trek Online" href="http://sto.perfectworld.com/seasons/season6" target="_blank">Season 6</a>, which features Fleet Starbases and introduces the Tholians, first seen in the original series, to the game.

With so many great games being released or updated, it is going to be a busy second half of the year for Octavian Vanguard. See you in game.

&nbsp;

