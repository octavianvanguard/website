---
banner: "css/images/banner.png"
title: Star Trek Online goes Free-to-Play
author: divvur
date: 2012-01-17
categories:
  - Star Trek Online

---
Star Trek Online, an MMO in set in the Trek Universe by Cryptic Studios, is now available free of charge.

The game was originally released in 2010 and featured a hybrid game engine, with both space and ground combat. Each player is the Captain of a starship, fighting, exploring and boldly going on behalf of either the United Federation of Planets or the Klingon Empire.

<div class="mceTemp">
  <img class="alignnone" title="Fighting the Borg" src="http://www.startrekonline.com/dyncontent/startrek/uploads/sto_f2p_screen_12.jpg" alt="A Federation Ship encounters a Borg Cube" width="277" height="155" /><img class="alignnone" title="Starfleet Away Team" src="http://startrekonline.com/dyncontent/startrek/uploads/sto_f2p_screen_21.jpg" alt="An Away Team ready for action" width="277" height="155" />
</div>

Now the Free-to-play game is available to anyone wishing to give the game a try, with future content funded through the sale of items in the in-game store. Octavian Vanguard originally set up a &#8216;Fleet&#8217; (STO guild) when the game launched and this fleet will likely see some activity as new content is released after the free-to-play launch.

For more information, and to watch the Free-to-Play Launch Trailer, [click here][1].

 [1]: http://www.startrekonline.com/f2p "Star Trek Online"
 
 
