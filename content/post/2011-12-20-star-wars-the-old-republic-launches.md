---
banner: "css/images/banner.png"
title: 'Star Wars: The Old Republic Launches'
author: divvur
date: 2011-12-20
categories:
  - The Old Republic

---
Octavian Vanguard have taken a step into a galaxy far, far away with the release of Star Wars: The Old Republic. The MMO game, created by Bioware, allows us to combine classic Octavian teamwork with a fantastic new storyline set in the Star Wars universe.

Octavian Vanguard are on a PvP server, **Ahto City**, as a Republic guild. Members that have been given early access to the game have already created a range of characters, including Jedi, Smugglers and Troopers.

<img class="alignnone" title="Republic Trooper" src="http://cdn-www.swtor.com//sites/all/files/en/classes/trooper/62img/ss01_full.jpg" alt="" width="277" height="155" /><img class="alignnone" title="Jedi Ship" src="http://cdn-www.swtor.com//sites/all/files/en/starships/defender/media/SS_Jedi_Ship01_full.jpg" alt="" width="277" height="155" />

Wondering what the game is all about? Watch the <a title="TOR Launch Trailer" href="http://www.swtor.com/media/trailers/your-saga-begins-launch-documentary" target="_blank">Star Wars: The Old Republic Launch Trailer</a>.

We look forward to seeing you in-game, either as allies loyal to the Republic or evil members of the Sith Empire that must be erradicated to save the galaxy. Interested in getting in contact with us? Why not post in the Star Wars Cantina on our [forums][1].

 [1]: http://www.octavianvanguard.net/forum/index.php "Octavian Vanguard Forums"
