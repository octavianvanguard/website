---
banner: "css/images/banner.png"
title: Guild Wars 2 launch date set
author: ohms
date: 2012-06-28
categories:
  - Guild Wars
tags:
  - Guild Wars

---
The wait is almost over. ArenaNet have finally announced the launch date for their online roleplaying game, Guild Wars 2.

According to <a title="ArenaNet Blog post" href="http://www.arena.net/blog/announcing-the-guild-wars-2-launch-date" target="_blank">this official blog post</a> the game will launch on **Tuesday, August 28<sup>th</sup>.**

Prior to this they have also announced that their next and final Beta Weekend Event is planned for **July 20-22**.

See you in-game!

<div class="concept-art last">
  <a class="fancybox" href="http://www.guildwars2.com/global/includes/images/screenshots/charr/charr-03.jpg" rel="screenshots"><img src="http://www.guildwars2.com/global/includes/images/screenshots/charr/thumbs/charr-03.jpg" alt="Screenshot: Charr" /></a>
</div>
