---
banner: "css/images/banner.png"
title: The 2012 Salute to Statesman
author: Terrahawk
date: 2011-12-03
categories:
  - City of Heroes
tags:
  - coh

---
The 2012 Loyalty Program is starting soon!

Maintain your VIP subscription from **December 1st 2011 to March 31st 2012,** and you will receive the following items on **April 18th , 2012**, on all of your existing and newly created characters:

  * **Tempered Through Fire**, a new Account-Wide Badge.
  * **The Helm of Zeus** and the **Helm of Tartarus**. Two new helmets, inspired by Statesman and Lord Recluse.
  * **Paragon**, a new Chest Emblem that all Heroes or Villains can wear proudly.
  * The **Inner Will** costume change emote. Tap into primal forces to change into a new costume.
  * A non-combat companion pet.
  * The **Battle Fury Aura**. Your character will appear to be channeling awesome cosmic powers.

So make sure your subscription doesn&#8217;t run out anytime between **December 1st 2011 to March 31st 2012** and that&#8217;s all yours!

In the meantime, here are a couple of concept art pieces for the **Helm of Zeus** and the **Helm of Tartarus** so you can already start imagining how you&#8217;ll use them for your existing or new characters!

Enjoy!
  
![Helm of Zeus][1]
  
![Helm of Tartarus][2]

 [1]: http://na.cityofheroes.com/en/global/includes/images/news/helm_of_zeus.jpg
 [2]: http://na.cityofheroes.com/en/global/includes/images/news/helm_of_tartarus.jpg
