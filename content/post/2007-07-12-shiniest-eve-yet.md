---
banner: "css/images/banner.png"
title: Shiniest EVE Yet
author: Lupus
date: 2007-07-12
categories:
  - EVE

---
![](/wp-content/uploads/2007/07/12379.jpg)

CCP has released the Trinity Expansion, and after some highly embarassing _boot.ini_ nukeage it is now up and running. The main feature is a totally new graphics engine which is available in two flavours: Standard and Premium. Both are designed to take advantage of modern graphics hardware, specifically hardware shaders which were not available when the original was produced. This has unloaded the CPU from shader tasks and allowed CCP to increase responsiveness and frame rates overall.

The Premium version is for graphics cards which support Shader Model 3 and enables High Dynamic Range lighting, complex shadows and takes advantage of new ship models with at least three times the number of polygons used in the original version along with new and more complex textures. The Standard version is for older cards and uses the original models and textures but still runs on the new engine, allowing the same (or perhaps a greater) performance increase. This is a truly ground breaking expansion for EVE, the original award winning graphics engine was a major factor in the game&#8217;s initial success and it is hoped that the cutting edge replacement will bring in yet more players over the next few years. Almost as a side note, CCP have introduced 18 new ships in this expansion, each with a highly specialised role.

Full patch notes are available [here][1].

There are two versions of the trailer available: [1080p (102Mb)][2] and [720p (64Mb)][3]

 [1]: http://myeve.eve-online.com/updates/patchnotes.asp
 [2]: http://myeve.eve-online.com/download/videos/Default.asp?a=download&vid=157
 [3]: http://myeve.eve-online.com/download/videos/Default.asp?a=download&vid=156
