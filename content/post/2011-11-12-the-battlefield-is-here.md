---
banner: "css/images/banner.png"
title: The battlefield is here!
author: Zeshin
date: 2011-11-12
categories:
  - Battlefield 3
tags:
  - bf3

---
![](/wp-content/uploads/2011/11/Battlefield-3-image.jpg)

There is a new battlefield and the Octavian Vanguard is amongst the first to participate!

As an elite platoon, our members are at the front lines of the intense battles raging on. Join us or face us, we&#8217;re right behind you regardless!

[Octavian Vanguard server](http://battlelog.battlefield.com/bf3/servers/show/89fff952-fa7c-4216-ab9f-e3e13eee3080/Octavian-Vanguard-UK/)

[Octavian Vanguard platoon](http://battlelog.battlefield.com/bf3/platoon/2832655391330408825/)
