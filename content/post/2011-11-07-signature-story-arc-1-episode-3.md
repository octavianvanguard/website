---
banner: "css/images/banner.png"
title: City of Heroes
author: Terrahawk
date: 2011-11-07
categories:
  - City of Heroes

---
**Signature Story Arc 1 &#8211; Episode 3**
  
<img src="http://na.cityofheroes.com/en/global/includes/images/news/ctssv1e3_lrg.png" alt="ctssv1e3_lrg.png" width="96" height="96" />

<span style="color: yellow;">Who Will Die &#8211; Episode 3</span> is part of a seven part story arc in our episodic &#8220;Signature Story&#8221; content. This story contains one hero and one villain story arc for characters that are Level 30+. <span style="color: yellow;">Summary:</span> Marshal Blitz, leader of the Rogue Arachnos, has requested a meeting to negotiate a peace treaty between his group and the heroes of Paragon. He has specifically requested that Alexis Cole-Duncan, daughter of Statesman and mother of Ms. Liberty, be present to negotiate. Is Blitz serious, or is he somehow part of the plot to kill a member of the Phalanx?

**Hero Contact:** Agent Kwahu in the Harvey Medical Center neighborhood, Founders Falls.
  
**Villain Contact:** Fortunata Gossamer in Black Mariah, St. Martial.

**Market Location**: Content > Story Arcs
  
**400 Points** for Free and Premium Players (_Free for VIP Players_)

