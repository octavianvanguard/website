---
banner: "css/images/banner.png"
title: Jumpgate Evolution
author: Lupus
date: 2007-06-23
categories:
  - Jumpgate
  - Upcoming Games

---
![](/wp-content/uploads/2007/06/11639.jpg)

[NetDevil][1] announced today that they are working on a significant expansion to their flagship product: Jumpgate. The new product entitled Jumpgate Evolution features a new graphics engine, all new assets, more accessible gameplay and much more. Brand new in-game screen shots as well as a beta sign-up are available.

 [1]: http://www.netdevil.com/
