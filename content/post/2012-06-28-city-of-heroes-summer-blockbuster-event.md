---
banner: "css/images/banner.png"
title: 'City of Heroes : Summer Blockbuster event!'
author: ohms
date: 2012-06-28
categories:
  - City of Heroes
tags:
  - City of Heroes
  - coh
  - MMO

---
This summer, _City of Heroes_® brings you an exciting double feature event of pulse-pounding action in the Summer Blockbuster Event! In these new back-to-back adventures inspired by classic movie themes, you battle through time or plan the perfect heist. The Summer Blockbuster Event is more action-packed and exhilarating than this year&#8217;s summer flicks. The best part? You&#8217;re the star!

 <a class="thickbox" href="http://na.cityofheroes.com/global/includes/images/screenshots/sb_lobby2.jpg" rel="gallery"><img class="mt-image-none" src="http://na.cityofheroes.com/global/includes/images/screenshots/thumbnails/sb_lobby2_t.jpg" alt="Summer Blockbuster" /></a>

## Time Gladiator

 <a class="thickbox" href="http://na.cityofheroes.com/global/includes/images/screenshots/sb_cola1l.jpg" rel="gallery"><img class="mt-image-none" src="http://na.cityofheroes.com/global/includes/images/screenshots/thumbnails/sb_cola1_t.jpg" alt="Time Gladiator" /></a> <a class="thickbox" href="http://na.cityofheroes.com/global/includes/images/screenshots/sb_colc1l.jpg" rel="gallery"><img class="mt-image-none" src="http://na.cityofheroes.com/global/includes/images/screenshots/thumbnails/sb_colc1_t.jpg" alt="Time Gladiator" /></a>

You are thrust through time into a time-lost gladiatorial arena. To escape, you must defeat the three champions of the coliseum and their bizarre and dangerous henchmen. Waiting for the most brilliant and deadly of combatants, at the end of it all, is the great God-Champion. Deadly assassins, gunslinging outlaws, cybernetic soldiers, and mythical monsters come at you tooth and nail in a frenzied and ferocious battle! Sway the arena&#8217;s crowd with your ferocity or cleverness to gain valuable buffs in this quintessential combat set piece.

## The Casino Heist

 <a class="thickbox" href="http://na.cityofheroes.com/global/includes/images/screenshots/sb_casino2l.jpg" rel="gallery"><img class="mt-image-none" src="http://na.cityofheroes.com/global/includes/images/screenshots/thumbnails/sb_casino2_t.jpg" alt="Casino Heist" /></a> <a class="thickbox" href="http://na.cityofheroes.com/global/includes/images/screenshots/sb_casino1l.jpg" rel="gallery"><img class="mt-image-none" src="http://na.cityofheroes.com/global/includes/images/screenshots/thumbnails/sb_casino1_t.jpg" alt="Casino Heist" /></a>

A vast fortune hides behind the doors of the Tyrant&#8217;s Palace Casino. With the right plan and a smart crew, this job could set you up for life. The only things in your way are armed guards, a state-of-the-art security system, and an impenetrable vault. It&#8217;s time for a no-holds-barred battle for fistfuls of cash! Form a crew with a hitter, a hacker, a grifter, and a thief. Each team member has a unique role and abilities that let you pull off the biggest job ever!

