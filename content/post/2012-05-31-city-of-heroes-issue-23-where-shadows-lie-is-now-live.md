---
banner: "css/images/banner.png"
title: 'City of Heroes : Issue 23: Where Shadows Lie is now Live!'
author: ohms
date: 2012-05-31
categories:
  - City of Heroes
tags:
  - City of Heroes
  - Issue
  - MMO

---
The Praetorian War comes to a dramatic conclusion in _Issue 23: Where Shadows Lie!_ The Heroes and Villains of Paragon City, still reeling from the deaths of the legendary Statesman and Sister Psyche, must rise to stop the ruthless Emperor Cole of Praetoria before his mad plans can shatter the dimensions.

##  <a class="thickbox" href="http://na.cityofheroes.com/media/global/includes/images/Issues/issue-22/Screenshots/nightward5l.jpg" rel="gallery"><img class="mt-image-none" src="http://na.cityofheroes.com/media/global/includes/images/Issues/issue-22/Screenshots/thumbs/nightward5_t.jpg" alt="Night Ward Environment" /></a> <a class="thickbox" href="http://na.cityofheroes.com/media/global/includes/images/Issues/issue-22/Screenshots/nightward1l.jpg" rel="gallery"><img class="mt-image-none" src="http://na.cityofheroes.com/media/global/includes/images/Issues/issue-22/Screenshots/thumbs/nightward1_t.jpg" alt="Night Ward Environment" /></a>

<p style="text-align: left;">
  <strong> New Zone: Night Ward</strong>
</p>

  * **A Realm of Magic:** Dark forces hide in Night Ward, threatening to destroy the world. Night Ward is a Level 30-35 co-op zone where characters encounter strange magical allies and foes. This Praetorian zone is connected to First Ward, and is a realm trapped between the land of the living and the world of the dead.
  * **New Mystical and Undead Enemies:** Encounter the creatures of Night Ward: the Animus Arcana, sentient spells that crave power; Drudges, who seek to guide souls (living and dead) to the realm of the dead; and the Black Knights, who run the eternal prison that is the Night Ward. The powerful Arch-Villain Shadowhunter also prowls this realm with his deadly army of beasts.<a class="thickbox" href="http://na.cityofheroes.com/media/global/includes/images/Issues/issue-22/Screenshots/shadowhunter_l.jpg" rel="gallery"> </a>

<a class="thickbox" href="http://na.cityofheroes.com/media/global/includes/images/Issues/issue-22/Screenshots/shadowhunter_l.jpg" rel="gallery"><img class="mt-image-none" src="http://na.cityofheroes.com/media/global/includes/images/Issues/issue-22/Screenshots/thumbs/shadowhunter_t.jpg" alt="Shadow Hunter" /></a> <a class="thickbox" href="http://na.cityofheroes.com/media/global/includes/images/Issues/issue-22/Screenshots/penelopeyin_l.jpg" rel="gallery"><img class="mt-image-none" src="http://na.cityofheroes.com/media/global/includes/images/Issues/issue-22/Screenshots/thumbs/penelopeyin_t.jpg" alt="Penelope Yin" /></a><a class="thickbox" href="http://na.cityofheroes.com/media/global/includes/images/Issues/issue-22/Screenshots/penelopeyin_l.jpg" rel="gallery"> </a>

## Blockbuster Conclusion to the Praetorian War

  * **The Magisterium Incarnate Trial:** The dimensional war against Emperor Cole comes to a climax in the new Magisterium Incarnate Trial. You&#8217;ve defeated all of Emperor&#8217;s Cole&#8217;s lieutenants and strongholds, and will now use knowledge gained in taking down Diabolique in an epic showdown against the Well&#8217;s Champion to determine the fate of two worlds!

## Quality of Life Upgrades

  * **Improved Free Player Chat and Grouping:** Now all players (VIP, premium, and free) can join a new serverwide Looking for Group chat channel that allows them to communicate freely with each other. Free and premium players can also now join Super Groups, regardless of their status in the Paragon Rewards program.
  * **Easy Portal Travel:** The new Interdimensional Tunnel System makes getting from one world to another quick and easy. Get to outlying areas like Praetoria and the Shadow Shard easily with a quick dimension jump through conveniently located portals.

## New VIP Content and Benefits

  * **Signature Story Arc Series 2: &#8220;Pandora&#8217;s Box&#8221;:** Both Heroes and Villains are being supercharged with new uncontrollable powers. Heroes team up with the remnants of the Freedom Phalanx to discover the secret menace behind the chaos, while Villains lay the groundwork for a new organization to rival the power of Arachnos.
  * **New Mecha Armor Tier 9 Costume Set:** The Mecha Armor costume set turns you into the sleek giant robot or high-tech warrior you&#8217;ve always dreamed of. Select from a new sword, rifle, backpack, and wings offered in this latest free costume set available only to VIP players as part of the Paragon Rewards program.

 <a class="thickbox" href="http://na.cityofheroes.com/global/includes/images/screenshots/mech_male_l.jpg" rel="gallery"><img class="mt-image-none" src="http://na.cityofheroes.com/global/includes/images/screenshots/thumbnails/mech_male_t.jpg" alt="Mecha Armor" /></a> <a class="thickbox" href="http://na.cityofheroes.com/global/includes/images/screenshots/mech_huge_tw_l.jpg" rel="gallery"><img class="mt-image-none" src="http://na.cityofheroes.com/global/includes/images/videos/sml_thumb/mech_huge_tw1_t.jpg" alt="Mecha Armor" /></a><a class="thickbox" href="http://na.cityofheroes.com/global/includes/images/screenshots/ComboA_logo.jpg" rel="gallery">  </a><a class="thickbox" href="http://na.cityofheroes.com/global/includes/images/screenshots/ComboA_logo.jpg" rel="gallery">  </a>

  * **New Chain Mail and Leather Armor Costume Set:** All VIP players have access to the Chain Mail and Leather Armor costume set. Look the part of a crusading knight, yeoman archer, or a rowdy barbarian in this versatile set.
  * **New Incarnate Powers:** Level 50 VIP players can now unlock the Hybrid Incarnate Tree, which grants them new powers based on the core Archetypes in City of Heroes®. You can choose a style that shores up a weakness, or one that further strengthens existing abilities. The Assault Tree increases your damage, the Control Tree raises your ability to lock down foes, the Melee Tree increases your defense and status protection, and the Support Tree grants bonuses to you and your teammates.

Full patch notes are available <a title="Issue 23 patch notes" href="http://boards.cityofheroes.com/showthread.php?t=290440" target="_blank">here</a>.

