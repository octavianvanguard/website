---
banner: "css/images/banner.png"
title: 'Star Wars: The Old Republic – Legacy'
author: divvur
date: 2012-04-13
categories:
  - The Old Republic
tags:
  - Legacy
  - MMO
  - 'Star Wars: The Old Republic'
  - TOR
  - Update

---
Bioware have released their biggest content update yet, Update 1.2 &#8211; Legacy, for Star Wars: The Old Republic.

The update includes a new flashpoint, new operation and introduces the Legacy system, which allows players to unlock features and abilities that are shared across all characters on a server.

![TOR Lost Island](/wp-content/uploads/2012/04/rockchewer_1600x900.jpg)
![TOR Explosive Conflict](/wp-content/uploads/2012/04/hovertanks_1600x900.jpg)

Highlights of the new patch, including video trailers can be found [here](http://www.swtor.com/gameupdates). Full patch notes can be found [here](http://www.swtor.com/patchnotes/1.2.0/legacy)

Octavian Vanguard plays regularly on the Rogue Moon (Imperial) and Hydian Way (Republic) servers.

