#!/bin/bash

set -e

hugo --cleanDestinationDir
aws s3 sync public/ s3://ov-website-beta/

